//
//  BonusListViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/25/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "BonusListViewController.h"
#import "ISO8601DateFormatter.h"
#import "RaiseAlertView.h"
#import "BonusTransferViewController.h"
#import "Global.h"
#import "VipPoint.h"
#import "ISO8601DateFormatter.h"

@interface BonusListViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPoints;
@property (strong, nonatomic) NSMutableArray *logs;
@property (strong, nonatomic) NSMutableArray *vipPoints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end

@implementation BonusListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Clear all navbar subviews
    NSArray *navbarSubviews = self.navigationController.navigationBar.subviews;
    for (UIView *subView in navbarSubviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [subView removeFromSuperview];
        }
    }
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.logs = [[NSMutableArray alloc] init];
    /*
    UIBarButtonItem *btnTransfer = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Transfer", nil) style:UIBarButtonItemStylePlain target:self action:@selector(onBtnTransfer:)];
    self.navigationItem.rightBarButtonItem = btnTransfer;
     */
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.title = NSLocalizedString(@"BonusHistory", nil);
    
    self.lblTotalPoints.text = [NSString stringWithFormat:@"%@: %d %@", NSLocalizedString(@"TotalPoints", nil), [[g_MemberInfo objectForKey:INFO_KEY_LAST_POINT] intValue], NSLocalizedString(@"Points", nil)];
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    //GetVipBonusList  => getTransactionLogs
    //[ws getTransactionLogs:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] isPoint:YES];
    [ws getVipBonusList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    [ws showWaitingView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)adjustHeightOfTableview
{
    /*
    CGFloat height = self.tableView.contentSize.height;
    CGFloat maxHeight = 0.85 * self.tableView.superview.frame.size.height;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the height constraint accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        self.tableViewHeightConstraint.constant = height;
        [self.view setNeedsUpdateConstraints];
    }];
    */
}

- (void)onBtnTransfer:(id)sender
{
    BonusTransferViewController *bonusTransferController = [[BonusTransferViewController alloc] initWithNibName:@"BonusTransferViewController" bundle:nil];
    
    // Hide back button text on next page
    self.title = @"";
    
    [self.navigationController pushViewController:bonusTransferController animated:YES];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.logs count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

    NSDictionary *log = [self.logs objectAtIndex:[indexPath row]];
    //NSString *destName = [log objectForKey:@"dest_name"];
    //NSString *srcName = [log objectForKey:@"src_name"];
    //NSString *message = [log objectForKey:@"msg"];
    
    NSString *myName = [g_MemberInfo objectForKey:INFO_KEY_NAME];
    NSString *message = [log objectForKey:@"bonuspt_point"];
    NSString *rowDate = [log objectForKey:@"input_date"];
    NSString *pointType = [log objectForKey:@"point_type"];
    
    NSLog(@"message: %@", message);
    //NSLog(@"date: %@", date);
     
     /*
    //NSLog(@"%@", destName);
    //NSLog(@"%@", srcName);
    */
    
    NSString *inputDate = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    
    ISO8601DateFormatter *iso8601Formatter = [[ISO8601DateFormatter alloc] init];
    
    // Date
    if (rowDate != nil && ![rowDate isKindOfClass:[NSNull class]]) {
        NSDate *date = [iso8601Formatter dateFromString:rowDate];
        inputDate = [dateFormatter stringFromDate:date];
    }
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%@  %@  點給%@", myName, pointType, message];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
    
    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.text = inputDate;
    cell.detailTextLabel.font = [UIFont systemFontOfSize:16];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = [Global colorWithType:COLOR_BTN_CAT_LIST_BG];
    if([indexPath row] % 2){
        cell.backgroundColor = [UIColor lightGrayColor];
    }
    /*
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    VipPoint *vipPoint = [self.vipPoints objectAtIndex:[indexPath row]];
    
    NSString *rawInputDate = vipPoint.inputDate;
    NSString *pointType = vipPoint.pointType;
    NSString *giftVipId = vipPoint.giftVipId;
    NSInteger bonus = vipPoint.bonusPoint;
    
    NSString *inputDate = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    
    ISO8601DateFormatter *iso8601Formatter = [[ISO8601DateFormatter alloc] init];
    
    // Date
    if (rawInputDate != nil && ![rawInputDate isKindOfClass:[NSNull class]]) {
        NSDate *date = [iso8601Formatter dateFromString:rawInputDate];
        inputDate = [dateFormatter stringFromDate:date];
    }
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = inputDate;
    cell.textLabel.font = [UIFont systemFontOfSize:16];

    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:18];
    cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    
    NSString *myName = [g_MemberInfo objectForKey:INFO_KEY_NAME];
    
    if (giftVipId && giftVipId.length>0 && ![giftVipId isEqualToString:myName]) {
        if ([pointType isEqualToString:@"點數轉出"]) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"你轉送 %ld 點給%@", bonus, giftVipId];
        } else if ([pointType isEqualToString:@"點數轉入"]) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@轉送 %ld 點給你", giftVipId, bonus];
        }
    } else {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %ld 點", pointType, bonus];
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
 //   cell.backgroundColor = [Global colorWithType:COLOR_TYPE_LIST_BG];
    */
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    NSDictionary *dept = [self.bonuses objectAtIndex:[indexPath row]];
    NSString *deptName = [dept objectForKey:@"dep_name"];
    NSString *deptId = [dept objectForKey:@"dep_id"];
    
    // Open ProductDetailViewController
    ProductListViewController *productListController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil];
    [productListController setDeptId:deptId];
    [productListController setDeptName:deptName];
    
    // Hide back button text on next page
    self.title = @"";
    
    [self.navigationController pushViewController:productListController animated:YES];
     */
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_TRANSACTION_LOGS] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ logs", datacnt);
                
                NSArray *logs = (NSArray *)[jsonObject objectForKey:@"logs"];
                if (logs) {
                    [self.logs setArray:logs];
                }
                [self.tableView reloadData];
                [self adjustHeightOfTableview];
            }
        }
        
        if ([resultName compare:WS_GET_VIP_BONUS_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSDictionary *bonusList = (NSDictionary *)[jsonObject objectForKey:@"objBonusList"];
                
                NSLog(@"Message %@ ", message);
                
                NSArray *vipPointList = [bonusList objectForKey:@"vippoint_list"];
                
                //NSArray *logs = (NSArray *)[jsonObject objectForKey:@"vippoint_list"];
                if (vipPointList) {
                    [self.logs setArray:vipPointList];
                }
                [self.tableView reloadData];
                //[self adjustHeightOfTableview];
            }
        }
        
        /*
         if ([resultName compare:WS_GET_VIP_BONUS_LIST] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                NSDictionary *bonusList = (NSDictionary *)[jsonObject objectForKey:@"objBonusList"];
                if (bonusList) {
                    NSString *lastPoint = [bonusList objectForKey:@"last_point"];
                    NSString *totalPoint = [bonusList objectForKey:@"total_point"];
                    NSString *lastStoreBalance = [bonusList objectForKey:@"last_stroebalance"];
                    
         
                    //if (totalPoint) {
                     //   self.lblTotalPoints.text = [NSString stringWithFormat:@"%@: %@ %@", NSLocalizedString(@"TotalPoints", nil), totalPoint, NSLocalizedString(@"Points", nil)];
                  //  }
         
                    
                    NSArray *vipPointList = [bonusList objectForKey:@"vippoint_list"];
                    if (vipPointList) {
                        [self.vipPoints removeAllObjects];
                        for (NSDictionary *vipPoint in vipPointList) {
                            VipPoint *newVipPoint = [[VipPoint alloc] initWithBonusPoint:[[vipPoint objectForKey:@"bonuspt_point"] integerValue] inputDate:[vipPoint objectForKey:@"input_date"] pointType:[vipPoint objectForKey:@"point_type"] shopId:[[vipPoint objectForKey:@"shop_id"] integerValue] ticketId:[vipPoint objectForKey:@"ticket_id"] giftVipId:[vipPoint objectForKey:@"gift_vipid"]];
                            [self.vipPoints addObject:newVipPoint];
                        }
                    }
                    
                    // Sort descending to input date
                    NSArray *sortedArray;
                    sortedArray = [self.vipPoints sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                        NSString *firstDateStr = [(VipPoint*)a inputDate];
                        NSString *secondDateStr = [(VipPoint*)b inputDate];
                        
                        ISO8601DateFormatter *dateFormatter = [[ISO8601DateFormatter alloc] init];
                        NSDate *firstDate = [dateFormatter dateFromString:firstDateStr];
                        NSDate *secondDate = [dateFormatter dateFromString:secondDateStr];
                        
                        return [secondDate compare:firstDate];
                    }];
                    
                    [self.vipPoints setArray:sortedArray];
                }
                [self.tableView reloadData];
                [self adjustHeightOfTableview];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        }
         */
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
