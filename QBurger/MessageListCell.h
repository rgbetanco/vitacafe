//
//  MessageListCell.h
//  Raise
//
//  Created by Kevin Phua on 11/5/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MessageListCell;

@protocol MessageListCellDelegate <NSObject>

- (void)btnSave:(id)sender;

@end

@interface MessageListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) id<MessageListCellDelegate> delegate;

@end
