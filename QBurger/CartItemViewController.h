//
//  CartItemViewController.h
//  QBurger
//
//  Created by Kevin Phua on 9/11/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "BaseViewController.h"
#import "Product.h"

@interface CartItemViewController : BaseViewController

@property (nonatomic, strong) Product *product;

@end
