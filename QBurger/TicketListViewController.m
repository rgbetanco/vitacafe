//
//  TicketListViewController.m
//  PosApp
//
//  Created by Kevin Phua on 10/12/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "TicketListViewController.h"
#import "ProductListCell.h"
#import "Global.h"
#import "UIImageView+WebCache.h"
#import "TicketDetailViewController.h"
#import "ISO8601DateFormatter.h"

@interface TicketListViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *logs;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end

@implementation TicketListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Clear all navbar subviews
    NSArray *navbarSubviews = self.navigationController.navigationBar.subviews;
    for (UIView *subView in navbarSubviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [subView removeFromSuperview];
        }
    }

    self.logs = [[NSMutableArray alloc] init];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedString(@"TicketHistory", nil);
    
    WebService *ws = [[WebService alloc] init];
    [ws getTransactionLogs:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] isPoint:NO];
    [ws setDelegate:self];
    [ws showWaitingView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)adjustHeightOfTableview
{
    CGFloat height = self.tableView.contentSize.height;
    CGFloat maxHeight = 0.85 * self.tableView.superview.frame.size.height;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the height constraint accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        self.tableViewHeightConstraint.constant = height;
        [self.view setNeedsUpdateConstraints];
    }];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.logs count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    NSDictionary *log = [self.logs objectAtIndex:[indexPath row]];
    //NSString *destName = [log objectForKey:@"dest_name"];
    //NSString *srcName = [log objectForKey:@"src_name"];
    NSString *message = [log objectForKey:@"msg"];
    NSString *date = [log objectForKey:@"times"];
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = message;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.text = date;
    cell.detailTextLabel.font = [UIFont systemFontOfSize:16];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = [Global colorWithType:COLOR_BTN_CAT_LIST_BG];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    NSDictionary *log = [self.logs objectAtIndex:[indexPath row]];
    NSString *message = [log objectForKey:@"msg"];
    NSArray *listItems = [message componentsSeparatedByString:@" "];
    NSString *ticketNo = [listItems objectAtIndex:1];
    
    // Open ticket detail
    TicketDetailViewController *ticketDetailController = [[TicketDetailViewController alloc] initWithNibName:@"TicketDetailViewController" bundle:nil];
    ticketDetailController.ticketNo = ticketNo;
    
    // Hide back button text on next page
    self.title = @"";

    [self.navigationController pushViewController:ticketDetailController animated:YES];
    */
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_TRANSACTION_LOGS] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ logs", datacnt);
                
                NSArray *logs = (NSArray *)[jsonObject objectForKey:@"logs"];
                if (logs) {
                    [self.logs setArray:logs];
                }
                [self.tableView reloadData];
                [self adjustHeightOfTableview];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}


@end
