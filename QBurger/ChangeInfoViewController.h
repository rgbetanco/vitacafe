//
//  ChangeInfoViewController.h
//  QBurger
//
//  Created by Kevin Phua on 11/17/16.
//  Copyright (c) 2016 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "WebService.h"

@interface ChangeInfoViewController : BaseViewController <WebServiceDelegate>

@end
