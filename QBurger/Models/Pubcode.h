//
//  Pubcode.h
//  QBurger
//
//  Created by Kevin Phua on 28/03/2017.
//  Copyright © 2017 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pubcode : NSObject

@property (nonatomic, strong) NSString *pmname;
@property (nonatomic) NSInteger serno;

@end
