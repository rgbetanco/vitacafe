//
//  RaiseAlertView.m
//  Raise
//
//  Created by Kevin Phua on 11/25/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "RaiseAlertView.h"

@implementation RaiseAlertView

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle {
    
    self = [super initWithTitle:title message:message cancelButtonTitle:cancelButtonTitle otherButtonTitle:otherButtonTitle];

    self.backgroundColor = [UIColor whiteColor];
    self.hideSeperator = YES;
    self.center = CGPointMake(([UIScreen mainScreen].bounds.size.width)/2, ([UIScreen mainScreen].bounds.size.height)/2);
    
    return self;
}

@end
