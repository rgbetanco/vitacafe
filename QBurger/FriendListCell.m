//
//  FriendListCell.m
//  Raise
//
//  Created by Kevin Phua on 11/7/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "FriendListCell.h"
#import "Global.h"

@implementation FriendListCell

@synthesize fId;

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.lblName.textColor = [UIColor whiteColor];
    self.lblPhone.textColor = [UIColor whiteColor];
    self.backgroundColor = [Global colorWithType:COLOR_BG_MAIN];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onBtnYes:(id)sender {
    [self.delegate btnYes:sender];
}

- (IBAction)onBtnNo:(id)sender {
    [self.delegate btnNo:sender];
}

@end
