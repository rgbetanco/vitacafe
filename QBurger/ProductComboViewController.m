//
//  ProductComboViewController.m
//  QBurger
//
//  Created by Kevin Phua on 10/28/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "ProductComboViewController.h"
#import "TasteViewCell.h"
#import "TasteDetail.h"
#import "TasteCollectionHeaderView.h"
#import "Combo.h"

@interface ProductComboViewController ()<TasteViewCellDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *combos;
@property (nonatomic) int selectedComboIndex;
    
@end

@implementation ProductComboViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UINib *cellNib = [UINib nibWithNibName:@"TasteViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"TasteViewCell"];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.collectionView registerClass:[TasteCollectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    _selectedComboIndex = -1;
    _combos = _combo.combDetails;
    
    // Merge selected combo into selected combo index
    if (_selectedCombo) {
        // Set selected combo index
        for (int i=0; i<_combos.count; i++) {
            Combo *combo = [_combos objectAtIndex:i];
            if ([combo.prodId isEqualToString:_selectedCombo.prodId]) {
                _selectedComboIndex = i;
                break;
            }
        }
    } 
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_collectionView reloadData];
    });    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnConfirm:(id)sender {
    if (self.delegate) {
        [self.delegate onSelectedCombo:_selectedCombo prodId:_combo.prodId sender:self];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onBtnCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//==================================================================
#pragma mark - UICollectionViewDelegate
//==================================================================

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _combos.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        // Default item
        TasteViewCell *cell = (TasteViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TasteViewCell" forIndexPath:indexPath];
        cell.checkBox.boxType = BEMBoxTypeCircle;
        cell.checkBox.animationDuration = 0.2;
        
        if (_selectedComboIndex == -1) {
            cell.checkBox.on = YES;
        } else {
            cell.checkBox.on = NO;
        }
        
        cell.lblTitle.text = _combo.prodName;
        
        cell.lblTitle.numberOfLines = 0;
        [cell.lblTitle sizeToFit];
        cell.delegate = self;
        return cell;
    } else {
        // Selected item
        Combo *combo = [_combos objectAtIndex:indexPath.row-1];
        
        TasteViewCell *cell = (TasteViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TasteViewCell" forIndexPath:indexPath];
        cell.checkBox.boxType = BEMBoxTypeCircle;
        cell.checkBox.animationDuration = 0.2;
        
        if (_selectedComboIndex == (int)indexPath.row-1) {
            cell.checkBox.on = YES;
        } else {
            cell.checkBox.on = NO;
        }

        if (combo.price > 0) {
            cell.lblTitle.text = [NSString stringWithFormat:@"%@ +%d", combo.prodName, combo.price];
        } else {
            cell.lblTitle.text = combo.prodName;
        }        
        cell.delegate = self;
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
}

//==================================================================
#pragma mark - TasteViewCellDelegate
//==================================================================

- (void)onTasteSelected:(BEMCheckBox *)sender
{
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
    
    if (indexPath.row > 0) {
        _selectedComboIndex = (int)indexPath.row-1;
    } else {
        // Default selection
        _selectedComboIndex = -1;
    }
    
    if (_selectedComboIndex >= 0) {
        Combo *combo = [_combos objectAtIndex:indexPath.row-1];
        
        // Add selected combo as comboDetail
        Combo *selectedCombo = [Combo new];
        selectedCombo.combSno = _combo.combSno;
        selectedCombo.prodId = _combo.prodId;
        selectedCombo.prodName = _combo.prodName;
        selectedCombo.price = _combo.price;
        selectedCombo.combDetails = [NSArray arrayWithObject:combo];
        _selectedCombo = selectedCombo;
    } else {
        _selectedCombo = nil;
    }
    
    [self.collectionView reloadData];
}

@end
