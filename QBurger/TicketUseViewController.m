//
//  TicketUseViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/26/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "TicketUseViewController.h"
#import "Global.h"

@interface TicketUseViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgBarcode;
@property (weak, nonatomic) IBOutlet UILabel *lblTicketName;

@end

@implementation TicketUseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Generate QRCode
    if (self.ticket) {
        NSString *ticketNo = [self.ticket objectForKey:@"tkno"];
        CGImageRef qrCodeImage = [Global createQRImageForString:ticketNo size:CGSizeMake(120,120)];
        _imgBarcode.image = [UIImage imageWithCGImage:qrCodeImage];
        self.lblTicketName.text = ticketNo;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedString(@"UseTicket", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
