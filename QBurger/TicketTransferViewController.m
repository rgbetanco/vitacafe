//
//  TicketTransferViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/26/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "TicketTransferViewController.h"
#import "RaiseAlertView.h"
#import "Global.h"

#define STATUS_AWAITING_APPROVAL        0
#define STATUS_REJECTED                 1
#define STATUS_APPROVED                 10

@interface TicketTransferViewController ()<DQAlertViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *allFriends;       // 所有好友
@property (weak, nonatomic) UITextField *txtPassword;
@property (nonatomic) NSInteger selectedRow;

@property (nonatomic, strong) NSString *selectedFriendName;
@property (nonatomic, strong) NSString *selectedFriendId;

@property (nonatomic, strong) DQAlertView *alertView;

@end

@implementation TicketTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Clear all navbar subviews
    NSArray *navbarSubviews = self.navigationController.navigationBar.subviews;
    for (UIView *subView in navbarSubviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [subView removeFromSuperview];
        }
    }
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIBarButtonItem *btnConfirm = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Confirm", nil) style:UIBarButtonItemStylePlain target:self action:@selector(onBtnConfirm:)];
    self.navigationItem.rightBarButtonItem = btnConfirm;
    
    self.allFriends = [NSMutableArray new];
    self.selectedRow = -1;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedString(@"SelectFriend", nil);
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getFriendList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    [ws showWaitingView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onBtnConfirm:(id)sender
{
    if (self.selectedRow < 0) {
        RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                  message:NSLocalizedString(@"PointTransferError", nil)
                                                        cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                         otherButtonTitle:nil];
        [alertView show];
        return;
    }
    
    NSString *prodName = [self.ticket objectForKey:@"ProdName"];
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"TicketTransferConfirmMsg", nil), prodName, self.selectedFriendName];
    
    _alertView = [[DQAlertView alloc] initWithTitle:NSLocalizedString(@"TransferConfirm", nil)
                                            message:message
                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                    otherButtonTitle:NSLocalizedString(@"OK", nil)];
    
    CGRect bounds = CGRectMake(0, 0, 320, 220);
    UIGraphicsBeginImageContext(CGSizeMake(320, 220));
    [[UIImage imageNamed:@"dialog_bkgnd"] drawInRect:bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _alertView.backgroundColor = [UIColor colorWithPatternImage:image];
    _alertView.hideSeperator = YES;
    _alertView.customFrame = CGRectMake(0, 0, 320, 200);
    _alertView.titleHeight = 50;
    _alertView.messageLeftRightPadding = 50;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    UITextField *txtPassword = [[UITextField alloc] initWithFrame:CGRectMake(60, 120, 200, 30)];
    txtPassword.backgroundColor = [UIColor blackColor];
    txtPassword.borderStyle = UITextBorderStyleNone;
    txtPassword.textAlignment = NSTextAlignmentCenter;
    txtPassword.secureTextEntry = YES;
    txtPassword.delegate = self;
    txtPassword.textColor = [UIColor whiteColor];
    self.txtPassword = txtPassword;
    [contentView addSubview:txtPassword];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 26, 200, 30)];
    lblTitle.font = [UIFont systemFontOfSize:18.0];
    lblTitle.text = NSLocalizedString(@"BuyVoucher", nil);
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [contentView addSubview:lblTitle];
    
    UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(60, 60, 200, 100)];
    lblMessage.font = [UIFont systemFontOfSize:15.0];
    lblMessage.text = message;
    lblMessage.numberOfLines = 0;
    [lblMessage sizeToFit];
    [contentView addSubview:lblMessage];
    contentView.backgroundColor = [UIColor clearColor];
    _alertView.contentView = contentView;
    
    _alertView.center = self.view.center;
    
    _alertView.cancelButtonAction = ^{
        NSLog(@"Cancel button clicked");
    };
    _alertView.otherButtonAction = ^{
        NSLog(@"Password entered was: %@", self.txtPassword.text);
        
        if ([self.txtPassword.text compare:g_MemberPassword] == NSOrderedSame) {
            NSString *serNo = [self.ticket objectForKey:@"serno"];
            NSString *tkNo = [self.ticket objectForKey:@"tkno"];
            
            WebService *ws = [[WebService alloc] init];
            ws.delegate = self;
            [ws ticketGift:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] fId:self.selectedFriendId serno:serNo tkno:tkNo];
            [ws showWaitingView:self.view];
        } else {
            RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                      message:NSLocalizedString(@"PasswordError", nil)
                                                            cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                             otherButtonTitle:nil];
            [alertView show];
        }
    };
    [_alertView show];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.allFriends count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    NSDictionary *friend = [self.allFriends objectAtIndex:indexPath.row];
    NSString *name = [friend objectForKey:@"name_2"];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = name;
    
    if (indexPath.row == self.selectedRow) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.backgroundColor = [Global colorWithType:COLOR_BTN_CAT_LIST_BG];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
    
    // Update delegate
    NSDictionary *friend = [self.allFriends objectAtIndex:indexPath.row];
    self.selectedFriendName = [friend objectForKey:@"name_2"];
    self.selectedFriendId = [friend objectForKey:@"vip_2"];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadData];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            
            [self.allFriends removeAllObjects];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_APPROVED) {
                        [self.allFriends addObject:friend];
                    } 
                }
                
                [self.tableView reloadData];
            } else {
                /*
                // Failure
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_TICKET_GIFT] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"TransferSuccess", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                
                alertView.cancelButtonAction = ^{
                    [self.navigationController popViewControllerAnimated:YES];
                };
                
                alertView.delegate = self;
                [alertView show];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"TransferFail", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                alertView.delegate = self;
                [alertView show];
            }
        } 
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

//==================================================================
#pragma DQAlertViewDelegate
//==================================================================

- (void)cancelButtonClickedOnAlertView:(DQAlertView *)alertView {
    [self.navigationController popViewControllerAnimated:YES];
}

//==================================================================
#pragma UITextFieldDelegate
//==================================================================
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    if (_alertView) {
        _alertView.center = CGPointMake(self.view.center.x, self.view.center.y - 100);
    }
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    if (_alertView) {
        _alertView.center = self.view.center;
    }
}

@end
