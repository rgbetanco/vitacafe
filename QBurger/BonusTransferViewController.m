//
//  BonusTransferViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/26/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "BonusTransferViewController.h"
#import "SelectFriendViewController.h"
#import "RaiseAlertView.h"
#import "Global.h"

@interface BonusTransferViewController ()<SelectFriendDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPoints;
@property (weak, nonatomic) UITextField *txtPoints;
@property (weak, nonatomic) UITextField *txtPassword;
@property (nonatomic) int transferPoints;
@property (nonatomic) int totalPoints;

@property (nonatomic, strong) NSString *selectedFriendName;
@property (nonatomic, strong) NSString *selectedFriendId;

@property (nonatomic, strong) DQAlertView *alertView;

@end

@implementation BonusTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Clear all navbar subviews
    NSArray *navbarSubviews = self.navigationController.navigationBar.subviews;
    for (UIView *subView in navbarSubviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [subView removeFromSuperview];
        }
    }
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIBarButtonItem *btnConfirm = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Confirm", nil) style:UIBarButtonItemStylePlain target:self action:@selector(onBtnConfirm:)];
    self.navigationItem.rightBarButtonItem = btnConfirm;
    
    self.transferPoints = -1;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedString(@"PointTransfer", nil);
    
    self.lblTotalPoints.text = [NSString stringWithFormat:@"%@: %d %@", NSLocalizedString(@"TotalPoints", nil), [[g_MemberInfo objectForKey:INFO_KEY_LAST_POINT] intValue], NSLocalizedString(@"Points", nil)];
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getVipBonusList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    [ws showWaitingView:self.view];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onBtnConfirm:(id)sender
{
    if (self.transferPoints < 0 || !self.selectedFriendId || !self.selectedFriendName) {
        RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                  message:NSLocalizedString(@"PointTransferError", nil)
                                                        cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                         otherButtonTitle:nil];
        [alertView show];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"PointTransferConfirmMsg", nil), self.transferPoints, self.selectedFriendName];
    
    _alertView = [[DQAlertView alloc] initWithTitle:NSLocalizedString(@"TransferConfirm", nil)
                                            message:message
                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                   otherButtonTitle:NSLocalizedString(@"OK", nil)];
    
    CGRect bounds = CGRectMake(0, 0, 320, 220);
    UIGraphicsBeginImageContext(CGSizeMake(320, 220));
    [[UIImage imageNamed:@"dialog_bkgnd"] drawInRect:bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _alertView.backgroundColor = [UIColor colorWithPatternImage:image];
    _alertView.hideSeperator = YES;
    _alertView.customFrame = CGRectMake(0, 0, 320, 200);
    _alertView.titleHeight = 50;
    _alertView.messageLeftRightPadding = 50;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    UITextField *txtPassword = [[UITextField alloc] initWithFrame:CGRectMake(60, 120, 200, 30)];
    txtPassword.backgroundColor = [UIColor whiteColor];
    txtPassword.borderStyle = UITextBorderStyleNone;
    txtPassword.textAlignment = NSTextAlignmentCenter;
    txtPassword.secureTextEntry = YES;
    txtPassword.delegate = self;
    self.txtPassword = txtPassword;
    [contentView addSubview:txtPassword];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 26, 200, 30)];
    lblTitle.font = [UIFont systemFontOfSize:18.0];
    lblTitle.text = NSLocalizedString(@"TransferConfirm", nil);
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [contentView addSubview:lblTitle];
    
    UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(60, 60, 200, 100)];
    lblMessage.font = [UIFont systemFontOfSize:15.0];
    lblMessage.text = message;
    lblMessage.numberOfLines = 0;
    [lblMessage sizeToFit];
    [contentView addSubview:lblMessage];
    contentView.backgroundColor = [UIColor clearColor];
    _alertView.contentView = contentView;
    
    _alertView.center = self.view.center;
    
    _alertView.cancelButtonAction = ^{
        NSLog(@"Cancel button clicked");
    };
    _alertView.otherButtonAction = ^{
        NSLog(@"Password entered was: %@", self.txtPassword.text);

        if ([self.txtPassword.text compare:g_MemberPassword] == NSOrderedSame) {
            NSString *point = [NSString stringWithFormat:@"%d", self.transferPoints];
            WebService *ws = [[WebService alloc] init];
            ws.delegate = self;
            [ws vipPointMoveShop:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] fId:self.selectedFriendId point:point];
            [ws showWaitingView:self.view];
        } else {
            RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                      message:NSLocalizedString(@"PasswordError", nil)
                                                            cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                             otherButtonTitle:nil];
            [alertView show];
        }
    };
    [_alertView show];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];

    if (indexPath.row == 0) {
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.text = NSLocalizedString(@"PleaseEnterPoints", nil);
        cell.detailTextLabel.textColor = [UIColor blackColor];
        
        if (self.transferPoints > 0) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", self.transferPoints];
        } else {
            cell.detailTextLabel.text = NSLocalizedString(@"PleaseEnter", nil);
        }
    } else if (indexPath.row == 1) {
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.text = NSLocalizedString(@"SelectFriend", nil);
        cell.detailTextLabel.textColor = [UIColor blackColor];
        
        if (self.selectedFriendName && self.selectedFriendName.length > 0) {
            cell.detailTextLabel.text = self.selectedFriendName;
        } else {
            cell.detailTextLabel.text = NSLocalizedString(@"NotSelected", nil);
        }
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = [Global colorWithType:COLOR_BTN_CAT_LIST_BG];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        _alertView = [[DQAlertView alloc] initWithTitle:NSLocalizedString(@"PointTransfer", nil)
                                                message:NSLocalizedString(@"PleaseEnterPoints", nil)
                                      cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                       otherButtonTitle:NSLocalizedString(@"OK", nil)];
        
        CGRect bounds = CGRectMake(0, 0, 320, 220);
        UIGraphicsBeginImageContext(CGSizeMake(320, 220));
        [[UIImage imageNamed:@"dialog_bkgnd"] drawInRect:bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        _alertView.backgroundColor = [UIColor colorWithPatternImage:image];
        _alertView.hideSeperator = YES;
        _alertView.customFrame = CGRectMake(0, 0, 320, 200);
        _alertView.titleHeight = 50;
        _alertView.messageLeftRightPadding = 50;
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
        UITextField *txtPoints = [[UITextField alloc] initWithFrame:CGRectMake(60, 110, 200, 30)];
        txtPoints.backgroundColor = [UIColor blackColor];
        txtPoints.borderStyle = UITextBorderStyleNone;
        txtPoints.textAlignment = NSTextAlignmentCenter;
        txtPoints.keyboardType = UIKeyboardTypeNumberPad;
        txtPoints.delegate = self;
        txtPoints.textColor = [UIColor whiteColor];
        self.txtPoints = txtPoints;
        [contentView addSubview:txtPoints];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 26, 200, 30)];
        lblTitle.font = [UIFont systemFontOfSize:18.0];
        lblTitle.text = NSLocalizedString(@"PointTransfer", nil);
        lblTitle.textAlignment = NSTextAlignmentCenter;
        [contentView addSubview:lblTitle];
        
        UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(60, 80, 200, 100)];
        lblMessage.font = [UIFont systemFontOfSize:15.0];
        lblMessage.text = NSLocalizedString(@"PleaseEnterPoints", nil);
        lblMessage.numberOfLines = 0;
        [lblMessage sizeToFit];
        [contentView addSubview:lblMessage];
        contentView.backgroundColor = [UIColor clearColor];
        _alertView.contentView = contentView;
        
        _alertView.center = self.view.center;
        
        _alertView.cancelButtonAction = ^{
            NSLog(@"Cancel button clicked");
        };
        _alertView.otherButtonAction = ^{
            int transferPoints = [self.txtPoints.text intValue];
            if (transferPoints > self.totalPoints) {
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:NSLocalizedString(@"InsufficientPoints", nil)
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)otherButtonTitle:nil];
                [alertView show];
            } else {
                self.transferPoints = transferPoints;
                [self.tableView reloadData];
            }
        };
        [_alertView show];
        
        
    } else if (indexPath.row == 1) {
        // Select friend
        SelectFriendViewController *selectFriendController = [[SelectFriendViewController alloc] initWithNibName:@"SelectFriendViewController" bundle:nil];
        
        // Hide back button text on next page
        self.title = @"";
        selectFriendController.delegate = self;
        
        [self.navigationController pushViewController:selectFriendController animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_VIP_BONUS_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSDictionary *bonusList = (NSDictionary *)[jsonObject objectForKey:@"objBonusList"];
                if (bonusList) {
                    //NSString *lastPoint = [bonusList objectForKey:@"last_point"];
                    //NSString *totalPoint = [bonusList objectForKey:@"total_point"];
                    //NSString *lastStoreBalance = [bonusList objectForKey:@"last_stroebalance"];
                   /*
                    if (totalPoint) {
                        self.totalPoints = [totalPoint intValue];
                        self.lblTotalPoints.text = [NSString stringWithFormat:@"%@: %@ %@", NSLocalizedString(@"TotalPoints", nil), totalPoint, NSLocalizedString(@"Points", nil)];
                    }*/
                    self.totalPoints = [[g_MemberInfo objectForKey:INFO_KEY_LAST_POINT] intValue];
                    
                }
                [self.tableView reloadData];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_VIP_POINT_MOVE_SHOP] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                //NSString *vipId = (NSString *)[jsonObject objectForKey:@"vip_id"];
                //NSString *vipPoint = (NSString *)[jsonObject objectForKey:@"vip_point"];
                //NSString *vipAmt = (NSString *)[jsonObject objectForKey:@"vip_amt"];
                
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"TransferSuccess", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                
                alertView.cancelButtonAction = ^{
                    [self.navigationController popViewControllerAnimated:YES];
                };

                [alertView show];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"TransferFail", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        } 
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

//==================================================================
#pragma SelectFriendDelegate
//==================================================================

- (void)selectedFriend:(NSString *)name fId:(NSString *)fId {
    self.selectedFriendName = name;
    self.selectedFriendId = fId;
}

//==================================================================
#pragma UITextFieldDelegate
//==================================================================
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    if (_alertView) {
        _alertView.center = CGPointMake(self.view.center.x, self.view.center.y - 100);
    }
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    if (_alertView) {
        _alertView.center = self.view.center;
    }
}

@end
