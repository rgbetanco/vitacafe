//
//  ProductViewController.h
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "WebService.h"

@interface ProductViewController : BaseViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, WebServiceDelegate>

@property (nonatomic) BOOL isOrder;     // 是否進入點餐流程

@end
