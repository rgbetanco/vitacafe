//
//  Global.h
//  PosApp
//
//  Created by Kevin Phua on 9/16/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Product.h"
#import "ShopInfo.h"
#import "CartItem.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define LEFT_MENU_WIDTH               200
#define TABLEVIEW_CORNER_RADIUS       12

#define BANNER_CHANGE_INTERVAL        5
#define LABEL_FONT_SIZE               18.0

#define GMS_API_KEY                   @"AIzaSyAv0ELXXvh7GTGjO3shtMBH1PRVdezQwuU"

// Configuration flags
#define CFG_BASE_URL                            @"csapp_ws_baseURL"
//#define CFG_APP_ID                              @"csapp_id"
#define CFG_APP_ID                              @"kultrp6zajt7xphrr8qa7rpfasebv9bp"
#define CFG_APP_URL                             @"csapp_url"
#define CFG_MSG_ENABLED                         @"csapp_msg_enabled"
#define CFG_POINTS_ENABLED                      @"csapp_points_enabled"
#define CFG_COLLECTIBLES_ENABLED                @"csapp_collectibles_enabled"
#define CFG_ORDERS_ENABLED                      @"csapp_orders_enabled"
#define CFG_FRIENDS_ENABLED                     @"csapp_friends_enabled"
#define CFG_USER_NEW_ENABLED                    @"csapp_user_new_enabled"
#define CFG_PRODUCTS_SORT_ENABLED               @"csapp_products_sort_enabled"
#define CFG_PRODUCTS_HEART_ENABLED              @"csapp_products_heart_enabled"
#define CFG_PRODUCTS_DROPDOWN_ENABLED           @"csapp_products_dd_enabled"
#define CFG_PRODUCTS_GROUP_MAINMENU_ENABLED     @"csapp_products_group_mm_enabled"
#define CFG_PRODUCTS_GROUP_SCREEN_ENABLED       @"csapp_products_group_scn_enabled"
#define CFG_BOOKMARKS_TOOL_ENABLED              @"csapp_bookmarks_tool_enabled"
#define CFG_RTLS_URL                            @"csapp_rtls_url"

// User defaults
#define UD_KEY_PHONE                            @"phone"
#define UD_KEY_PASSWORD                         @"password"
#define UD_KEY_LAST_LOGIN                       @"last_login"
#define UD_DEVICE_TOKEN                         @"device_token"
#define UD_KEY_FIRST_USE                        @"first_use"
#define UD_KEY_LAST_SELECTED_SHOP_ID            @"last_selected_shop_id"
#define UD_KEY_LAST_SELECTED_SHOP_NAME          @"last_selected_shop_name"
#define UD_KEY_LAST_SELECTED_DATE               @"last_selected_date"
#define UD_KEY_LAST_SELECTED_TIME               @"last_selected_time"
#define UD_KEY_LAST_SELECTED_TYPE               @"last_selected_type"
#define UD_KEY_LAST_SELECTED_ADDR               @"last_selected_addr"

// Member info
#define INFO_KEY_VIPID                @"vip_id"
#define INFO_KEY_VIP_LEVEL            @"vip_level"
#define INFO_KEY_VIP_LEVEL_NAME       @"vip_level_name"
#define INFO_KEY_NAME                 @"name"
#define INFO_KEY_ZIP                  @"zip"
#define INFO_KEY_ADDRESS              @"address"
#define INFO_KEY_TELEPHONE            @"telephone"
#define INFO_KEY_SEX                  @"sex"
#define INFO_KEY_BIRTHDAY             @"birthday"
#define INFO_KEY_EMAIL                @"email"
#define INFO_KEY_MOBILE               @"mobile"
#define INFO_KEY_LAST_POINT           @"last_point"
#define INFO_KEY_LAST_AMT             @"last_amt"
#define INFO_KEY_LAST_IC_POINT        @"last_icpoint"
#define INFO_KEY_CARD                 @"card"
#define INFO_KEY_END_DATE             @"end_date"
#define INFO_KEY_ID_CARD              @"id_card"
#define INFO_KEY_ACCKEY               @"acckey"
#define INFO_KEY_MSGS_UNREAD          @"msgs_unread"
#define INFO_KEY_PENDING_FRIENDS      @"pending_friends"
#define INFO_KEY_PENDING_FAVORITES    @"pending_favorites"

// Member VIP level
#define VIP_LEVEL_BLACK               @"01"
#define VIP_LEVEL_RED                 @"02"
#define VIP_LEVEL_SILVER              @"03"
#define VIP_LEVEL_GOLD                @"04"

// Friend info
#define SECTION_AWAITING_JOIN           0
#define SECTION_AWAITING_APPROVAL       1
#define SECTION_ALL_FRIENDS             2

#define KIND_INVITED_FRIENDS            1
#define KIND_INVITING_FRIENDS           2

#define STATUS_AWAITING_APPROVAL        0
#define STATUS_REJECTED                 1
#define STATUS_APPROVED                 10

// Cache
#define CART_FILE                       @"cart"
#define FAVORITES_FILE                  @"favorites"
#define PRODUCTS_FILE                   @"products"

typedef enum {
    COLOR_TEXT_MENU,
    COLOR_TEXT_TITLE,
    COLOR_TEXT_BTN,
    COLOR_TEXT_GROUP,
    COLOR_TEXT_ITEM_TITLE,
    COLOR_TEXT_ITEM_DATA,
    COLOR_TEXT_STORE_TITLE,
    COLOR_TEXT_STORE_TIME,
    COLOR_BG_MAIN,
    COLOR_BG_TITLE,
    COLOR_TEXT_PRODUCT_PRICE,
    COLOR_TEXT_PRODUCT_TITLE,
    COLOR_BTN_CAT_BG,
    COLOR_BTN_CAT_BG_SEL,
    COLOR_BTN_CAT_LIST_BG,
    COLOR_TYPE_LINK,
    COLOR_TYPE_STEPS,
    COLOR_TYPE_TEXTBOX_BG,
    COLOR_TYPE_BUTTON_UP,
    COLOR_TYPE_NAVBAR_BG,
    COLOR_TYPE_TEXT
} ColorType;

typedef enum {
    PARENT_PAGE_DEFAULT,
    PARENT_PAGE_INTRO
} ParentPage;

extern BOOL g_IsLogin;
extern BOOL g_IsOnline;
extern BOOL g_IsFirstUse;
extern NSString *g_MemberPhone;
extern NSString *g_MemberPassword;
extern NSMutableDictionary *g_MemberInfo;
extern NSString *g_WSLastServerUrl;
extern NSString *g_WSLastCommand;
extern NSString *g_WSLastCommandParams;
extern NSDictionary *g_SysConfig;
extern NSMutableArray *g_MyFavorites;
extern NSMutableArray *g_ShoppingCart;  // Array of WebOrder01
extern NSMutableDictionary *g_Products; // Dictionary of selected products
extern NSString *g_SelectedShopId;
extern NSString *g_SelectedShopName;
extern int g_SelectedSourceType;
extern NSString *g_SelectedMealDate;
extern NSString *g_SelectedMealTime;
extern int g_SelectedSaleMethod;
extern int g_SelectedPaymentTerms;
extern NSString *g_SelectedRecAddr;
extern NSString *g_SelectedWebOrder01;
extern NSString *g_SelectedWebOrder011;
extern NSString *g_LastSelectedShopId;
extern NSString *g_LastSelectedRecAddr;
extern NSString *g_PrivacyUrl;

@interface Global : NSObject

+ (NSString *)cfgBaseUrl;
+ (NSString *)cfgAppId;
+ (NSString *)cfgAppUrl;
+ (BOOL)cfgMsgEnabled;
+ (BOOL)cfgPointsEnabled;
+ (BOOL)cfgCollectiblesEnabled;
+ (BOOL)cfgOrdersEnabled;
+ (BOOL)cfgFriendsEnabled;
+ (BOOL)cfgUserNewEnabled;
+ (BOOL)cfgProductsSortEnabled;
+ (BOOL)cfgProductsHeartEnabled;
+ (BOOL)cfgProductsDropdownEnabled;
+ (BOOL)cfgProductsGroupMainMenuEnabled;
+ (BOOL)cfgProductsGroupScreenEnabled;
+ (BOOL)cfgBookmarksToolEnabled;
+ (NSString *)cfgRtlsUrl;
+ (void)addFavorite:(Product *)product;
+ (void)removeFavorite:(Product *)product;
+ (BOOL)isFavorite:(Product *)product;
+ (void)saveFavoritesToFile:(NSMutableArray *)favorites;
+ (NSMutableArray *)retrieveFavoritesFromFile;
+ (void)addShoppingCart:(CartItem *)item;
+ (void)modifyShoppingCart:(CartItem *)item atIndex:(NSInteger)index;
+ (void)clearShoppingCart;
+ (void)saveShoppingCartToFile:(NSMutableArray *)cart;
+ (NSMutableArray *)retrieveShoppingCartFromFile;
+ (void)initUserDefaults;
+ (void)clearCartUserDefaults;
+ (Product *)getCachedProduct:(NSString *)prodId;
+ (void)cacheProduct:(Product *)product;
+ (void)saveProductsToFile;
+ (NSMutableDictionary *)retrieveProductsFromFile;

+ (UIColor *)colorWithType:(ColorType)type;
+ (CGImageRef)createQRImageForString:(NSString *)string size:(CGSize)size;
+ (UIImage *)imageResize:(UIImage*)image height:(CGFloat)height;
+ (CGFloat)pixelToPoints:(CGFloat)px;
+ (NSString *)hexStringFromData:(NSData *)data;
+ (NSString *)stringByStrippingHTML:(NSString *)s;
+ (int)randomNumber;
+ (NSString *)urlEncodedString:(NSString *)string;

@end
