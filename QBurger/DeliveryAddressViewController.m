//
//  DeliveryAddressViewController.m
//  QBurger
//
//  Created by Kevin Phua on 10/5/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "DeliveryAddressViewController.h"
#import "WebService.h"
#import "Global.h"
#import "ActionSheetPicker.h"
#import "PostArea00.h"
#import "PostArea01.h"
#import "PostAreaRd.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface DeliveryAddressViewController () <WebServiceDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (nonatomic, assign) IBOutlet UIView *viewBg;
@property (nonatomic, assign) IBOutlet UIView *viewSubBg;
@property (nonatomic, assign) IBOutlet UIPickerView *pickerView;
@property (nonatomic, assign) IBOutlet UITextField *txtAddressNo;
@property (nonatomic, assign) IBOutlet UITextField *txtAddressOther;

@property (strong, nonatomic) NSMutableArray *postAreaCities;
@property (strong, nonatomic) NSMutableArray *postAreaDistricts;
@property (strong, nonatomic) NSMutableArray *postAreaRoads;

@property (strong, nonatomic) NSMutableString *address;

@end


@implementation DeliveryAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    _postAreaCities = [NSMutableArray new];
    _postAreaDistricts = [NSMutableArray new];
    _postAreaRoads = [NSMutableArray new];
    _address = [NSMutableString new];
    
    self.title = NSLocalizedString(@"OrderAddress", nil);
    self.viewBg.backgroundColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1.0];
    self.viewSubBg.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:239.0/255.0 blue:240.0/255.0 alpha:1.0];
    self.viewBg.layer.cornerRadius = 10;
    self.viewSubBg.layer.cornerRadius = 10;
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapView:)];
    [self.view addGestureRecognizer:tapView];
    
    UIBarButtonItem *btnRightOK = [[UIBarButtonItem alloc] initWithTitle:@"確定" style:UIBarButtonItemStylePlain target:self action:@selector(onBtnDone:)];
    self.navigationItem.rightBarButtonItem = btnRightOK;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getPostArea00:[Global cfgAppId] areaId:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onTapView:(UITapGestureRecognizer *)tapGesture {
    // Dismiss keyboard
    [self.view endEditing:YES];
}

- (void)updateAddress {
    PostArea00 *postAreaCity = [_postAreaCities objectAtIndex:[_pickerView selectedRowInComponent:0]];
    PostArea01 *postAreaDistrict = [_postAreaDistricts objectAtIndex:[_pickerView selectedRowInComponent:1]];
    PostAreaRd *postAreaRoad = [_postAreaRoads objectAtIndex:[_pickerView selectedRowInComponent:2]];
    
    _address.string = @"";
    if (postAreaCity) {
        [_address appendString:postAreaCity.postAreaName];
    }
    if (postAreaDistrict) {
        [_address appendString:postAreaDistrict.townName];
    }
    if (postAreaRoad) {
        [_address appendString:postAreaRoad.postRdName];
    }
    if (_txtAddressNo.text && _txtAddressNo.text.length>0) {
        [_address appendString:_txtAddressNo.text];
        [_address appendString:@"號"];
    }
    if (_txtAddressOther.text && _txtAddressOther.text.length>0) {
        [_address appendString:_txtAddressOther.text];
    }
    
    NSLog(@"Full address = %@", _address);
}

- (void)onBtnDone:(id)sender {
    if (self.delegate) {
        [self updateAddress];
        [self.delegate deliveryAddressSelected:_address];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

//==================================================================
#pragma UIPickerViewDataSource
//==================================================================

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

// returns the number of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger result = 0;
    switch (component) {
        case 0:
            result = [self.postAreaCities count];
            break;
        case 1:
            result = [self.postAreaDistricts count];
            break;
        case 2:
            result = [self.postAreaRoads count];
            break;
        default:
            break;
    }
    return result;
}

//==================================================================
#pragma UIPickerViewDelegate
//==================================================================

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *result = @"";
    switch (component) {
        case 0:
            {
                PostArea00 *postAreaCity = [_postAreaCities objectAtIndex:row];
                result = postAreaCity.postAreaName;
            }
            break;
        case 1:
            {
                PostArea01 *postAreaDistrict = [_postAreaDistricts objectAtIndex:row];
                result = postAreaDistrict.townName;
            }
            break;
        case 2:
            {
                PostAreaRd *postAreaRoad = [_postAreaRoads objectAtIndex:row];
                result = postAreaRoad.postRdName;
            }
            break;
        default:
            break;
    }
    return result;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            {
                PostArea00 *postAreaCity = [_postAreaCities objectAtIndex:row];
                [_postAreaDistricts removeAllObjects];
                [_postAreaRoads removeAllObjects];
                [self.pickerView reloadComponent:1];
                [self.pickerView reloadComponent:2];
                
                WebService *ws = [WebService new];
                ws.delegate = self;
                [ws getPostArea01:[Global cfgAppId] postAreaId:postAreaCity.postAreaId];
            }
            break;
        case 1:
            {
                PostArea01 *postAreaDistrict = [_postAreaDistricts objectAtIndex:row];
                [_postAreaRoads removeAllObjects];
                [self.pickerView reloadComponent:2];
                
                WebService *ws = [WebService new];
                ws.delegate = self;
                [ws getPostAreaRd:[Global cfgAppId] postAreaId:postAreaDistrict.postAreaId postAreaSNo:postAreaDistrict.postAreaSno];
            }
            break;
        case 2:
            {
                [self updateAddress];
            }
            break;
        default:
            break;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView) {
        tView = [[UILabel alloc] init];
        tView.textAlignment = NSTextAlignmentCenter;
        tView.adjustsFontSizeToFitWidth = YES;
    }
    
    tView.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return tView;
}

//==================================================================
#pragma UITextFieldDelegate
//==================================================================

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateAddress];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_POST_AREA00] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *postAreas = [jsonObject objectForKey:@"data"];
                [_postAreaCities removeAllObjects];
                for (NSDictionary *postArea in postAreas) {
                    PostArea00 *postArea00 = [PostArea00 new];
                    postArea00.positionId = [postArea objectForKey:@"position_id"];
                    postArea00.postAreaId = [postArea objectForKey:@"pstarea_id"];
                    postArea00.postAreaName = [postArea objectForKey:@"pstarea_name"];
                    [_postAreaCities addObject:postArea00];
                }
                [self.pickerView reloadComponent:0];
            }
        } else if ([resultName compare:WS_GET_POST_AREA01] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *postAreas = [jsonObject objectForKey:@"data"];
                [_postAreaDistricts removeAllObjects];
                for (NSDictionary *postArea in postAreas) {
                    PostArea01 *postArea01 = [PostArea01 new];
                    postArea01.positionId = [postArea objectForKey:@"position_id"];
                    postArea01.postAreaId = [postArea objectForKey:@"pstarea_id"];
                    postArea01.postAreaSno = [postArea objectForKey:@"pstarea_sno"];
                    postArea01.townName = [postArea objectForKey:@"town_name"];
                    [_postAreaDistricts addObject:postArea01];
                }
                [self.pickerView reloadComponent:1];
            }
        } else if ([resultName compare:WS_GET_POST_AREARD] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *postRoads = [jsonObject objectForKey:@"data"];
                [_postAreaRoads removeAllObjects];
                for (NSDictionary *postRoad in postRoads) {
                    PostAreaRd *postAreaRd = [PostAreaRd new];
                    postAreaRd.positionId = [postRoad objectForKey:@"position_id"];
                    postAreaRd.postAreaId = [postRoad objectForKey:@"pstarea_id"];
                    postAreaRd.postAreaSno = [postRoad objectForKey:@"pstarea_sno"];
                    postAreaRd.postRdFstName = [postRoad objectForKey:@"pstrd_fstname"];
                    postAreaRd.postRdId = [postRoad objectForKey:@"pstrd_id"];
                    postAreaRd.postRdName = [postRoad objectForKey:@"pstrd_name"];
                    [_postAreaRoads addObject:postAreaRd];
                }
                [self.pickerView reloadComponent:2];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.viewNetworkError setHidden:NO];
}

@end
