//
//  PromoViewController.m
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "PromoViewController.h"
#import "SWRevealViewController.h"
#import "Global.h"
#import "PromoListCell.h"
#import "PromoDetailViewController.h"
#import "Pubcode.h"
#import "HMSegmentedControl.h"
#import "UIImageView+WebCache.h"

#define HEADER_IMAGE_HEIGHT        180     // points

@interface PromoViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) HMSegmentedControl *segPubcode;
@property (strong, nonatomic) NSMutableArray *promotions;
@property (strong, nonatomic) NSMutableArray *pubcodes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end

@implementation PromoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.promotions = [NSMutableArray new];
    self.pubcodes = [NSMutableArray new];
    
    self.segPubcode = [HMSegmentedControl new];
    self.segPubcode.frame = CGRectMake(0, 80, self.view.frame.size.width, 50);
    self.segPubcode.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segPubcode.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
    self.segPubcode.selectionIndicatorBoxColor = [UIColor colorWithRed:100.0/255.0f green:191.0/255.0f blue:147.0/255.0f alpha:1.0f];
    self.segPubcode.selectionIndicatorBoxOpacity = 1.0;
    self.segPubcode.backgroundColor = [UIColor colorWithRed:244.0/255.0f green:199.0/255.0f blue:54.0/255.0f alpha:1.0f];
    self.segPubcode.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segPubcode.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self.segPubcode addTarget:self
                         action:@selector(segPubcodeValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.segPubcode];

    self.tableView.backgroundColor = [UIColor clearColor];
    //self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.contentInset = UIEdgeInsetsMake(-80, 0, 0, 0);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getPubcode:[Global cfgAppId]];
    [ws showWaitingView:self.view];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = NSLocalizedString(@"Promotions", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)adjustHeightOfTableview
{
    CGFloat height = self.tableView.contentSize.height;
    CGFloat maxHeight = 0.85 * self.tableView.superview.frame.size.height;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the height constraint accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        self.tableViewHeightConstraint.constant = height;
        [self.view setNeedsUpdateConstraints];
    }];
}

- (void)segPubcodeValueChanged:(id)sender {
    NSInteger selectedIndex = self.segPubcode.selectedSegmentIndex;
    Pubcode *pubcode = [_pubcodes objectAtIndex:selectedIndex];
    NSLog(@"Selected serno = %ld", selectedIndex);
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getNews:[Global cfgAppId] pmId:pubcode.serno];
    [ws showWaitingView:self.view];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.promotions count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PromoListCell";
    PromoListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"PromoListCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }

    NSDictionary *promo = [self.promotions objectAtIndex:[indexPath row]];
    cell.lblDate.text = [promo objectForKey:@"bdate"];
    cell.lblTitle.text = [promo objectForKey:@"subject"];
    cell.lblTitle.numberOfLines = 0;
    [cell.lblTitle sizeToFit];
    cell.lblDate.adjustsFontSizeToFitWidth = YES;
    
    NSString *htmlBody = [promo objectForKey:@"htmlbody"];
    if ([htmlBody isKindOfClass:[NSNull class]]) {
        cell.lblDesc.text = @"";
    } else {
        NSError *error;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[htmlBody dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:&error];
        NSRange range = NSMakeRange(0, attributedString.length);
        UIFont *font = [UIFont systemFontOfSize:13.0f];
        [attributedString addAttribute:NSFontAttributeName value:font range:range];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
        cell.lblDesc.attributedText = attributedString;
        cell.lblDesc.numberOfLines = 0;
        [cell.lblDesc sizeToFit];
    }
    
    NSString *imageUrl = [promo objectForKey:@"imgfile1"];
    if ([imageUrl isKindOfClass:[NSNull class]]) {
        //No image
        cell.imgFile.image = nil;
    } else {
        NSString *imagePath = [NSString stringWithFormat:@"%@%@", NEWS_IMAGE_PATH, imageUrl];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:imagePath]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                                 NSLog(@"Received image %ld of %ld bytes", receivedSize, expectedSize);
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    NSLog(@"Received image width=%.1f, height=%.lf", image.size.width, image.size.height);
                                    
                                    //UIImage *resizedImage = [self imageResize:image];
                                    
                                    //NSLog(@"Resized image width=%.1f, height=%.lf", resizedImage.size.width, resizedImage.size.height);
                                    
                                    cell.imgFile.image = image;
                                }
                            }];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *promo = [self.promotions objectAtIndex:[indexPath row]];
    
    // Open ProductDetailViewController
    PromoDetailViewController *productDetailController = [[PromoDetailViewController alloc] initWithNibName:@"PromoDetailViewController" bundle:nil];
    [productDetailController setPromo:promo];
    [productDetailController setPromoName:[promo objectForKey:@"subject"]];
    
    // Hide back button text on next page
    self.title = @"";

    [self.navigationController pushViewController:productDetailController animated:YES];
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject  {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_NEWS] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ promotions: %@", datacnt, message);
                
                NSArray *news = (NSArray *)[jsonObject objectForKey:@"news"];
                
                if (news) {
                    [self.promotions setArray:news];
                }
                [self.tableView reloadData];
                [self adjustHeightOfTableview];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
            [self.tableView setHidden:NO];
            [self.viewNetworkError setHidden:YES];
        } else if ([resultName compare:WS_GET_PUBCODE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                NSArray *pubcodes = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (pubcodes) {
                    for (NSDictionary *pubcode in pubcodes) {
                        Pubcode *pubCode = [Pubcode new];
                        pubCode.pmname = (NSString *)[pubcode objectForKey:@"pmname"];
                        pubCode.serno = [[pubcode objectForKey:@"serno"] integerValue];
                        [_pubcodes addObject:pubCode];
                    }
                    
                    // Sort pub codes by serno
                    NSArray *sortedArray = [_pubcodes sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                        NSNumber *first = [NSNumber numberWithInteger:[(Pubcode*)a serno]];
                        NSNumber *second = [NSNumber numberWithInteger:[(Pubcode*)b serno]];
                        return [first compare:second];
                    }];
                    
                    _pubcodes = [NSMutableArray arrayWithArray:sortedArray];
                    
                    NSMutableArray *pmnameArray = [NSMutableArray new];
                    for (Pubcode *pubcode in _pubcodes) {
                        [pmnameArray addObject:pubcode.pmname];
                    }
                    
                    // Update segmented control
                    [self.segPubcode setSectionTitles:pmnameArray];
                    self.segPubcode.selectedSegmentIndex = 0;
                    
                    Pubcode *firstPubcode = [_pubcodes firstObject];
                    WebService *ws = [[WebService alloc] init];
                    ws.delegate = self;
                    [ws getNews:[Global cfgAppId] pmId:firstPubcode.serno];
                    [ws showWaitingView:self.view];
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.tableView setHidden:YES];
    [self.viewNetworkError setHidden:NO];
}

- (UIImage *)imageResize:(UIImage*)image
{
    CGFloat ratio = image.size.width/image.size.height;
    CGSize newSize;
    CGPoint offset = CGPointMake(0, 0);
    
    NSLog(@"Image width=%.1f, height=%.1f, ratio=%.1f", image.size.width, image.size.height, ratio);
    
    CGFloat newHeight = HEADER_IMAGE_HEIGHT;
    CGFloat newWidth = ratio * newHeight;
    newSize = CGSizeMake(newWidth, newHeight);
    
    NSLog(@"New image size w=%.1f, h=%.1f", newSize.width, newSize.height);
    
    CGRect resizeRect = CGRectMake(offset.x, offset.y, newSize.width, newSize.height);
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    } else {
        UIGraphicsBeginImageContext(newSize);
    }
    [image drawInRect:resizeRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
