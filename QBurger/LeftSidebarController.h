//
//  LeftSidebarController.h
//  RunnerPlaza
//
//  Created by Kevin Phua on 10/7/14.
//  Copyright (c) 2014 Hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftSidebarController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *menuItems;

- (void)setCurrentRow:(int)row;

@end
