//
//  SelectShopViewController.m
//  QBurger
//
//  Created by Kevin Phua on 15/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "SelectShopViewController.h"
#import "WebService.h"
#import "ShopInfo.h"
#import "Global.h"
#import "LocationEngine.h"

#define SECTION_CACHE_LIST          0
#define SECTION_SHOP_LIST           1
#define ROW_LAST_SELECTED_SHOP      0
#define ROW_NEAREST_SHOP            1

@interface SelectShopViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *shops;
@property (strong, nonatomic) NSMutableArray *shopNames;

@property (strong, nonatomic) ShopInfo *lastSelectedShop;
@property (strong, nonatomic) ShopInfo *nearestShop;
@property (strong, nonatomic) LocationEngine *locationEngine;

@end

@implementation SelectShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Init LocationManager
    self.locationEngine = [LocationEngine getInstance];
    
    _shops = [NSMutableArray new];
    _shopNames = [NSMutableArray new];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.layer.cornerRadius = 10;
    
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getShop:[Global cfgAppId] shopId:@"" areaId:@"" pstAreaId:@"" city:@"" pstAreaSNo:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sortShopsByDistance {
    NSArray *sortedShops;
    sortedShops = [_shops sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:((ShopInfo *)a).latitude longitude:((ShopInfo *)a).longitude];
        CLLocation *location2 = [[CLLocation alloc] initWithLatitude:((ShopInfo *)b).latitude longitude:((ShopInfo *)b).longitude];
        CLLocationDistance distance1 = [self.locationEngine.currentLocation distanceFromLocation:location1];
        CLLocationDistance distance2 = [self.locationEngine.currentLocation distanceFromLocation:location2];
        return (distance1 < distance2) ? NSOrderedAscending : (distance1 > distance2) ? NSOrderedDescending : NSOrderedSame;
    }];
    _shops = [NSMutableArray arrayWithArray:sortedShops];
}

- (void)findNearestShop {
    CLLocationDistance minDistance = 0;
    for (ShopInfo *shop in _shops) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:shop.latitude longitude:shop.longitude];
        CLLocationDistance distance = [self.locationEngine.currentLocation distanceFromLocation:location];
        if (minDistance == 0 || distance < minDistance) {
            minDistance = distance;
            _nearestShop = shop;
        }
    }    
    NSLog(@"Nearest shop = %@, distance = %.1f", _nearestShop.shopName, minDistance);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == SECTION_CACHE_LIST) {
        return 2;
    }
    return _shops.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == SECTION_CACHE_LIST) {
        return 50;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == SECTION_CACHE_LIST) {
        return NSLocalizedString(@"OrderBranch", nil);
    }
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SECTION_CACHE_LIST) {
        static NSString *CellIdentifier = @"CacheCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }

        if (indexPath.row == ROW_LAST_SELECTED_SHOP) {
            NSMutableAttributedString *text = [[NSMutableAttributedString alloc]
             initWithString:NSLocalizedString(@"LastSelectedShop", nil)];
        
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor colorWithRed:0 green:102.0/255.0f blue:51.0/255.0 alpha:1.0f]
                         range:NSMakeRange(0, text.length)];
            
            if (_lastSelectedShop) {
                [text appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", _lastSelectedShop.shopName]]];
                cell.detailTextLabel.font = [UIFont systemFontOfSize:16.0];
                cell.detailTextLabel.textColor = [UIColor darkGrayColor];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@\n%@", _lastSelectedShop.address, _lastSelectedShop.tel];
                cell.detailTextLabel.numberOfLines = 0;
                [cell.detailTextLabel sizeToFit];
            }
            
            cell.textLabel.attributedText = text;
            cell.textLabel.numberOfLines = 0;
            [cell.textLabel sizeToFit];
        } else if (indexPath.row == ROW_NEAREST_SHOP) {
            NSMutableAttributedString *text = [[NSMutableAttributedString alloc]
                                               initWithString:NSLocalizedString(@"NearestShop", nil)];

            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor colorWithRed:0 green:102.0/255.0f blue:51.0/255.0f alpha:1.0f]
                         range:NSMakeRange(0, text.length)];

            if (_nearestShop) {
                [text appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", _nearestShop.shopName]]];
                cell.textLabel.attributedText = text;
                cell.textLabel.numberOfLines = 0;
                [cell.textLabel sizeToFit];
                cell.detailTextLabel.font = [UIFont systemFontOfSize:16.0];
                cell.detailTextLabel.textColor = [UIColor darkGrayColor];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@\n%@", _nearestShop.address, _nearestShop.tel];
                cell.detailTextLabel.numberOfLines = 0;
                [cell.detailTextLabel sizeToFit];
            }
        }
        return cell;
    } else {
        static NSString *CellIdentifier = @"ShopCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        ShopInfo *shop = [_shops objectAtIndex:indexPath.row];
        cell.textLabel.text = shop.shopName;
        cell.textLabel.font = [UIFont systemFontOfSize:20.0];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:16.0];
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@\n%@", shop.address, shop.tel];
        cell.detailTextLabel.numberOfLines = 0;
        [cell.detailTextLabel sizeToFit];
        return cell;
    }
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SECTION_CACHE_LIST) {
        if (indexPath.row == ROW_LAST_SELECTED_SHOP) {
            if (_lastSelectedShop) {
                [self.delegate onSelectShop:_lastSelectedShop];
            }
        } else if (indexPath.row == ROW_NEAREST_SHOP) {
            if (_nearestShop) {
                [self.delegate onSelectShop:_nearestShop];
            }
        }
    } else {
        ShopInfo *shop = [_shops objectAtIndex:indexPath.row];
        [self.delegate onSelectShop:shop];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_SHOP] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *shops = [jsonObject objectForKey:@"data"];
                [_shops removeAllObjects];
                for (NSDictionary *shop in shops) {
                    ShopInfo *shopInfo = [ShopInfo new];
                    shopInfo.shopId = [shop objectForKey:@"shop_id"];
                    shopInfo.shopName = [shop objectForKey:@"shop_name"];
                    shopInfo.address = [shop objectForKey:@"address"];
                    shopInfo.fax = [shop objectForKey:@"fax"];
                    shopInfo.tel = [shop objectForKey:@"tel"];
                    shopInfo.latitude = [[shop objectForKey:@"latitude"] doubleValue];
                    shopInfo.longitude = [[shop objectForKey:@"longitude"] doubleValue];
                    shopInfo.workTime = [shop objectForKey:@"worktime"];
                    shopInfo.htmlBody = [shop objectForKey:@"htmlbody"];
                    [_shops addObject:shopInfo];
                    
                    if (g_LastSelectedShopId && [shopInfo.shopId isEqualToString:g_LastSelectedShopId]) {
                        _lastSelectedShop = shopInfo;
                    }
                }
                [self sortShopsByDistance];
                [self findNearestShop];
                [self.tableView reloadData];
            }
        } 
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
