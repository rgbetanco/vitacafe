//
//  IntroViewController.m
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "IntroViewController.h"
#import "CustomPageViewController.h"
#import "SWRevealViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Global.h"
#import "ProductViewController.h"
#import "MemberInfoViewController.h"
#import "PromoViewController.h"
#import "LocationsViewController.h"
#import "LeftSidebarController.h"
#import "OrderMainViewController.h"
#import "OrderRecordsViewController.h"
#import "UIView+Toast.h"
#import "IntroContentViewController.h"
#import "AppCategory.h"

#define WEBSITE_URL                 @"http://www.la-join.com.tw:2080/appserver/lajoin/webservice/"


@interface IntroViewController () <WebServiceDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) NSString *websiteUrl;
@property (strong, nonatomic) NSMutableArray *contentImages;
@property (strong, nonatomic) NSMutableArray *contentUrls;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (nonatomic) NSInteger currentPage;

@property (strong, nonatomic) NSTimer *bannerTimer;

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Set page view controller
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenWidth * 1280 / 1440;
    CGRect pageVCFrame = CGRectMake(self.contentView.frame.origin.x,
                                    self.contentView.frame.origin.y,
                                    screenWidth,
                                    screenHeight);
    [self.pageViewController.view setFrame:pageVCFrame];
    [self.pageViewController.view setTranslatesAutoresizingMaskIntoConstraints:false];
    self.pageViewController.dataSource = self;
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    // Try to login
    g_MemberPhone = [[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_PHONE];
    g_MemberPassword = [[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_PASSWORD];
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:UD_DEVICE_TOKEN];
    
    if (g_MemberPhone && g_MemberPassword) {
        // Try to login
        WebService *ws = [[WebService alloc] init];
        ws.delegate = self;
        [ws login:[Global cfgAppId] sId:g_MemberPhone password:g_MemberPassword deviceId:deviceToken];
        [ws showWaitingView:self.view];
    }
    
    [self getProducts];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateNavigationBar];
    [self getPageList];
    [self updateFavorites:PARENT_PAGE_INTRO];
    [self updateMessages:PARENT_PAGE_INTRO];
    //[self updateCart];
    
    _contentImages = [NSMutableArray new];
    _contentUrls = [NSMutableArray new];
    [self getBanner];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopBannerTimer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getProducts {
    WebService *ws = [WebService new];
    [ws setDelegate:self];
    [ws getProdcate00:[Global cfgAppId]];
}

- (void)getBanner {
    // Get banner
    WebService *ws = [WebService new];
    [ws setDelegate:self];
    [ws getBanner:[Global cfgAppId]];
}

- (IBAction)onBtnMakeOrder:(id)sender {
    if (!g_IsLogin) {
        [self.view makeToast:NSLocalizedString(@"PleaseLogin", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    }
    OrderMainViewController *orderMainVC = [[OrderMainViewController alloc] initWithNibName:@"OrderMainViewController" bundle:nil];
    [self.navigationController pushViewController:orderMainVC animated:YES];
}

- (IBAction)onBtnOrderHistory:(id)sender {
    NSLog(@"YOU JUST PRESSED ME");
    if (!g_IsLogin) {
        [self.view makeToast:NSLocalizedString(@"PleaseLogin", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    }
    OrderRecordsViewController *orderRecords = [[OrderRecordsViewController alloc] initWithNibName:@"OrderRecordsViewController" bundle:nil];
    [self.navigationController pushViewController:orderRecords animated:YES];
}

- (IBAction)onBtnLocator:(id)sender {
    LocationsViewController *locationsViewController = [[LocationsViewController alloc] initWithNibName:@"LocationsViewController" bundle:nil];
    [self.navigationController pushViewController:locationsViewController animated:YES];
}

- (IBAction)onBtnNews:(id)sender {
    PromoViewController *promoViewController = [[PromoViewController alloc] initWithNibName:@"PromoViewController" bundle:nil];
    [self.navigationController pushViewController:promoViewController animated:YES];
}

- (IBAction)onBtnMemberCenter:(id)sender {
    if (!g_IsLogin) {
        [self.view makeToast:NSLocalizedString(@"PleaseLogin", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    }
    MemberInfoViewController *memberInfoViewController = [[MemberInfoViewController alloc] initWithNibName:@"MemberInfoViewController" bundle:nil];
    [self.navigationController pushViewController:memberInfoViewController animated:YES];
}

- (IBAction)onBtnShareApp:(id)sender {
    NSString *appUrl = [Global cfgAppUrl];
    NSString *text = [NSString stringWithFormat:NSLocalizedString(@"ShareApp", nil), appUrl];
    
    NSLog(@"Share text = %@", text);

    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[text]
                                                                             applicationActivities:nil];
    
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // iPhone
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        // iPad -Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (void)registerDeviceToken {
    NSString *acckey = [g_MemberInfo objectForKey:INFO_KEY_ACCKEY];
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:UD_DEVICE_TOKEN];
    
    if (acckey && acckey.length>0 && deviceToken && deviceToken.length > 0) {
        WebService *ws = [[WebService alloc] init];
        [ws setDelegate:self];
        [ws registerDevice:[Global cfgAppId] accKey:acckey deviceId:deviceToken];
    }
}

//==================================================================
#pragma UIPageViewControllerDataSource
//==================================================================

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    if (viewController == nil) {
        return [self viewControllerAtIndex:0];
    }
    
    NSUInteger index = [(IntroContentViewController *)viewController pageIndex];
    if (index <= 0) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(IntroContentViewController *)viewController pageIndex];
    if (index >= self.contentImages.count-1) {
        return nil;
    }
    
    index++;
    return [self viewControllerAtIndex:index];
}

- (IntroContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (index < [self.contentImages count]) {
        self.currentPage = index;
        IntroContentViewController *introContentViewController = [[IntroContentViewController alloc] initWithNibName:@"IntroContentViewController" bundle:nil];
        introContentViewController.pageIndex = index;
        introContentViewController.imagePath = [NSString stringWithString:[_contentImages objectAtIndex:index]];
        introContentViewController.websiteUrl = [NSString stringWithString:[_contentUrls objectAtIndex:index]];
        return introContentViewController;
    }
    return nil;
}

- (void)loadNextController {
    _currentPage++;
    if (_currentPage >= [_contentImages count]) {
        _currentPage = 0;
    }
    IntroContentViewController *nextViewController = [self viewControllerAtIndex:_currentPage];
    if (nextViewController == nil) {
        _currentPage = 0;
        nextViewController = [self viewControllerAtIndex:_currentPage];
    }
    
    //NSLog(@"Displaying banner %ld", _currentPage);
    
    [self.pageViewController setViewControllers:@[nextViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed) {
        IntroContentViewController *vc = (IntroContentViewController *)[self.pageViewController.viewControllers lastObject];
        NSInteger currentIndex = vc.pageIndex;
    }
}

/*
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return _contentImages.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return _currentPage;
}
*/

- (void)startBannerTimer {
    [self stopBannerTimer];
    _bannerTimer = [NSTimer scheduledTimerWithTimeInterval:BANNER_CHANGE_INTERVAL
                                 target:self
                               selector:@selector(loadNextController)
                               userInfo:nil
                                repeats:YES];
}

- (void)stopBannerTimer {
    if (_bannerTimer) {
        [_bannerTimer invalidate];
        _bannerTimer = nil;
    }
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_LOGIN] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *accKey = (NSString *)[jsonObject objectForKey:@"acckey"];
                if (accKey && accKey.length > 0) {
                    [g_MemberInfo setObject:accKey forKey:INFO_KEY_ACCKEY];
                }
                NSString *vipId = (NSString *)[jsonObject objectForKey:@"vip_id"];
                if (vipId && vipId.length > 0) {
                    [g_MemberInfo setObject:vipId forKey:INFO_KEY_VIPID];
                }
                NSString *vipLevel = (NSString *)[jsonObject objectForKey:@"vip_level"];
                if (vipLevel && vipLevel.length > 0) {
                    [g_MemberInfo setObject:vipLevel forKey:INFO_KEY_VIP_LEVEL];
                }
                NSString *vipLevelName = (NSString *)[jsonObject objectForKey:@"vip_level_name"];
                if (vipLevelName && vipLevelName.length > 0) {
                    [g_MemberInfo setObject:vipLevelName forKey:INFO_KEY_VIP_LEVEL_NAME];
                }
                NSString *name = (NSString *)[jsonObject objectForKey:@"name"];
                if (name && name.length > 0) {
                    [g_MemberInfo setObject:name forKey:INFO_KEY_NAME];
                }
                id zip = [jsonObject objectForKey:@"zip"];
                if ([zip isKindOfClass:[NSString class]]) {
                    NSString *zipString = (NSString *)zip;
                    [g_MemberInfo setObject:zipString forKey:INFO_KEY_ZIP];
                }
                NSString *address = (NSString *)[jsonObject objectForKey:@"address"];
                if (address && address.length > 0) {
                    [g_MemberInfo setObject:address forKey:INFO_KEY_ADDRESS];
                }
                NSString *telephone = (NSString *)[jsonObject objectForKey:@"telephone"];
                if (telephone && telephone.length > 0) {
                    [g_MemberInfo setObject:telephone forKey:INFO_KEY_TELEPHONE];
                }
                NSString *sex = (NSString *)[jsonObject objectForKey:@"sex"];
                if (sex && sex.length > 0) {
                    [g_MemberInfo setObject:sex forKey:INFO_KEY_SEX];
                }
                NSString *birthday = (NSString *)[jsonObject objectForKey:@"birthday"];
                if (birthday && birthday.length > 0) {
                    NSRange range = [birthday rangeOfString:@"T"];
                    if (range.location != NSNotFound) {
                        birthday = [birthday substringToIndex:range.location];
                    }
                    [g_MemberInfo setObject:birthday forKey:INFO_KEY_BIRTHDAY];
                }
                NSString *email = (NSString *)[jsonObject objectForKey:@"email"];
                if (email && email.length > 0) {
                    [g_MemberInfo setObject:email forKey:INFO_KEY_EMAIL];
                }
                NSString *mobile = (NSString *)[jsonObject objectForKey:@"mobile"];
                if (mobile && mobile.length > 0) {
                    [g_MemberInfo setObject:mobile forKey:INFO_KEY_MOBILE];
                }
                NSString *lastPoint = (NSString *)[jsonObject objectForKey:@"last_point"];
                if (lastPoint && lastPoint.length > 0) {
                    NSNumber *lastPointValue = [NSNumber numberWithInteger:[lastPoint intValue]];
                    [g_MemberInfo setObject:lastPointValue forKey:INFO_KEY_LAST_POINT];
                }
                NSString *lastAmt = (NSString *)[jsonObject objectForKey:@"last_amt"];
                if (lastAmt && lastAmt.length > 0) {
                    [g_MemberInfo setObject:lastAmt forKey:INFO_KEY_LAST_AMT];
                }
                NSString *lastIcPoint = (NSString *)[jsonObject objectForKey:@"last_icpoint"];
                if (lastIcPoint && lastIcPoint.length > 0) {
                    [g_MemberInfo setObject:lastIcPoint forKey:INFO_KEY_LAST_IC_POINT];
                }
                NSString *card = (NSString *)[jsonObject objectForKey:@"card"];
                if (card && card.length > 0) {
                    [g_MemberInfo setObject:card forKey:INFO_KEY_CARD];
                }
                NSString *endDate = (NSString *)[jsonObject objectForKey:@"end_date"];
                if (endDate && endDate.length > 0) {
                    [g_MemberInfo setObject:endDate forKey:INFO_KEY_END_DATE];
                }
                NSString *idCard = (NSString *)[jsonObject objectForKey:@"id_card"];
                if (idCard && idCard.length > 0) {
                    [g_MemberInfo setObject:idCard forKey:INFO_KEY_ID_CARD];
                }
                
                NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                                      dateStyle:NSDateFormatterShortStyle
                                                                      timeStyle:NSDateFormatterFullStyle];
                [[NSUserDefaults standardUserDefaults] setObject:dateString forKey:UD_KEY_LAST_LOGIN];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self registerDeviceToken];
                
                g_IsLogin = YES;
                
                [self updateVipPoints];
                [self getFriendList];
                [self getMessageList];
                [self getPageList];
                [self updateFavorites:PARENT_PAGE_INTRO];
                [self updateMessages:PARENT_PAGE_INTRO];
                //[self updateCart];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                if ([resultName compare:WS_LOGIN] == NSOrderedSame) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LoginFailed", nil)
message:message
                                                                       delegate:nil
                                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                              otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }
            
            [self updateNavigationBar];
        } else if ([resultName compare:WS_REGISTER_DEVICE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSLog(@"Register device success: %@", message);
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSLog(@"Register device failed: %@", message);
            }
        } else if ([resultName compare:WS_SEARCH_LAST_POINT_VALUE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *vipPoint = (NSString *)[jsonObject objectForKey:@"vip_point"];
                if (vipPoint && vipPoint.length > 0) {
                    NSNumber *vipPointValue = [NSNumber numberWithInteger:[vipPoint intValue]];
                    [g_MemberInfo setObject:vipPointValue forKey:INFO_KEY_LAST_POINT];

                }
                [self updateVipPoints];
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            NSMutableArray *pendingFriends = [NSMutableArray new];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_AWAITING_APPROVAL) {
                        NSNumber *kind = [friend objectForKey:@"kind"];
                        if ([kind intValue] == KIND_INVITED_FRIENDS) {
                            [pendingFriends addObject:friend];
                        }
                    }
                }
                
                NSNumber *numPending = [NSNumber numberWithInteger:pendingFriends.count];
                [g_MemberInfo setObject:numPending forKey:INFO_KEY_PENDING_FRIENDS];
                [self updateFriends:PARENT_PAGE_INTRO];
                
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_MESSAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ messages", datacnt);
                
                NSArray *messages = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (messages) {
                    int unreadMessages = 0;
                    for (NSDictionary *message in messages) {
                        NSString *readTime = [message objectForKey:@"read_time"];
                        if ([readTime isEqualToString:@""]) {
                            unreadMessages++;
                        }
                    }
                    
                    [g_MemberInfo setObject:[NSNumber numberWithInt:unreadMessages] forKey:INFO_KEY_MSGS_UNREAD];
                    [self updateMessages:PARENT_PAGE_INTRO];
                }
            }
        } else if ([resultName compare:WS_GET_BANNER] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                [_contentImages removeAllObjects];
                [_contentUrls removeAllObjects];
                
                NSArray *data = [jsonObject objectForKey:@"data"];
                NSString *imgPath = [jsonObject objectForKey:@"imgpath"];
                long dataCount = [[jsonObject objectForKey:@"datacnt"] longValue];
                
                if (dataCount == 0) {
                    NSLog(@"No banner images!!!");
                    return;
                }
                
                for (NSDictionary *imgData in data) {
                    NSString *imgFile = [NSString stringWithFormat:@"%@%@", imgPath, [imgData objectForKey:@"imgfile1"]];
                    NSString *url = [imgData objectForKey:@"url"];
                    [_contentImages addObject:imgFile];
                    [_contentUrls addObject:url];
                }
                
                // Set first image
                if (_contentImages.count>0) {
                    NSArray *startingViewControllers = @[[self viewControllerAtIndex:0]];
                    [self.pageViewController setViewControllers:startingViewControllers
                                                      direction:UIPageViewControllerNavigationDirectionForward
                                                       animated:YES
                                                     completion:nil];
                    [self addChildViewController:self.pageViewController];
                    [self.view addSubview:self.pageViewController.view];
                    [self.pageViewController didMoveToParentViewController:self];
                }
                
                [self startBannerTimer];
            }
        } else if ([resultName compare:WS_GET_PAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *pages = (NSArray *)[jsonObject objectForKey:@"data"];
                if (pages) {
                    for (NSDictionary *page in pages) {
                        NSString *url = [page objectForKey:@"url"];
                        NSString *cCode = [page objectForKey:@"c_code"];
                        if (![cCode isKindOfClass:[NSNull class]] &&
                            [cCode isEqualToString:@"PRIVACY"]) {
                            // Save url
                            g_PrivacyUrl = [NSString stringWithString:url];
                        }
                    }
                }
            }
        } else if ([resultName compare:WS_GET_PRODCATE_00] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ categories", datacnt);
                
                NSArray *categories = (NSArray *)[jsonObject objectForKey:@"data"];
                for (NSDictionary *category in categories) {
                    NSString *deptId = [category objectForKey:@"serno"];
                    
                    WebService *ws = [[WebService alloc] init];
                    ws.delegate = self;
                    [ws getProduct:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] shopId:nil saleMethod:1 posDeptId:nil deptId:deptId];
                }
            }
        } else if ([resultName compare:WS_GET_PRODUCT] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Get product message = %@", message);
                NSLog(@"Total %@ products", datacnt);
                
                NSArray *products = (NSArray *)[jsonObject objectForKey:@"product"];
                NSString *imgPath = [jsonObject objectForKey:@"imgpath"];
                for (NSDictionary *product in products) {
                    Product *prodInfo = [Product new];
                    prodInfo.prodId = [product objectForKey:@"prod_id"];
                    prodInfo.prodContent = [product objectForKey:@"prod_content"];
                    prodInfo.prodName1 = [product objectForKey:@"prod_name1"];
                    prodInfo.prodName2 = [product objectForKey:@"prod_name2"];
                    
                    double price1 = [[product objectForKey:@"price1"] doubleValue];
                    prodInfo.price1 = [NSString stringWithFormat:@"%d", (int)price1];
                    double price2 = [[product objectForKey:@"price2"] doubleValue];
                    prodInfo.price2 = [NSString stringWithFormat:@"%d", (int)price2];
                    
                    prodInfo.imgFile1 = [NSString stringWithFormat:@"%@%@", imgPath, [product objectForKey:@"imgfile1"]];
                    prodInfo.depId = [product objectForKey:@"dep_id"];
                    prodInfo.enable = [[product objectForKey:@"enable"] boolValue];
                    prodInfo.isRedeem = [[product objectForKey:@"isredeem"] boolValue];
                    prodInfo.isPack = [[product objectForKey:@"ispack"] boolValue];
                    prodInfo.isComb = [[product objectForKey:@"iscomb"] boolValue];
                    prodInfo.stopSale = [[product objectForKey:@"stop_sale"] boolValue];
                    
                    NSArray *appCates = (NSArray *)[product objectForKey:@"app_cate"];
                    NSMutableArray *appCategories = [NSMutableArray new];
                    for (NSDictionary *appCate in appCates) {
                        AppCategory *appCategory = [AppCategory new];
                        appCategory.imgFile1 = [NSString stringWithFormat:@"%@%@", CATE_IMAGE_PATH, [appCate objectForKey:@"imgfile1"]];
                        appCategory.imgFile2 = [NSString stringWithFormat:@"%@%@", CATE_IMAGE_PATH, [appCate objectForKey:@"imgfile2"]];
                        appCategory.serNo = [[appCate objectForKey:@"serno"] intValue];
                        appCategory.subject = [appCate objectForKey:@"subject"];
                        [appCategories addObject:appCategory];
                    }
                    prodInfo.appCate = appCategories;
                    [Global cacheProduct:prodInfo];
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.viewNetworkError setHidden:NO];
}

@end
