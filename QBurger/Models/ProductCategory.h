//
//  ProductCategory.h
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductCategory : NSObject

@property (nonatomic, strong) NSString *imgFile1;
@property (nonatomic, strong) NSString *imgFile2;
@property (nonatomic) BOOL isHome;
@property (nonatomic) BOOL isTag;
@property (nonatomic) int serNo;
@property (nonatomic, strong) NSString *subject;

@end
