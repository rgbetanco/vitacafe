//
//  ChangePasswordViewController.m
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "SignupViewController.h"
#import "CustomBadge.h"
#import "Global.h"

@interface ChangePasswordViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblOldPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPasswordConfirm;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPasswordConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePassword;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"ChangePassword", nil);
    
    self.lblOldPassword.text = NSLocalizedString(@"OldPassword", nil);
    self.lblNewPassword.text = NSLocalizedString(@"NewPassword", nil);
    self.lblNewPasswordConfirm.text = NSLocalizedString(@"ConfirmNewPassword", nil);
    self.txtOldPassword.placeholder = NSLocalizedString(@"OldPassword", nil);
    self.txtNewPassword.placeholder = NSLocalizedString(@"NewPassword", nil);
    self.txtNewPasswordConfirm.placeholder = NSLocalizedString(@"ConfirmNewPassword", nil);
    self.txtOldPassword.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
    self.txtNewPassword.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
    self.txtNewPasswordConfirm.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
    
    [self.btnChangePassword setTitle:NSLocalizedString(@"ChangePassword", nil) forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapView:)];
    [self.view addGestureRecognizer:tapView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnChangePassword:(id)sender {
    if (!_txtOldPassword.text || [_txtOldPassword.text length] == 0 ||
        !_txtNewPassword.text || [_txtNewPassword.text length] == 0 ||
        !_txtNewPasswordConfirm.text || [_txtNewPasswordConfirm.text length] == 0 ||
        ![_txtNewPassword.text isEqualToString:_txtNewPasswordConfirm.text]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                            message:NSLocalizedString(@"PasswordError", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    WebService *ws = [[WebService alloc] init];
    [ws setDelegate:self];
    
    NSString *vipId = [g_MemberInfo objectForKey:INFO_KEY_VIPID];
    NSString *acckey = [g_MemberInfo objectForKey:INFO_KEY_ACCKEY];
    
    [ws changePassword:[Global cfgAppId] sId:vipId acckey:acckey oldpass:self.txtOldPassword.text password:self.txtNewPassword.text];
    [ws showWaitingView:self.view];
}

- (void)onTapView:(UITapGestureRecognizer *)tapGesture
{
    // Dismiss keyboard
    [self.view endEditing:YES];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        long code = [[jsonObject objectForKey:@"code"] longValue];
        if ([resultName compare:WS_CHANGE_PASSWORD] == NSOrderedSame) {
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ChangePassword", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                
                // Save new password
                [[NSUserDefaults standardUserDefaults] setObject:_txtNewPassword.text forKey:UD_KEY_PASSWORD];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                // Go back
                [self.navigationController popViewControllerAnimated:YES];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ChangePasswordFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_SEARCH_LAST_POINT_VALUE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *vipPoint = (NSString *)[jsonObject objectForKey:@"vip_point"];
                if (vipPoint && vipPoint.length > 0) {
                    NSNumber *vipPointValue = [NSNumber numberWithInteger:[vipPoint intValue]];
                    [g_MemberInfo setObject:vipPointValue forKey:INFO_KEY_LAST_POINT];
                }
                [self updateVipPoints];
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            NSMutableArray *pendingFriends = [NSMutableArray new];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_AWAITING_APPROVAL) {
                        NSNumber *kind = [friend objectForKey:@"kind"];
                        if ([kind intValue] == KIND_INVITED_FRIENDS) {
                            [pendingFriends addObject:friend];
                        }
                    }
                }
                
                NSNumber *numPending = [NSNumber numberWithInteger:pendingFriends.count];
                [g_MemberInfo setObject:numPending forKey:INFO_KEY_PENDING_FRIENDS];
                [self updateFriends:PARENT_PAGE_DEFAULT];
                
            } else {
                // Failure
                /*
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_MESSAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ messages", datacnt);
                
                NSArray *messages = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (messages) {
                    int unreadMessages = 0;
                    for (NSDictionary *message in messages) {
                        NSString *readTime = [message objectForKey:@"read_time"];
                        if ([readTime isEqualToString:@""]) {
                            unreadMessages++;
                        }
                    }
                    
                    [g_MemberInfo setObject:[NSNumber numberWithInt:unreadMessages] forKey:INFO_KEY_MSGS_UNREAD];
                    [self updateMessages:PARENT_PAGE_DEFAULT];
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
