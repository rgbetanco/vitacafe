//
//  CartItem.m
//  QBurger
//
//  Created by Kevin Phua on 04/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "CartItem.h"

@implementation CartItem

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]){
        self.product = [aDecoder decodeObjectForKey:@"product"];
        self.webOrder01s = [aDecoder decodeObjectForKey:@"webOrder01s"];
        //self.webOrder011s = [aDecoder decodeObjectForKey:@"webOrder011s"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.product forKey:@"product"];
    [aCoder encodeObject:self.webOrder01s forKey:@"webOrder01s"];
    //[aCoder encodeObject:self.webOrder011s forKey:@"webOrder011s"];
}

@end
