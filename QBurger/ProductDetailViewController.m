//
//  ProductDetailViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/1/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "Global.h"
#import "UIImageView+WebCache.h"
#import "DQAlertView.h"
#import "WebService.h"
#import "Taste.h"
#import "TasteDetail.h"
#import "CartItem.h"
#import "UIView+Toast.h"
#import "Taste.h"
#import "TasteDetail.h"
#import "Combo.h"
#import "Pack.h"
#import "ItemViewCell.h"
#import "ProductTasteViewController.h"
#import "ProductComboViewController.h"
#import "WebOrder011.h"
#import "ProductDetailViewCell.h"

@interface ProductDetailViewController () <WebServiceDelegate, UITableViewDelegate, UITableViewDelegate, ItemViewCellDelegate, ProductTasteDelegate, ProductComboDelegate, ProductDetailDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) int quantity;

@property (nonatomic, strong) UIButton *btnShare;

@property (strong, nonatomic) NSMutableArray *tastes;
@property (strong, nonatomic) NSMutableArray *combos;
@property (strong, nonatomic) NSMutableArray *packs;

@property (strong, nonatomic) NSMutableArray *selectedTastes;
@property (strong, nonatomic) NSMutableArray *selectedCombos;

@property (nonatomic) int getTasteCount;    // No. of get taste web requests sent

@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.quantity = 0;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.getTasteCount = 0;

    _tastes = [NSMutableArray new];
    _combos = [NSMutableArray new];
    _packs = [NSMutableArray new];
    _selectedTastes = [NSMutableArray new];
    _selectedCombos = [NSMutableArray new];
    
    if (self.product) {
        if (_product.isComb) {
            // 組合餐(combination) 流程  get_comb + get_taste
            [self getComb];
        } else if (_product.isPack) {
            // 系列組合流程 (4種計算方式)  get_pack + get_taste
            [self getPack];
        } else if (!_product.isPack && !_product.isComb) {
            // 單一商品訂購，口味加值流程 get_taste 單一商品價格計算方式
            [self getTaste:_product.prodId];
        }
    }
    
    // Auto set quantity to 1
    _quantity = 1;
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateShare];
    [self updateFavorites:PARENT_PAGE_DEFAULT];
    
    if (_product.prodName1 && _product.prodName1.length>0) {
        self.title = _product.prodName1;
    } else {
        self.title = _product.prodName2;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_btnShare removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)importCartItems {
    if (self.cartItem) {
        if (_product.isComb) {
            // Import quantity, tastes from cart item
            for (int i=0; i<_combos.count; i++) {
                Combo *combo = [_combos objectAtIndex:i];
                WebOrder01 *webOrder = [_cartItem.webOrder01s objectAtIndex:i+1];   // Skip parent item
                
                NSLog(@"WebOrder01 = %@", [webOrder description]);
                
                if (webOrder.combType == 2) {
                    // Child item
                    _quantity = webOrder.qty;
                    
                    if (!webOrder.isDefault) {
                        // Not default item, add to selected combos
                        Combo *selectedCombo = [combo copy];
                        Combo *selectedComboDetail;
                        for (Combo *comboDetails in selectedCombo.combDetails) {
                            if ([comboDetails.prodId isEqualToString:webOrder.prodId]) {
                                selectedComboDetail = [comboDetails copy];
                                break;
                            }
                        }
                        if (selectedComboDetail) {
                            selectedCombo.combDetails = [NSArray arrayWithObject:selectedComboDetail];
                        }
                        [_selectedCombos addObject:selectedCombo];
                    }
                }
                
                // Look for selected tastes
                for (WebOrder011 *webDetail in webOrder.webOrder011s) {
                    NSLog(@"WebOrder011 = %@", [webDetail description]);
                    
                    for (Taste *taste in _tastes) {
                        if ([taste.prodId isEqualToString:combo.prodId] &&
                            webDetail.worderSno == webOrder.worderSno) {
                            // Add to selected tastes
                            Taste *newSelectedTaste = [taste copy];
                            BOOL selectedTasteExists = NO;
                            
                            for (Taste *selectedTaste in _selectedTastes) {
                                if ([selectedTaste.tasteId isEqualToString:newSelectedTaste.tasteId] &&
                                    [selectedTaste.prodId isEqualToString:combo.prodId]) {
                                    selectedTasteExists = YES;
                                    newSelectedTaste = selectedTaste;
                                }
                            }
                            
                            TasteDetail *selectedTasteDetail = [TasteDetail new];
                            selectedTasteDetail.tasteSno = [NSString stringWithFormat:@"%d", webDetail.tasteSno];
                            selectedTasteDetail.name = webDetail.name;
                            selectedTasteDetail.price = webDetail.price;
                            
                            if (!selectedTasteExists) {
                                newSelectedTaste.tasteDetails = [NSArray arrayWithObject:selectedTasteDetail];
                                [_selectedTastes addObject:newSelectedTaste];
                            } else {
                                // Update select taste
                                NSMutableArray *tasteDetails = [NSMutableArray arrayWithArray:newSelectedTaste.tasteDetails];
                                [tasteDetails addObject:selectedTasteDetail];
                                newSelectedTaste.tasteDetails = tasteDetails;
                            }
                            break;
                        }
                    }
                }
            }
        } else {
            // Import quantity and tastes
            WebOrder01 *webOrder = [_cartItem.webOrder01s firstObject];
            _quantity = webOrder.qty;
            
            for (WebOrder011 *webDetail in webOrder.webOrder011s) {
                for (Taste *taste in _tastes) {
                    BOOL selectedTasteExists = NO;
                    Taste *newSelectedTaste = [taste copy];
                    for (Taste *selectedTaste in _selectedTastes) {
                        if ([selectedTaste.tasteId isEqualToString:taste.tasteId]) {
                            selectedTasteExists = YES;
                            newSelectedTaste = selectedTaste;
                            break;
                        }
                    }
                    
                    for (TasteDetail *tasteDetail in taste.tasteDetails) {
                        if ([tasteDetail.name isEqualToString:webDetail.name]) {
                            TasteDetail *selectedTasteDetail = [TasteDetail new];
                            selectedTasteDetail.tasteSno = [NSString stringWithFormat:@"%d", webDetail.tasteSno];
                            selectedTasteDetail.name = webDetail.name;
                            selectedTasteDetail.price = webDetail.price;
                            
                            if (selectedTasteExists) {
                                // Update select taste
                                NSMutableArray *tasteDetails = [NSMutableArray arrayWithArray:newSelectedTaste.tasteDetails];
                                
                                [tasteDetails addObject:selectedTasteDetail];
                                newSelectedTaste.tasteDetails = tasteDetails;
                            } else {
                                
                                newSelectedTaste.tasteDetails = [NSArray arrayWithObject:selectedTasteDetail];
                                [_selectedTastes addObject:newSelectedTaste];
                            }
                            break;
                        }
                    }
                }
            }
        }
        
        [self.tableView reloadData];
    }
}

- (void)getTaste:(NSString *)prodId {
    _getTasteCount++;
    
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getTaste:[Global cfgAppId] prodId:prodId];
}

- (void)getComb {
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getComb:[Global cfgAppId] prodId:_product.prodId];
}

- (void)getPack {
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getPack:[Global cfgAppId] prodId:_product.prodId];
}

- (void)updateShare {
    int navBarWidth = self.navigationController.navigationBar.frame.size.width;
    
    // Share button
    if (!_btnShare) {
        _btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnShare setImage:[UIImage imageNamed:@"ic_menu_share"] forState:UIControlStateNormal];
        [_btnShare addTarget:self action:@selector(onBtnShare:) forControlEvents:UIControlEventTouchUpInside];
        _btnShare.frame = CGRectMake(navBarWidth-81, 6, 32, 32);
        [_btnShare.titleLabel setHidden:YES];
    }
    [self.navigationController.navigationBar addSubview:_btnShare];
}
    
- (NSString *)buildTasteMemo:(Combo *)combo prodId:(NSString *)prodId {
    NSMutableString *tasteMemo = [NSMutableString new];
    for (Taste *taste in _tastes) {
        if ([taste.prodId isEqualToString:combo.prodId]) {
            for (Taste *selectedTaste in _selectedTastes) {
                if ([selectedTaste.prodId isEqualToString:prodId] &&
                    [taste.tasteId isEqualToString:selectedTaste.tasteId]) {
                    if (selectedTaste.tasteDetails && selectedTaste.tasteDetails.count>0) {
                        // Retrieve selected taste detail bitmap
                        for (TasteDetail *tasteDetail in selectedTaste.tasteDetails) {
                            if (tasteDetail.name && tasteDetail.name.length>0) {
                                if (tasteMemo.length > 0) {
                                    [tasteMemo appendString:@", "];
                                }
                                if (tasteDetail.price > 0) {
                                    [tasteMemo appendFormat:@"%@+%d", tasteDetail.name, tasteDetail.price];
                                } else {
                                    [tasteMemo appendFormat:@"%@", tasteDetail.name];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    NSLog(@"Taste memo = %@", tasteMemo);
    return tasteMemo;
}

- (NSString *)buildTasteMemoSingleItem {
    NSMutableString *tasteMemo = [NSMutableString new];
    for (Taste *taste in _tastes) {
        if ([taste.prodId isEqualToString:_product.prodId]) {
            for (Taste *selectedTaste in _selectedTastes) {
                if ([selectedTaste.prodId isEqualToString:_product.prodId] &&
                    [taste.tasteId isEqualToString:selectedTaste.tasteId]) {
                    if (selectedTaste.tasteDetails && selectedTaste.tasteDetails.count>0) {
                        // Retrieve selected taste detail bitmap
                        for (TasteDetail *tasteDetail in selectedTaste.tasteDetails) {
                            if (tasteDetail.name && tasteDetail.name.length>0) {
                                if (tasteMemo.length > 0) {
                                    [tasteMemo appendString:@", "];
                                }
                                if (tasteDetail.price > 0) {
                                    [tasteMemo appendFormat:@"%@+%d", tasteDetail.name, tasteDetail.price];
                                } else {
                                    [tasteMemo appendFormat:@"%@", tasteDetail.name];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    NSLog(@"%@", tasteMemo);
    return tasteMemo;
}

- (void)onBtnShare:(id)sender {
    NSString *appUrl = [Global cfgAppUrl];
    NSString *text = [NSString stringWithFormat:NSLocalizedString(@"ShareMsg", nil), _product.prodName1, appUrl];
    
    NSLog(@"Share text = %@", text);
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[text]
                                                                             applicationActivities:nil];
    
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo];
    
    [self presentViewController:controller animated:YES completion:nil];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else {
        if (_product.isComb) {
            return [_combos count];
        } else {
            return 1;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 400;
    }
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"ProductDetailViewCell";
        ProductDetailViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            [tableView registerNib:[UINib nibWithNibName:@"ProductDetailViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
        cell.delegate = self;
        
        if (self.cartItem) {
            [cell.btnAddToCart setTitle:NSLocalizedString(@"ModifyShoppingCart", nil) forState:UIControlStateNormal];
        }
        
        if (!self.isOrder) {
            [cell.btnAddToCart setHidden:YES];
            [cell.btnAdd setHidden:YES];
            [cell.btnSubtract setHidden:YES];
            [cell.lblQty setHidden:YES];
            [cell.lblQtyTitle setHidden:YES];
        }
        
        if ([Global isFavorite:self.product]) {
            [cell.btnAddFavorites setTitle:NSLocalizedString(@"RemoveMyFavorites", nil) forState:UIControlStateNormal];
        } else {
            [cell.btnAddFavorites setTitle:NSLocalizedString(@"AddMyFavorites", nil) forState:UIControlStateNormal];
        }

        cell.lblQty.text = [NSString stringWithFormat:@"%d", _quantity];
        cell.lblProductName.text = self.product.prodName1;
        cell.lblOriginalPrice.text = [NSString stringWithFormat:@"%@: $%@", NSLocalizedString(@"OriginalPrice", nil), self.product.price1];
        if (![self.product.price1 isEqualToString:self.product.price2]) {
            cell.lblDiscountPrice.text = [NSString stringWithFormat:@"%@: $%@", NSLocalizedString(@"DiscountPrice", nil), self.product.price2];
        }
        
        NSString *content = self.product.prodContent;
        if ([content isKindOfClass:[NSNull class]]) {
            cell.tvProductDesc.text = @"";
            [cell.tvProductDesc sizeToFit];
        } else {
            NSError *error;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:&error];
            NSRange range = NSMakeRange(0, attributedString.length);
            UIFont *font = [UIFont systemFontOfSize:16.0f];
            [attributedString addAttribute:NSFontAttributeName value:font range:range];
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
            cell.tvProductDesc.attributedText = attributedString;
            [cell.tvProductDesc sizeToFit];
            [cell.tvProductDesc setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
        }
        
        NSString *imageUrl = self.product.imgFile1;
        if ([imageUrl isKindOfClass:[NSNull class]]) {
            //No image
        } else {
            NSString *imagePath = imageUrl;
            imagePath = [imagePath stringByReplacingOccurrencesOfString:@"_s" withString:@""];
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[NSURL URLWithString:imagePath]
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                     NSLog(@"Received image %ld of %ld bytes", receivedSize, expectedSize);
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    cell.imgProduct.image = image;
                                }];
        }
        
        return cell;
        
    } else {
        static NSString *CellIdentifier = @"ItemViewCell";
        ItemViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            [tableView registerNib:[UINib nibWithNibName:@"ItemViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
        cell.delegate = self;
        
        if (_product.isComb) {
            // Combo item
            Combo *combo = [_combos objectAtIndex:[indexPath row]];
            cell.lblName.text = combo.prodName;
            
            if (_selectedCombos && combo.combDetails) {
                int comboFound = NO;
                for (int i=0; i<_selectedCombos.count; i++) {
                    Combo *selectedCombo = [_selectedCombos objectAtIndex:i];
                    if ([selectedCombo.prodId containsString:combo.prodId]) {
                        Combo *newCombo = selectedCombo.combDetails.firstObject;
                        if (newCombo) {
                            if (newCombo.price > 0) {
                                cell.lblName.text = [NSString stringWithFormat:@"%@+%d", newCombo.prodName, newCombo.price];
                            } else {
                                cell.lblName.text = newCombo.prodName;
                            }
                            comboFound = YES;
                        }
                        break;
                    }
                }
                if (!comboFound) {
                    cell.lblName.text = combo.prodName;
                }
            }
            
            NSString *imageUrl = combo.imgFile;
            if (![imageUrl isKindOfClass:[NSNull class]]) {
                NSString *imagePath = [NSString stringWithFormat:@"%@%@", PRODUCT_IMAGE_PATH, imageUrl];
                
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:[NSURL URLWithString:imagePath]
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code
                                         NSLog(@"Received image %ld of %ld bytes", receivedSize, expectedSize);
                                     }
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                        cell.imgProduct.image = image;
                                    }];
            }
            
            if (combo.combDetails && combo.combDetails.count > 0) {
                cell.btnChange.hidden = NO;
            } else {
                cell.btnChange.hidden = YES;
            }
            
            cell.btnTaste.hidden = YES;
            for (Taste *taste in _tastes) {
                if ([taste.prodId isEqualToString:combo.prodId]) {
                    cell.btnTaste.hidden = NO;
                    break;
                }
            }
            
            NSMutableString *detailText = [NSMutableString new];
            BOOL hasDetail = NO;
            
            for (Taste *taste in _tastes) {
                if ([taste.prodId isEqualToString:combo.prodId]) {
                    cell.btnTaste.hidden = NO;
            
                    // Display selected tastes
                    for (Taste *selectedTaste in _selectedTastes) {
                        if ([selectedTaste.prodId isEqualToString:combo.prodId] &&
                            [taste.tasteId isEqualToString:selectedTaste.tasteId]) {
                            if (selectedTaste.tasteDetails && selectedTaste.tasteDetails.count>0) {
                                // Retrieve selected taste detail bitmap
                                for (TasteDetail *tasteDetail in selectedTaste.tasteDetails) {
                                    if (tasteDetail.name && tasteDetail.name.length>0) {
                                        hasDetail = YES;
                                        if (detailText.length > 0) {
                                            [detailText appendString:@", "];
                                        }
                                        if (tasteDetail.price > 0) {
                                            [detailText appendFormat:@"%@+%d", tasteDetail.name, tasteDetail.price];
                                        } else {
                                            [detailText appendFormat:@"%@", tasteDetail.name];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    cell.lblTaste.text = detailText;
                }
            }
            
            if (!hasDetail) {
                cell.lblTaste.text = @"";
            }

            cell.lblTaste.numberOfLines = 0;
            [cell.lblTaste sizeToFit];
            cell.btnChange.layer.cornerRadius = 5;
            cell.btnTaste.layer.cornerRadius = 5;
            
            return cell;
        } else {
            // Single item
            cell.lblName.text = _product.prodName1;
            
            NSString *imageUrl = _product.imgFile1;
            if (![imageUrl isKindOfClass:[NSNull class]]) {
                NSString *imagePath = [NSString stringWithFormat:@"%@%@", PRODUCT_IMAGE_PATH, imageUrl];
                
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:[NSURL URLWithString:imagePath]
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code
                                         NSLog(@"Received image %ld of %ld bytes", receivedSize, expectedSize);
                                     }
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                        cell.imgProduct.image = image;
                                    }];
            }
            
            cell.btnTaste.hidden = YES;
            for (Taste *taste in _tastes) {
                if ([taste.prodId isEqualToString:_product.prodId]) {
                    cell.btnTaste.hidden = NO;
                    break;
                }
            }
            
            NSMutableString *detailText = [NSMutableString new];
            BOOL hasDetail = NO;
            
            for (Taste *taste in _tastes) {
                if ([taste.prodId isEqualToString:_product.prodId]) {
                    cell.btnTaste.hidden = NO;
                    
                    // Display selected tastes
                    for (Taste *selectedTaste in _selectedTastes) {
                        if ([selectedTaste.prodId isEqualToString:_product.prodId] &&
                            [taste.tasteId isEqualToString:selectedTaste.tasteId]) {
                            if (selectedTaste.tasteDetails && selectedTaste.tasteDetails.count>0) {
                                // Retrieve selected taste detail bitmap
                                for (TasteDetail *tasteDetail in selectedTaste.tasteDetails) {
                                    if (tasteDetail.name && tasteDetail.name.length>0) {
                                        hasDetail = YES;
                                        if (detailText.length > 0) {
                                            [detailText appendString:@", "];
                                        }
                                        if (tasteDetail.price > 0) {
                                            [detailText appendFormat:@"%@+%d", tasteDetail.name, tasteDetail.price];
                                        } else {
                                            [detailText appendFormat:@"%@", tasteDetail.name];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    cell.lblTaste.text = detailText;
                }
            }
            
            if (!hasDetail) {
                cell.lblTaste.text = @"";
            }
            
            cell.lblTaste.numberOfLines = 0;
            [cell.lblTaste sizeToFit];
            cell.btnChange.layer.cornerRadius = 5;
            cell.btnTaste.layer.cornerRadius = 5;
            
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}


//==================================================================
#pragma ProductDetailDelegate
//==================================================================

- (void)onBtnAddToShoppingCart:(id)sender {
    if (_quantity == 0) {
        [self.view makeToast:NSLocalizedString(@"QuantityNotSet", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    }
    
    CartItem *item = [CartItem new];
    item.product = _product;
    
    if (!_product.isComb) {
        // Single item
        WebOrder01 *order = [WebOrder01 new];
        order.prodId = _product.prodId;
        order.prodName = _product.prodName1;
        order.salePrice = [_product.price1 intValue];
        order.worderSno = 1;
        order.qty = _quantity;
        order.combQty = 0;
        order.combType = 0;     // 非組合商品
        order.tasteMemo = [self buildTasteMemoSingleItem];
        
        // Add tastes
        NSMutableArray *tasteItems = [NSMutableArray new];
        
        if (_selectedTastes.count>0) {
            for (Taste *taste in _tastes) {
                if ([taste.prodId isEqualToString:_product.prodId]) {
                    for (Taste *selectedTaste in _selectedTastes) {
                        if ([selectedTaste.prodId isEqualToString:_product.prodId] &&
                            [taste.tasteId isEqualToString:selectedTaste.tasteId]) {
                            if (selectedTaste.tasteDetails && selectedTaste.tasteDetails.count>0) {
                                // Retrieve selected taste detail bitmap
                                int tasteId = 1;
                                for (TasteDetail *tasteDetail in selectedTaste.tasteDetails) {
                                    WebOrder011 *webOrder011 = [WebOrder011 new];
                                    webOrder011.worderSno = 1;
                                    webOrder011.tasteId = tasteId++;
                                    webOrder011.tasteSno = [tasteDetail.tasteSno intValue];
                                    webOrder011.name = tasteDetail.name;
                                    webOrder011.price = tasteDetail.price;
                                    [tasteItems addObject:webOrder011];
                                }
                            }
                        }
                    }
                }
            }
        }
        
        order.webOrder011s = tasteItems;
        item.webOrder01s = [NSArray arrayWithObject:order];
        
    } else {
        // Combination item
        NSMutableArray *orderItems = [NSMutableArray new];
        int parentSno = 1;
        int worderSno = 1;
        
        // Combo parent item
        WebOrder01 *parentItem = [WebOrder01 new];
        parentItem.prodId = _product.prodId;
        parentItem.prodName = _product.prodName1;
        parentItem.salePrice = [_product.price1 intValue];
        parentItem.worderSno = worderSno++;
        parentItem.qty = _quantity;
        parentItem.combType = 1;    // 套餐父項
        parentItem.combQty = 0;
        parentItem.itemDisc = 0;
        parentItem.tasteMemo = @"";
        [orderItems addObject:parentItem];
        
        int childSno = 1;
        for (Combo *combo in _combos) {
            NSMutableArray *tasteItems = [NSMutableArray new];
            
            // Combo child item
            WebOrder01 *childItem = [WebOrder01 new];
            
            // Check if there is selected combo
            Combo *newCombo = nil;
            if (combo.combDetails) {
                for (int i=0; i<_selectedCombos.count; i++) {
                    Combo *selectedCombo = [_selectedCombos objectAtIndex:i];
                    if ([selectedCombo.prodId containsString:combo.prodId]) {
                        newCombo = selectedCombo.combDetails.firstObject;
                        break;
                    }
                }
            }
            
            if (newCombo) {
                // Use new combo item
                childItem.prodId = newCombo.prodId;
                childItem.prodName = newCombo.prodName;
                childItem.salePrice = newCombo.price;
                childItem.worderSno = worderSno;
                childItem.qty = _quantity;
                childItem.combType = 2;     // 套餐子項
                childItem.combQty = newCombo.quantity;
                childItem.combSaleSno = parentSno;
                childItem.combSno = childSno;
                childItem.itemDisc = 0;
                childItem.isDefault = NO;
                childItem.tasteMemo = [self buildTasteMemo:newCombo prodId:combo.prodId];
            } else {
                // Use default combo item
                childItem.prodId = combo.prodId;
                childItem.prodName = combo.prodName;
                childItem.salePrice = combo.price;
                childItem.worderSno = worderSno;
                childItem.qty = _quantity;
                childItem.combType = 2;     // 套餐子項
                childItem.combQty = combo.quantity;
                childItem.combSaleSno = parentSno;
                childItem.combSno = childSno;
                childItem.itemDisc = 0;
                childItem.isDefault = YES;
                childItem.tasteMemo = [self buildTasteMemo:combo prodId:combo.prodId];
            }
            
            // Add tastes
            for (Taste *taste in _tastes) {
                if ([taste.prodId isEqualToString:combo.prodId]) {
                    for (Taste *selectedTaste in _selectedTastes) {
                        if ([selectedTaste.prodId isEqualToString:childItem.prodId] &&
                            [taste.tasteId isEqualToString:selectedTaste.tasteId]) {
                            if (selectedTaste.tasteDetails && selectedTaste.tasteDetails.count>0) {
                                // Retrieve selected taste detail bitmap
                                int tasteId = 1;
                                for (TasteDetail *tasteDetail in selectedTaste.tasteDetails) {
                                    WebOrder011 *webOrder011 = [WebOrder011 new];
                                    webOrder011.worderSno = worderSno;
                                    webOrder011.tasteId = tasteId++;
                                    webOrder011.tasteSno = [tasteDetail.tasteSno intValue];
                                    webOrder011.name = tasteDetail.name;
                                    webOrder011.price = tasteDetail.price;
                                    [tasteItems addObject:webOrder011];
                                }
                            }
                        }
                    }
                }
            }
            
            childItem.webOrder011s = tasteItems;
            [orderItems addObject:childItem];
            
            worderSno++;
        }
        
        item.webOrder01s = orderItems;
    }
    
    if (self.cartItem) {
        [Global modifyShoppingCart:item atIndex:self.cartItemIndex];
    } else {
        [Global addShoppingCart:item];
    }
    [Global cacheProduct:_product];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                  message:NSLocalizedString(@"ItemAddedToShoppingCart", nil)
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"Confirm", nil)
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action) {
                             [self.navigationController popViewControllerAnimated:YES];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)onBtnFavorite:(id)sender {
    if ([Global isFavorite:self.product]) {
        [Global removeFavorite:self.product];
    } else {
        [Global addFavorite:self.product];
    }
    [self.tableView reloadData];
}

- (void)onBtnAdd:(id)sender {
    _quantity++;
    [self.tableView reloadData];
}

- (void)onBtnSubtract:(id)sender {
    _quantity--;
    if (_quantity < 0) {
        _quantity = 0;
    }
    [self.tableView reloadData];
}

//==================================================================
#pragma ItemViewCellDelegate
//==================================================================

- (void)onBtnChange:(id)sender {
    ItemViewCell *cell = (ItemViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Combo *combo = [_combos objectAtIndex:[indexPath row]];
    
    ProductComboViewController *comboVC = [[ProductComboViewController alloc] initWithNibName:@"ProductComboViewController" bundle:nil];
    
    comboVC.combo = combo;
    comboVC.delegate = self;

    for (Combo *selectedCombo in _selectedCombos) {
        if ([selectedCombo.prodId containsString:combo.prodId]) {
            comboVC.selectedCombo = selectedCombo.combDetails.firstObject;
            break;
        }
    }
    
    comboVC.delegate = self;
    [self.navigationController pushViewController:comboVC animated:YES];
}

- (void)onBtnTaste:(id)sender {
    ItemViewCell *cell = (ItemViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    if (_product.isComb) {
        Combo *combo = [_combos objectAtIndex:[indexPath row]];
    
        ProductTasteViewController *tasteVC = [[ProductTasteViewController alloc] initWithNibName:@"ProductTasteViewController" bundle:nil];

        NSMutableArray *productTastes = [NSMutableArray new];
        for (Taste *taste in _tastes) {
            if ([taste.prodId isEqualToString:combo.prodId]) {
                [productTastes addObject:taste];
            }
        }
        
        tasteVC.tastes = productTastes;
        tasteVC.inputSelectedTastes = _selectedTastes;
        tasteVC.delegate = self;
        [self.navigationController pushViewController:tasteVC animated:YES];
    } else {
        ProductTasteViewController *tasteVC = [[ProductTasteViewController alloc] initWithNibName:@"ProductTasteViewController" bundle:nil];
        
        NSMutableArray *productTastes = [NSMutableArray new];
        for (Taste *taste in _tastes) {
            if ([taste.prodId isEqualToString:_product.prodId]) {
                [productTastes addObject:taste];
            }
        }
        
        tasteVC.tastes = productTastes;
        tasteVC.inputSelectedTastes = _selectedTastes;
        tasteVC.delegate = self;
        [self.navigationController pushViewController:tasteVC animated:YES];
    }
}

//==================================================================
#pragma ProductTasteDelegate
//==================================================================

- (void)onSelectedTaste:(NSArray *)tastes sender:(id)sender {
    if (_selectedTastes.count == 0) {
        [_selectedTastes addObjectsFromArray:tastes];
    } else {
        // Update selected tastes
        for (Taste *taste in tastes) {
            BOOL tasteExists = NO;
            for (Taste *selectedTaste in _selectedTastes) {
                if ([taste.prodId isEqualToString:selectedTaste.prodId] &&
                    [taste.tasteId isEqualToString:selectedTaste.tasteId]) {
                    tasteExists = YES;
                    selectedTaste.tasteDetails = [NSArray arrayWithArray:taste.tasteDetails];
                }
            }
            
            if (!tasteExists) {
                [_selectedTastes addObject:taste];
            }
        }
    }
    
    [self.tableView reloadData];
}

//==================================================================
#pragma ProductComboDelegate
//==================================================================

- (void)onSelectedCombo:(Combo *)combo prodId:(NSString *)prodId sender:(id)sender {
    if (combo) {
        if (_selectedCombos.count == 0) {
            [_selectedCombos addObject:combo];
        } else {
            // Update selected combos
            BOOL comboFound = NO;
            for (int i=0; i<_selectedCombos.count; i++) {
                Combo *selectedCombo = [_selectedCombos objectAtIndex:i];
                if ([prodId isEqualToString:selectedCombo.prodId]) {
                    [_selectedCombos setObject:combo atIndexedSubscript:i];
                    comboFound = YES;
                    break;
                }
            }
            
            if (!comboFound) {
                [_selectedCombos addObject:combo];
            }
        }
    } else {
        // Revert to default item
        if (_selectedCombos.count > 0) {
            // Update selected combos
            for (int i=0; i<_selectedCombos.count; i++) {
                Combo *selectedCombo = [_selectedCombos objectAtIndex:i];
                if ([prodId isEqualToString:selectedCombo.prodId]) {
                    [_selectedCombos removeObjectAtIndex:i];
                    break;
                }
            }
        }
    }
    
    [self.tableView reloadData];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_TASTE] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                //NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Get taste message = %@", message);
                
                NSArray *tastes = [jsonObject objectForKey:@"taste"];
                for (NSDictionary *taste in tastes) {
                    Taste *tasteItem = [Taste new];
                    tasteItem.tasteId = [taste objectForKey:@"taste_id"];
                    tasteItem.prodId = [taste objectForKey:@"prod_id"];
                    tasteItem.tasteName = [taste objectForKey:@"taste_name"];
                    tasteItem.mutex = [[taste objectForKey:@"mutex"] boolValue];
                    tasteItem.tasteKind = [[taste objectForKey:@"kind"] boolValue];
                    
                    NSMutableArray *detailsItem = [NSMutableArray new];
                    
                    NSArray *details = [taste objectForKey:@"detail"];
                    for (NSDictionary *detail in details) {
                        TasteDetail *tasteDetail = [TasteDetail new];
                        tasteDetail.tasteSno = [detail objectForKey:@"taste_sno"];
                        tasteDetail.name = [detail objectForKey:@"name"];
                        tasteDetail.price = [[detail objectForKey:@"price"] intValue];
                        [detailsItem addObject:tasteDetail];
                    }
                    
                    tasteItem.tasteDetails = detailsItem;
                    [_tastes addObject:tasteItem];
                }
                
                [self.tableView reloadData];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
            
            // Import cart items after all combos and tastes retrieved
            _getTasteCount--;
            if (_getTasteCount <= 0) {
                [self importCartItems];
            }
            
            [self.tableView reloadData];
            
        } else if ([resultName compare:WS_GET_COMB] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                //NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Get comb message = %@", message);
                
                [_combos removeAllObjects];
                NSArray *combs = [jsonObject objectForKey:@"comb"];
                for (NSDictionary *comb in combs) {
                    Combo *combItem = [Combo new];
                    combItem.combSno = [comb objectForKey:@"comb_sno"];
                    combItem.prodId = [comb objectForKey:@"prod_id"];
                    combItem.prodName = [comb objectForKey:@"prod_name1"];
                    combItem.quantity = [[comb objectForKey:@"quantity"] intValue];
                    combItem.price = [[comb objectForKey:@"price"] intValue];
                    combItem.isDefault = [[comb objectForKey:@"isdefault"] boolValue];
                    combItem.imgFile = [comb objectForKey:@"imgfile1"];
                    
                    NSArray *details = [comb objectForKey:@"detail"];
                    NSMutableArray *detailsItem = [NSMutableArray new];
                    for (NSDictionary *detail in details) {
                        Combo *combDetailItem = [Combo new];
                        combDetailItem.combSno = [detail objectForKey:@"comb_sno"];
                        combDetailItem.prodId = [detail objectForKey:@"prod_id"];
                        combDetailItem.prodName = [detail objectForKey:@"prod_name1"];
                        combDetailItem.quantity = [[detail objectForKey:@"quantity"] intValue];
                        combDetailItem.price = [[detail objectForKey:@"price"] intValue];
                        [detailsItem addObject:combDetailItem];
                    }
                    
                    combItem.combDetails = detailsItem;
                    [_combos addObject:combItem];
                    
                    [self getTaste:combItem.prodId];
                }
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_GET_PACK] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                //NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Get pack message = %@", message);
                
                [_packs removeAllObjects];
                NSArray *packs = [jsonObject objectForKey:@"pack"];
                for (NSDictionary *pack in packs) {
                    Pack *packItem = [Pack new];
                    packItem.packId = [pack objectForKey:@"pack_id"];
                    packItem.psuitId = [pack objectForKey:@"psuit_id"];
                    packItem.psuitName = [pack objectForKey:@"psuit_name"];
                    packItem.packPrcType = [[pack objectForKey:@"packprc_type"] intValue];
                    packItem.packQty = [[pack objectForKey:@"pack_qty"] intValue];
                    packItem.price = [[pack objectForKey:@"price"] intValue];
                    NSMutableArray *detailsItem = [NSMutableArray new];
                    
                    NSArray *details = [pack objectForKey:@"detail"];
                    for (NSDictionary *detail in details) {
                        Pack *packItem = [Pack new];
                        packItem.packId = [detail objectForKey:@"pack_id"];
                        packItem.psuitId = [detail objectForKey:@"psuit_id"];
                        packItem.psuitName = [detail objectForKey:@"psuit_name"];
                        packItem.packPrcType = [[detail objectForKey:@"packprc_type"] intValue];
                        packItem.packQty = [[detail objectForKey:@"pack_qty"] intValue];
                        packItem.price = [[detail objectForKey:@"price"] intValue];
                        packItem.plusPrice = [[detail objectForKey:@"plus_price"] intValue];
                        [detailsItem addObject:packItem];
                    }
                    
                    packItem.packDetails = detailsItem;
                    [_packs addObject:packItem];
                }
                
                [self getTaste:_product.prodId];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
