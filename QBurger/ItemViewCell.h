//
//  ItemViewCell.h
//  QBurger
//
//  Created by Kevin Phua on 10/28/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ItemViewCellDelegate <NSObject>

- (void)onBtnChange:(id)sender;
- (void)onBtnTaste:(id)sender;

@end

@interface ItemViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTaste;
@property (weak, nonatomic) IBOutlet UIButton *btnChange;
@property (weak, nonatomic) IBOutlet UIButton *btnTaste;
@property (assign, nonatomic) id<ItemViewCellDelegate> delegate;

@end
