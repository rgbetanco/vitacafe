//
//  Taste.h
//  QBurger
//
//  Created by Kevin Phua on 9/8/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Taste : NSObject <NSCopying>

@property (nonatomic, strong) NSString *prodId;
@property (nonatomic, strong) NSString *tasteId;
@property (nonatomic, strong) NSString *tasteName;
@property (nonatomic, strong) NSArray *tasteDetails;
@property (nonatomic) int tasteKind;
@property (nonatomic) BOOL mutex;

@end
