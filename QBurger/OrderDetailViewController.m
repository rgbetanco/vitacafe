//
//  OrderDetailViewController.m
//  QBurger
//
//  Created by Kevin Phua on 30/10/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "OrderMainViewController.h"
#import "WebService.h"
#import "Global.h"
#import "Product.h"
#import "WebOrder01.h"
#import "WebOrder011.h"

@interface OrderDetailViewController () <WebServiceDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblOrderId;
@property (weak, nonatomic) IBOutlet UILabel *lblRetriveDate;
@property (weak, nonatomic) IBOutlet UILabel *lblRetrieveType;
@property (weak, nonatomic) IBOutlet UITextView *tvOrderDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnStatus;
    
@property (strong, nonatomic) NSMutableArray *webOrder01s;

@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    _webOrder01s = [NSMutableArray new];
    
    UIBarButtonItem *btnTransfer = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"TransferToShoppingCart", nil) style:UIBarButtonItemStylePlain target:self action:@selector(onBtnTransferToShoppingCart:)];
    self.navigationItem.rightBarButtonItem = btnTransfer;
}

- (void)viewWillAppear:(BOOL)animated {
    _lblOrderId.text = [NSString stringWithFormat:@"訂單編號：%@", _worderId];
    _lblRetriveDate.text = [NSString stringWithFormat:@"取貨時間：%@", _mealDate];
    _lblRetrieveType.text = [NSString stringWithFormat:@"取貨方式：%@", _methodName];

    [_btnStatus setTitle:_status forState:UIControlStateNormal];
    
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getOrderInfo:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] worderId:self.worderId];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void)onBtnTransferToShoppingCart:(id)sender {
    [self transferToShoppingCart];
}
    
- (void)transferToShoppingCart {
    CartItem *item;
    int parentSno = -1;
    BOOL itemsStopSale = NO;
    NSMutableArray *webOrder01s;
    NSMutableArray *stopSaleItems = [NSMutableArray new];
    for (WebOrder01 *order in _webOrder01s) {
        switch (order.combType) {
            case 0:     // Ordinary item
                if (parentSno != -1) {
                    // Add previous item
                    item.webOrder01s = webOrder01s;
                    [Global addShoppingCart:item];
                }
                item = [CartItem new];
                item.product = [Global getCachedProduct:order.prodId];
                if (item.product) {
                    order.salePrice = [item.product.price1 intValue];
                } else {
                    NSLog(@"Nil product %@", order.prodId);
                }
                order.tasteMemo = [self buildTasteMemo:order.webOrder011s];
                item.webOrder01s = [NSArray arrayWithObject:[order copy]];
                
                // Check if item stop sale
                if (!order.enable || order.stopSale) {
                    itemsStopSale = YES;
                    [stopSaleItems addObject:item.product.prodName1];
                } else {
                    [Global addShoppingCart:item];
                }
                break;
            case 1:     // Parent combo item
                if (parentSno != order.worderSno) {
                    if (parentSno != -1) {
                        // Different parent, add previous item
                        item.webOrder01s = webOrder01s;
                        
                        if (!order.enable || order.stopSale) {
                            itemsStopSale = YES;
                            [stopSaleItems addObject:item.product.prodName1];
                        } else {
                            [Global addShoppingCart:item];
                        }
                    }
                    // Create new cart item
                    item = [CartItem new];
                    item.product = [Global getCachedProduct:order.prodId];
                    if (item.product) {
                        order.salePrice = [item.product.price1 intValue];
                    } else {
                        NSLog(@"Nil product %@", order.prodId);
                        itemsStopSale = YES;
                        [stopSaleItems addObject:order.prodName];
                    }
                    parentSno = order.worderSno;
                    webOrder01s = [NSMutableArray new];
                    [webOrder01s addObject:[order copy]];
                }
                break;
            case 2:     // Child combo item
                if (order.combSaleSno == parentSno) {
                    order.tasteMemo = [self buildTasteMemo:order.webOrder011s];
                    [webOrder01s addObject:[order copy]];
                }
                if (order == _webOrder01s.lastObject) {
                    item.webOrder01s = webOrder01s;
                    
                    // Check if item stop sale
                    if (!order.enable || order.stopSale) {
                        itemsStopSale = YES;
                        [stopSaleItems addObject:item.product.prodName1];
                    } else {
                        [Global addShoppingCart:item];
                    }
                }
                break;
        }
    }
    
    // Check if item stop sale
    if (itemsStopSale) {
        NSMutableString *stopSaleItemStr = [NSMutableString new];
        for (NSString *stopSaleItem in stopSaleItems) {
            [stopSaleItemStr appendFormat:@"%@ ", stopSaleItem];
        }
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"ItemStopSale", nil), stopSaleItemStr];
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Confirm", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                             }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:NSLocalizedString(@"ItemAddedToShoppingCart", nil)
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Confirm", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 // Jump to order main view
                                 OrderMainViewController *orderMainVC = [[OrderMainViewController alloc] initWithNibName:@"OrderMainViewController" bundle:nil];
                                 [self.navigationController pushViewController:orderMainVC animated:YES];
                             }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)setProductDescription {
    NSMutableString *orderDesc = [NSMutableString new];
    int quantity = 0;
    int salePrice = 0;
    int extraPrice = 0;
    BOOL isFirstLine = true;
    for (WebOrder01 *order in _webOrder01s) {
        quantity = order.qty;
        switch (order.combType) {
            case 0:     // Ordinary item
                if (isFirstLine) {
                    isFirstLine = false;
                } else {
                    [orderDesc appendString:@"\n"];
                }
                [orderDesc appendFormat:@"%@ x %d", order.prodName, order.qty];
                salePrice = order.salePrice;
                break;
            case 1:     // Parent combo item
                if (isFirstLine) {
                    isFirstLine = false;
                } else {
                    [orderDesc appendString:@"\n"];
                }
                [orderDesc appendFormat:@"%@ x %d", order.prodName, order.qty];
                salePrice = order.salePrice;
                break;
            case 2:     // Child combo item
                [orderDesc appendFormat:@"         - %@ x %d", order.prodName, order.qty];
                extraPrice += order.salePrice;
                break;
        }

        NSMutableString *tasteMemo = [NSMutableString new];
        for (WebOrder011 *tasteDetail in order.webOrder011s) {
            if (tasteDetail.price > 0) {
                extraPrice += tasteDetail.price;
            }
            if (tasteDetail.name && tasteDetail.name.length>0) {
                if (tasteMemo.length > 0) {
                    [tasteMemo appendString:@", "];
                }
                if (tasteDetail.price > 0) {
                    [tasteMemo appendFormat:@"%@+%d", tasteDetail.name, tasteDetail.price];
                } else {
                    [tasteMemo appendFormat:@"%@", tasteDetail.name];
                }
            }
        }
        if (tasteMemo.length > 0) {
            [orderDesc appendFormat:@" (%@)\n", tasteMemo];
        } else {
            [orderDesc appendString:@"\n"];
        }
    }
    
    [orderDesc appendString:@"\n\n"];
    [orderDesc appendFormat:@"消費金額：$%d", [_totalSales intValue]];
    
    _tvOrderDetail.text = orderDesc;
}

- (NSString *)buildTasteMemo:(NSArray *)webOrder011s {
    NSMutableString *tasteMemo = [NSMutableString new];
    for (WebOrder011 *webOrder011 in webOrder011s) {
        if (webOrder011.name && webOrder011.name.length>0) {
            if (tasteMemo.length > 0) {
                [tasteMemo appendString:@", "];
            }
            if (webOrder011.price > 0) {
                [tasteMemo appendFormat:@"%@+%d", webOrder011.name, webOrder011.price];
            } else {
                [tasteMemo appendFormat:@"%@", webOrder011.name];
            }
        }
    }
    
    NSLog(@"Taste memo = %@", tasteMemo);
    return tasteMemo;
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_ORDER_INFO] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                NSLog(@"Get order info success: %@", message);
                
                [_webOrder01s removeAllObjects];
                NSArray *weborders = [jsonObject objectForKey:@"weborder01"];
                for (NSDictionary *weborder in weborders) {
                    WebOrder01 *webOrder = [WebOrder01 new];
                    webOrder.worderId = [weborder objectForKey:@"worder_id"];
                    webOrder.prodId = [weborder objectForKey:@"prod_id"];
                    webOrder.prodName = [weborder objectForKey:@"prod_name1"];
                    
                    id worderSno = [weborder objectForKey:@"worder_sno"];
                    if (worderSno && ![worderSno isKindOfClass:[NSNull class]]) {
                        webOrder.worderSno = [worderSno intValue];
                    }
                    id qty = [weborder objectForKey:@"qty"];
                    if (qty && ![qty isKindOfClass:[NSNull class]]) {
                        webOrder.qty = [qty intValue];
                    }
                    id itemDisc = [weborder objectForKey:@"item_disc"];
                    if (itemDisc && ![itemDisc isKindOfClass:[NSNull class]]) {
                        webOrder.itemDisc = [itemDisc intValue];
                    }
                    id combSno = [weborder objectForKey:@"comb_sno"];
                    if (combSno && ![combSno isKindOfClass:[NSNull class]]) {
                        webOrder.combSno = [combSno intValue];
                    }
                    id combSaleSno = [weborder objectForKey:@"comb_sale_sno"];
                    if (combSaleSno && ![combSaleSno isKindOfClass:[NSNull class]]) {
                        webOrder.combSaleSno = [combSaleSno intValue];
                    }
                    id combType = [weborder objectForKey:@"comb_type"];
                    if (combType && ![combType isKindOfClass:[NSNull class]]) {
                        webOrder.combType = [combType intValue];
                    }
                    id combQty = [weborder objectForKey:@"comb_qty"];
                    if (combQty && ![combQty isKindOfClass:[NSNull class]]) {
                        webOrder.combQty = [combQty intValue];
                    }
                    id enable = [weborder objectForKey:@"enable"];
                    if (enable && ![enable isKindOfClass:[NSNull class]]) {
                        webOrder.enable = [enable boolValue];
                    }
                    id stopSale = [weborder objectForKey:@"stop_sale"];
                    if (stopSale && ![stopSale isKindOfClass:[NSNull class]]) {
                        webOrder.stopSale = [stopSale boolValue];
                    }
                    NSArray *webdetails = [weborder objectForKey:@"weborder011"];
                    NSMutableArray *webOrder011s = [NSMutableArray new];
                    for (NSDictionary *webdetail in webdetails) {
                        WebOrder011 *webDetail = [WebOrder011 new];
                        webDetail.worderSno = [[webdetail objectForKey:@"worder_sno"] intValue];
                        webDetail.tasteId = [[webdetail objectForKey:@"taste_id"] intValue];
                        webDetail.tasteSno = [[webdetail objectForKey:@"taste_sno"] intValue];
                        webDetail.name = [webdetail objectForKey:@"name"];
                        webDetail.price = [[webdetail objectForKey:@"price"] intValue];
                        [webOrder011s addObject:webDetail];
                    }

                    webOrder.webOrder011s = webOrder011s;
                    [_webOrder01s addObject:webOrder];
                }
                
                [self setProductDescription];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
