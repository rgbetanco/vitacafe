//
//  PromoDetailViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/5/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "PromoDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "WebService.h"
#import "Global.h"

#define HEADER_IMAGE_HEIGHT        180     // points

@interface PromoDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgPromo;
@property (weak, nonatomic) IBOutlet UILabel *lblPromoDate;
@property (weak, nonatomic) IBOutlet UITextView *tvPromoContent;

@end

@implementation PromoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.title = _promoName;
    self.imgPromo.contentMode = UIViewContentModeScaleAspectFill;
    self.imgPromo.clipsToBounds = YES;
    
    if (self.promo) {
        self.lblPromoDate.text = [_promo objectForKey:@"bdate"];
        
        NSString *htmlBody = [_promo objectForKey:@"htmlbody"];
        if ([htmlBody isKindOfClass:[NSNull class]]) {
            self.tvPromoContent.text = @"";
        } else {
            NSError *error;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[htmlBody dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:&error];
            NSRange range = NSMakeRange(0, attributedString.length);
            UIFont *font = [UIFont systemFontOfSize:16.0f];
            [attributedString addAttribute:NSFontAttributeName value:font range:range];
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
            self.tvPromoContent.attributedText = attributedString;
        }
        
        NSString *imageUrl = [_promo objectForKey:@"imgfile1"];
        if ([imageUrl isKindOfClass:[NSNull class]]) {
            //No image
        } else {
            NSString *imagePath = [NSString stringWithFormat:@"%@%@", NEWS_IMAGE_PATH, imageUrl];
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[NSURL URLWithString:imagePath]
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                     NSLog(@"Received image %ld of %ld bytes", (long)receivedSize, expectedSize);
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        NSLog(@"Received image width=%.1f, height=%.lf", image.size.width, image.size.height);
                                        
                                        UIImage *resizedImage = [self imageResize:image];
                                        
                                        NSLog(@"Resized image width=%.1f, height=%.lf", resizedImage.size.width, resizedImage.size.height);
                                        
                                        self.imgPromo.image = resizedImage;
                                    }
                                }];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)imageResize:(UIImage*)image
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat ratio = image.size.width/image.size.height;
    CGSize newSize;
    CGPoint offset = CGPointMake(0, 0);
    
    NSLog(@"Image width=%.1f, height=%.1f, ratio=%.1f", image.size.width, image.size.height, ratio);
    
    if (ratio <= 1.0) {
        // Width 100%
        CGFloat newWidth = screenWidth;
        CGFloat newHeight = newWidth / ratio;
        newSize = CGSizeMake(newWidth, newHeight);
        if (newHeight > HEADER_IMAGE_HEIGHT) {
            offset = CGPointMake(0, 48);
        }
    } else {
        CGFloat newHeight = HEADER_IMAGE_HEIGHT;
        CGFloat newWidth = ratio * newHeight;
        newSize = CGSizeMake(newWidth, newHeight);
    }
    
    NSLog(@"New image size w=%.1f, h=%.1f", newSize.width, newSize.height);
    
    CGRect resizeRect = CGRectMake(offset.x, offset.y, newSize.width, newSize.height);
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    } else {
        UIGraphicsBeginImageContext(newSize);
    }
    [image drawInRect:resizeRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
