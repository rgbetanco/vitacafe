//
//  FavoritesViewCell.m
//  QBurger
//
//  Created by Kevin Phua on 3/10/17.
//  Copyright © 2017 Lafresh. All rights reserved.
//

#import "FavoritesViewCell.h"

@implementation FavoritesViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self setNeedsDisplay]; // force drawRect:
}

- (IBAction)onBtnDelete:(id)sender {
    if (self.delegate) {
        [self.delegate onBtnDelete:sender];
    }
}

@end
