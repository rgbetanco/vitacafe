//
//  CAKeyframeAnimation+FunctionBased.h
//  tthsu
//
//  Created by Chin-Yu Hsu on 2/7/14.
//  Copyright (c) 2014 Chinsoft Co., Ltd. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

typedef CGFloat (^TimeInterpolator)(CGFloat);	// input is time between 0 and 1

@interface CAKeyframeAnimation (FunctionBased)
+ (id) animationWithKeyPath:(NSString *)path interpolator:(TimeInterpolator)fncBlock fromValue:(CGFloat)fromValue toValue:(CGFloat)toValue;
+ (id) animationWithKeyPath:(NSString *)path interpolator:(TimeInterpolator)fncBlock fromValue:(CGFloat)fromValue toValue:(CGFloat)toValue duration:(CFTimeInterval)seconds fps:(CGFloat)fps;
//  the more steps, the smoother the animation
+ (id) animationWithKeyPath:(NSString *)path interpolator:(TimeInterpolator)fncBlock fromValue:(CGFloat)fromValue toValue:(CGFloat)toValue steps:(NSUInteger)steps;

@end
