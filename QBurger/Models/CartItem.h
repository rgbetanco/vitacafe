//
//  CartItem.h
//  QBurger
//
//  Created by Kevin Phua on 04/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebOrder01.h"
#import "Product.h"

@interface CartItem : NSObject <NSCoding>

@property (nonatomic, strong) Product *product;
@property (nonatomic, strong) NSArray *webOrder01s;

@end
