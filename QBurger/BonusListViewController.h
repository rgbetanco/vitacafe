//
//  BonusListViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/25/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "WebService.h"

@interface BonusListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, WebServiceDelegate>

@end
