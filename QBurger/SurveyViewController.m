//
//  SurveyViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/6/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "SurveyViewController.h"
#import "Global.h"

@interface SurveyViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *websiteUrl;

@end

@implementation SurveyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.backgroundColor = [UIColor clearColor];
    
    // Do any additional setup after loading the view from its nib.
    [self.webView setOpaque:NO];
    [self.webView setScalesPageToFit:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getPageList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadWebpage {
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_websiteUrl]]];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_PAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *pages = (NSArray *)[jsonObject objectForKey:@"data"];
                if (pages) {
                    for (NSDictionary *page in pages) {
                        NSString *subject = [page objectForKey:@"subject"];
                        NSString *url = [page objectForKey:@"url"];
                        if ([subject isEqualToString:@"意見表"]) {
                            NSString *urlPath = [NSString stringWithFormat:@"%@%@", BASE_URL, url];
                            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlPath]]];
                            _websiteUrl = url;
                            return;
                        }
                    }
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.webView setHidden:YES];
    [self.viewNetworkError setHidden:NO];
}

//==================================================================
#pragma UIWebViewDelegate
//==================================================================
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.webView setHidden:NO];
    [self.viewNetworkError setHidden:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.webView setHidden:YES];
    [self.viewNetworkError setHidden:NO];
}

@end
