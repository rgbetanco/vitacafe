//
//  MessageViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/6/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "MessageViewController.h"
#import "Global.h"
#import "MessageListCell.h"
#import "RaiseAlertView.h"

@interface MessageViewController ()<MessageListCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) NSMutableArray *messagesRead;

@property (nonatomic, strong) NSMutableArray *tickets;

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"Messages", nil);
    
    self.messages = [NSMutableArray new];
    self.messagesRead = [NSMutableArray new];
    self.tickets = [NSMutableArray new];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated {
    // Get message list
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getMessageList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    [ws showWaitingView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (void)adjustHeightOfTableview
{
    CGFloat height = self.tableView.contentSize.height;
    CGFloat maxHeight = 0.85 * self.tableView.superview.frame.size.height;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the height constraint accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        self.tableViewHeightConstraint.constant = height;
        [self.view setNeedsUpdateConstraints];
    }];
}
 */

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed = [[self.messagesRead objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[self.messages count]; i++) {
            if (indexPath.section==i) {
                [self.messagesRead replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];

        // Set message as read
        NSDictionary *message = [self.messages objectAtIndex:indexPath.section];
        NSString *infoId = [message objectForKey:@"infoid"];
        NSString *readTime = [message objectForKey:@"read_time"];
        
        if ([readTime isEqualToString:@""]) {
            WebService *ws = [[WebService alloc] init];
            ws.delegate = self;
            [ws readMessage:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] infoId:infoId];
        }
    }
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.messages count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[_messagesRead objectAtIndex:section] boolValue]) {
        return 1;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[_messagesRead objectAtIndex:indexPath.section] boolValue]) {
        return 160;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *message = [self.messages objectAtIndex:section];
    NSString *date = [message objectForKey:@"sendtime"];
    NSString *dateString = @"";
    if (![date isKindOfClass:[NSNull class]] && [date isKindOfClass:[NSString class]]) {
        dateString = date;
    }
    NSString *subject = [message objectForKey:@"subject"];
    NSString *readTime = [message objectForKey:@"read_time"];
    
    UIView *sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
    sectionView.tag = section;
    sectionView.backgroundColor = [Global colorWithType:COLOR_BTN_CAT_LIST_BG];
    
    UILabel *lblDate = [[UILabel alloc]initWithFrame:CGRectMake(20, 5, self.tableView.frame.size.width-20, 25)];
    lblDate.backgroundColor = [UIColor clearColor];
    lblDate.textColor = [UIColor blackColor];
    lblDate.font = [UIFont systemFontOfSize:16];
    lblDate.text = dateString;
    [sectionView addSubview:lblDate];
    
    UILabel *lblSubject = [[UILabel alloc]initWithFrame:CGRectMake(20, 30, self.tableView.frame.size.width-20, 25)];
    lblSubject.backgroundColor = [UIColor clearColor];
    
    if (readTime && readTime.length > 0) {
        lblSubject.textColor = [UIColor blackColor];
    } else {
        lblSubject.textColor = [UIColor redColor];
    }
    lblSubject.font = [UIFont systemFontOfSize:16];
    lblSubject.text = subject;
    lblSubject.adjustsFontSizeToFitWidth = YES;
    [sectionView addSubview:lblSubject];
    
    BOOL collapsed = [[self.messagesRead objectAtIndex:section] boolValue];
    if (!collapsed) {
        /* Add a custom Separator with Section view */
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 60, self.tableView.frame.size.width, 1)];
        separatorLineView.backgroundColor = [UIColor blackColor];
        [sectionView addSubview:separatorLineView];
    }
    
    // Add UITapGestureRecognizer to SectionView
    UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    
    return  sectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessageListCell";
    MessageListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"MessageListCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    cell.delegate = self;
    
    NSDictionary *message = [self.messages objectAtIndex:indexPath.section];
        
    //NSString *subject = [message objectForKey:@"subject"];
    NSString *content = [message objectForKey:@"brief"];
    long recvTime = [[message objectForKey:@"receive_time"] longLongValue];
    NSString *tkNo = [message objectForKey:@"tkno"];

    cell.lblContent.text = content;
    
    // Check if ticket has been collected
    if (tkNo && tkNo.length > 0 && recvTime == 0) {
        cell.btnSave.hidden = NO;
    } else {
        cell.btnSave.hidden = NO;
        [cell.btnSave setTitle:NSLocalizedString(@"Collected", nil) forState:UIControlStateNormal];
        [cell.btnSave setEnabled:NO];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /* Close the section, once the data is selected */
    [_messagesRead replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);

        long code = [[jsonObject objectForKey:@"code"] longValue];

        if ([resultName compare:WS_GET_MESSAGE_LIST] == NSOrderedSame) {
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ messages", datacnt);
                
                NSArray *messages = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (messages) {
                    [self.messages setArray:messages];
                    [self.messagesRead removeAllObjects];
                    for (int i=0; i<messages.count; i++) {
                        NSNumber *messageRead = [NSNumber numberWithBool:NO];
                        [self.messagesRead addObject:messageRead];
                    }
                }
                [self.tableView reloadData];
                //[self adjustHeightOfTableview];

                // Get ticket list
                WebService *ws = [[WebService alloc] init];
                [ws getTicketList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
                [ws setDelegate:self];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitle:nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_READ_MESSAGE] == NSOrderedSame) {
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSLog(@"Read message success! %@", message);
                [self.tableView reloadData];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_RECEIVE_TICKET] == NSOrderedSame) {
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                if (!message) {
                    message = NSLocalizedString(@"CollectTicketSuccess", nil);
                }
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Collect", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_GET_TICKET_LIST] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ tickets", datacnt);
                
                NSArray *tickets = (NSArray *)[jsonObject objectForKey:@"ticket"];
                if (tickets) {
                    [self.tickets setArray:tickets];
                }
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

//==================================================================
#pragma mark - MessageListCellDelegate
//==================================================================

- (void)btnSave:(id)sender {
    MessageListCell *cell = (MessageListCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    if (indexPath) {
        NSDictionary *message = [self.messages objectAtIndex:indexPath.section];
        NSString *tkno = [message objectForKey:@"tkno"];
        
        if (tkno && tkno.length>0) {
            WebService *ws = [[WebService alloc] init];
            ws.delegate = self;
            [ws receiveTicket:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] tkno:tkno];
        }
        
        [cell.btnSave setTitle:NSLocalizedString(@"Collected", nil) forState:UIControlStateNormal];
        [cell.btnSave setEnabled:NO];
        
        [self.tableView reloadData];
    } else {
        NSLog(@"Invalid indexPath!!!");
    }
}

@end
