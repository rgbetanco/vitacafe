//
//  VipPoint.m
//  Raise
//
//  Created by Kevin Phua on 12/31/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "VipPoint.h"

@implementation VipPoint


- (id)initWithBonusPoint:(NSInteger)bonusPoint inputDate:(NSString *)inputDate pointType:(NSString *)pointType shopId:(NSInteger)shopId ticketId:(NSString *)ticketId giftVipId:(NSString *)giftVipId {
    self = [super init];
    if (self) {
        _bonusPoint = bonusPoint;
        _inputDate = inputDate;
        _pointType = pointType;
        _shopId = shopId;
        _ticketId = ticketId;
        _giftVipId = giftVipId;
    }
    return self;
}

@end
