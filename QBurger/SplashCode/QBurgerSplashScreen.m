//
//  QBurgerSplashScreen.m
//  QBSplash_IOS
//
//  Created by Chin-Yu Hsu on 7/25/16.
//  Copyright © 2016 Chinsoft Co., Ltd. All rights reserved.
//

#import "QBurgerSplashScreen.h"
#import "CAKeyframeAnimation+FunctionBased.h"


CGFloat bounce(CGFloat t) {
    return t * t * 8.0f;
}

CGFloat bounceInterpolator( CGFloat t ) {
    t *= 1.1226f;
    if (t < 0.3535f) return bounce(t);
    else if (t < 0.7408f) return bounce(t - 0.54719f) + 0.7f;
    else if (t < 0.9644f) return bounce(t - 0.8526f) + 0.9f;
    else return bounce(t - 1.0435f) + 0.95f;
}

// function used by the Android version
TimeInterpolator fncBounce = ^CGFloat(CGFloat t) {
    return bounceInterpolator(t);
};

@interface QBurgerSplashScreen()
@property (strong, nonatomic) IBOutlet UIImageView *vwShadow;
@property (strong, nonatomic) IBOutlet UIImageView *vwEgg;
@property (strong, nonatomic) IBOutlet UIImageView *vwTitle;
@property (strong, nonatomic) IBOutlet UIImageView *vwSubTitle;
@property (strong, nonatomic) IBOutlet UIImageView *vwLogo;
@property (strong, nonatomic) IBOutlet UIImageView *vwEggBottom;
@property (strong, nonatomic) IBOutlet UIImageView *vwEggTop;

@end

@implementation QBurgerSplashScreen {
    
}

+ (instancetype) showSplashScreen : (UIViewController *)parent {
    UIView *superview = (parent.navigationController)?parent.navigationController.view:parent.view;
    QBurgerSplashScreen *loader = [[QBurgerSplashScreen alloc] initWithFrame:superview.bounds];
    
    [superview addSubview:loader];
    
    return loader;
}

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame withNibNamed:@"QBurgerSplashScreen" duration:0];
    return self;
}

- (void)logoShow {
    
    CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        _vwTitle.hidden = NO;

        // show subtitle
        [CATransaction begin];

        CABasicAnimation *animFadeInTitle = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animFadeInTitle.duration = 2.f;
        animFadeInTitle.fromValue = [NSNumber numberWithFloat:0.0];
        animFadeInTitle.toValue = [NSNumber numberWithFloat:1.0];
        animFadeInTitle.fillMode = kCAFillModeBoth;
        animFadeInTitle.removedOnCompletion = NO;
        [_vwTitle.layer addAnimation:animFadeInTitle forKey:@"opacity"];
        
        [CATransaction commit];
        [self queueClose:5];
    
    }];

    CABasicAnimation *animLogoUp = [CABasicAnimation animationWithKeyPath:@"position.y" ];
    animLogoUp.duration = .750f;
    animLogoUp.fillMode = kCAFillModeBoth;
    animLogoUp.removedOnCompletion = NO;
    animLogoUp.fromValue = [NSNumber numberWithFloat:center.y];
    animLogoUp.toValue = [NSNumber numberWithFloat:center.y - _vwLogo.bounds.size.height * 1.1f];
    [_vwLogo.layer addAnimation:animLogoUp forKey:@"position_y"];
    
    [CATransaction commit];
}

- (void)eggOpen {
    _vwEgg.hidden = YES;
    _vwLogo.hidden = NO;
    _vwEggTop.hidden = NO;
    _vwEggBottom.hidden = NO;
    
    _vwEggTop.layer.anchorPoint = CGPointMake(1,1);
    _vwEggTop.layer.position = CGPointMake(_vwEggTop.frame.origin.x+_vwEggTop.bounds.size.width * 1.5, _vwEggTop.frame.origin.y+_vwEggTop.bounds.size.height * 1.5);
    
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        _vwEggTop.hidden = YES;
        _vwEggBottom.hidden = YES;
        _vwShadow.hidden = YES;
        [self logoShow];
    }];
    
    CABasicAnimation *animEggTopFade = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animEggTopFade.duration = .5f;
    animEggTopFade.fromValue = [NSNumber numberWithFloat:1.0];
    animEggTopFade.toValue = [NSNumber numberWithFloat:0.0];
    animEggTopFade.fillMode = kCAFillModeBoth;
    animEggTopFade.removedOnCompletion = NO;
    [_vwEggTop.layer addAnimation:animEggTopFade forKey:@"opacity"];
    [_vwEggBottom.layer addAnimation:animEggTopFade forKey:@"opacity"];
    [_vwShadow.layer addAnimation:animEggTopFade forKey:@"opacity"];

    CABasicAnimation *animEggTopRotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animEggTopRotate.toValue = @(M_PI); // The angle we are rotating to
    animEggTopRotate.duration = .5;
    
    [_vwEggTop.layer addAnimation:animEggTopRotate forKey:@"rotate"];
    
    [CATransaction commit];
}

- (void)eggCrack {
    NSArray *eggs = [NSArray arrayWithObjects:
                     (id)[UIImage imageNamed:@"splash_egg_1.png"].CGImage,
                     (id)[UIImage imageNamed:@"splash_egg_2.png"].CGImage,
                     (id)[UIImage imageNamed:@"splash_egg_3.png"].CGImage,
                     nil];

    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        [self eggOpen];
    }];
    
    CAKeyframeAnimation *animEggCrack = [CAKeyframeAnimation animationWithKeyPath:@"contents"];
    animEggCrack.calculationMode = kCAAnimationDiscrete;
    animEggCrack.beginTime = CACurrentMediaTime() + 0.500f;
    animEggCrack.duration = 1.2f;   // 400ms per frame
    animEggCrack.values = eggs;
    animEggCrack.repeatCount = 1;
    animEggCrack.fillMode = kCAFillModeForwards;
    animEggCrack.removedOnCompletion = NO;
    [_vwEgg.layer addAnimation:animEggCrack forKey:@"eggcrack"];
    [CATransaction commit];
}

- (void)initSplash {
    CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    
    [CATransaction begin]; {
        // show subtitle
        CABasicAnimation *animFadeInSubTitle = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animFadeInSubTitle.beginTime = CACurrentMediaTime() + 0.5f;
        animFadeInSubTitle.duration = 1.2f;
        animFadeInSubTitle.fromValue = [NSNumber numberWithFloat:0.0];
        animFadeInSubTitle.toValue = [NSNumber numberWithFloat:1.0];
        animFadeInSubTitle.fillMode = kCAFillModeBoth;
        animFadeInSubTitle.removedOnCompletion = NO;
        [_vwSubTitle.layer addAnimation:animFadeInSubTitle forKey:@"opacity"];

        [CATransaction begin]; {
            [CATransaction setCompletionBlock:^{
                [self eggCrack];
            }];
            
            // animate egg falling down
            CAAnimation *animEggSlideIn = [CAKeyframeAnimation animationWithKeyPath:@"position.y" interpolator:fncBounce fromValue:self.bounds.origin.y-_vwEgg.bounds.size.height toValue:center.y];
            animEggSlideIn.beginTime = CACurrentMediaTime() + 1.f;
            animEggSlideIn.duration = .500f;
            animEggSlideIn.fillMode = kCAFillModeBoth;
            animEggSlideIn.removedOnCompletion = NO;
            [_vwEgg.layer addAnimation:animEggSlideIn forKey:@"position_y"];
            
            // animate shadow sync to egg drop
            CAAnimation *animEggShadow = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale" interpolator:fncBounce fromValue:0 toValue:1];
            animEggShadow.beginTime = CACurrentMediaTime() + 1.f;
            animEggShadow.duration = .500f;
            animEggShadow.fillMode = kCAFillModeBoth;
            animEggShadow.removedOnCompletion = NO;
            [_vwShadow.layer addAnimation:animEggShadow forKey:@"scale"];
        } [CATransaction commit];
    
    } [CATransaction commit];
    
}


@end
