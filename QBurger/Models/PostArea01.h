//
//  PostArea01.h
//  QBurger
//
//  Created by Kevin Phua on 9/23/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostArea01 : NSObject

@property (nonatomic, strong) NSString *postAreaId;
@property (nonatomic, strong) NSString *postAreaSno;
@property (nonatomic, strong) NSString *townName;
@property (nonatomic, strong) NSString *positionId;

@end
