//
//  BaseViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/5/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic, strong) UIView *viewNetworkError;

- (void)updateNavigationBar;
- (void)clearNavigationBar;

- (void)updateFavorites:(int)parentPage;
- (void)updateFriends:(int)parentPage;
- (void)updateMessages:(int)parentPage;
- (void)updateCart:(int)parentPage;

- (void)updateVipPoints;
- (void)getFriendList;
- (void)getMessageList;
- (void)getPageList;

// Abstract method to reload webview
- (void)reloadWebpage;

@end
