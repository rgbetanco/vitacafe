//
//  Product.h
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *prodId;
@property (nonatomic, strong) NSString *prodName1;
@property (nonatomic, strong) NSString *prodName2;
@property (nonatomic, strong) NSString *prodContent;
@property (nonatomic, strong) NSArray *appCate;

@property (nonatomic, strong) NSString *depId;
@property (nonatomic, strong) NSString *imgFile1;
@property (nonatomic, strong) NSString *price1;
@property (nonatomic, strong) NSString *price2;
@property (nonatomic, strong) NSString *spec;
@property (nonatomic, strong) NSString *tax;

@property (nonatomic) BOOL enable;
@property (nonatomic) BOOL isRedeem;
@property (nonatomic) BOOL isPack;
@property (nonatomic) BOOL isComb;
@property (nonatomic) BOOL nonBonusPt;
@property (nonatomic) BOOL nonServiceCharge;
@property (nonatomic) BOOL stopSale;

@property (nonatomic) int redeemPoint;
@property (nonatomic) int sizeType;
@property (nonatomic) int unit;

@end
