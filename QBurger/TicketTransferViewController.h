//
//  TicketTransferViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/26/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface TicketTransferViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, WebServiceDelegate>

@property (nonatomic, strong) NSDictionary *ticket;

@end
