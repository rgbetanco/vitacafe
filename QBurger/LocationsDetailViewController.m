//
//  LocationsDetailViewController.m
//  QBurger
//
//  Created by Kevin Phua on 8/3/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "LocationsDetailViewController.h"

@interface LocationsDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblWorktime;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblFax;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UITextView *tvHtmlBody;

@end

@implementation LocationsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (!_shopInfo) {
        return;
    }
    
    if (_shopInfo.shopName && _shopInfo.shopName.length>0) {
        _lblName.text = _shopInfo.shopName;
    } else {
        _lblName.text = @"";
    }
    if (_shopInfo.workTime && _shopInfo.workTime.length>0) {
        _lblWorktime.text = _shopInfo.workTime;
    } else {
        _lblWorktime.text = @"";
    }
    if (_shopInfo.tel && _shopInfo.tel.length>0) {
        _lblPhone.text = _shopInfo.tel;
    } else {
        _lblPhone.text = @"";
    }
    if (_shopInfo.fax && _shopInfo.fax.length>0) {
        _lblFax.text = _shopInfo.fax;
    } else {
        _lblFax.text = @"";
    }
    if (_shopInfo.address && _shopInfo.address.length>0) {
        _lblAddress.text = _shopInfo.address;
    } else {
        _lblAddress.text = @"";
    }
    if (_shopInfo.htmlBody && _shopInfo.htmlBody.length>0) {
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData:[_shopInfo.htmlBody dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        _tvHtmlBody.attributedText = attributedString;
    } else {
        _tvHtmlBody.text = @"";
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
