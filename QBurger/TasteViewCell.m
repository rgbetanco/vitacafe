//
//  TasteViewCell.m
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "TasteViewCell.h"

@implementation TasteViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self setNeedsDisplay]; // force drawRect:
}

- (void)didTapCheckBox:(BEMCheckBox *)checkBox {
    if (self.delegate) {
        [self.delegate onTasteSelected:checkBox];
    }
}

@end
