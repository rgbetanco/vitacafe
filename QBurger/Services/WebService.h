//
//  WebService.h
//  FarGlory
//
//  Created by Kevin Phua on 9/4/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BASE_URL                    @"http://www.la-join.com.tw:2080/appserver/lajoin/webservice/"

#define MEMBER_SERVER_URL           @"http://www.la-join.com.tw:2080/appserver/lajoin/webservice/member/"
#define PRODUCT_SERVER_URL          @"http://www.la-join.com.tw:2080/appserver/lajoin/webservice/product/"
#define BASIC_SERVER_URL            @"http://www.la-join.com.tw:2080/appserver/lajoin/webservice/basic/"
#define TICKET_SERVER_URL           @"http://www.la-join.com.tw:2080/appserver/lajoin/webservice/ticket/"
#define ORDER_SERVER_URL            @"http://www.la-join.com.tw:2080/appserver/lajoin/webservice/order/"
#define PRODCATE_SERVER_URL         @"http://www.la-join.com.tw:2080/appserver/lajoin/public/prodcate00/"

#define PRODUCT_IMAGE_PATH          @"http://www.la-join.com.tw:2080/appserver/lajoin/public/product00/"
#define NEWS_IMAGE_PATH             @"http://www.la-join.com.tw:2080/appserver/lajoin/public/newsinfo/"
#define TICKET_IMAGE_PATH           @"http://www.la-join.com.tw:2080/appserver/lajoin/public/activity/"
#define CATE_IMAGE_PATH             @"http://www.la-join.com.tw:2080/appserver/lajoin/public/prodcate00/"

#define WS_LOGIN                    @"login"
#define WS_REGISTER                 @"register"
#define WS_AUTH                     @"auth"
#define WS_SET_PASSWORD             @"set_pass"
#define WS_CHANGE_PASSWORD          @"pass_change"
#define WS_REGISTER_DEVICE          @"register_device"
#define WS_SEARCH_MEMBER            @"SearchMember"
#define WS_REGISTER_NEW_MEMBER      @"RegisterNewMember"
#define WS_UPDATE_MEMBER            @"UpdateMember"

#define WS_GET_DEPARTMENT           @"get_department"
#define WS_GET_PRODCATE_00          @"get_prodcate00"
#define WS_GET_PRODUCT              @"get_product"
#define WS_GET_TASTE                @"get_taste"
#define WS_GET_COMB                 @"get_comb"
#define WS_GET_PACK                 @"get_pack"

#define WS_GET_FRIEND_LIST          @"get_friend_list"
#define WS_INVITE_FRIEND            @"invite"
#define WS_FRIEND_AGREE             @"friend_agree"
#define WS_FRIEND_DENY              @"friend_deny"
#define WS_FRIEND_DELETE            @"friend_delete"

#define WS_SEARCH_LAST_POINT_VALUE  @"SearchLastPointValue"
#define WS_VIP_POINT_MOVE_SHOP      @"VipPointMove_Shop"
#define WS_GET_TRANSACTION_LOGS     @"get_transaction_logs"
#define WS_GET_VIRTUAL_CARD         @"GetVirtualCard"

#define WS_GET_TICKET_LIST          @"get_ticket_list"
#define WS_TICKET_GIFT              @"ticket_gift"
#define WS_USE_TICKET               @"use_ticket"
#define WS_RECEIVE_TICKET           @"receive_ticket"
#define WS_UPDATE_TICKET_RULE       @"update_ticket_rule"
#define WS_UPDATE_TICKET            @"update_ticket"
#define WS_GET_VIP_BONUS_LIST       @"GetVipBonusList"

#define WS_GET_NEWS                 @"get_news"
#define WS_GET_PUBCODE              @"get_pubcode"
#define WS_GET_MESSAGE_LIST         @"get_message_list"
#define WS_READ_MESSAGE             @"read_message"
#define WS_GET_PAGE_LIST            @"get_page_list"
#define WS_GET_SHARE_MESSAGE        @"get_share_msg"
#define WS_GET_BANNER               @"get_banner"
#define WS_GET_SHOP                 @"get_shop"
#define WS_GET_POST_AREA            @"get_post_area"
#define WS_GET_POST_AREA00          @"get_post_area00"
#define WS_GET_POST_AREA01          @"get_post_area01"
#define WS_GET_POST_AREARD          @"get_post_areard"

#define WS_GET_SHOP00_CONFIG        @"get_shop00_config"
#define WS_GET_SALE_METHOD          @"get_sale_method"
#define WS_GET_MEAL_TIME            @"get_meal_time"
#define WS_ADD_ORDER                @"add_order"
#define WS_GET_ORDER_LIST           @"get_order_list"
#define WS_GET_ORDER_INFO           @"get_order_info"

#define MALE                        0
#define FEMALE                      1

@protocol WebServiceDelegate <NSObject>

@required
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject;
- (void)connectFail:(NSString *)resultName;
@end

@interface WebService : NSObject

@property (nonatomic, strong) id <WebServiceDelegate> delegate;
@property (nonatomic, strong) id userObject;

- (void)showWaitingView:(UIView*)parentView;
- (void)dismissWaitingView;
- (void)reloadLastCommand;

// Member
- (void)login:(NSString *)authkey sId:(NSString *)sid password:(NSString *)password deviceId:(NSString *)deviceId;
- (void)registerAccount:(NSString *)authkey sId:(NSString *)sid;
- (void)authenticate:(NSString *)authkey sId:(NSString *)sid authCode:(NSString *)authCode;
- (void)setPassword:(NSString *)authkey sId:(NSString *)vipId acckey:(NSString *)acckey password:(NSString *)password;
- (void)changePassword:(NSString *)authkey sId:(NSString *)vipId acckey:(NSString *)acckey
               oldpass:(NSString *)oldpass password:(NSString *)password;
- (void)registerDevice:(NSString *)authkey accKey:(NSString *)acckey deviceId:(NSString *)deviceId;
- (void)searchMember:(NSString *)authkey mobile:(NSString *)mobile;
- (void)registerNewMember:(NSString *)authkey applyshop:(NSString *)applyshop name:(NSString *)name sex:(int)sex email:(NSString *)email mobile:(NSString *)mobile birthday:(NSString *)birthday address:(NSString *)address telephone:(NSString *)telephone idcard:(NSString *)idcard regflag:(int)regflag;
- (void)updateMember:(NSString *)authkey accKey:(NSString *)acckey sex:(int)sex email:(NSString *)email mobile:(NSString *)mobile address:(NSString *)address telephone:(NSString *)telephone idcard:(NSString *)idcard;

// Products
- (void)getDepartment:(NSString *)authkey accKey:(NSString *)acckey;
- (void)getProdcate00:(NSString *)authkey;
- (void)getProduct:(NSString *)authkey accKey:(NSString *)acckey shopId:(NSString *)shopId saleMethod:(int)saleMethod posDeptId:(NSString *)posDeptId deptId:(NSString *)deptId;
- (void)getTaste:(NSString *)authkey prodId:(NSString *)prodId;
- (void)getComb:(NSString *)authkey prodId:(NSString *)prodId;
- (void)getPack:(NSString *)authkey prodId:(NSString *)prodId;

// Friends
- (void)getFriendList:(NSString *)authkey accKey:(NSString *)acckey;
- (void)inviteFriend:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId;
- (void)friendAgree:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId;
- (void)friendDeny:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId;
- (void)friendDelete:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId;

// Points
- (void)searchLastPointValue:(NSString *)authkey accKey:(NSString *)acckey;
- (void)vipPointMoveShop:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId point:(NSString *)point;
- (void)getTransactionLogs:(NSString *)authkey accKey:(NSString *)acckey isPoint:(BOOL)isPoint; // YES=point, NO=ticket
- (void)getVirtualCard:(NSString *)authkey accKey:(NSString *)acckey;

- (void)getTicketList:(NSString *)authkey accKey:(NSString *)acckey;
- (void)ticketGift:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId serno:(NSString *)serno tkno:(NSString *)tkno;
- (void)useTicket:(NSString *)authkey accKey:(NSString *)acckey tkno:(NSString *)tkno;
- (void)receiveTicket:(NSString *)authkey accKey:(NSString *)acckey tkno:(NSString *)tkno;
- (void)getVipBonusList:(NSString *)authkey accKey:(NSString *)acckey;

- (void)getNews:(NSString *)authkey pmId:(NSInteger)pmId;
- (void)getPubcode:(NSString *)authkey;
- (void)getMessageList:(NSString *)authkey accKey:(NSString *)acckey;
- (void)readMessage:(NSString *)authkey accKey:(NSString *)acckey infoId:(NSString *)infoId;
- (void)getPageList:(NSString *)authkey accKey:(NSString *)acckey;
- (void)getShareMessage:(NSString *)authkey accKey:(NSString *)acckey;
- (void)getBanner:(NSString *)authkey;
- (void)getShop:(NSString *)authkey shopId:(NSString *)shopId areaId:(NSString *)areaId pstAreaId:(NSString *)pstAreaId city:(NSString *)city pstAreaSNo:(NSString *)pstAreaSNo;
- (void)getPostArea:(NSString *)authkey;
- (void)getPostArea00:(NSString *)authkey areaId:(NSString *)areaId;
- (void)getPostArea01:(NSString *)authkey postAreaId:(NSString *)postAreaId;
- (void)getPostAreaRd:(NSString *)authkey postAreaId:(NSString *)postAreaId postAreaSNo:(NSString *)postAreaSNo;

// Order
- (void)getShop00Config:(NSString *)authkey shopId:(NSString *)shopId;
- (void)getSaleMethod:(NSString *)authkey shopId:(NSString *)shopId;
- (void)getMealTime:(NSString *)authkey shopId:(NSString *)shopId date:(NSString *)date;
- (void)addOrder:(NSString *)authkey accKey:(NSString *)acckey shopId:(NSString *)shopId sourceType:(int)sourceType mealDate:(NSString *)mealDate saleMethod:(int)saleMethod paymentTerms:(int)paymentTerms recAddr:(NSString *)recAddr webOrder01:(NSString *)webOrder01 webOrder011:(NSString *)webOrder011;
- (void)getOrderList:(NSString *)authkey accKey:(NSString *)acckey;
- (void)getOrderInfo:(NSString *)authkey accKey:(NSString *)acckey worderId:(NSString *)worderId;

@end

