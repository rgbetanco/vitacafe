//
//  ProductDetailViewCell.m
//  QBurger
//
//  Created by Kevin Phua on 29/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "ProductDetailViewCell.h"
#import "Global.h"

@implementation ProductDetailViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgProduct.contentMode = UIViewContentModeScaleAspectFill;
    self.imgProduct.clipsToBounds = YES;
    
    self.lblProductName.textColor = [Global colorWithType:COLOR_TEXT_PRODUCT_PRICE];
    self.lblOriginalPrice.textColor = [Global colorWithType:COLOR_TEXT_PRODUCT_PRICE];
    //self.lblProductName.textColor = [Global colorWithType:COLOR_TYPE_NAVBAR_BG];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onBtnAddToShoppingCart:(id)sender {
    [self.delegate onBtnAddToShoppingCart:sender];
}

- (IBAction)onBtnFavorite:(id)sender {
    [self.delegate onBtnFavorite:sender];
}

- (IBAction)onBtnAdd:(id)sender {
    [self.delegate onBtnAdd:sender];
}

- (IBAction)onBtnSubtract:(id)sender {
    [self.delegate onBtnSubtract:sender];
}

@end
