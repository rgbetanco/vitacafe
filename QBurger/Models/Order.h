//
//  Order.h
//  QBurger
//
//  Created by Kevin Phua on 30/10/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Order : NSObject

@property (nonatomic, strong) NSString *worderId;
@property (nonatomic, strong) NSString *inputDate;
@property (nonatomic, strong) NSString *mealDate;
@property (nonatomic, strong) NSString *methodName;
@property (nonatomic, strong) NSString *recAddr;
@property (nonatomic, strong) NSString *recMobile;
@property (nonatomic, strong) NSString *recName;
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *totalSales;
@property (nonatomic, strong) NSArray *worders;   // Array of WebOrder01

@end
