//
//  SaleMethod.h
//  QBurger
//
//  Created by Kevin Phua on 10/5/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaleMethod : NSObject

@property (nonatomic) BOOL isAddr;
@property (nonatomic, strong) NSString *methodId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) BOOL serviceCharge;
@property (nonatomic) BOOL vipNonDisc;

@end
