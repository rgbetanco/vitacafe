//
//  VipPoint.h
//  Raise
//
//  Created by Kevin Phua on 12/31/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VipPoint : NSObject

@property (nonatomic) NSInteger bonusPoint;
@property (nonatomic, strong) NSString *inputDate;
@property (nonatomic, strong) NSString *pointType;
@property (nonatomic) NSInteger shopId;
@property (nonatomic, strong) NSString *ticketId;
@property (nonatomic, strong) NSString *giftVipId;

- (id)initWithBonusPoint:(NSInteger)bonusPoint inputDate:(NSString *)inputDate pointType:(NSString *)pointType shopId:(NSInteger)shopId ticketId:(NSString *)ticketId giftVipId:(NSString *)giftVipId;

@end
