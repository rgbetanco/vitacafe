//
//  Taste.m
//  QBurger
//
//  Created by Kevin Phua on 9/8/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "Taste.h"

@implementation Taste

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // Copy NSObject subclasses
        [copy setProdId:[self.prodId copyWithZone:zone]];
        [copy setTasteName:[self.tasteName copyWithZone:zone]];
        [copy setTasteId:[self.tasteId copyWithZone:zone]];
        [copy setTasteDetails:[self.tasteDetails copyWithZone:zone]];
        
        // Set primitives
        [copy setTasteKind:self.tasteKind];
        [copy setMutex:self.mutex];
    }
    
    return copy;
}

@end
