//
//  FavoritesViewCell.h
//  QBurger
//
//  Created by Kevin Phua on 3/10/17.
//  Copyright © 2017 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FavoritesViewCellDelegate <NSObject>

- (void)onBtnDelete:(id)sender;

@end

@interface FavoritesViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIImageView *imgCat1;
@property (weak, nonatomic) IBOutlet UIImageView *imgCat2;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@property (weak, nonatomic) id<FavoritesViewCellDelegate> delegate;

@end
