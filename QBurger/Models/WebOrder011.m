//
//  WebOrder011.m
//  QBurger
//
//  Created by Kevin Phua on 04/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "WebOrder011.h"

@implementation WebOrder011

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // Copy NSObject subclasses
        [copy setName:[self.name copyWithZone:zone]];
        
        // Set primitives
        [copy setWorderSno:self.worderSno];
        [copy setTasteId:self.tasteId];
        [copy setTasteSno:self.tasteSno];
        [copy setPrice:self.price];
    }
    
    return copy;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]){
        self.worderSno = [aDecoder decodeIntForKey:@"worderSno"];
        self.tasteId = [aDecoder decodeIntForKey:@"tasteId"];
        self.tasteSno = [aDecoder decodeIntForKey:@"tasteSno"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.price = [aDecoder decodeIntForKey:@"price"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInt:self.worderSno forKey:@"worderSno"];
    [aCoder encodeInt:self.tasteId forKey:@"tasteId"];
    [aCoder encodeInt:self.tasteSno forKey:@"tasteSno"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeInt:self.price forKey:@"price"];
}

- (NSString *)description {
    NSMutableString *desc = [NSMutableString new];
    [desc appendFormat:@"worderSno = %d\n", self.worderSno];
    [desc appendFormat:@"tasteId = %d\n", self.tasteId];
    [desc appendFormat:@"tasteSno = %d\n", self.tasteSno];
    [desc appendFormat:@"name = %@\n", self.name];
    [desc appendFormat:@"price = %d\n", self.price];
    return desc;
}

@end
