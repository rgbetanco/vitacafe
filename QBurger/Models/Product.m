//
//  Product.m
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "Product.h"

@implementation Product

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // Copy NSObject subclasses
        [copy setProdId:[self.prodId copyWithZone:zone]];
        [copy setProdName1:[self.prodName1 copyWithZone:zone]];
        [copy setProdName2:[self.prodName2 copyWithZone:zone]];
        [copy setProdContent:[self.prodContent copyWithZone:zone]];
        [copy setAppCate:[self.appCate copyWithZone:zone]];
        [copy setDepId:[self.depId copyWithZone:zone]];
        [copy setImgFile1:[self.imgFile1 copyWithZone:zone]];
        [copy setPrice1:[self.price1 copyWithZone:zone]];
        [copy setPrice2:[self.price2 copyWithZone:zone]];
        [copy setSpec:[self.spec copyWithZone:zone]];
        [copy setTax:[self.tax copyWithZone:zone]];
        
        // Set primitives
        [copy setEnable:self.enable];
        [copy setIsRedeem:self.isRedeem];
        [copy setIsPack:self.isPack];
        [copy setIsComb:self.isComb];
        [copy setNonBonusPt:self.nonBonusPt];
        [copy setNonServiceCharge:self.nonServiceCharge];
        [copy setStopSale:self.stopSale];
        [copy setRedeemPoint:self.redeemPoint];
        [copy setSizeType:self.sizeType];
        [copy setUnit:self.unit];
    }
    
    return copy;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]){
        self.prodId = [aDecoder decodeObjectForKey:@"prodId"];
        self.prodName1 = [aDecoder decodeObjectForKey:@"prodName1"];
        self.prodName2 = [aDecoder decodeObjectForKey:@"prodName2"];
        self.prodContent = [aDecoder decodeObjectForKey:@"prodContent"];
        self.appCate = [aDecoder decodeObjectForKey:@"appCate"];
        self.depId = [aDecoder decodeObjectForKey:@"depId"];
        self.imgFile1 = [aDecoder decodeObjectForKey:@"imgFile1"];
        self.price1 = [aDecoder decodeObjectForKey:@"price1"];
        self.price2 = [aDecoder decodeObjectForKey:@"price2"];
        self.spec = [aDecoder decodeObjectForKey:@"spec"];
        self.tax = [aDecoder decodeObjectForKey:@"tax"];
        self.enable = [aDecoder decodeBoolForKey:@"enable"];
        self.isRedeem = [aDecoder decodeBoolForKey:@"isRedeem"];
        self.isPack = [aDecoder decodeBoolForKey:@"isPack"];
        self.isComb = [aDecoder decodeBoolForKey:@"isComb"];
        self.nonBonusPt = [aDecoder decodeBoolForKey:@"nonBonusPt"];
        self.nonServiceCharge = [aDecoder decodeBoolForKey:@"nonServiceCharge"];
        self.stopSale = [aDecoder decodeBoolForKey:@"stopSale"];
        self.redeemPoint = [aDecoder decodeIntForKey:@"redeemPoint"];
        self.sizeType = [aDecoder decodeIntForKey:@"sizeType"];
        self.unit = [aDecoder decodeIntForKey:@"unit"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.prodId forKey:@"prodId"];
    [aCoder encodeObject:self.prodName1 forKey:@"prodName1"];
    [aCoder encodeObject:self.prodName2 forKey:@"prodName2"];
    [aCoder encodeObject:self.prodContent forKey:@"prodContent"];
    [aCoder encodeObject:self.appCate forKey:@"appCate"];
    [aCoder encodeObject:self.depId forKey:@"depId"];
    [aCoder encodeObject:self.imgFile1 forKey:@"imgFile1"];
    [aCoder encodeObject:self.price1 forKey:@"price1"];
    [aCoder encodeObject:self.price2 forKey:@"price2"];
    [aCoder encodeObject:self.spec forKey:@"spec"];
    [aCoder encodeObject:self.tax forKey:@"tax"];
    [aCoder encodeBool:self.enable forKey:@"enable"];
    [aCoder encodeBool:self.isRedeem forKey:@"isRedeem"];
    [aCoder encodeBool:self.isPack forKey:@"isPack"];
    [aCoder encodeBool:self.isComb forKey:@"isComb"];
    [aCoder encodeBool:self.nonBonusPt forKey:@"nonBonusPt"];
    [aCoder encodeBool:self.nonServiceCharge forKey:@"nonServiceCharge"];
    [aCoder encodeBool:self.stopSale forKey:@"stopSale"];
    [aCoder encodeInt:self.redeemPoint forKey:@"redeemPoint"];
    [aCoder encodeInt:self.sizeType forKey:@"sizeType"];
    [aCoder encodeInt:self.unit forKey:@"unit"];
}

@end
