//
//  LocationsViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/6/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "LocationsViewController.h"
#import "LocationsDetailViewController.h"
#import "LocationsCell.h"
#import "ShopInfo.h"
#import "Global.h"
#import <MapKit/MapKit.h>
#import "LocationEngine.h"
#import "PostArea.h"
#import "PostArea00.h"
#import "HMSegmentedControl.h"

@interface LocationsViewController () <UITableViewDelegate, UITableViewDataSource, LocationsCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblNoResults;
@property (strong, nonatomic) HMSegmentedControl *segCtrlArea;
@property (strong, nonatomic) HMSegmentedControl *segCtrlCity;

@property (strong, nonatomic) NSString *websiteUrl;
@property (strong, nonatomic) NSMutableArray *shops;
@property (strong, nonatomic) NSMutableArray *postAreas;
@property (strong, nonatomic) NSMutableArray *postAreaCities;

@property (strong, nonatomic) LocationEngine *locationEngine;

@end

@implementation LocationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _shops = [NSMutableArray new];
    _postAreas = [NSMutableArray new];
    _postAreaCities = [NSMutableArray new];
    
    // Init LocationManager
    self.locationEngine = [LocationEngine getInstance];
    
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"Locations", nil);
    self.tableView.contentInset = UIEdgeInsetsMake(-80, 0, 0, 0);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.segCtrlArea = [HMSegmentedControl new];
    self.segCtrlArea.frame = CGRectMake(0, 80, self.view.frame.size.width, 30);
    self.segCtrlArea.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segCtrlArea.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
    self.segCtrlArea.selectionIndicatorBoxColor = [UIColor colorWithRed:100.0/255.0f green:191.0/255.0f blue:147.0/255.0f alpha:1.0f];
    self.segCtrlArea.selectionIndicatorBoxOpacity = 1.0;
    self.segCtrlArea.backgroundColor = [UIColor colorWithRed:244.0/255.0f green:199.0/255.0f blue:54.0/255.0f alpha:1.0f];
    self.segCtrlArea.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segCtrlArea.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self.segCtrlArea addTarget:self
                        action:@selector(segPostAreaValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.segCtrlArea.hidden = true;
    //[self.view addSubview:self.segCtrlArea];

    self.segCtrlCity = [HMSegmentedControl new];
    self.segCtrlCity.frame = CGRectMake(0, 120, self.view.frame.size.width, 30);
    self.segCtrlCity.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segCtrlCity.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
    self.segCtrlCity.selectionIndicatorBoxColor = [UIColor colorWithRed:100.0/255.0f green:191.0/255.0f blue:147.0/255.0f alpha:1.0f];
    self.segCtrlCity.selectionIndicatorBoxOpacity = 1.0;
    self.segCtrlCity.backgroundColor = [UIColor colorWithRed:244.0/255.0f green:199.0/255.0f blue:54.0/255.0f alpha:1.0f];
    self.segCtrlCity.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segCtrlCity.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self.segCtrlCity addTarget:self
                         action:@selector(segPostAreaCitiesValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.segCtrlCity.hidden = true;
    //[self.view addSubview:self.segCtrlCity];
    
    [self getPostArea];
     
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)getPostArea {
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws showWaitingView:self.view];
    [ws getPostArea:[Global cfgAppId]];
}

- (void)getPostArea00:(NSString *)areaId {
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getPostArea00:[Global cfgAppId] areaId:areaId];
}

- (void)getShop:(NSString *)areaId pstAreaId:(NSString *)pstAreaId {
    WebService *ws = [WebService new];
    ws.delegate = self;
    //[ws getShop:[Global cfgAppId] shopId:@"" areaId:areaId pstAreaId:pstAreaId city:@"" pstAreaSNo:@""];
    [ws getShop:[Global cfgAppId] shopId:@"" areaId:@"" pstAreaId:@"" city:@"" pstAreaSNo:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segPostAreaValueChanged:(id)sender {
    NSInteger areaIndex = self.segCtrlArea.selectedSegmentIndex;
    NSLog(@"Selected area index = %ld", areaIndex);
    PostArea *postArea = [_postAreas objectAtIndex:areaIndex];
    if (postArea) {
        [self getPostArea00:postArea.areaId];
    }
}

- (void)segPostAreaCitiesValueChanged:(id)sender {
    NSInteger cityIndex = self.segCtrlCity.selectedSegmentIndex;
    NSLog(@"Selected city index = %ld", cityIndex);
    PostArea00 *postAreaCity = [_postAreaCities objectAtIndex:cityIndex];
    if (postAreaCity) {
        [self getShop:postAreaCity.areaId pstAreaId:postAreaCity.postAreaId];
    }
}

- (void)sortShopsByDistance {
    NSArray *sortedShops;
    sortedShops = [_shops sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:((ShopInfo *)a).latitude longitude:((ShopInfo *)a).longitude];
        CLLocation *location2 = [[CLLocation alloc] initWithLatitude:((ShopInfo *)b).latitude longitude:((ShopInfo *)b).longitude];
        CLLocationDistance distance1 = [self.locationEngine.currentLocation distanceFromLocation:location1];
        CLLocationDistance distance2 = [self.locationEngine.currentLocation distanceFromLocation:location2];
        return (distance1 < distance2) ? NSOrderedAscending : (distance1 > distance2) ? NSOrderedDescending : NSOrderedSame;
    }];
    _shops = [NSMutableArray arrayWithArray:sortedShops];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.shops.count > 0) {
        _lblNoResults.hidden = YES;
    } else {
        _lblNoResults.hidden = NO;
    }    
    return [self.shops count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LocationsCell";
    LocationsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"LocationsCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    ShopInfo *shop = [self.shops objectAtIndex:[indexPath row]];
    cell.lblName.text = shop.shopName;
//    cell.lblFax.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"LocationFax", nil), shop.fax];
    cell.lblPhone.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"LocationTel", nil), shop.tel];
    cell.lblAddress.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"LocationAddress", nil), shop.address];
    cell.lblAddress.numberOfLines = 0;
    [cell.lblAddress sizeToFit];
    cell.delegate = self;
    
    if (shop.latitude != 0 && shop.longitude != 0) {
        cell.btnLocation.hidden = NO;
        
        // Calculate distance from current location
        cell.lblDistance.hidden = NO;
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:shop.latitude longitude:shop.longitude];
        CLLocationDistance distance = [self.locationEngine.currentLocation distanceFromLocation:location];
        if (distance==0) {
            [cell.lblDistance setText:@""];
        } else if (distance > 1000) {
            [cell.lblDistance setText:[NSString stringWithFormat:@"%.1f km", distance/1000.0]];
        } else {
            [cell.lblDistance setText:[NSString stringWithFormat:@"%d m", (int)distance]];
        }
    } else {
        cell.btnLocation.hidden = YES;
        cell.lblDistance.hidden = YES;
    }
    
    if (shop.tel && shop.tel.length>0) {
        cell.btnCall.hidden = NO;
    } else {
        cell.btnCall.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LocationsDetailViewController *detailVC = [[LocationsDetailViewController alloc] initWithNibName:@"LocationsDetailViewController" bundle:nil];
    
    // Hide back button text on next page
    self.title = @"";
    
    ShopInfo *shop = [self.shops objectAtIndex:[indexPath row]];
    detailVC.shopInfo = shop;
    [self.navigationController pushViewController:detailVC animated:YES];    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//==================================================================
#pragma LocationsCellDelegate
//==================================================================

- (void)btnLocation:(id)sender
{
    LocationsCell *cell = (LocationsCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath) {
        ShopInfo *shop = [self.shops objectAtIndex:[indexPath row]];
        if (shop.latitude != 0 && shop.longitude != 0) {
            // Try opening in Google Maps
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?center=%f,%f&saddr=&daddr=%f,%f",shop.latitude, shop.longitude, shop.latitude, shop.longitude]];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                // Open Apple Maps
                CLLocationCoordinate2D coordinate =  CLLocationCoordinate2DMake(shop.latitude, shop.longitude);
                
                //create MKMapItem out of coordinates
                MKPlacemark* placeMark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
                MKMapItem* destination =  [[MKMapItem alloc] initWithPlacemark:placeMark];
                [destination setName:shop.shopName];
                if([destination respondsToSelector:@selector(openInMapsWithLaunchOptions:)]) {
                    //using iOS6 native maps app
                    [destination openInMapsWithLaunchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving}];
                } else {
                    NSString *addressString = [NSString stringWithFormat:@"http://maps.apple.com/?ll=%f,%f", shop.latitude, shop.longitude];
                    NSString *encodedAddress = [addressString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *url = [NSURL URLWithString:encodedAddress];
                    [[UIApplication sharedApplication] openURL:url];
                }
            }
        }
    }
}

- (void)btnCall:(id)sender
{
    LocationsCell *cell = (LocationsCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath) {
        ShopInfo *shop = [self.shops objectAtIndex:[indexPath row]];
        if (shop.tel && shop.tel.length > 0) {
            NSString *phoneNumber = [@"tel://" stringByAppendingString:shop.tel];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        }
    }
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_SHOP] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *shops = [jsonObject objectForKey:@"data"];
                NSString *datacnt = [jsonObject objectForKey:@"datacnt"];
                NSLog(@"Total number of shops = %@", datacnt);
                
                [_shops removeAllObjects];
                for (NSDictionary *shop in shops) {
                    ShopInfo *shopInfo = [ShopInfo new];
                    shopInfo.address = [shop objectForKey:@"address"];
                    shopInfo.fax = [shop objectForKey:@"fax"];
                    shopInfo.shopName = [shop objectForKey:@"shop_name"];
                    shopInfo.tel = [shop objectForKey:@"tel"];
                    shopInfo.latitude = [[shop objectForKey:@"latitude"] doubleValue];
                    shopInfo.longitude = [[shop objectForKey:@"longitude"] doubleValue];
                    shopInfo.workTime = [shop objectForKey:@"worktime"];
                    shopInfo.htmlBody = [shop objectForKey:@"htmlbody"];
                    [_shops addObject:shopInfo];
                }
                [self sortShopsByDistance];
                [self.tableView reloadData];
            }
        } else if ([resultName isEqualToString:WS_GET_POST_AREA]) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *postAreas = [jsonObject objectForKey:@"data"];
                [_postAreas removeAllObjects];
                for (NSDictionary *postArea in postAreas) {
                    PostArea *newPostArea = [PostArea new];
                    newPostArea.areaId = [postArea objectForKey:@"area_id"];
                    newPostArea.areaName = [postArea objectForKey:@"area_name"];
                    [_postAreas addObject:newPostArea];
                }

                // Update segmented control
                NSMutableArray *areaTitles = [NSMutableArray new];
                for (int i=0; i<_postAreas.count; i++) {
                    PostArea *postArea = [_postAreas objectAtIndex:i];
                    [areaTitles addObject:postArea.areaName];
                }
                [self.segCtrlArea setSectionTitles:areaTitles];
                self.segCtrlArea.selectedSegmentIndex = 0;
                
                PostArea *defaultPostArea = [_postAreas firstObject];
                [self getPostArea00:defaultPostArea.areaId];
            }
        } else if ([resultName isEqualToString:WS_GET_POST_AREA00]) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *postAreaCities = [jsonObject objectForKey:@"data"];
                [_postAreaCities removeAllObjects];
                for (NSDictionary *postAreaCity in postAreaCities) {
                    PostArea00 *newPostAreaCity = [PostArea00 new];
                    newPostAreaCity.postAreaId = [postAreaCity objectForKey:@"pstarea_id"];
                    newPostAreaCity.postAreaName = [postAreaCity objectForKey:@"pstarea_name"];
                    newPostAreaCity.positionId = [postAreaCity objectForKey:@"pstarea_name"];
                    newPostAreaCity.areaId = [postAreaCity objectForKey:@"area_id"];
                    newPostAreaCity.areaName = [postAreaCity objectForKey:@"area_name"];
                    [_postAreaCities addObject:newPostAreaCity];
                }
                // Update segmented control
                NSMutableArray *cityTitles = [NSMutableArray new];
                for (int i=0; i<_postAreaCities.count; i++) {
                    PostArea00 *postAreaCity = [_postAreaCities objectAtIndex:i];
                    [cityTitles addObject:postAreaCity.postAreaName];
                }
                [self.segCtrlCity setSectionTitles:cityTitles];
                self.segCtrlCity.selectedSegmentIndex = 0;
                
                PostArea00 *defaultPostAreaCity = [_postAreaCities firstObject];
                [self getShop:defaultPostAreaCity.areaId pstAreaId:defaultPostAreaCity.postAreaId];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.viewNetworkError setHidden:NO];
}

@end
