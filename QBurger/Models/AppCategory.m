//
//  AppCategory.m
//  QBurger
//
//  Created by Kevin Phua on 28/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "AppCategory.h"

@implementation AppCategory

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]){
        self.imgFile1 = [aDecoder decodeObjectForKey:@"imgFile1"];
        self.imgFile2 = [aDecoder decodeObjectForKey:@"imgFile2"];
        self.serNo = [aDecoder decodeIntForKey:@"serNo"];
        self.subject = [aDecoder decodeObjectForKey:@"subject"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.imgFile1 forKey:@"imgFile1"];
    [aCoder encodeObject:self.imgFile2 forKey:@"imgFile2"];
    [aCoder encodeInt:self.serNo forKey:@"serNo"];
    [aCoder encodeObject:self.subject forKey:@"subject"];
}

@end
