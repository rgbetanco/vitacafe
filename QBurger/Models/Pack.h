//
//  Pack.h
//  QBurger
//
//  Created by Kevin Phua on 9/8/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pack : NSObject

@property (nonatomic, strong) NSString *packId;
@property (nonatomic, strong) NSString *psuitId;
@property (nonatomic, strong) NSString *psuitName;
@property (nonatomic) int packPrcType;
@property (nonatomic) int packQty;
@property (nonatomic) int price;
@property (nonatomic) int plusPrice;
@property (nonatomic, strong) NSString *prodName;
@property (nonatomic, strong) NSArray *packDetails;

@end
