//
//  SelectFriendViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/26/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@protocol SelectFriendDelegate <NSObject>

- (void)selectedFriend:(NSString *)name fId:(NSString *)fId;

@end

@interface SelectFriendViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, WebServiceDelegate>

@property (strong, nonatomic) id<SelectFriendDelegate> delegate;

@end
