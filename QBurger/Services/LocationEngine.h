//
//  LocationEngine.h
//  
//
//  Created by loegg little on 12/5/23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>

@interface LocationEngine : NSObject <CLLocationManagerDelegate> {
	CLLocationManager* locationManager;
    CLLocation *currentLocation;
}

+ (LocationEngine*)getInstance;
- (void) startUpdate;
- (void) stopUpdate;
- (CLLocation*) getLocation;

@property (nonatomic, strong) CLLocation *currentLocation;

@end
