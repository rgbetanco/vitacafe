//
//  ProductViewController.m
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "ProductViewController.h"
#import "ProductListViewController.h"
#import "SWRevealViewController.h"
#import "RaiseAlertView.h"
#import "Global.h"
#import "ProductCategory.h"
#import "UIImageView+WebCache.h"
#import "ProductListCell.h"

@interface ProductViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *categories;

@end

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.categories = [[NSMutableArray alloc] init];
    
    UINib *cellNib = [UINib nibWithNibName:@"ProductListCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView setBackgroundColor:[Global colorWithType:COLOR_TYPE_NAVBAR_BG]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.title = NSLocalizedString(@"Products", nil);
    
    [self updateMessages:PARENT_PAGE_DEFAULT];
    //[self updateCart:PARENT_PAGE_DEFAULT];
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getProdcate00:[Global cfgAppId]];
    [ws showWaitingView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//==================================================================
#pragma mark - UICollectionViewDelegate
//==================================================================

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _categories.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    ProductListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    ProductCategory *category = [self.categories objectAtIndex:[indexPath row]];
    
    cell.lblTitle.text = category.subject;
        
    UIImageView *imgView = cell.imgProduct;
    NSString *imgPath = category.imgFile2;
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgPath]
               placeholderImage:[UIImage imageNamed:@"login_logo"]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductCategory *category = [self.categories objectAtIndex:[indexPath row]];
    
    // Open ProductDetailViewController
    ProductListViewController *productListController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil];
    productListController.isOrder = _isOrder;
    [productListController setDeptId:[NSString stringWithFormat:@"%d", category.serNo]];
    [productListController setDeptName:category.subject];
    
    // Hide back button text on next page
    self.title = @"";
    
    [self.navigationController pushViewController:productListController animated:YES];
}

//==================================================================
#pragma mark - UICollectionViewDelegateFlowLayout
//==================================================================

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat collectionViewWidth = collectionView.contentSize.width;
    // Leave 10 pixel spacing
    CGFloat cellWidth = (collectionViewWidth / 3) - 15;
    CGFloat cellHeight = (3 * cellWidth / 4) + 25;
    return CGSizeMake(cellWidth, cellHeight);
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_PRODCATE_00] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ categories", datacnt);
                
                [_categories removeAllObjects];
                NSArray *categories = (NSArray *)[jsonObject objectForKey:@"data"];
                NSString *imgPath = [jsonObject objectForKey:@"imgpath"];
                for (NSDictionary *category in categories) {
                    ProductCategory *prodCat = [ProductCategory new];
                    prodCat.imgFile1 = [NSString stringWithFormat:@"%@%@", imgPath, [category objectForKey:@"imgfile1"]];
                    prodCat.imgFile2 = [NSString stringWithFormat:@"%@%@", imgPath, [category objectForKey:@"imgfile2"]];
                    prodCat.isHome = [[category objectForKey:@"ishome"] boolValue];
                    prodCat.isTag = [[category objectForKey:@"istag"] boolValue];
                    prodCat.serNo = [[category objectForKey:@"serno"] intValue];
                    prodCat.subject = [category objectForKey:@"subject"];
                    [_categories addObject:prodCat];
                }
                [self.collectionView reloadData];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitle:nil];
                [alertView show];
            }
            [self.collectionView setHidden:NO];
            [self.viewNetworkError setHidden:YES];
        } else if ([resultName compare:WS_SEARCH_LAST_POINT_VALUE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *vipPoint = (NSString *)[jsonObject objectForKey:@"vip_point"];
                if (vipPoint && vipPoint.length > 0) {
                    NSNumber *vipPointValue = [NSNumber numberWithInteger:[vipPoint intValue]];
                    [g_MemberInfo setObject:vipPointValue forKey:INFO_KEY_LAST_POINT];
                }
                [self updateVipPoints];
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            NSMutableArray *pendingFriends = [NSMutableArray new];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_AWAITING_APPROVAL) {
                        NSNumber *kind = [friend objectForKey:@"kind"];
                        if ([kind intValue] == KIND_INVITED_FRIENDS) {
                            [pendingFriends addObject:friend];
                        }
                    }
                }
                
                NSNumber *numPending = [NSNumber numberWithInteger:pendingFriends.count];
                [g_MemberInfo setObject:numPending forKey:INFO_KEY_PENDING_FRIENDS];
                [self updateFriends:PARENT_PAGE_DEFAULT];
                
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_MESSAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ messages", datacnt);
                
                NSArray *messages = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (messages) {
                    int unreadMessages = 0;
                    for (NSDictionary *message in messages) {
                        NSString *readTime = [message objectForKey:@"read_time"];
                        if ([readTime isEqualToString:@""]) {
                            unreadMessages++;
                        }
                    }
                    
                    [g_MemberInfo setObject:[NSNumber numberWithInt:unreadMessages] forKey:INFO_KEY_MSGS_UNREAD];
                    [self updateMessages:PARENT_PAGE_DEFAULT];
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.collectionView setHidden:YES];
    [self.viewNetworkError setHidden:NO];
}


@end
