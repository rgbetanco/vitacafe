//
//  CartViewCell.h
//  QBurger
//
//  Created by Kevin Phua on 9/23/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CartViewCellDelegate <NSObject>

- (void)onBtnDelete:(id)sender;
- (void)onBtnEdit:(id)sender;

@end

@interface CartViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceQty;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

@property (assign, nonatomic) id<CartViewCellDelegate> delegate;

@end
