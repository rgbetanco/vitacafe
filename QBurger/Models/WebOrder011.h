//
//  WebOrder011.h
//  QBurger
//
//  Created by Kevin Phua on 04/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebOrder011 : NSObject <NSCoding, NSCopying>

@property (nonatomic) int worderSno;
@property (nonatomic) int tasteId;
@property (nonatomic) int tasteSno;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) int price;

@end
