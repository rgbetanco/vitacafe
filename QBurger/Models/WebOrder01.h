//
//  WebOrder01.h
//  QBurger
//
//  Created by Kevin Phua on 04/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebOrder01 : NSObject <NSCoding, NSCopying>

@property (nonatomic) int worderSno;
@property (nonatomic, strong) NSString *worderId;
@property (nonatomic, strong) NSString *prodId;
@property (nonatomic, strong) NSString *prodName;
@property (nonatomic) int qty;
@property (nonatomic) int salePrice;
@property (nonatomic) int combSaleSno;
@property (nonatomic) int combSno;
@property (nonatomic) int combType;
@property (nonatomic) int combQty;
@property (nonatomic) int itemDisc;
@property (nonatomic) BOOL isDefault;
@property (nonatomic) BOOL enable;
@property (nonatomic) BOOL stopSale;
@property (nonatomic, strong) NSString *tasteMemo;
@property (nonatomic, strong) NSArray *webOrder011s;

@end
