//
//  OrderRecordsViewController.m
//  QBurger
//
//  Created by Kevin Phua on 30/10/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "OrderRecordsViewController.h"
#import "OrderDetailViewController.h"
#import "WebService.h"
#import "Global.h"
#import "Order.h"

@interface OrderRecordsViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *orders;

@end

@implementation OrderRecordsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _orders = [NSMutableArray new];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedString(@"BtnOrderHistory", nil);
    
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getOrderList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.orders count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

    Order *order = [_orders objectAtIndex:indexPath.row];
    
    if (order.methodName && ![order.methodName isKindOfClass:[NSNull class]]) {
        cell.textLabel.text = order.methodName;
    } else {
        cell.textLabel.text = @"";
    }
    cell.detailTextLabel.text = order.inputDate;
    
    UILabel *lblStatus = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    lblStatus.textAlignment = NSTextAlignmentRight;
    lblStatus.text = order.status;
    cell.accessoryView = lblStatus;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Order *order = [_orders objectAtIndex:indexPath.row];
    
    OrderDetailViewController *orderDetailVC = [[OrderDetailViewController alloc] initWithNibName:@"OrderDetailViewController" bundle:nil];
    orderDetailVC.worderId = order.worderId;
    orderDetailVC.mealDate = order.mealDate;
    orderDetailVC.methodName = order.methodName;
    orderDetailVC.status = order.status;
    orderDetailVC.totalSales = order.totalSales;
    [self.navigationController pushViewController:orderDetailVC animated:YES];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_ORDER_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                NSLog(@"Get order list success: %@", message);
                
                NSArray *orderRecords = [jsonObject objectForKey:@"data"];
                [_orders removeAllObjects];
                for (NSDictionary *orderRecord in orderRecords) {
                    Order *order = [Order new];
                    order.inputDate = [orderRecord objectForKey:@"input_date"];
                    order.mealDate = [orderRecord objectForKey:@"meal_date"];
                    order.methodName = [orderRecord objectForKey:@"method_name"];
                    order.recAddr = [orderRecord objectForKey:@"rec_addr"];
                    order.recMobile = [orderRecord objectForKey:@"rec_mobile"];
                    order.recName = [orderRecord objectForKey:@"rec_name"];
                    order.shopId = [orderRecord objectForKey:@"shop_id"];
                    order.status = [orderRecord objectForKey:@"status"];
                    order.totalSales = [orderRecord objectForKey:@"tot_sales"];
                    order.worderId = [orderRecord objectForKey:@"worder_id"];
                    [_orders addObject:order];
                }
                
                [self.tableView reloadData];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
