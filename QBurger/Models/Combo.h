//
//  Combo.h
//  QBurger
//
//  Created by Kevin Phua on 9/8/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Combo : NSObject <NSCopying>

@property (nonatomic, strong) NSString *combSno;
@property (nonatomic, strong) NSString *prodId;
@property (nonatomic, strong) NSString *prodName;
@property (nonatomic, strong) NSString *imgFile;
@property (nonatomic) int quantity;
@property (nonatomic) int price;
@property (nonatomic) BOOL isDefault;
@property (nonatomic, strong) NSArray *combDetails;

@end
