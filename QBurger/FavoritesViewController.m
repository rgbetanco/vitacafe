//
//  FavoritesViewController.m
//  QBurger
//
//  Created by Kevin Phua on 13/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "FavoritesViewController.h"
#import "Global.h"
#import "Product.h"
#import "FavoritesViewCell.h"
#import "UIImageView+WebCache.h"
#import "ProductDetailViewController.h"
#import "UIView+Toast.h"
#import "AppCategory.h"

@interface FavoritesViewController () <FavoritesViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) BOOL isSelected;

@end

@implementation FavoritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    UINib *cellNib = [UINib nibWithNibName:@"FavoritesViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"FavoritesViewCell"];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedString(@"MyFavorites", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//==================================================================
#pragma mark - UICollectionViewDelegate
//==================================================================

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return g_MyFavorites.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FavoritesViewCell *cell = (FavoritesViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"FavoritesViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    Product *product = [g_MyFavorites objectAtIndex:[indexPath row]];
    
    cell.lblTitle.text = product.prodName1;
    cell.lblPrice.text = [NSString stringWithFormat:@"$%@", product.price1];
    
    NSArray *appCates = product.appCate;
    int appCateIndex = 0;
    for (AppCategory *appCate in appCates) {
        if (appCateIndex == 0) {
            [cell.imgCat1 sd_setImageWithURL:[NSURL URLWithString:appCate.imgFile1]
                            placeholderImage:nil];
        } else if (appCateIndex == 1) {
            [cell.imgCat2 sd_setImageWithURL:[NSURL URLWithString:appCate.imgFile1]
                            placeholderImage:nil];
        }
        appCateIndex++;
    }
    
    UIImageView *imgView = cell.imgProduct;
    NSString *imgPath = product.imgFile1;
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgPath]
               placeholderImage:[UIImage imageNamed:@"login_logo"]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isSelected)
        return;
    
    // Prevent multiple selections
    _isSelected = YES;
    
    Product *product = [g_MyFavorites objectAtIndex:[indexPath row]];
    
    // Check if product stop sale
    if (product.stopSale) {
        [self.view makeToast:NSLocalizedString(@"ProductStopSale", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        // Remove from favorites
        [Global removeFavorite:product];
        [self.collectionView reloadData];
        return;
    }
    
    // Open ProductDetailViewController
    ProductDetailViewController *productDetailController = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
    productDetailController.isOrder = YES;
    [productDetailController setProduct:product];
    
    // Hide back button text on next page
    self.title = @"";
    
    [self.navigationController pushViewController:productDetailController animated:YES];
}

//==================================================================
#pragma mark - UICollectionViewDelegateFlowLayout
//==================================================================

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat collectionViewWidth = collectionView.contentSize.width;
    // Leave 10 pixel spacing
    CGFloat cellWidth = (collectionViewWidth / 2) - 15;
    return CGSizeMake(cellWidth, 250);
}

//==================================================================
#pragma FavoritesViewCellDelegate
//==================================================================

- (void)onBtnDelete:(UIButton *)sender {
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
    
    if (indexPath) {
        // Remove from favorites
        Product *product = [g_MyFavorites objectAtIndex:[indexPath row]];
        [Global removeFavorite:product];
        [self.collectionView reloadData];
    }
}


@end
