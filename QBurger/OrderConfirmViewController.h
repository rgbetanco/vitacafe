//
//  OrderConfirmViewController.h
//  QBurger
//
//  Created by Kevin Phua on 16/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrderConfirmViewDelegate <NSObject>

- (void)onConfirmOrder:(id)sender;

@end

@interface OrderConfirmViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblOrderShop;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderTime;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderType;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderContact;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderTotal;

@property (nonatomic) int orderTotal;

@property (nonatomic, assign) id<OrderConfirmViewDelegate> delegate;

@end
