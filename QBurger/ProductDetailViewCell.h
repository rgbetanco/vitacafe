//
//  ProductDetailViewCell.h
//  QBurger
//
//  Created by Kevin Phua on 29/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductDetailDelegate <NSObject>

- (void)onBtnAddToShoppingCart:(id)sender;
- (void)onBtnFavorite:(id)sender;
- (void)onBtnAdd:(id)sender;
- (void)onBtnSubtract:(id)sender;

@end

@interface ProductDetailViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblOriginalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFavorites;
@property (weak, nonatomic) IBOutlet UITextView *tvProductDesc;

@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnSubtract;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet UILabel *lblQtyTitle;

@property (weak, nonatomic) id<ProductDetailDelegate> delegate;

@end
