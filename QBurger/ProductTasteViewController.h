//
//  ProductTasteViewController.h
//  QBurger
//
//  Created by Kevin Phua on 10/28/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "BaseViewController.h"
#import "Taste.h"

@protocol ProductTasteDelegate <NSObject>

// Returns selected tasteDetails
- (void)onSelectedTaste:(NSArray *)tastes sender:(id)sender;

@end

@interface ProductTasteViewController : BaseViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray *tastes;
@property (nonatomic, strong) NSArray *inputSelectedTastes;
@property (nonatomic, weak) id<ProductTasteDelegate> delegate;

@end
