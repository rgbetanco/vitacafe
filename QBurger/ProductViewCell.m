//
//  ProductViewCell.m
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "ProductViewCell.h"

@implementation ProductViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self setNeedsDisplay]; // force drawRect:
}

- (IBAction)onBtnAdd:(id)sender {
    if (self.delegate) {
        [self.delegate onBtnAdd:sender];
    }
}

- (IBAction)onBtnSubtract:(id)sender {
    if (self.delegate) {
        [self.delegate onBtnSubtract:sender];
    }
}

@end
