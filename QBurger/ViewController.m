//
//  ViewController.m
//  Raise
//
//  Created by Kevin Phua on 10/28/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "ViewController.h"
#import "RaiseAlertView.h"
#import "AppDelegate.h"
#import "LoginViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:8.0f target:self selector:@selector(showAlertDialog) userInfo:nil repeats:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlertDialog {
    RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"StartTitle", nil)
                                                              message:NSLocalizedString(@"StartMessage", nil)
                                                    cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                     otherButtonTitle:NSLocalizedString(@"Yes", nil)];
    
    alertView.cancelButtonAction = ^{
        // Jump to intro page
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate goToMainView];
    };
    alertView.otherButtonAction = ^{
        // Jump to login page
        LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
        
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.window.rootViewController = navigationController;
        [appDelegate.window makeKeyAndVisible];
    };
    
    [alertView show];
}

@end
