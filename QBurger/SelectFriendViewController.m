//
//  SelectFriendViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/26/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "SelectFriendViewController.h"
#import "RaiseAlertView.h"
#import "Global.h"

#define STATUS_AWAITING_APPROVAL        0
#define STATUS_REJECTED                 1
#define STATUS_APPROVED                 10

@interface SelectFriendViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *allFriends;       // 所有好友
@property (nonatomic) NSInteger selectedRow;

@end

@implementation SelectFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.allFriends = [NSMutableArray new];
    self.selectedRow = -1;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedString(@"SelectFriend", nil);
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getFriendList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    [ws showWaitingView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.allFriends count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    NSDictionary *friend = [self.allFriends objectAtIndex:indexPath.row];
    NSString *name = [friend objectForKey:@"name_2"];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = name;
    
    if (indexPath.row == self.selectedRow) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.backgroundColor = [Global colorWithType:COLOR_BTN_CAT_LIST_BG];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
    
    // Update delegate
    NSDictionary *friend = [self.allFriends objectAtIndex:indexPath.row];
    NSString *name = [friend objectForKey:@"name_2"];
    NSString *fId = [friend objectForKey:@"vip_2"];
    if (self.delegate) {
        [self.delegate selectedFriend:name fId:fId];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadData];
    
    // Automatically go back to previous page
    [self.navigationController popViewControllerAnimated:YES];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            
            [self.allFriends removeAllObjects];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_APPROVED) {
                        [self.allFriends addObject:friend];
                    } 
                }
                
                [self.tableView reloadData];
            } else {
                // Failure
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
