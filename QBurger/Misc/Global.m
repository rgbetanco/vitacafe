//
//  Global.m
//  PosApp
//
//  Created by Kevin Phua on 9/16/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"

@implementation Global

+ (NSString *)cfgBaseUrl
{
    return [g_SysConfig objectForKey:CFG_BASE_URL];
}

+ (NSString *)cfgAppId
{
    return @"kultrp6zajt7xphrr8qa7rpfasebv9bp";
    //return [g_SysConfig objectForKey:CFG_APP_ID];
}

+ (NSString *)cfgAppUrl
{
    return [g_SysConfig objectForKey:CFG_APP_URL];
}

+ (BOOL)cfgMsgEnabled
{
    return [[g_SysConfig objectForKey:CFG_MSG_ENABLED] boolValue];
}

+ (BOOL)cfgPointsEnabled
{
    return [[g_SysConfig objectForKey:CFG_POINTS_ENABLED] boolValue];
}

+ (BOOL)cfgCollectiblesEnabled
{
    return [[g_SysConfig objectForKey:CFG_COLLECTIBLES_ENABLED] boolValue];
}

+ (BOOL)cfgOrdersEnabled
{
    return [[g_SysConfig objectForKey:CFG_ORDERS_ENABLED] boolValue];
}

+ (BOOL)cfgFriendsEnabled
{
    return [[g_SysConfig objectForKey:CFG_FRIENDS_ENABLED] boolValue];
}

+ (BOOL)cfgUserNewEnabled
{
    return [[g_SysConfig objectForKey:CFG_USER_NEW_ENABLED] boolValue];
}

+ (BOOL)cfgProductsSortEnabled
{
    return [[g_SysConfig objectForKey:CFG_PRODUCTS_SORT_ENABLED] boolValue];
}

+ (BOOL)cfgProductsHeartEnabled
{
    return [[g_SysConfig objectForKey:CFG_PRODUCTS_HEART_ENABLED] boolValue];
}

+ (BOOL)cfgProductsDropdownEnabled
{
    return [[g_SysConfig objectForKey:CFG_PRODUCTS_DROPDOWN_ENABLED] boolValue];
}

+ (BOOL)cfgProductsGroupMainMenuEnabled
{
    return [[g_SysConfig objectForKey:CFG_PRODUCTS_GROUP_MAINMENU_ENABLED] boolValue];
}

+ (BOOL)cfgProductsGroupScreenEnabled
{
    return [[g_SysConfig objectForKey:CFG_PRODUCTS_GROUP_SCREEN_ENABLED] boolValue];
}

+ (BOOL)cfgBookmarksToolEnabled
{
    return [[g_SysConfig objectForKey:CFG_BOOKMARKS_TOOL_ENABLED] boolValue];
}

+ (NSString *)cfgRtlsUrl
{
    return [g_SysConfig objectForKey:CFG_RTLS_URL];
}

+ (UIColor *)colorWithType:(ColorType)type
{
    UIColor *color;
    switch (type) {
        case COLOR_TEXT_MENU:
        case COLOR_TEXT_TITLE:
        case COLOR_TEXT_BTN:
        case COLOR_TEXT_STORE_TITLE:
        case COLOR_BG_MAIN:
        case COLOR_BG_TITLE:
        case COLOR_BTN_CAT_BG:  // 
        {
            color = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f];
            break;
        }
        case COLOR_TEXT_GROUP:
        case COLOR_TEXT_PRODUCT_TITLE:  // 9CC859
        {
            color = [UIColor colorWithRed:156/255.0f green:200/255.0f blue:89/255.0f alpha:1.0f];
            break;
        }
        case COLOR_TEXT_ITEM_TITLE:
        case COLOR_TEXT_ITEM_DATA:
        case COLOR_TEXT_STORE_TIME:  // 1A1918
        {
            color = [UIColor colorWithRed:26/255.0f green:25/255.0f blue:24/255.0f alpha:1.0f];
            break;
        }
        case COLOR_TEXT_PRODUCT_PRICE:  // CC2826
        {
            //color = [UIColor colorWithRed:204/255.0f green:40/255.0f blue:38/255.0f alpha:1.0f];
            color = [UIColor colorWithRed:255/255.0f green:128/255.0f blue:0/255.0f alpha:1.0f];
            break;
        }
        case COLOR_BTN_CAT_BG_SEL:  // 666666
        {
            color = [UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:30/255.0f];
            break;
        }
        case COLOR_BTN_CAT_LIST_BG: // 666666
        {
            color = [UIColor whiteColor];
            break;
        }
        case COLOR_TYPE_LINK:   // FEFEFE
        {
            color = [UIColor colorWithRed:254/255.0f green:254/255.0f blue:254/255.0f alpha:1.0f];
            //color = [UIColor colorWithRed:103/255.0f green:190/255.0f blue:190/255.0f alpha:1.0f];
            break;
        }
        case COLOR_TYPE_STEPS:  //
        {
            color = [UIColor colorWithRed:103/255.0f green:190/255.0f blue:190/255.0f alpha:1.0f];
            //color = [UIColor colorWithRed:154/255.0f green:154/255.0f blue:154/255.0f alpha:1.0f];
            break;

        }
        case COLOR_TYPE_TEXTBOX_BG:
        case COLOR_TYPE_BUTTON_UP:
        {
            color = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f];
            break;
        }
        case COLOR_TYPE_NAVBAR_BG:  // F39700
        {
            //color = [UIColor colorWithRed:100/255.0f green:100/255.0f blue:255/255.0f alpha:1.0f];
            color = [UIColor colorWithRed:93/255.0f green:180/255.0f blue:181/255.0f alpha:1.0f];
            break;
        }
        case COLOR_TYPE_TEXT:
        {
            color = [UIColor blackColor];
            break;
        }
        default:
            break;
    }
    return color;
}

+ (CGImageRef)createQRImageForString:(NSString *)string size:(CGSize)size {
    // Setup the QR filter with our string
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    CIImage *image = [filter valueForKey:@"outputImage"];
    
    // Calculate the size of the generated image and the scale for the desired image size
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size.width / CGRectGetWidth(extent), size.height / CGRectGetHeight(extent));
    
    // Since CoreImage nicely interpolates, we need to create a bitmap image that we'll draw into
    // a bitmap context at the desired size;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // Create an image with the contents of our bitmap
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    
    // Cleanup
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    
    return scaledImage;
}

+ (UIImage *)imageResize:(UIImage*)image height:(CGFloat)height
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat ratio = image.size.width/image.size.height;
    CGSize newSize;
    CGPoint offset = CGPointMake(0, 0);
    
    NSLog(@"Image width=%.1f, height=%.1f, ratio=%.1f", image.size.width, image.size.height, ratio);
    
    // Width 100%
    CGFloat newWidth = screenWidth;
    CGFloat newHeight = newWidth / ratio;
    newSize = CGSizeMake(newWidth, newHeight);
    if (newHeight > height) {
        offset = CGPointMake(0, 100);
    }
    
    NSLog(@"New image size w=%.1f, h=%.1f", newSize.width, newSize.height);
    
    CGRect resizeRect = CGRectMake(-offset.x, -offset.y, newSize.width, newSize.height);
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    } else {
        UIGraphicsBeginImageContext(newSize);
    }
    [image drawInRect:resizeRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(CGFloat)pixelToPoints:(CGFloat)px
{
    CGFloat pointsPerInch = 72.0; // see: http://en.wikipedia.org/wiki/Point%5Fsize#Current%5FDTP%5Fpoint%5Fsystem
    CGFloat scale = 1; // We dont't use [[UIScreen mainScreen] scale] as we don't want the native pixel, we want pixels for UIFont - it does the retina scaling for us
    float pixelPerInch; // aka dpi
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        pixelPerInch = 132 * scale;
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_IPHONE_4_OR_LESS) {
            pixelPerInch = 164.8 * scale;   // @1x
        } else if (IS_IPHONE_5) {
            pixelPerInch = 163 * scale;     // @2x
        } else if (IS_IPHONE_6) {
            pixelPerInch = 162.8 * scale;   // @2x
        } else {
            pixelPerInch = 153.5 * scale;   // @3x, then downsized
        }
    }
    CGFloat result = px * pointsPerInch / pixelPerInch;
    return result;
}

+ (NSString *)hexStringFromData:(NSData *)data
{
    unichar* hexChars = (unichar*)malloc(sizeof(unichar) * (data.length*2));
    unsigned char* bytes = (unsigned char*)data.bytes;
    for (NSUInteger i = 0; i < data.length; i++) {
        unichar c = bytes[i] / 16;
        if (c < 10) c += '0';
        else c += 'a' - 10;
        hexChars[i*2] = c;
        c = bytes[i] % 16;
        if (c < 10) c += '0';
        else c += 'a' - 10;
        hexChars[i*2+1] = c;
    }
    NSString* retVal = [[NSString alloc] initWithCharactersNoCopy:hexChars
                                                           length:data.length*2
                                                     freeWhenDone:YES];
    return retVal;
}

+ (NSString *)stringByStrippingHTML:(NSString *)s
{
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

+ (void)addFavorite:(Product *)product;
{
    BOOL productExists = NO;
    for (Product *prod in g_MyFavorites) {
        if ([prod.prodId isEqualToString:product.prodId]) {
            NSLog(@"Product %@ already exists!", prod.prodId);
            productExists = YES;
            break;
        }
    }
    
    if (!productExists) {
        [g_MyFavorites addObject:product];
        [self saveFavoritesToFile:g_MyFavorites];
    }
}

+ (void)removeFavorite:(Product *)product;
{
    for (Product *prod in g_MyFavorites) {
        if ([prod.prodId isEqualToString:product.prodId]) {
            [g_MyFavorites removeObject:prod];
            [self saveFavoritesToFile:g_MyFavorites];
            break;
        }
    }
}

+ (BOOL)isFavorite:(Product *)product
{
    BOOL productExists = NO;
    for (Product *prod in g_MyFavorites) {
        if ([prod.prodId isEqualToString:product.prodId]) {
            NSLog(@"Product %@ already exists!", prod.prodId);
            productExists = YES;
            break;
        }
    }
    return productExists;
}

+ (void)saveFavoritesToFile:(NSMutableArray *)favorites
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    if ([urls count] > 0) {
        NSURL *documentsFolder = urls[0];
        NSString *filePath = [[documentsFolder path] stringByAppendingPathComponent:FAVORITES_FILE];
        [NSKeyedArchiver archiveRootObject:favorites toFile:filePath];
    } else {
        NSLog(@"saveFavoritesToFile: could not find the Documents folder.");
    }
}

+ (NSMutableArray *)retrieveFavoritesFromFile
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    if ([urls count] > 0) {
        NSURL *documentsFolder = urls[0];
        NSString *filePath = [[documentsFolder path] stringByAppendingPathComponent:FAVORITES_FILE];
        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    } else {
        NSLog(@"retrieveFavoritesFromFile: could not find the Documents folder.");
        return nil;
    }
}

+ (void)addShoppingCart:(CartItem *)item
{
    [g_ShoppingCart addObject:item];
    [self saveShoppingCartToFile:g_ShoppingCart];
}

+ (void)modifyShoppingCart:(CartItem *)item atIndex:(NSInteger)index
{
    if (index >= g_ShoppingCart.count) {
        NSLog(@"Error modifying shopping cart!");
    }
    [g_ShoppingCart setObject:item atIndexedSubscript:index];
    [self saveShoppingCartToFile:g_ShoppingCart];
}
    
+ (void)clearShoppingCart
{
    [g_ShoppingCart removeAllObjects];
    [self saveShoppingCartToFile:g_ShoppingCart];
}

+ (void)saveShoppingCartToFile:(NSMutableArray *)cart
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    if ([urls count] > 0) {
        NSURL *documentsFolder = urls[0];
        NSString *filePath = [[documentsFolder path] stringByAppendingPathComponent:CART_FILE];
        [NSKeyedArchiver archiveRootObject:cart toFile:filePath];
    } else {
        NSLog(@"saveShoppingCartToFile: could not find the Documents folder.");
    }
}

+ (NSMutableArray *)retrieveShoppingCartFromFile
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    if ([urls count] > 0) {
        NSURL *documentsFolder = urls[0];
        NSString *filePath = [[documentsFolder path] stringByAppendingPathComponent:CART_FILE];
        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    } else {
        NSLog(@"retrieveShoppingCartFromFile: could not find the Documents folder.");
        return nil;
    }
}

+ (void)initUserDefaults
{
    // Retrieve favorites from file
    NSMutableArray *favData = [Global retrieveFavoritesFromFile];
    if (favData != nil) {
        g_MyFavorites = [[NSMutableArray alloc] initWithArray:favData];
    } else {
        g_MyFavorites = [NSMutableArray new];
    }
    
    // Retrieve shopping cart from file
    NSMutableArray *cartData = [Global retrieveShoppingCartFromFile];
    if (cartData != nil) {
        g_ShoppingCart = [[NSMutableArray alloc] initWithArray:cartData];
    } else {
        g_ShoppingCart = [NSMutableArray new];
    }
    
    // Retrieve cached products
    NSMutableDictionary *productCache = [Global retrieveProductsFromFile];
    if (productCache) {
        g_Products = [[NSMutableDictionary alloc] initWithDictionary:productCache];
    } else {
        g_Products = [NSMutableDictionary new];
    }
    
    // Retrieve previous shopping cart info
    NSString *selectedShopId = [[NSUserDefaults standardUserDefaults] valueForKey:UD_KEY_LAST_SELECTED_SHOP_ID];
    if (selectedShopId && selectedShopId.length>0) {
        g_LastSelectedShopId = selectedShopId;
        g_SelectedShopId = selectedShopId;
    }
    
    NSString *selectedShopName = [[NSUserDefaults standardUserDefaults] valueForKey:UD_KEY_LAST_SELECTED_SHOP_NAME];
    if (selectedShopName && selectedShopName.length>0) {
        g_SelectedShopName = selectedShopName;
    }
    
    NSString *selectedDate = [[NSUserDefaults standardUserDefaults] valueForKey:UD_KEY_LAST_SELECTED_DATE];
    if (selectedDate && selectedDate.length>0) {
        g_SelectedMealDate = selectedDate;
    }
    
    g_SelectedSaleMethod = (int)[[NSUserDefaults standardUserDefaults] integerForKey:UD_KEY_LAST_SELECTED_TYPE];
    
    NSString *selectedAddr = [[NSUserDefaults standardUserDefaults] valueForKey:UD_KEY_LAST_SELECTED_ADDR];
    if (selectedAddr && selectedAddr.length>0) {
        g_LastSelectedRecAddr = selectedAddr;
        g_SelectedRecAddr = selectedAddr;
    }
}

+ (void)clearCartUserDefaults
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:nil forKey:UD_KEY_LAST_SELECTED_SHOP_ID];
    [userDefaults setObject:nil forKey:UD_KEY_LAST_SELECTED_SHOP_NAME];
    [userDefaults setInteger:0 forKey:UD_KEY_LAST_SELECTED_TYPE];
    [userDefaults setObject:nil forKey:UD_KEY_LAST_SELECTED_DATE];
    [userDefaults setObject:nil forKey:UD_KEY_LAST_SELECTED_ADDR];
}

+ (Product *)getCachedProduct:(NSString *)prodId
{
    return [g_Products objectForKey:prodId];
}

+ (void)cacheProduct:(Product *)product
{
    [g_Products setObject:[product copy] forKey:product.prodId];
    [self saveProductsToFile];
}

+ (void)saveProductsToFile
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    if ([urls count] > 0) {
        NSURL *documentsFolder = urls[0];
        NSString *filePath = [[documentsFolder path] stringByAppendingPathComponent:PRODUCTS_FILE];
        [NSKeyedArchiver archiveRootObject:g_Products toFile:filePath];
    } else {
        NSLog(@"saveProductsToFile: could not find the Documents folder.");
    }
}

+ (NSMutableDictionary *)retrieveProductsFromFile
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    if ([urls count] > 0) {
        NSURL *documentsFolder = urls[0];
        NSString *filePath = [[documentsFolder path] stringByAppendingPathComponent:PRODUCTS_FILE];
        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    } else {
        NSLog(@"retrieveProductsFromFile: could not find the Documents folder.");
        return nil;
    }
}

+ (int)randomNumber
{
    return arc4random_uniform(2147483647);
}

+ (NSString *)urlEncodedString:(NSString *)string
{
    return [string stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
}

@end
