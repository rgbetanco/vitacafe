//
//  WebService.m
//  FarGlory
//
//  Created by Kevin Phua on 9/4/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "WebService.h"
#import "UIWaitingView.h"
#import "Global.h"

@interface WebService ()

@property (nonatomic, strong) NSMutableData *webData;
@property (nonatomic, strong) UIWaitingView *waitingView;
@property (nonatomic) NSString *resultName;

@end

@implementation WebService

@synthesize delegate;

- (id)init
{
	if (self = [super init]) {
		self.resultName = @"";
	}	
   
	return self;
}

- (void)postData:(NSString *)serverUrl params:(NSString *)params
{
    NSData *postData = [params dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%ld", [postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    
    g_WSLastServerUrl = serverUrl;
    g_WSLastCommand = self.resultName;
    g_WSLastCommandParams = params;

    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection) {
        self.webData = [NSMutableData data];
    } else {
        NSLog(@"Connection is NULL");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.webData setLength: 0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.webData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
    [self.delegate connectFail:self.resultName];
    [self performSelectorOnMainThread:@selector(dismissWaitingView) withObject:nil waitUntilDone:NO];
    NSLog(@"Error %@",error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"DONE. Received Bytes: %lu", (unsigned long)[self.webData length]);
    
    [self performSelectorOnMainThread:@selector(dismissWaitingView) withObject:nil waitUntilDone:NO];
    
    if (self.delegate != nil) {
        [self.delegate didReceiveData:self.webData resultName:self.resultName userObject:self.userObject];
    }
}

- (void)showWaitingView:(UIView*)parentView
{
    self.waitingView = [[UIWaitingView alloc] init];
    [self.waitingView show:parentView];
}

- (void)dismissWaitingView
{
    if (self.waitingView) {
        [self.waitingView dismiss];
        self.waitingView = nil;
    }
}

- (void)reloadLastCommand
{
    if (g_WSLastCommand && g_WSLastServerUrl && g_WSLastCommandParams) {
        self.resultName = g_WSLastCommand;
        [self postData:g_WSLastServerUrl params:g_WSLastCommandParams];
    }
}

#pragma mark - WebService functions

- (void)login:(NSString *)authkey sId:(NSString *)sid password:(NSString *)password deviceId:(NSString *)deviceId
{
    self.resultName = WS_LOGIN;
    NSString *params = [NSString stringWithFormat:@"op=login&authkey=%@&sId=%@&password=%@&ostype=ios&deviceid=%@", authkey, sid, password, deviceId];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)registerAccount:(NSString *)authkey sId:(NSString *)sid
{
    self.resultName = WS_REGISTER;
    NSString *params = [NSString stringWithFormat:@"op=register&authkey=%@&sId=%@", authkey, sid];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)authenticate:(NSString *)authkey sId:(NSString *)sid authCode:(NSString *)authCode
{
    self.resultName = WS_AUTH;
    NSString *params = [NSString stringWithFormat:@"op=auth&authkey=%@&sId=%@&auth_code=%@", authkey, sid, authCode];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)setPassword:(NSString *)authkey sId:(NSString *)vipId acckey:(NSString *)acckey password:(NSString *)password
{
    self.resultName = WS_SET_PASSWORD;
    NSString *params = [NSString stringWithFormat:@"op=set_pass&authkey=%@&sId=%@&acckey=%@&password=%@", authkey, vipId, acckey, password];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)changePassword:(NSString *)authkey sId:(NSString *)vipId acckey:(NSString *)acckey
               oldpass:(NSString *)oldpass password:(NSString *)password
{
    self.resultName = WS_CHANGE_PASSWORD;
    NSString *params = [NSString stringWithFormat:@"op=pass_change&authkey=%@&sId=%@&acckey=%@&opass=%@&password=%@", authkey, vipId, acckey, oldpass, password];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)registerDevice:(NSString *)authkey accKey:(NSString *)acckey deviceId:(NSString *)deviceId
{
    self.resultName = WS_REGISTER_DEVICE;
    NSString *params = [NSString stringWithFormat:@"op=register_device&authkey=%@&acckey=%@&ostype=ios&deviceid=%@", authkey, acckey, deviceId];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)searchMember:(NSString *)authkey mobile:(NSString *)mobile
{
    self.resultName = WS_SEARCH_MEMBER;
    NSString *params = [NSString stringWithFormat:@"op=SearchMember&authkey=%@&sId=%@", authkey, mobile];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)registerNewMember:(NSString *)authkey applyshop:(NSString *)applyshop name:(NSString *)name sex:(int)sex email:(NSString *)email mobile:(NSString *)mobile birthday:(NSString *)birthday address:(NSString *)address telephone:(NSString *)telephone idcard:(NSString *)idcard regflag:(int)regflag
{
    self.resultName = WS_REGISTER_NEW_MEMBER;
    NSString *params = [NSString stringWithFormat:@"op=RegisterNewMember&authkey=%@&applyshop=%@&name=%@&sex=%d&email=%@&mobile=%@&birthday=%@&address=%@&telephone=%@&idcard=%@&regflag=%d", authkey, applyshop, name, sex, email, mobile, birthday, address, telephone, idcard, regflag];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)updateMember:(NSString *)authkey accKey:(NSString *)acckey sex:(int)sex email:(NSString *)email mobile:(NSString *)mobile address:(NSString *)address telephone:(NSString *)telephone idcard:(NSString *)idcard
{
    self.resultName = WS_UPDATE_MEMBER;
    NSString *params = [NSString stringWithFormat:@"op=UpdateMember&authkey=%@&acckey=%@&sex=%d&email=%@&mobile=%@&address=%@&telephone=%@&idcard=%@", authkey, acckey, sex, email, mobile, address, telephone, idcard];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)getDepartment:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_DEPARTMENT;
    NSString *params = [NSString stringWithFormat:@"op=get_department&authkey=%@&acckey=%@", authkey, acckey];
    [self postData:PRODUCT_SERVER_URL params:params];
}

- (void)getProdcate00:(NSString *)authkey
{
    self.resultName = WS_GET_PRODCATE_00;
    NSString *params = [NSString stringWithFormat:@"op=get_prodcate00&authkey=%@", authkey];
    [self postData:PRODUCT_SERVER_URL params:params];
}

- (void)getProduct:(NSString *)authkey accKey:(NSString *)acckey shopId:(NSString *)shopId saleMethod:(int)saleMethod posDeptId:(NSString *)posDeptId deptId:(NSString *)deptId
{
    self.resultName = WS_GET_PRODUCT;
    NSMutableString *params = [NSMutableString stringWithFormat:@"op=get_product&authkey=%@&acckey=%@", authkey, acckey];
    if (shopId) {
        [params appendFormat:@"&shop_id=%@", shopId];
    } else {
        [params appendString:@"&shop_id="];
    }
    if (posDeptId) {
        [params appendFormat:@"&pos_dep_id=%@", posDeptId];
    } else {
        [params appendString:@"&pos_dep_id="];
    }
    if (deptId) {
        [params appendFormat:@"&dep_id=%@", deptId];
    } else {
        [params appendString:@"&dep_id="];
    }
    [params appendFormat:@"&sale_method=%d", saleMethod];
    [self postData:PRODUCT_SERVER_URL params:params];
}

- (void)getTaste:(NSString *)authkey prodId:(NSString *)prodId
{
    self.resultName = WS_GET_TASTE;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&prod_id=%@", WS_GET_TASTE, authkey, prodId];
    [self postData:PRODUCT_SERVER_URL params:params];
}

- (void)getComb:(NSString *)authkey prodId:(NSString *)prodId
{
    self.resultName = WS_GET_COMB;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&prod_id=%@", WS_GET_COMB, authkey, prodId];
    [self postData:PRODUCT_SERVER_URL params:params];
}

- (void)getPack:(NSString *)authkey prodId:(NSString *)prodId
{
    self.resultName = WS_GET_PACK;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&prod_id=%@", WS_GET_PACK, authkey, prodId];
    [self postData:PRODUCT_SERVER_URL params:params];
}

- (void)getFriendList:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_FRIEND_LIST;
    NSString *params = [NSString stringWithFormat:@"op=get_friend_list&authkey=%@&acckey=%@", authkey, acckey];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)inviteFriend:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId
{
    self.resultName = WS_INVITE_FRIEND;
    NSString *params = [NSString stringWithFormat:@"op=invite&authkey=%@&acckey=%@&fId=%@", authkey, acckey, fId];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)friendAgree:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId
{
    self.resultName = WS_FRIEND_AGREE;
    NSString *params = [NSString stringWithFormat:@"op=friend_agree&authkey=%@&acckey=%@&fId=%@", authkey, acckey, fId];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)friendDeny:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId
{
    self.resultName = WS_FRIEND_DENY;
    NSString *params = [NSString stringWithFormat:@"op=friend_deny&authkey=%@&acckey=%@&fId=%@", authkey, acckey, fId];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)friendDelete:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId
{
    self.resultName = WS_FRIEND_DELETE;
    NSString *params = [NSString stringWithFormat:@"op=friend_delete&authkey=%@&acckey=%@&fId=%@", authkey, acckey, fId];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)searchLastPointValue:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_SEARCH_LAST_POINT_VALUE;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@", WS_SEARCH_LAST_POINT_VALUE, authkey, acckey];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)vipPointMoveShop:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId point:(NSString *)point
{
    self.resultName = WS_VIP_POINT_MOVE_SHOP;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&fId=%@&point=%@", WS_VIP_POINT_MOVE_SHOP, authkey, acckey, fId, point];
    [self postData:MEMBER_SERVER_URL params:params];
}
                           
- (void)getTransactionLogs:(NSString *)authkey accKey:(NSString *)acckey isPoint:(BOOL)isPoint
{
    // isPoint YES=point, NO=ticket
    self.resultName = WS_GET_TRANSACTION_LOGS;
    
    NSString *type;
    if (isPoint) {
        type = @"point";
    } else {
        type = @"ticket";
    }
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&tp=%@", WS_GET_TRANSACTION_LOGS, authkey, acckey, type];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)getVirtualCard:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_VIRTUAL_CARD;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@", WS_GET_VIRTUAL_CARD, authkey, acckey];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)getTicketList:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_TICKET_LIST;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@", WS_GET_TICKET_LIST, authkey, acckey];
    [self postData:TICKET_SERVER_URL params:params];
}

- (void)ticketGift:(NSString *)authkey accKey:(NSString *)acckey fId:(NSString *)fId serno:(NSString *)serno tkno:(NSString *)tkno
{
    self.resultName = WS_TICKET_GIFT;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&fId=%@&serno=%@&tkno=%@", WS_TICKET_GIFT, authkey, acckey, fId, serno, tkno];
    [self postData:TICKET_SERVER_URL params:params];
}

- (void)useTicket:(NSString *)authkey accKey:(NSString *)acckey tkno:(NSString *)tkno
{
    self.resultName = WS_USE_TICKET;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&tkno=%@", WS_USE_TICKET, authkey, acckey, tkno];
    [self postData:TICKET_SERVER_URL params:params];
}

- (void)receiveTicket:(NSString *)authkey accKey:(NSString *)acckey tkno:(NSString *)tkno
{
    self.resultName = WS_RECEIVE_TICKET;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&tkno=%@", WS_RECEIVE_TICKET, authkey, acckey, tkno];
    [self postData:TICKET_SERVER_URL params:params];
}

- (void)getVipBonusList:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_VIP_BONUS_LIST;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@", WS_GET_VIP_BONUS_LIST, authkey, acckey];
    [self postData:MEMBER_SERVER_URL params:params];
}

- (void)getNews:(NSString *)authkey pmId:(NSInteger)pmId
{
    self.resultName = WS_GET_NEWS;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&pmid=%ld", WS_GET_NEWS, authkey, pmId];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getPubcode:(NSString *)authkey
{
    self.resultName = WS_GET_PUBCODE;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&dtype=newscate", WS_GET_PUBCODE, authkey];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getMessageList:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_MESSAGE_LIST;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&ostype=ios", WS_GET_MESSAGE_LIST, authkey, acckey];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)readMessage:(NSString *)authkey accKey:(NSString *)acckey infoId:(NSString *)infoId
{
    self.resultName = WS_READ_MESSAGE;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&infoid=%@", WS_READ_MESSAGE, authkey, acckey, infoId];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getPageList:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_PAGE_LIST;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@", WS_GET_PAGE_LIST, authkey, acckey];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getShareMessage:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_SHARE_MESSAGE;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@", WS_GET_SHARE_MESSAGE, authkey, acckey];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getBanner:(NSString *)authkey
{
    self.resultName = WS_GET_BANNER;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@", WS_GET_BANNER, authkey];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getShop:(NSString *)authkey shopId:(NSString *)shopId areaId:(NSString *)areaId pstAreaId:(NSString *)pstAreaId city:(NSString *)city pstAreaSNo:(NSString *)pstAreaSNo
{
    self.resultName = WS_GET_SHOP;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&shop_id=%@&area_id=%@&pstarea_id=%@&city=%@&pstarea_sno=%@", WS_GET_SHOP, authkey, shopId, areaId, pstAreaId, city, pstAreaSNo];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getPostArea:(NSString *)authkey
{
    self.resultName = WS_GET_POST_AREA;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@", WS_GET_POST_AREA, authkey];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getPostArea00:(NSString *)authkey areaId:(NSString *)areaId
{
    self.resultName = WS_GET_POST_AREA00;
    NSString *params;
    if (areaId && areaId.length>0) {
        params = [NSString stringWithFormat:@"op=%@&authkey=%@&area_id=%@", WS_GET_POST_AREA00, authkey, areaId];
    } else {
        params = [NSString stringWithFormat:@"op=%@&authkey=%@", WS_GET_POST_AREA00, authkey];
    }
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getPostArea01:(NSString *)authkey postAreaId:(NSString *)postAreaId
{
    self.resultName = WS_GET_POST_AREA01;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&pstarea_id=%@", WS_GET_POST_AREA01, authkey, postAreaId];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getPostAreaRd:(NSString *)authkey postAreaId:(NSString *)postAreaId postAreaSNo:(NSString *)postAreaSNo
{
    self.resultName = WS_GET_POST_AREARD;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&pstarea_id=%@&pstarea_sno=%@", WS_GET_POST_AREARD, authkey, postAreaId, postAreaSNo];
    [self postData:BASIC_SERVER_URL params:params];
}

- (void)getShop00Config:(NSString *)authkey shopId:(NSString *)shopId
{
    self.resultName = WS_GET_SHOP00_CONFIG;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&shop_id=%@", WS_GET_SHOP00_CONFIG, authkey, shopId];
    [self postData:ORDER_SERVER_URL params:params];
}

- (void)getSaleMethod:(NSString *)authkey shopId:(NSString *)shopId
{
    self.resultName = WS_GET_SALE_METHOD;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&shop_id=%@", WS_GET_SALE_METHOD, authkey, shopId];
    [self postData:ORDER_SERVER_URL params:params];
}

- (void)getMealTime:(NSString *)authkey shopId:(NSString *)shopId date:(NSString *)date
{
    self.resultName = WS_GET_MEAL_TIME;
    NSMutableString *params = [NSMutableString stringWithFormat:@"op=%@&authkey=%@", WS_GET_MEAL_TIME,authkey];
    if (shopId) {
        [params appendFormat:@"&shop_id=%@", shopId];
    } else {
        [params appendString:@"&shop_id="];
    }
    if (date) {
        [params appendFormat:@"&date=%@", date];
    } else {
        [params appendString:@"&date="];
    }
    [self postData:ORDER_SERVER_URL params:params];
}

- (void)addOrder:(NSString *)authkey accKey:(NSString *)acckey shopId:(NSString *)shopId sourceType:(int)sourceType mealDate:(NSString *)mealDate saleMethod:(int)saleMethod paymentTerms:(int)paymentTerms recAddr:(NSString *)recAddr webOrder01:(NSString *)webOrder01 webOrder011:(NSString *)webOrder011
{
    self.resultName = WS_ADD_ORDER;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&shop_id=%@&source_type=%d&meal_date=%@&sale_method=%d&payment_terms=%d&rec_addr=%@&weborder01=%@&weborder011=%@", WS_ADD_ORDER, authkey, acckey, shopId, sourceType, mealDate, saleMethod, paymentTerms, recAddr, webOrder01, webOrder011];
    [self postData:ORDER_SERVER_URL params:params];
}

- (void)getOrderList:(NSString *)authkey accKey:(NSString *)acckey
{
    self.resultName = WS_GET_ORDER_LIST;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@", WS_GET_ORDER_LIST, authkey, acckey];
    [self postData:ORDER_SERVER_URL params:params];
}

- (void)getOrderInfo:(NSString *)authkey accKey:(NSString *)acckey worderId:(NSString *)worderId
{
    self.resultName = WS_GET_ORDER_INFO;
    NSString *params = [NSString stringWithFormat:@"op=%@&authkey=%@&acckey=%@&worder_id=%@", WS_GET_ORDER_INFO, authkey, acckey, worderId];
    [self postData:ORDER_SERVER_URL params:params];
}

@end
