//
//  TicketDetailViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/1/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "TicketDetailViewController.h"
#import "Global.h"
#import "UIImageView+WebCache.h"
#import "DQAlertView.h"
#import "TicketTransferViewController.h"
#import "TicketUseViewController.h"
#import "RaiseAlertView.h"
#import "WebService.h"
#import "KLCPopup.h"

#define HEADER_IMAGE_HEIGHT     180

@interface TicketDetailViewController ()<WebServiceDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgTicket;
@property (weak, nonatomic) IBOutlet UILabel *lblTicketName;
@property (weak, nonatomic) IBOutlet UILabel *lblTicketValidDate;
@property (weak, nonatomic) IBOutlet UITextView *tvTicketDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnTransferTicket;
@property (weak, nonatomic) IBOutlet UIButton *btnUseTicket;

@property (weak, nonatomic) UITextField *txtPassword;

@end

@implementation TicketDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
    self.imgTicket.contentMode = UIViewContentModeScaleAspectFill;
    self.imgTicket.clipsToBounds = YES;
    
    self.btnTransferTicket.titleLabel.text = NSLocalizedString(@"Transfer", nil);
    self.btnUseTicket.titleLabel.text = NSLocalizedString(@"UseNow", nil);

    // Clear all navbar subviews
    NSArray *navbarSubviews = self.navigationController.navigationBar.subviews;
    for (UIView *subView in navbarSubviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [subView removeFromSuperview];
        }
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.btnTransferTicket.backgroundColor = [Global colorWithType:COLOR_TEXT_BTN];
    self.btnTransferTicket.titleLabel.text = NSLocalizedString(@"Transfer", nil);
    self.btnUseTicket.backgroundColor = [Global colorWithType:COLOR_TEXT_BTN];
    self.btnUseTicket.titleLabel.text = NSLocalizedString(@"UseNow", nil);
    
    if (self.ticket) {
        [self processTicket];
    } 
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = NSLocalizedString(@"TicketHistory", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)processTicket {
    NSString *ticketName = [self.ticket objectForKey:@"ProdName"];
    NSString *beginDate = [self.ticket objectForKey:@"bdate"];
    NSString *endDate = [self.ticket objectForKey:@"edate"];
    NSString *validDate = [NSString stringWithFormat:@"%@ ~ %@", beginDate, endDate];
    
    self.lblTicketName.text = ticketName;
    self.lblTicketValidDate.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"ValidDate", nil), validDate];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[[self.ticket objectForKey:@"htmlbody"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, attributedString.length)];
    
    UIFont *font = [UIFont systemFontOfSize:14.0];
    [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, attributedString.length)];

    self.tvTicketDesc.attributedText = attributedString;
    
    NSString *imageUrl = [_ticket objectForKey:@"imgfile1"];
    if ([imageUrl isKindOfClass:[NSNull class]]) {
        //No image
    } else {
        NSString *imagePath = [NSString stringWithFormat:@"%@%@", TICKET_IMAGE_PATH, imageUrl];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:imagePath]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                                 NSLog(@"Received image %ld of %ld bytes", (long)receivedSize, (long)expectedSize);
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    NSLog(@"Received image width=%.1f, height=%.lf", image.size.width, image.size.height);
                                    
                                    UIImage *resizedImage = [self imageResize:image];
                                    
                                    NSLog(@"Resized image width=%.1f, height=%.lf", resizedImage.size.width, resizedImage.size.height);
                                    
                                    self.imgTicket.image = resizedImage;
                                }
                            }];
    }
}

- (IBAction)onBtnTransferTicket:(id)sender {
    TicketTransferViewController *ticketTransferController = [[TicketTransferViewController alloc] initWithNibName:@"TicketTransferViewController" bundle:nil];
    [ticketTransferController setTicket:self.ticket];
    
    // Hide back button text on next page
    self.title = @"";
    
    [self.navigationController pushViewController:ticketTransferController animated:YES];
}

- (IBAction)onBtnUseTicket:(id)sender {
    NSString *ticketType = [self.ticket objectForKey:@"ticket_type"];
    if ([ticketType isEqualToString:@"點數票卷"]) {
        RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"UseNowTitle", nil)
                                                                  message:NSLocalizedString(@"UseNowMsg", nil)
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                         otherButtonTitle:NSLocalizedString(@"Confirm", nil)];
        
        alertView.otherButtonAction = ^{
            // Use ticket
            NSString *ticketNo = [self.ticket objectForKey:@"tkno"];
            WebService *ws = [WebService new];
            ws.delegate = self;
            [ws useTicket:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] tkno:ticketNo];
            [ws showWaitingView:self.view];
        };
        [alertView show];
    } else {
        // Generate QRCode
        NSString *ticketNo = [self.ticket objectForKey:@"tkno"];
        CGImageRef qrCodeImage = [Global createQRImageForString:ticketNo size:CGSizeMake(120,120)];
        
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(screenWidth/2-75, screenHeight/2-75, 150, 150)];
        contentView.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imgBarcode = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 120, 120)];
        imgBarcode.image = [UIImage imageWithCGImage:qrCodeImage];
        [contentView addSubview:imgBarcode];
        
        UILabel *lblTicketName = [[UILabel alloc] initWithFrame:CGRectMake(15, 130, 120, 20)];
        lblTicketName.text = ticketNo;
        lblTicketName.textAlignment = NSTextAlignmentCenter;
        [contentView addSubview:lblTicketName];

        // Popup QRCode
        KLCPopup *popup = [KLCPopup popupWithContentView:contentView];
        [popup show];
        
        /*
        TicketUseViewController *ticketUseController = [[TicketUseViewController alloc] initWithNibName:@"TicketUseViewController" bundle:nil];
        [ticketUseController setTicket:self.ticket];
        
        // Hide back button text on next page
        self.title = @"";
        
        [self.navigationController pushViewController:ticketUseController animated:YES];
         */
    }
}

- (UIImage *)imageResize:(UIImage*)image
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat ratio = image.size.width/image.size.height;
    CGSize newSize;
    CGPoint offset = CGPointMake(0, 0);
    
    NSLog(@"Image width=%.1f, height=%.1f, ratio=%.1f", image.size.width, image.size.height, ratio);
    
    if (ratio <= 1.0) {
        // Width 100%
        CGFloat newWidth = screenWidth;
        CGFloat newHeight = newWidth / ratio;
        newSize = CGSizeMake(newWidth, newHeight);
        if (newHeight > HEADER_IMAGE_HEIGHT) {
            offset = CGPointMake(0, self.imgTicket.frame.size.height);
        }
    } else {
        CGFloat newHeight = HEADER_IMAGE_HEIGHT;
        CGFloat newWidth = ratio * newHeight;
        newSize = CGSizeMake(newWidth, newHeight);
    }
    
    NSLog(@"New image size w=%.1f, h=%.1f", newSize.width, newSize.height);
    
    CGRect resizeRect = CGRectMake(offset.x, offset.y, newSize.width, newSize.height);
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    } else {
        UIGraphicsBeginImageContext(newSize);
    }
    [image drawInRect:resizeRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_USE_TICKET] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"UseNow", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
