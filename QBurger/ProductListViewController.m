//
//  ProductListViewController.m
//  PosApp
//
//  Created by Kevin Phua on 10/12/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductListCell.h"
#import "Global.h"
#import "UIImageView+WebCache.h"
#import "ProductDetailViewController.h"
#import "Product.h"
#import "UIImageView+WebCache.h"
#import "ProductViewCell.h"
#import "ShoppingCartViewController.h"
#import "CartItemViewController.h"
#import "UIView+Toast.h"
#import "CartItem.h"
#import "Combo.h"
#import "WebOrder01.h"
#import "AppCategory.h"

@interface ProductListViewController () <WebServiceDelegate, ProductViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *products;
@property (strong, nonatomic) NSMutableArray *productsQty;
@property (strong, nonatomic) NSMutableDictionary *combosDict;      // NSDictionary of combo arrays, key = product id

@property (nonatomic, weak) IBOutlet UIButton *btnAddToCart;
@property (nonatomic, weak) IBOutlet UIButton *btnCancel;

@property (nonatomic) BOOL isSelected;

@end

@implementation ProductListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.title = self.deptName;
    self.products = [NSMutableArray new];
    self.productsQty = [NSMutableArray new];
    self.combosDict = [NSMutableDictionary new];
    
    UINib *cellNib = [UINib nibWithNibName:@"ProductViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"ProductViewCell"];
    //[self.collectionView setBackgroundColor:[UIColor grayColor]];
    
    [self.btnAddToCart setTitle:NSLocalizedString(@"AddToShoppingCart", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_isOrder) {
        self.title = NSLocalizedString(@"Order", nil);
        [self updateCart:PARENT_PAGE_DEFAULT];
    } else {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.btnAddToCart attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.btnCancel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0]];
        
        self.title = self.deptName;
        [self updateFavorites:PARENT_PAGE_DEFAULT];
        [self updateMessages:PARENT_PAGE_DEFAULT];
        [self updateCart:PARENT_PAGE_DEFAULT];
    }
    
    self.isSelected = NO;
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getProduct:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] shopId:self.shopId saleMethod:1 posDeptId:self.posDeptId deptId:self.deptId];
    [ws showWaitingView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnAddShoppingCart:(id)sender {
    int totalQty = 0;
    for (int i=0; i<_productsQty.count; i++) {
        NSNumber *quantity = [_productsQty objectAtIndex:i];
        int qty = [quantity intValue];
        totalQty += qty;
        if (qty > 0) {
            CartItem *item = [CartItem new];
            Product *product = [_products objectAtIndex:i];
            item.product = product;
            
            if (product.isComb) {
                NSMutableArray *orderItems = [NSMutableArray new];
                int parentSno = 1;
                int worderSno = 1;
                
                // Combo parent item
                WebOrder01 *parentItem = [WebOrder01 new];
                parentItem.prodId = product.prodId;
                parentItem.prodName = product.prodName1;
                parentItem.salePrice = [product.price1 intValue];
                parentItem.worderSno = worderSno++;
                parentItem.qty = qty;
                parentItem.combType = 1;    // 套餐父項
                parentItem.combQty = 0;
                parentItem.itemDisc = 0;
                parentItem.tasteMemo = @"";
                [orderItems addObject:parentItem];
                
                NSArray *combos = [_combosDict objectForKey:product.prodId];
                if (!combos) {
                    NSLog(@"*** NO COMBOS!!! ****");
                }
                
                int childSno = 1;
                for (Combo *combo in combos) {
                    // Combo child item
                    WebOrder01 *childItem = [WebOrder01 new];
                    
                    // Use default combo item
                    childItem.prodId = combo.prodId;
                    childItem.prodName = combo.prodName;
                    childItem.salePrice = combo.price;
                    childItem.worderSno = worderSno++;
                    childItem.qty = qty;
                    childItem.combType = 2;     // 套餐子項
                    childItem.combQty = combo.quantity;
                    childItem.combSaleSno = parentSno;
                    childItem.combSno = childSno;
                    parentItem.itemDisc = 0;
                    childItem.tasteMemo = @"";
                    [orderItems addObject:childItem];
                }
                
                item.webOrder01s = orderItems;
            } else {
                WebOrder01 *order = [WebOrder01 new];
                order.worderSno = 1;
                order.prodId = product.prodId;
                order.prodName = product.prodName1;
                order.salePrice = [product.price1 intValue];
                order.qty = qty;
                order.combSaleSno = 0;
                order.combSno = 0;
                order.combType = 0;
                order.combQty = 1;
                order.tasteMemo = @"";
                item.webOrder01s = [NSArray arrayWithObject:order];
            }
            
            [Global addShoppingCart:item];
            [Global cacheProduct:product];
        }
    }
    
    if (totalQty == 0) {
        [self.view makeToast:NSLocalizedString(@"QuantityNotSet", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    }
            
    // Reset quantity
    [self resetQuantity];
    [self updateCart:PARENT_PAGE_DEFAULT];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:NSLocalizedString(@"ItemAddedToShoppingCart", nil)
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"Confirm", nil)
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action) {
                             [self.navigationController popViewControllerAnimated:YES];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)onBtnCancel:(id)sender {
    // Reset quantity
    [self resetQuantity];
}

- (void)resetQuantity {
    for (int i=0; i<_productsQty.count; i++) {
        NSNumber *quantity = [_productsQty objectAtIndex:i];
        int qty = [quantity intValue];
        if (qty > 0) {
            qty = 0;
            quantity = [NSNumber numberWithInt:qty];
            [_productsQty setObject:quantity atIndexedSubscript:i];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_collectionView reloadData];
    });
}

- (void)getComb:(NSString *)prodId {
    NSLog(@"Getting combos for product %@", prodId);
    WebService *ws = [WebService new];
    ws.delegate = self;
    ws.userObject = prodId;
    [ws getComb:[Global cfgAppId] prodId:prodId];
}

//==================================================================
#pragma mark - UICollectionViewDelegate
//==================================================================

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _products.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ProductViewCell *cell = (ProductViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ProductViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    Product *product = [_products objectAtIndex:[indexPath row]];
    
    cell.lblTitle.text = product.prodName1;
    cell.lblPrice.text = [NSString stringWithFormat:@"$%@", product.price1];
    
    NSArray *appCates = product.appCate;
    int appCateIndex = 0;
    for (AppCategory *appCate in appCates) {
        if (appCateIndex == 0) {
            [cell.imgCat1 sd_setImageWithURL:[NSURL URLWithString:appCate.imgFile1]
                       placeholderImage:nil];
        } else if (appCateIndex == 1) {
            [cell.imgCat2 sd_setImageWithURL:[NSURL URLWithString:appCate.imgFile1]
                            placeholderImage:nil];
        }
        appCateIndex++;
    }
    
    UIImageView *imgView = cell.imgProduct;
    NSString *imgPath = product.imgFile1;
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgPath]
               placeholderImage:[UIImage imageNamed:@"login_logo"]];
    
    // Quantity view
    if (_isOrder) {
        [cell.btnAdd setHidden:NO];
        [cell.btnSubtract setHidden:NO];
        [cell.lblQty setHidden:NO];
        int quantity = [[_productsQty objectAtIndex:indexPath.row] intValue];
        cell.lblQty.text = [NSString stringWithFormat:@"%d", quantity];
    } else {
        [cell.btnAdd setHidden:YES];
        [cell.btnSubtract setHidden:YES];
        [cell.lblQty setHidden:YES];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isSelected)
        return;

    // Prevent multiple selections
    _isSelected = YES;
    
    Product *product = [self.products objectAtIndex:[indexPath row]];
    
    // Open ProductDetailViewController
    ProductDetailViewController *productDetailController = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
    productDetailController.isOrder = _isOrder;
    [productDetailController setProduct:product];
    
    // Hide back button text on next page
    self.title = @"";
    
    [self.navigationController pushViewController:productDetailController animated:YES];
}

//==================================================================
#pragma mark - UICollectionViewDelegateFlowLayout
//==================================================================

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat collectionViewWidth = collectionView.contentSize.width;
    // Leave 10 pixel spacing
    CGFloat cellWidth = (collectionViewWidth / 2) - 15;
    CGFloat cellHeight = 3 * cellWidth / 4;
    
    if (_isOrder) {
        return CGSizeMake(cellWidth, cellHeight+95);
    } else {
        return CGSizeMake(cellWidth, cellHeight+55);
    }
}

//==================================================================
#pragma ProductViewCellDelegate
//==================================================================

- (void)onBtnAdd:(UIButton *)sender {
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
    
    if (indexPath) {
        NSNumber *quantity = [_productsQty objectAtIndex:indexPath.row];
        int qty = [quantity intValue];
        ++qty;
        quantity = [NSNumber numberWithInt:qty];
        [_productsQty setObject:quantity atIndexedSubscript:indexPath.row];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_collectionView reloadData];
        });
    }
}

- (void)onBtnSubtract:(UIButton *)sender {
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
    
    if (indexPath) {
        NSNumber *quantity = [_productsQty objectAtIndex:indexPath.row];
        int qty = [quantity intValue];
        --qty;
        if (qty < 0)
            qty = 0;
        quantity = [NSNumber numberWithInt:qty];
        [_productsQty setObject:quantity atIndexedSubscript:indexPath.row];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_collectionView reloadData];
        });
    }
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_PRODUCT] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Get product message = %@", message);
                NSLog(@"Total %@ products", datacnt);
                
                NSArray *products = (NSArray *)[jsonObject objectForKey:@"product"];
                [_products removeAllObjects];
                [_productsQty removeAllObjects];
                NSString *imgPath = [jsonObject objectForKey:@"imgpath"];
                for (NSDictionary *product in products) {
                    Product *prodInfo = [Product new];
                    prodInfo.prodId = [product objectForKey:@"prod_id"];
                    prodInfo.prodContent = [product objectForKey:@"prod_content"];
                    prodInfo.prodName1 = [product objectForKey:@"prod_name1"];
                    prodInfo.prodName2 = [product objectForKey:@"prod_name2"];
                    
                    double price1 = [[product objectForKey:@"price1"] doubleValue];
                    prodInfo.price1 = [NSString stringWithFormat:@"%d", (int)price1];
                    double price2 = [[product objectForKey:@"price2"] doubleValue];
                    prodInfo.price2 = [NSString stringWithFormat:@"%d", (int)price2];
                    
                    prodInfo.imgFile1 = [NSString stringWithFormat:@"%@%@", imgPath, [product objectForKey:@"imgfile1"]];
                    prodInfo.depId = [product objectForKey:@"dep_id"];
                    prodInfo.enable = [[product objectForKey:@"enable"] boolValue];
                    prodInfo.isRedeem = [[product objectForKey:@"isredeem"] boolValue];
                    prodInfo.isPack = [[product objectForKey:@"ispack"] boolValue];
                    prodInfo.isComb = [[product objectForKey:@"iscomb"] boolValue];
                    prodInfo.stopSale = [[product objectForKey:@"stop_sale"] boolValue];
                    
                    NSArray *appCates = (NSArray *)[product objectForKey:@"app_cate"];
                    NSMutableArray *appCategories = [NSMutableArray new];
                    for (NSDictionary *appCate in appCates) {
                        AppCategory *appCategory = [AppCategory new];
                        appCategory.imgFile1 = [NSString stringWithFormat:@"%@%@", CATE_IMAGE_PATH, [appCate objectForKey:@"imgfile1"]];
                        appCategory.imgFile2 = [NSString stringWithFormat:@"%@%@", CATE_IMAGE_PATH, [appCate objectForKey:@"imgfile2"]];
                        appCategory.serNo = [[appCate objectForKey:@"serno"] intValue];
                        appCategory.subject = [appCate objectForKey:@"subject"];
                        [appCategories addObject:appCategory];
                    }
                    prodInfo.appCate = appCategories;
                    [_products addObject:prodInfo];
                    
                    // Init product quantity
                    [_productsQty addObject:[NSNumber numberWithInt:0]];
                    
                    // Get combos
                    if (prodInfo.isComb) {
                        [self getComb:prodInfo.prodId];
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_collectionView reloadData];
                });
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
            [self.collectionView setHidden:NO];
            [self.viewNetworkError setHidden:YES];
        } else if ([resultName compare:WS_GET_COMB] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSMutableArray *combos = [NSMutableArray new];
                NSArray *combs = [jsonObject objectForKey:@"comb"];
                for (NSDictionary *comb in combs) {
                    Combo *combItem = [Combo new];
                    combItem.combSno = [comb objectForKey:@"comb_sno"];
                    combItem.prodId = [comb objectForKey:@"prod_id"];
                    combItem.prodName = [comb objectForKey:@"prod_name1"];
                    combItem.quantity = [[comb objectForKey:@"quantity"] intValue];
                    combItem.price = [[comb objectForKey:@"price"] intValue];
                    combItem.isDefault = [[comb objectForKey:@"isdefault"] boolValue];
                    combItem.imgFile = [comb objectForKey:@"imgfile1"];
                    
                    NSArray *details = [comb objectForKey:@"detail"];
                    NSMutableArray *detailsItem = [NSMutableArray new];
                    for (NSDictionary *detail in details) {
                        Combo *combDetailItem = [Combo new];
                        combDetailItem.combSno = [detail objectForKey:@"comb_sno"];
                        combDetailItem.prodId = [detail objectForKey:@"prod_id"];
                        combDetailItem.prodName = [detail objectForKey:@"prod_name1"];
                        combDetailItem.quantity = [[detail objectForKey:@"quantity"] intValue];
                        combDetailItem.price = [[detail objectForKey:@"price"] intValue];
                        [detailsItem addObject:combDetailItem];
                    }
                    
                    combItem.combDetails = detailsItem;
                    [combos addObject:combItem];
                }
                
                if (userObject) {
                    NSString *prodId = (NSString *)userObject;
                    [_combosDict setObject:combos forKey:prodId];
                } else {
                    NSLog(@"ERROR: No product ID!!");
                }
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_GET_MESSAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ messages", datacnt);
                
                NSArray *messages = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (messages) {
                    int unreadMessages = 0;
                    for (NSDictionary *message in messages) {
                        NSString *readTime = [message objectForKey:@"read_time"];
                        if ([readTime isEqualToString:@""]) {
                            unreadMessages++;
                        }
                    }
                    
                    [g_MemberInfo setObject:[NSNumber numberWithInt:unreadMessages] forKey:INFO_KEY_MSGS_UNREAD];
                    //[self updateNavigationBarButtons];
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.collectionView setHidden:YES];
    [self.viewNetworkError setHidden:NO];
}


@end
