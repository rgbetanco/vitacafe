//
//  CartViewCell.m
//  QBurger
//
//  Created by Kevin Phua on 9/23/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "CartViewCell.h"

@implementation CartViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onBtnDelete:(id)sender {
    if (self.delegate) {
        [self.delegate onBtnDelete:sender];
    }
}

- (IBAction)onBtnEdit:(id)sender {
    if (self.delegate) {
        [self.delegate onBtnEdit:sender];
    }
}

@end
