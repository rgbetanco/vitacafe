//
//  ChangeInfoViewController.m
//  QBurger
//
//  Created by Kevin Phua on 11/17/16.
//  Copyright (c) 2016 hagarsoft. All rights reserved.
//

#import "ChangeInfoViewController.h"
#import "SignupViewController.h"
#import "CustomBadge.h"
#import "Global.h"
#import "DLRadioButton.h"

#define kOFFSET_FOR_KEYBOARD    160.0

@interface ChangeInfoViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblSex;
@property (weak, nonatomic) IBOutlet DLRadioButton *btnMale;
@property (weak, nonatomic) IBOutlet DLRadioButton *btnFemale;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblTelephone;
@property (weak, nonatomic) IBOutlet UITextField *txtTelephone;
@property (weak, nonatomic) IBOutlet UILabel *lblIdCard;
@property (weak, nonatomic) IBOutlet UITextField *txtIdCard;

@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;

@end

@implementation ChangeInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"ChangeDetails", nil);
    
    _lblSex.text = NSLocalizedString(@"Sex", nil);
    
    NSMutableArray *otherButtons = [NSMutableArray new];
    [_btnMale setTitle:NSLocalizedString(@"Male", nil) forState:UIControlStateNormal];
    _btnMale.titleLabel.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
    _btnMale.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
    [_btnMale setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnMale setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
    [_btnMale setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    _btnMale.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _btnMale.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
    [_btnFemale setTitle:NSLocalizedString(@"Female", nil) forState:UIControlStateNormal];
    _btnFemale.titleLabel.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
    _btnFemale.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
    [_btnFemale setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnFemale setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
    [_btnFemale setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    _btnFemale.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _btnFemale.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
    [otherButtons addObject:_btnFemale];
    _btnMale.otherButtons = otherButtons;

    _lblEmail.text = NSLocalizedString(@"Email", nil);
    _lblMobile.text = NSLocalizedString(@"MobilePhone", nil);
    _lblAddress.text = NSLocalizedString(@"Address", nil);
    _lblTelephone.text = NSLocalizedString(@"Telephone", nil);
    _lblIdCard.text = NSLocalizedString(@"IdCard", nil);
    
    NSString *sex = [g_MemberInfo objectForKey:INFO_KEY_SEX];
    if (sex && sex.length>0) {
        if ([sex isEqualToString:@"0"]) {
            _btnMale.selected = YES;
        } else {
            _btnFemale.selected = YES;
        }
    } else {
        _btnMale.selected = YES;
    }
    
    NSString *email = [g_MemberInfo objectForKey:INFO_KEY_EMAIL];
    if (email && email.length>0) {
        _txtEmail.text = email;
    } else {
        _txtEmail.text = @"";
    }

    NSString *mobile = [g_MemberInfo objectForKey:INFO_KEY_MOBILE];
    if (mobile && mobile.length>0) {
        _txtMobile.text = mobile;
    } else {
        _txtMobile.text = @"";
    }

    NSString *address = [g_MemberInfo objectForKey:INFO_KEY_ADDRESS];
    if (address && address.length>0) {
        _txtAddress.text = address;
    } else {
        _txtAddress.text = @"";
    }

    NSString *telephone = [g_MemberInfo objectForKey:INFO_KEY_TELEPHONE];
    if (telephone && telephone.length>0) {
        _txtTelephone.text = telephone;
    } else {
        _txtTelephone.text = @"";
    }

    NSString *idCard = [g_MemberInfo objectForKey:INFO_KEY_ID_CARD];
    if (idCard && idCard.length>0) {
        _txtIdCard.text = idCard;
    } else {
        _txtIdCard.text = @"";
    }
    
    [self.btnConfirm setTitle:NSLocalizedString(@"Confirm", nil) forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapView:)];
    [self.view addGestureRecognizer:tapView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnConfirm:(id)sender {
    WebService *ws = [[WebService alloc] init];
    [ws setDelegate:self];
    [ws updateMember:[Global cfgAppId]
              accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]
                 sex:[_btnMale isSelected] ? 0 : 1
               email:_txtEmail.text
              mobile:_txtMobile.text
             address:[_txtAddress.text stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]
           telephone:_txtTelephone.text
              idcard:_txtIdCard.text];
    [ws showWaitingView:self.view];
}

- (void)onTapView:(UITapGestureRecognizer *)tapGesture
{
    // Dismiss keyboard
    [self.view endEditing:YES];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        long code = [[jsonObject objectForKey:@"code"] longValue];
        if ([resultName compare:WS_UPDATE_MEMBER] == NSOrderedSame) {
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ChangePassword", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                
                // Update member info
                [g_MemberInfo setObject:([_btnMale isSelected] ? @"0" : @"1") forKey:INFO_KEY_SEX];
                [g_MemberInfo setObject:_txtEmail.text forKey:INFO_KEY_EMAIL];
                [g_MemberInfo setObject:_txtMobile.text forKey:INFO_KEY_MOBILE];
                [g_MemberInfo setObject:_txtAddress.text forKey:INFO_KEY_ADDRESS];
                [g_MemberInfo setObject:_txtTelephone.text forKey:INFO_KEY_TELEPHONE];
                [g_MemberInfo setObject:_txtIdCard.text forKey:INFO_KEY_ID_CARD];
                
                // Go back
                [self.navigationController popViewControllerAnimated:YES];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ChangeInfoFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        }     }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
