//
//  LocationsDetailViewController.h
//  QBurger
//
//  Created by Kevin Phua on 8/3/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "BaseViewController.h"
#import "ShopInfo.h"

@interface LocationsDetailViewController : BaseViewController

@property (nonatomic, strong) ShopInfo *shopInfo;

@end
