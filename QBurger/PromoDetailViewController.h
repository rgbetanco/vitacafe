//
//  PromoDetailViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/5/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "BaseViewController.h"

@interface PromoDetailViewController : BaseViewController

@property (nonatomic, strong) NSDictionary *promo;
@property (nonatomic, strong) NSString *promoName;

@end
