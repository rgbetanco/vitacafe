//
//  QBurgerSplashScreen.h
//  QBSplash_IOS
//
//  Created by Chin-Yu Hsu on 7/25/16.
//  Copyright © 2016 Chinsoft Co., Ltd. All rights reserved.
//

#import "CSHSplashScreen.h"

@interface QBurgerSplashScreen : CSHSplashScreen
+ (instancetype) showSplashScreen : (UIViewController *)parent;
@end
