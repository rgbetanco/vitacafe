//
//  ShoppingCartViewController.h
//  QBurger
//
//  Created by Kevin Phua on 9/11/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ShoppingCartViewController : BaseViewController

@end
