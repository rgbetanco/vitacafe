//
//  ItemViewCell.m
//  QBurger
//
//  Created by Kevin Phua on 10/28/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "ItemViewCell.h"

@implementation ItemViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    self.lblName.text = @"";
    self.lblTaste.text = @"";
    self.btnChange.hidden = YES;
    self.btnTaste.hidden = YES;    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onBtnChange:(id)sender {
    if (self.delegate) {
        [self.delegate onBtnChange:sender];
    }
}

- (IBAction)onBtnTaste:(id)sender {
    if (self.delegate) {
        [self.delegate onBtnTaste:sender];
    }
}


@end
