//
//  CSHSplashScreen.h
//  QBSplash_IOS
//
//  Created by Chin-Yu Hsu on 7/25/16.
//  Copyright © 2016 Chinsoft Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSHSplashScreen : UIView
- (id)initWithFrame: (CGRect)frame withNibNamed : (NSString *)nibName duration : (NSTimeInterval) duration;
+ (instancetype) splashScreenOnSuperView : (UIView *)superview withNibNamed : (NSString *)nibName duration : (NSTimeInterval) duration;
+ (instancetype) showSplashScreen : (UIViewController *)parent withNibNamed : (NSString *)nibName duration : (NSTimeInterval) duration;
- (void) initSplash;
- (void) fadeOutAndClose;
- (void) queueClose : (NSTimeInterval) duration;

@end
