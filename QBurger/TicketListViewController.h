//
//  TicketListViewController.h
//  PosApp
//
//  Created by Kevin Phua on 10/12/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface TicketListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, WebServiceDelegate>

@end
