//
//  ProductViewCell.h
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductViewCellDelegate <NSObject>

- (void)onBtnAdd:(id)sender;
- (void)onBtnSubtract:(id)sender;

@end

@interface ProductViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIImageView *imgCat1;
@property (weak, nonatomic) IBOutlet UIImageView *imgCat2;

@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnSubtract;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;

@property (weak, nonatomic) id<ProductViewCellDelegate> delegate;

@end
