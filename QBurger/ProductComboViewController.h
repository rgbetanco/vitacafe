//
//  ProductComboViewController.h
//  QBurger
//
//  Created by Kevin Phua on 10/28/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "BaseViewController.h"
#import "Combo.h"

@protocol ProductComboDelegate <NSObject>

// Returns selected combo
- (void)onSelectedCombo:(Combo *)combo prodId:(NSString *)prodId sender:(id)sender;

@end

@interface ProductComboViewController : BaseViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) Combo *combo;
@property (nonatomic, strong) Combo *selectedCombo;
@property (nonatomic, weak) id<ProductComboDelegate> delegate;

@end
