//
//  PrivacyViewController.h
//  QBurger
//
//  Created by Kevin Phua on 11/04/2017.
//  Copyright © 2017 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyViewController : UIViewController

@property (nonatomic, strong) NSString *url;

@end
