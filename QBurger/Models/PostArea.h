//
//  PostArea.h
//  QBurger
//
//  Created by Kevin Phua on 14/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostArea : NSObject

@property (nonatomic, strong) NSString *areaId;
@property (nonatomic, strong) NSString *areaName;

@end
