//
//  IntroContentViewController.h
//  VitaCafe
//
//  Created by Kevin Phua on 2016/11/28.
//  Copyright (c) 2016 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroContentViewController : UIViewController

@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) NSString *websiteUrl;
@property (nonatomic) NSUInteger pageIndex;

@end
