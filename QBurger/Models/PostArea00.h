//
//  PostArea00.h
//  QBurger
//
//  Created by Kevin Phua on 9/23/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostArea00 : NSObject

@property (nonatomic, strong) NSString *postAreaId;
@property (nonatomic, strong) NSString *postAreaName;
@property (nonatomic, strong) NSString *positionId;
@property (nonatomic, strong) NSString *areaId;
@property (nonatomic, strong) NSString *areaName;

@end
