//
//  SignupViewController.m
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "SignupViewController.h"
#import "Global.h"
#import "ActionSheetDatePicker.h"
#import "DLRadioButton.h"

#define STEP_1              0
#define STEP_2              1
#define STEP_3              2
#define STEP_4              3
#define STEP_5              4

@interface SignupViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UITextField *txtPhoneNumber;
@property (strong, nonatomic) UITextField *txtName;
@property (strong, nonatomic) UITextField *txtBirthday;
@property (strong, nonatomic) UITextField *txtEmail;
@property (strong, nonatomic) DLRadioButton *btnMale;
@property (strong, nonatomic) DLRadioButton *btnFemale;
@property (strong, nonatomic) UITextField *txtVerificationCode;
@property (strong, nonatomic) UITextField *txtPassword;
@property (strong, nonatomic) UITextField *txtConfirmPassword;

@property (nonatomic, strong) NSArray *headers;
@property (nonatomic) int currentStep;

@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *verificationCode;

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"RegisterAccount", nil);
    
    self.headers = [NSArray arrayWithObjects:NSLocalizedString(@"StepOne", nil),
                    NSLocalizedString(@"StepTwo", nil),
                    NSLocalizedString(@"StepThree", nil),
                    NSLocalizedString(@"StepFour", nil),
                    NSLocalizedString(@"StepFive", nil), nil];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.currentStep = STEP_1;
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapView:)];
    [self.view addGestureRecognizer:tapView];
}

- (void)viewDidAppear:(BOOL)animated {
    self.phoneNumber = @"";
    self.verificationCode = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSAttributedString *)attributedResendString
{
    NSMutableAttributedString *paragraph = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"NotReceivedVerCode", nil) attributes:@{NSForegroundColorAttributeName:[UIColor redColor],                                                                                  NSFontAttributeName:[UIFont systemFontOfSize:18]}];
    
    NSAttributedString* attributedString1 = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"SendAgain", nil)
                                                                            attributes:@{NSForegroundColorAttributeName:[Global colorWithType:COLOR_TYPE_LINK],NSFontAttributeName:[UIFont systemFontOfSize:18], NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),  @"resendVerCode": @(YES)}];
    
    [paragraph appendAttributedString:attributedString1];
    return [paragraph copy];
}

- (void)onTapResend:(UITapGestureRecognizer *)tapGesture
{
    UITextView *textView = (UITextView *)tapGesture.view;
    
    // Location of the tap in text-container coordinates
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [tapGesture locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    //NSLog(@"location: %@", NSStringFromCGPoint(location));
    
    // Find the character that's been tapped on
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        NSRange range;
        NSDictionary *attributes = [textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range];
        //NSLog(@"%@, %@", attributes, NSStringFromRange(range));
        
        // Based on the attributes, do something
        if ([attributes objectForKey:@"resendVerCode"]) {
            [self onBtnStep1:nil];
        }
    }
}

- (void)onTapView:(UITapGestureRecognizer *)tapGesture
{
    // Dismiss keyboard
    [self.view endEditing:YES];
}

- (void)onTapDateField:(id)sender
{
    // Dismiss keyboard
    [self.view endEditing:YES];
    
    [ActionSheetDatePicker showPickerWithTitle:NSLocalizedString(@"Birthday", nil)
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:[NSDate date]
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                                        NSDate *birthDate = (NSDate *)selectedDate;
                                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                                        [dateFormat setDateFormat:@"YYYY-MM-dd"];
                                        NSString *birthday = [dateFormat stringFromDate:birthDate];
                                         _txtBirthday.text = birthday;
                                     }
                                   cancelBlock:^(ActionSheetDatePicker *picker) {
                                       
                                   }
                                        origin:sender];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [_headers count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    
    /* Create custom view to display section header... */
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 200, 30)];
    titleLabel.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
    titleLabel.text = [_headers objectAtIndex:section];
    titleLabel.textColor = [Global colorWithType:COLOR_TYPE_STEPS];
    [titleLabel sizeToFit];
    [view addSubview:titleLabel];

    if (section < _currentStep) {
        CGFloat lblWidth = titleLabel.frame.size.width;
        UIImageView *imgTick = [[UIImageView alloc] initWithFrame:CGRectMake(lblWidth+25, 15, 24, 24)];
        imgTick.image = [UIImage imageNamed:@"btn_check_on"];
        [view addSubview:imgTick];
    }
    
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == _currentStep) {
        switch (section) {
            case STEP_2:
                return 5;
            case STEP_3:
            case STEP_4:
                return 3;
            case STEP_5:
                return 1;
            default:
                return 2;
        }
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == STEP_3 && indexPath.row == 0) {
        return 60;
    }
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.backgroundColor = [UIColor clearColor];
    
    NSInteger section = [indexPath section];
    NSInteger row = [indexPath row];
    
    switch (section) {
        case STEP_1:
            if (row == 0) {
                // Add phone number text field
                _txtPhoneNumber = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-70, 30)];
                _txtPhoneNumber.textAlignment = NSTextAlignmentLeft;
                _txtPhoneNumber.borderStyle = UITextBorderStyleRoundedRect;
                _txtPhoneNumber.delegate = self;
                _txtPhoneNumber.placeholder = NSLocalizedString(@"PhoneNumber", nil);
                _txtPhoneNumber.keyboardType = UIKeyboardTypeNumberPad;
                _txtPhoneNumber.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _txtPhoneNumber.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                _txtPhoneNumber.textColor = [UIColor blackColor];
                [cell.contentView addSubview:_txtPhoneNumber];
            } else if (row == 1) {
                // Add next button
                CGRect buttonRect = CGRectMake(cell.frame.size.width/2-72, 0, 72, 36);
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = buttonRect;
                [button setBackgroundImage:[UIImage imageNamed:@"btn_login"] forState:UIControlStateNormal];
                [button setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
                [button addTarget:self action:@selector(onBtnStep1:) forControlEvents:UIControlEventTouchUpInside];
                [button.titleLabel setFont:[UIFont systemFontOfSize:LABEL_FONT_SIZE]];
                button.titleLabel.textColor = [UIColor blackColor];
                [cell.contentView addSubview:button];
            }
            break;
        case STEP_2:
            if (row == 0) {
                // Add name text field
                _txtName = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-70, 30)];
                _txtName.textAlignment = NSTextAlignmentLeft;
                _txtName.borderStyle = UITextBorderStyleRoundedRect;
                _txtName.delegate = self;
                _txtName.placeholder = NSLocalizedString(@"Name", nil);
                _txtName.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _txtName.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                _txtName.textColor = [UIColor blackColor];
                [cell.contentView addSubview:_txtName];
            } else if (row == 1) {
                // Add birthday text field
                _txtBirthday = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-70, 30)];
                _txtBirthday.textAlignment = NSTextAlignmentLeft;
                _txtBirthday.borderStyle = UITextBorderStyleRoundedRect;
                _txtBirthday.delegate = self;
                _txtBirthday.placeholder = NSLocalizedString(@"Birthday", nil);
                _txtBirthday.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _txtBirthday.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                _txtBirthday.textColor = [UIColor blackColor];
                [cell.contentView addSubview:_txtBirthday];
            } else if (row == 2) {
                // Add email text field
                _txtEmail = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-70, 30)];
                _txtEmail.textAlignment = NSTextAlignmentLeft;
                _txtEmail.borderStyle = UITextBorderStyleRoundedRect;
                _txtEmail.delegate = self;
                _txtEmail.placeholder = NSLocalizedString(@"Email", nil);
                _txtEmail.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _txtEmail.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                _txtEmail.textColor = [UIColor blackColor];
                [cell.contentView addSubview:_txtEmail];
            } else if (row == 3) {
                UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-70, 30)];
                bgView.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                bgView.layer.cornerRadius = 5.0f;
                
                NSMutableArray *otherButtons = [NSMutableArray new];
                
                // Add sex radio buttons
                _btnMale = [[DLRadioButton alloc] initWithFrame:CGRectMake(20, 0, 80, 30)];
                [_btnMale setTitle:NSLocalizedString(@"Male", nil) forState:UIControlStateNormal];
                _btnMale.titleLabel.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _btnMale.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                [_btnMale setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [_btnMale setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
                [_btnMale setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
                _btnMale.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                _btnMale.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
                [bgView addSubview:_btnMale];
                
                _btnFemale = [[DLRadioButton alloc] initWithFrame:CGRectMake(80, 0, 80, 30)];
                [_btnFemale setTitle:NSLocalizedString(@"Female", nil) forState:UIControlStateNormal];
                _btnFemale.titleLabel.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _btnFemale.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                [_btnFemale setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [_btnFemale setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
                [_btnFemale setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
                _btnFemale.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                _btnFemale.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
                [otherButtons addObject:_btnFemale];
                [bgView addSubview:_btnFemale];

                _btnMale.otherButtons = otherButtons;
                
                [cell.contentView addSubview:bgView];
            } else if (row == 4) {
                // Add next button
                CGRect buttonRect = CGRectMake(cell.frame.size.width/2-72, 0, 72, 36);
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = buttonRect;
                [button setBackgroundImage:[UIImage imageNamed:@"btn_login"] forState:UIControlStateNormal];
                [button setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
                [button addTarget:self action:@selector(onBtnStep2:) forControlEvents:UIControlEventTouchUpInside];
                [button.titleLabel setFont:[UIFont systemFontOfSize:LABEL_FONT_SIZE]];
                button.titleLabel.textColor = [UIColor blackColor];
                [cell.contentView addSubview:button];
            }
            break;
        case STEP_3:
            if (row == 0) {
                // Add verification code field
                _txtVerificationCode = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-70, 30)];
                _txtVerificationCode.textAlignment = NSTextAlignmentLeft;
                _txtVerificationCode.borderStyle = UITextBorderStyleRoundedRect;
                _txtVerificationCode.delegate = self;
                _txtVerificationCode.placeholder = NSLocalizedString(@"VerificationCode", nil);
                _txtVerificationCode.keyboardType = UIKeyboardTypeNumberPad;
                _txtVerificationCode.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _txtVerificationCode.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                _txtVerificationCode.textColor = [UIColor blackColor];
                [cell.contentView addSubview:_txtVerificationCode];
            } else if (row == 1) {
                // Add next button
                CGRect buttonRect = CGRectMake(cell.frame.size.width/2-72, 0, 72, 36);
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = buttonRect;
                [button setBackgroundImage:[UIImage imageNamed:@"btn_login"] forState:UIControlStateNormal];
                [button setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
                [button addTarget:self action:@selector(onBtnStep3:) forControlEvents:UIControlEventTouchUpInside];
                [button.titleLabel setFont:[UIFont systemFontOfSize:LABEL_FONT_SIZE]];
                button.titleLabel.textColor = [UIColor blackColor];
                [cell.contentView addSubview:button];
            } else if (row == 2) {
                // Add resend label
                UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, 30)];
                textView.attributedText = [self attributedResendString];
                UITapGestureRecognizer *tapGestureResend = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapResend:)];
                [tapGestureResend setNumberOfTouchesRequired:1];
                [tapGestureResend setNumberOfTapsRequired:1];
                textView.backgroundColor = [UIColor clearColor];
                [textView addGestureRecognizer:tapGestureResend];
                [cell.contentView addSubview:textView];
            }
            break;
        case STEP_4:
            if (row == 0) {
                // Add password field
                _txtPassword = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-70, 30)];
                _txtPassword.textAlignment = NSTextAlignmentLeft;
                _txtPassword.borderStyle = UITextBorderStyleRoundedRect;
                _txtPassword.delegate = self;
                _txtPassword.placeholder = NSLocalizedString(@"NewPassword", nil);
                _txtPassword.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _txtPassword.secureTextEntry = YES;
                _txtPassword.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                _txtPassword.textColor = [UIColor blackColor];
                [cell.contentView addSubview:_txtPassword];
            } else if (row == 1) {
                // Add confirm password field
                _txtConfirmPassword = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-70, 30)];
                _txtConfirmPassword.textAlignment = NSTextAlignmentLeft;
                _txtConfirmPassword.borderStyle = UITextBorderStyleRoundedRect;
                _txtConfirmPassword.delegate = self;
                _txtConfirmPassword.placeholder = NSLocalizedString(@"ConfirmPassword", nil);
                _txtConfirmPassword.font = [UIFont systemFontOfSize:LABEL_FONT_SIZE];
                _txtConfirmPassword.secureTextEntry = YES;
                _txtConfirmPassword.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
                _txtConfirmPassword.textColor = [UIColor blackColor];
                [cell.contentView addSubview:_txtConfirmPassword];
            } else if (row == 2) {
                // Add next button
                CGRect buttonRect = CGRectMake(cell.frame.size.width/2-72, 0, 72, 36);
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = buttonRect;
                [button setBackgroundImage:[UIImage imageNamed:@"btn_login"] forState:UIControlStateNormal];
                [button setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
                [button addTarget:self action:@selector(onBtnStep4:) forControlEvents:UIControlEventTouchUpInside];
                [button.titleLabel setFont:[UIFont systemFontOfSize:LABEL_FONT_SIZE]];
                button.titleLabel.textColor = [UIColor blackColor];
                [cell.contentView addSubview:button];
            }
            break;
        case STEP_5:
            if (row == 0) {
                // Add done button
                CGRect buttonRect = CGRectMake(cell.frame.size.width/2-72, 0, 72, 36);
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = buttonRect;
                [button setBackgroundImage:[UIImage imageNamed:@"btn_login"] forState:UIControlStateNormal];
                [button setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
                [button addTarget:self action:@selector(onBtnStep5:) forControlEvents:UIControlEventTouchUpInside];
                [button.titleLabel setFont:[UIFont systemFontOfSize:LABEL_FONT_SIZE]];
                button.titleLabel.textColor = [UIColor blackColor];
                [cell.contentView addSubview:button];
            }
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)onBtnStep1:(id)sender {
    if (!_txtPhoneNumber || [_txtPhoneNumber.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                            message:NSLocalizedString(@"PhoneNumberError", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws searchMember:[Global cfgAppId] mobile:_txtPhoneNumber.text];
    [ws showWaitingView:self.view];
}

- (void)onBtnStep2:(id)sender {
    if (!_txtName || [_txtName.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                            message:NSLocalizedString(@"NameError", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    if (!_txtEmail || [_txtEmail.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                            message:NSLocalizedString(@"EmailError", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    if (!_btnMale.selected && !_btnFemale.selected) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                            message:NSLocalizedString(@"SexError", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    
    NSString *name = [_txtName.text stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    int sex = [_btnMale isSelected] ? 0 : 1;
    NSString *email = _txtEmail.text;
    NSString *mobile = _txtPhoneNumber.text;
    
    [ws registerNewMember:[Global cfgAppId] applyshop:@"" name:name sex:sex email:email mobile:mobile birthday:_txtBirthday.text address:@"" telephone:@"" idcard:@"" regflag:0];
    [ws showWaitingView:self.view];
}

- (void)onBtnStep3:(id)sender {
    if (!_txtVerificationCode || [_txtVerificationCode.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                            message:NSLocalizedString(@"VerificationCodeError", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }

    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws authenticate:[Global cfgAppId] sId:self.phoneNumber authCode:_txtVerificationCode.text];
    [ws showWaitingView:self.view];
}

- (void)onBtnStep4:(id)sender {
    if (!_txtPassword || [_txtPassword.text length] == 0 ||
        !_txtConfirmPassword || [_txtConfirmPassword.text length] == 0 ||
        [_txtPassword.text compare:_txtConfirmPassword.text] != NSOrderedSame) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                            message:NSLocalizedString(@"PasswordError", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws setPassword:[Global cfgAppId] sId:[g_MemberInfo objectForKey:INFO_KEY_VIPID] acckey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] password:_txtPassword.text];
    [ws showWaitingView:self.view];
}

- (void)onBtnStep5:(id)sender {
    // Go back to login page
    [self.navigationController popViewControllerAnimated:YES];
}

//==================================================================
#pragma UITextFieldDelegate
//==================================================================

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == _txtBirthday) {
        [self onTapDateField:_txtBirthday];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtBirthday) {
        [self.txtEmail becomeFirstResponder];
    } else if (textField == self.txtConfirmPassword) {
        [textField resignFirstResponder];
    } else if (textField == self.txtPassword) {
        [self.txtConfirmPassword becomeFirstResponder];
    }
    return YES;
}


//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_REGISTER] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterSuccess", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                
                self.phoneNumber = _txtPhoneNumber.text;
                _currentStep = STEP_3;
                [self.tableView reloadData];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_AUTH] == NSOrderedSame) {
            // Auth
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"VerificationSuccess", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];

                NSString *vipId = (NSString *)[jsonObject objectForKey:@"vip_id"];
                if (vipId && vipId.length > 0) {
                    [g_MemberInfo setObject:vipId forKey:INFO_KEY_VIPID];
                }
                NSString *accKey = (NSString *)[jsonObject objectForKey:@"acckey"];
                if (accKey && accKey.length > 0) {
                    [g_MemberInfo setObject:accKey forKeyedSubscript:INFO_KEY_ACCKEY];
                }
                
                _currentStep = STEP_4;
                [self.tableView reloadData];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"VerificationFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_SET_PASSWORD] == NSOrderedSame) {
            // Set password
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PasswordSuccess", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                
                NSString *vipId = (NSString *)[jsonObject objectForKey:@"vip_id"];
                if (vipId && vipId.length > 0) {
                    [g_MemberInfo setObject:vipId forKey:INFO_KEY_VIPID];
                }
                
                _currentStep = STEP_5;
                [self.tableView reloadData];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PasswordFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_REGISTER_NEW_MEMBER] == NSOrderedSame) {
            // Register new member
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterSuccess", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                
                self.phoneNumber = _txtPhoneNumber.text;
                _currentStep = STEP_3;
                [self.tableView reloadData];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_SEARCH_MEMBER] == NSOrderedSame) {
            // Register new member
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 1) {
                // POS and APP member, re-register
                WebService *ws = [[WebService alloc] init];
                ws.delegate = self;
                [ws registerAccount:[Global cfgAppId] sId:_txtPhoneNumber.text];
            } else if (code == 2) {
                // POS member, non-APP member, do register
                WebService *ws = [[WebService alloc] init];
                ws.delegate = self;
                [ws registerAccount:[Global cfgAppId] sId:_txtPhoneNumber.text];
            } else if (code == 3) {
                // Non POS member, do RegisterNewMember
                self.phoneNumber = _txtPhoneNumber.text;
                _currentStep = STEP_2;
                [self.tableView reloadData];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
