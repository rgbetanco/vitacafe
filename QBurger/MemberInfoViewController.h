//
//  MemberInfoViewController.h
//  PosApp
//
//  Created by Kevin Phua on 9/21/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "WebService.h"

@interface MemberInfoViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, WebServiceDelegate>

@end
