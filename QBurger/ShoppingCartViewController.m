//
//  ShoppingCartViewController.m
//  QBurger
//
//  Created by Kevin Phua on 9/11/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import "OrderConfirmViewController.h"
#import "ProductDetailViewController.h"
#import "Global.h"
#import "CartViewCell.h"
#import "CartItem.h"
#import "Order.h"
#import "WebOrder01.h"
#import "WebOrder011.h"
#import "WebService.h"
#import "MBProgressHUD.h"
#import "RaiseAlertView.h"
#import "AppDelegate.h"

@interface ShoppingCartViewController () <UITableViewDelegate, UITableViewDataSource, CartViewCellDelegate, WebServiceDelegate, OrderConfirmViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *btnUsePoints;
@property (nonatomic, weak) IBOutlet UIButton *btnUseVoucher;
@property (nonatomic, weak) IBOutlet UILabel *lblSubtotalTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblSubtotal;
@property (nonatomic, weak) IBOutlet UILabel *lblDiscountTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblDiscount;
@property (nonatomic, weak) IBOutlet UILabel *lblTotalTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblTotal;

@property (nonatomic, strong) MBProgressHUD *addOrderHud;
@property (nonatomic) int orderTotal;

@end

@implementation ShoppingCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"ShoppingCart", nil);
    [self.btnUsePoints setTitle:NSLocalizedString(@"UsePoints", nil) forState:UIControlStateNormal];
    [self.btnUseVoucher setTitle:NSLocalizedString(@"UseVoucher", nil) forState:UIControlStateNormal];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.lblSubtotalTitle.text = NSLocalizedString(@"Subtotal", nil);
    self.lblDiscountTitle.text = NSLocalizedString(@"Discount", nil);
    self.lblTotalTitle.text = NSLocalizedString(@"Total", nil);
    
    UIBarButtonItem *btnCheckout = [[UIBarButtonItem alloc] initWithTitle:@"結帳" style:UIBarButtonItemStylePlain target:self action:@selector(onBtnCheckout:)];
    self.navigationItem.rightBarButtonItem = btnCheckout;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    [self calculateTotals];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onBtnCheckout:(id)sender {
    OrderConfirmViewController *orderConfirmVC = [[OrderConfirmViewController alloc] initWithNibName:@"OrderConfirmViewController" bundle:nil];
    orderConfirmVC.modalPresentationStyle = UIModalPresentationPopover;
    orderConfirmVC.orderTotal = _orderTotal;
    orderConfirmVC.delegate = self;
    [self presentViewController:orderConfirmVC animated:YES completion:^(void) {

    }];
}

- (IBAction)onBtnUsePoints:(id)sender {
    
}

- (IBAction)onBtnUseVoucher:(id)sender {
    
}

- (void)calculateTotals {
    int discount = 0, total = 0;
    for (CartItem *item in g_ShoppingCart) {
        int salePrice = 0;
        int extraPrice = 0;
        int qty = 0;
        for (WebOrder01 *order in item.webOrder01s) {
            qty = order.qty;
            switch (order.combType) {
                case 0:     // Ordinary item
                    salePrice = order.salePrice;
                    break;
                case 1:     // Parent combo item
                    salePrice = order.salePrice;
                    break;
                case 2:     // Child combo item
                    extraPrice += order.salePrice;
                    break;
            }
            for (WebOrder011 *tasteDetail in order.webOrder011s) {
                if (tasteDetail.price > 0) {
                    extraPrice += tasteDetail.price;
                }
            }
        }
        total += (salePrice+extraPrice) * qty;
    }

    self.lblSubtotal.text = [NSString stringWithFormat:@"%d", total];
    self.lblDiscount.text = [NSString stringWithFormat:@"%d", discount];
    self.lblTotal.text = [NSString stringWithFormat:@"%d", total-discount];
    _orderTotal = total-discount;
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [g_ShoppingCart count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CartViewCell";
    CartViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"CartViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    cell.delegate = self;

    CartItem *item = [g_ShoppingCart objectAtIndex:[indexPath row]];
    NSMutableString *orderDesc = [NSMutableString new];
    int quantity = 0;
    int salePrice = 0;
    int extraPrice = 0;
    for (WebOrder01 *order in item.webOrder01s) {
        quantity = order.qty;
        switch (order.combType) {
            case 0:     // Ordinary item
                cell.lblTitle.text = order.prodName;
                salePrice = order.salePrice;
                if (order.tasteMemo && order.tasteMemo.length>0) {
                    [orderDesc appendFormat:@"- %@", order.tasteMemo];
                }
                break;
            case 1:     // Parent combo item
                cell.lblTitle.text = order.prodName;
                salePrice = order.salePrice;
                break;
            case 2:     // Child combo item
                if (order.tasteMemo && order.tasteMemo.length>0) {
                    [orderDesc appendFormat:@"- %@ (%@)\n", order.prodName, order.tasteMemo];
                } else {
                    [orderDesc appendFormat:@"- %@\n", order.prodName];
                }
                extraPrice += order.salePrice;
                break;
        }
        
        for (WebOrder011 *tasteDetail in order.webOrder011s) {
            if (tasteDetail.price > 0) {
                extraPrice += tasteDetail.price;
            }
        }
    }

    cell.lblDescription.text = orderDesc;
    cell.lblDescription.numberOfLines = 0;
    
    cell.lblPriceQty.text = [NSString stringWithFormat:@"%d x %d", salePrice+extraPrice, quantity];
    cell.lblPrice.text = [NSString stringWithFormat:@"%d", (salePrice+extraPrice) * quantity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//==================================================================
#pragma mark - CartViewDelegate
//==================================================================

- (void)onBtnDelete:(id)sender {
    // Remove item from shopping cart
    CartViewCell *cell = (CartViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath) {
        [g_ShoppingCart removeObjectAtIndex:indexPath.row];
        [Global saveShoppingCartToFile:g_ShoppingCart];
        [self.tableView reloadData];
        [self calculateTotals];
    } else {
        NSLog(@"Invalid indexPath!!!");
    }
}

- (void)onBtnEdit:(id)sender {
    // Remove item from shopping cart
    CartViewCell *cell = (CartViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath) {
        CartItem *item = [g_ShoppingCart objectAtIndex:[indexPath row]];
        
        // Open ProductDetailViewController
        ProductDetailViewController *productDetailController = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
        productDetailController.isOrder = YES;
        productDetailController.cartItem = item;
        productDetailController.cartItemIndex = indexPath.row;
        [productDetailController setProduct:item.product];
        [self.navigationController pushViewController:productDetailController animated:YES];
    } else {
        NSLog(@"Invalid indexPath!!!");
    }
}
     
//==================================================================
#pragma mark - OrderConfirmViewDelegate
//==================================================================
     
- (void)onConfirmOrder:(id)sender {
     NSMutableArray *webOrder01 = [NSMutableArray new];
     NSMutableArray *webOrder011 = [NSMutableArray new];
     
     int worderSno = 1;
     int parentSno = 1;
     for (CartItem *item in g_ShoppingCart) {
         int combSno = 1;
         for (WebOrder01 *order in item.webOrder01s) {
             NSMutableDictionary *orderItem = [NSMutableDictionary new];
             [orderItem setObject:[NSNumber numberWithInt:worderSno] forKey:@"worder_sno"];
             [orderItem setObject:order.prodId forKey:@"prod_id"];
             if (order.combType == 1) {
                 // Parent
                 parentSno = worderSno;
             } else if (order.combType == 2) {
                 // Child
                 [orderItem setObject:[NSNumber numberWithInt:parentSno] forKey:@"comb_sale_sno"];
                 [orderItem setObject:[NSNumber numberWithInt:combSno] forKey:@"comb_sno"];
                 combSno++;
             }
             [orderItem setObject:[NSNumber numberWithInt:order.salePrice] forKey:@"sale_price"];
             [orderItem setObject:[NSNumber numberWithInt:order.qty] forKey:@"qty"];
             [orderItem setObject:[NSNumber numberWithInt:order.combQty] forKey:@"comb_qty"];
             [orderItem setObject:[NSNumber numberWithInt:order.combType] forKey:@"comb_type"];
             if (order.tasteMemo) {
                 [orderItem setObject:[Global urlEncodedString:order.tasteMemo] forKey:@"taste_memo"];
             } else {
                 [orderItem setObject:@"" forKey:@"taste_memo"];
             }
             [webOrder01 addObject:orderItem];
             
             int tasteSno = 1;
             for (WebOrder011 *orderDetail in order.webOrder011s) {
                 NSMutableDictionary *comboItem = [NSMutableDictionary new];
                 [comboItem setObject:[NSNumber numberWithInt:worderSno] forKey:@"worder_sno"];
                 [comboItem setObject:[NSNumber numberWithInt:orderDetail.tasteId] forKey:@"taste_id"];
                 [comboItem setObject:[NSNumber numberWithInt:tasteSno] forKey:@"taste_sno"];
                 [comboItem setObject:[Global urlEncodedString:orderDetail.name] forKey:@"name"];
                 [comboItem setObject:[NSNumber numberWithInt:orderDetail.price] forKey:@"price"];
                 [webOrder011 addObject:comboItem];
                 tasteSno++;
             }
             
             worderSno++;
         }
     }
     
     NSError *error;
     NSData *webOrder01JsonData = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithObject:webOrder01 forKey:@"data"]
                                                                  options:NSJSONWritingPrettyPrinted
                                                                    error:&error];
     NSString *webOrder01JsonDataStr = [[NSString alloc] initWithData:webOrder01JsonData encoding:NSUTF8StringEncoding];
     NSLog(@"WebOrder01 = %@", webOrder01JsonDataStr);
     
     NSData *webOrder011JsonData = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithObject:webOrder011 forKey:@"data"]
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
     NSString *webOrder011JsonDataStr = [[NSString alloc] initWithData:webOrder011JsonData encoding:NSUTF8StringEncoding];
     NSLog(@"WebOrder011 = %@", webOrder011JsonDataStr);
     
     NSString *recAddr = g_SelectedRecAddr ? g_SelectedRecAddr : @"";
     recAddr = [recAddr stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
     
     WebService *ws = [WebService new];
     ws.delegate = self;
     [ws addOrder:[Global cfgAppId]
           accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]
           shopId:g_SelectedShopId ? g_SelectedShopId : @""
       sourceType:g_SelectedSourceType
         mealDate:[NSString stringWithFormat:@"%@ %@", g_SelectedMealDate, g_SelectedMealTime]
       saleMethod:g_SelectedSaleMethod
     paymentTerms:g_SelectedPaymentTerms
          recAddr:recAddr
       webOrder01:webOrder01JsonDataStr
      webOrder011:webOrder011JsonDataStr];
     
     _addOrderHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     _addOrderHud.label.text = NSLocalizedString(@"SendingOrder", nil);
     _addOrderHud.mode = MBProgressHUDModeIndeterminate;
 }

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_ADD_ORDER] == NSOrderedSame) {
            if (_addOrderHud) {
                [_addOrderHud hideAnimated:YES];
                _addOrderHud = nil;
            }
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *worderId = (NSString *)[jsonObject objectForKey:@"worder_id"];
                NSLog(@"Add order success: Order ID = %@ (%@)", worderId, message);
                
                // Clear shopping cart
                [Global clearShoppingCart];
                [Global clearCartUserDefaults];
                
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"OrderSuccess", nil)
                                                                          message:NSLocalizedString(@"OrderSuccessMsg", nil)
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                
                alertView.cancelButtonAction = ^{
                    // Jump to intro page
                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    [appDelegate goToMainView];
                    //[self.navigationController popViewControllerAnimated:YES];
                };
                
                [alertView show];

                
            } else {
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSLog(@"Add order error: %@", message);
                
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"OrderFail", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.viewNetworkError setHidden:NO];
}

@end
