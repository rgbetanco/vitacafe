//
//  BaseViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/5/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "BaseViewController.h"
#import "SWRevealViewController.h"
#import "LoginViewController.h"
#import "FriendListViewController.h"
#import "MessageViewController.h"
#import "MemberInfoViewController.h"
#import "FavoritesViewController.h"
#import "AppDelegate.h"
#import "Global.h"
#import "WebService.h"
#import "RaiseAlertView.h"
#import "ShoppingCartViewController.h"
#import "Order.h"
#import "CartItem.h"

@interface BaseViewController ()<WebServiceDelegate>

@property (nonatomic, strong) UILabel *lblName;
//@property (nonatomic, strong) UILabel *lblPoints;
@property (nonatomic, strong) UIButton *btnFavorites;
@property (nonatomic, strong) UIButton *btnFriends;
@property (nonatomic, strong) UIButton *btnMessages;
@property (nonatomic, strong) UIButton *btnCart;
@property (nonatomic, strong) UIImageView *imgFriendsBadge;
@property (nonatomic, strong) UIImageView *imgFavoritesBadge;
@property (nonatomic, strong) UIImageView *imgMessagesBadge;
@property (nonatomic, strong) UIImageView *imgCartBadge;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Init reveal view controller
    if ( self == [self.navigationController.viewControllers objectAtIndex:0] ) {
        [self initSidebarDrawerButton];
    }
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    // Init network error view
    _viewNetworkError = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];

    UILabel *lblDownloadError = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, screenWidth, 50)];
    lblDownloadError.textColor = [UIColor blackColor];
    lblDownloadError.font = [UIFont systemFontOfSize:18.0f];
    lblDownloadError.textAlignment = NSTextAlignmentCenter;
    lblDownloadError.text = NSLocalizedString(@"DownloadFailed", nil);
    lblDownloadError.center = _viewNetworkError.center;
    [_viewNetworkError addSubview:lblDownloadError];
    
    UIImageView *imgDownloadError = [[UIImageView alloc] initWithFrame:CGRectMake(_viewNetworkError.center.x-30, _viewNetworkError.center.y-120, 60, 79)];
    imgDownloadError.image = [UIImage imageNamed:@"err_fail"];
    [_viewNetworkError addSubview:imgDownloadError];

    UITextView *tvDownloadAgain = [[UITextView alloc] initWithFrame:CGRectMake(0, _viewNetworkError.center.y+30, screenWidth, 50)];
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString* attributedString = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"DownloadAgain", nil) attributes:@{NSForegroundColorAttributeName:[Global colorWithType:COLOR_TYPE_LINK],NSFontAttributeName:[UIFont systemFontOfSize:18], NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),NSParagraphStyleAttributeName:paragraphStyle,    @"reloadPage": @(YES)}];
    
    tvDownloadAgain.backgroundColor = [UIColor clearColor];
    tvDownloadAgain.textAlignment = NSTextAlignmentCenter;
    tvDownloadAgain.attributedText = attributedString;
    tvDownloadAgain.editable = NO;
    tvDownloadAgain.selectable = NO;
    [_viewNetworkError addSubview:tvDownloadAgain];
    [_viewNetworkError setHidden:YES];
    
    UITapGestureRecognizer *tapGestureReload = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapReload:)];
    [tvDownloadAgain addGestureRecognizer:tapGestureReload];
    
    [self.view addSubview:_viewNetworkError];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Clear all navigation bar subviews
    [self clearNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initSidebarDrawerButton {
    SWRevealViewController *revealController = [self revealViewController];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu_drawer"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    if (self.revealViewController != nil) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    } else {
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [self.view addGestureRecognizer:appDelegate.viewController.panGestureRecognizer];
    }
}

- (void)updateNavigationBar {
    if (g_IsLogin) {
        // Setup navigation status bar with member info and messages
        //int navBarWidth = self.navigationController.navigationBar.frame.size.width;
        
        UITapGestureRecognizer *tapGestureMemberInfo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openMemberInfo:)];

        // Member name
        if (!_lblName) {
            _lblName = [[UILabel alloc] initWithFrame:CGRectMake(60, 10, 200, 25)];
            _lblName.textColor = [UIColor whiteColor];
            _lblName.font = [UIFont systemFontOfSize:16.0];
            _lblName.userInteractionEnabled = YES;
            [_lblName addGestureRecognizer:tapGestureMemberInfo];
        }
        _lblName.text = [g_MemberInfo objectForKey:INFO_KEY_NAME];
        [self.navigationController.navigationBar addSubview:_lblName];
        
        // Member points
        /*
        if (!_lblPoints) {
            _lblPoints = [[UILabel alloc] initWithFrame:CGRectMake(55, 20, 200, 25)];
            _lblPoints.textColor = [UIColor whiteColor];
            _lblPoints.font = [UIFont systemFontOfSize:14.0];
            _lblPoints.userInteractionEnabled = YES;
            [_lblPoints addGestureRecognizer:tapGestureMemberInfo];
        }
        NSNumber *points = [g_MemberInfo objectForKey:INFO_KEY_LAST_POINT];
        _lblPoints.text = [NSString stringWithFormat:@"%@:%d",
                           NSLocalizedString(@"MemberPoints", nil),
                           points.intValue];
        [self.navigationController.navigationBar addSubview:_lblPoints];
        */
        
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        UIBarButtonItem *btnLogin = [[UIBarButtonItem alloc]
                                     initWithTitle:NSLocalizedString(@"MemberLogin", nil)
                                     style:UIBarButtonItemStylePlain
                                     target:self
                                     action:@selector(onBtnLogin:)];
        self.navigationItem.rightBarButtonItem = btnLogin;
        
        if (_btnFriends) {
            [_btnFriends removeFromSuperview];
            _btnFriends = nil;
        }
        if (_btnMessages) {
            [_btnMessages removeFromSuperview];
            _btnMessages = nil;
        }
        if (_imgFriendsBadge) {
            [_imgFriendsBadge removeFromSuperview];
            _imgFriendsBadge = nil;
        }
        if (_imgMessagesBadge) {
            [_imgMessagesBadge removeFromSuperview];
            _imgMessagesBadge = nil;
        }
    }
}

- (void)updateFavorites:(int)parentPage {
    int navBarWidth = self.navigationController.navigationBar.frame.size.width;
    
    // Favorites button
    if (g_IsLogin) {
        if ([Global cfgProductsHeartEnabled]) {
            if (!_btnFavorites) {
                _btnFavorites = [UIButton buttonWithType:UIButtonTypeCustom];
                [_btnFavorites setImage:[UIImage imageNamed:@"ic_menu_star"] forState:UIControlStateNormal];
                [_btnFavorites setImage:[UIImage imageNamed:@"ic_menu_star_enabled"] forState:UIControlStateSelected];
                [_btnFavorites addTarget:self action:@selector(onBtnFavorites:) forControlEvents:UIControlEventTouchUpInside];
                
                if (parentPage == PARENT_PAGE_INTRO) {
                    _btnFavorites.frame = CGRectMake(navBarWidth-84, 6, 32, 32);
                } else {
                    _btnFavorites.frame = CGRectMake(navBarWidth-121, 6, 32, 32);
                }
                [_btnFavorites.titleLabel setHidden:YES];
            }
            [self.navigationController.navigationBar addSubview:_btnFavorites];
            
            // Favorites button badge
            if (!_imgFavoritesBadge) {
                if (parentPage == PARENT_PAGE_INTRO) {
                    _imgFavoritesBadge = [[UIImageView alloc] initWithFrame:CGRectMake(navBarWidth-64, 2, 15, 15)];
                } else {
                    _imgFavoritesBadge = [[UIImageView alloc] initWithFrame:CGRectMake(navBarWidth-101, 2, 15, 15)];
                }
            } else {
                [_imgFavoritesBadge removeFromSuperview];
            }
            NSNumber *badge = [g_MemberInfo objectForKey:INFO_KEY_PENDING_FAVORITES];
            if (badge && [badge intValue]>0) {
                int badgeNumber = [badge intValue];
                NSString *badgeFile;
                if (badgeNumber <= 9) {
                    badgeFile = [NSString stringWithFormat:@"ic_menu_%d", badgeNumber];
                } else {
                    badgeFile = @"ic_menu_N";
                }
                [_imgFavoritesBadge setImage:[UIImage imageNamed:badgeFile]];
                [self.navigationController.navigationBar addSubview:_imgFavoritesBadge];
            }
        }
    }
}

- (void)updateFriends:(int)parentPage {
    int navBarWidth = self.navigationController.navigationBar.frame.size.width;
    
    // Friends button
    if (g_IsLogin) {
        if ([Global cfgFriendsEnabled]) {
            if (!_btnFriends) {
                _btnFriends = [UIButton buttonWithType:UIButtonTypeCustom];
                [_btnFriends setImage:[UIImage imageNamed:@"ic_menu_friends"] forState:UIControlStateNormal];
                [_btnFriends addTarget:self action:@selector(onBtnFriends:) forControlEvents:UIControlEventTouchUpInside];
                _btnFriends.frame = CGRectMake(navBarWidth-121, 6, 32, 32);
                [_btnFriends.titleLabel setHidden:YES];
            }
            [self.navigationController.navigationBar addSubview:_btnFriends];
            
            // Friends button badge
            if (!_imgFriendsBadge) {
                _imgFriendsBadge = [[UIImageView alloc] initWithFrame:CGRectMake(navBarWidth-101, 2, 15, 15)];
            } else {
                [_imgFriendsBadge removeFromSuperview];
            }
            NSNumber *badge = [g_MemberInfo objectForKey:INFO_KEY_PENDING_FRIENDS];
            if (badge && [badge intValue]>0) {
                int badgeNumber = [badge intValue];
                NSString *badgeFile;
                if (badgeNumber <= 9) {
                    badgeFile = [NSString stringWithFormat:@"ic_menu_%d", badgeNumber];
                } else {
                    badgeFile = @"ic_menu_N";
                }
                [_imgFriendsBadge setImage:[UIImage imageNamed:badgeFile]];
                [self.navigationController.navigationBar addSubview:_imgFriendsBadge];
            }
        }
    }
}

- (void)updateMessages:(int)parentPage {
    int navBarWidth = self.navigationController.navigationBar.frame.size.width;
    
    // Messages button
    if (g_IsLogin) {
        if ([Global cfgMsgEnabled]) {
            if (!_btnMessages) {
                _btnMessages = [UIButton buttonWithType:UIButtonTypeCustom];
                [_btnMessages setImage:[UIImage imageNamed:@"ic_menu_msg"] forState:UIControlStateNormal];
                [_btnMessages addTarget:self action:@selector(onBtnMessages:) forControlEvents:UIControlEventTouchUpInside];
                
                if (parentPage == PARENT_PAGE_INTRO) {
                    _btnMessages.frame = CGRectMake(navBarWidth-47, 6, 32, 32);
                } else {
                    _btnMessages.frame = CGRectMake(navBarWidth-84, 6, 32, 32);
                }
                
                [_btnMessages.titleLabel setHidden:YES];
            }
            [self.navigationController.navigationBar addSubview:_btnMessages];
            
            // Messages button badge
            if (!_imgMessagesBadge) {
                if (parentPage == PARENT_PAGE_INTRO) {
                    _imgMessagesBadge = [[UIImageView alloc] initWithFrame:CGRectMake(navBarWidth-27, 2, 15, 15)];
                } else {
                    _imgMessagesBadge = [[UIImageView alloc] initWithFrame:CGRectMake(navBarWidth-64, 2, 15, 15)];
                }
            } else {
                [_imgMessagesBadge removeFromSuperview];
            }
            NSNumber *msgBadge = [g_MemberInfo objectForKey:INFO_KEY_MSGS_UNREAD];
            if (msgBadge && [msgBadge intValue]>0) {
                int badgeNumber = [msgBadge intValue];
                NSString *badgeFile;
                if (badgeNumber <= 9) {
                    badgeFile = [NSString stringWithFormat:@"ic_menu_%d", badgeNumber];
                } else {
                    badgeFile = @"ic_menu_N";
                }
                [_imgMessagesBadge setImage:[UIImage imageNamed:badgeFile]];
                [self.navigationController.navigationBar addSubview:_imgMessagesBadge];
            }
        }
    }
}

- (void)updateCart:(int)parentPage {
    int navBarWidth = self.navigationController.navigationBar.frame.size.width;
    
    // Shopping cart button
    if (g_IsLogin) {
        if ([Global cfgOrdersEnabled]) {
            if (!_btnCart) {
                _btnCart = [UIButton buttonWithType:UIButtonTypeCustom];
                [_btnCart setImage:[UIImage imageNamed:@"ic_menu_cart"] forState:UIControlStateNormal];
                [_btnCart addTarget:self action:@selector(onBtnCart:) forControlEvents:UIControlEventTouchUpInside];
                _btnCart.frame = CGRectMake(navBarWidth-47, 6, 32, 32);
                [_btnCart.titleLabel setHidden:YES];
            }
            [self.navigationController.navigationBar addSubview:_btnCart];
            
            // Cart button badge
            if (!_imgCartBadge) {
                _imgCartBadge = [[UIImageView alloc] initWithFrame:CGRectMake(navBarWidth-27, 2, 15, 15)];
            } else {
                [_imgCartBadge removeFromSuperview];
            }
            
            // Badge number is total quantity
            int badgeNumber = 0;
            for (CartItem *item in g_ShoppingCart) {
                NSArray *webOrders = item.webOrder01s;
                WebOrder01 *webOrder01 = [webOrders firstObject];
                badgeNumber += webOrder01.qty;
            }
            if (badgeNumber > 0) {
                NSString *badgeFile;
                if (badgeNumber <= 9) {
                    badgeFile = [NSString stringWithFormat:@"ic_menu_%d", badgeNumber];
                } else {
                    badgeFile = @"ic_menu_N";
                }
                [_imgCartBadge setImage:[UIImage imageNamed:badgeFile]];
                [self.navigationController.navigationBar addSubview:_imgCartBadge];
             }
        }
    }
}

- (void)clearNavigationBar {
    if (_lblName) {
        [_lblName removeFromSuperview];
    }
    //if (_lblPoints) {
    //    [_lblPoints removeFromSuperview];
    //}
    if (_btnFavorites) {
        [_btnFavorites removeFromSuperview];
    }
    if (_btnFriends) {
        [_btnFriends removeFromSuperview];
    }
    if (_btnMessages) {
        [_btnMessages removeFromSuperview];
    }
    if (_btnCart) {
        [_btnCart removeFromSuperview];
    }
    if (_imgFriendsBadge) {
        [_imgFriendsBadge removeFromSuperview];
    }
    if (_imgFavoritesBadge) {
        [_imgFavoritesBadge removeFromSuperview];
    }
    if (_imgMessagesBadge) {
        [_imgMessagesBadge removeFromSuperview];
    }
    if (_imgFavoritesBadge) {
        [_imgFavoritesBadge removeFromSuperview];
    }
    if (_imgCartBadge) {
        [_imgCartBadge removeFromSuperview];
    }
}

- (void)reloadWebpage {
    // Implement to reload webpage
}

- (void)onBtnLogin:(id)sender {
    LoginViewController *loginController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [self.navigationController pushViewController:loginController animated:YES];
}

- (void)onBtnFavorites:(id)sender {
    [self clearNavigationBar];
    self.title = @"";
    
    FavoritesViewController *favoritesController = [[FavoritesViewController alloc] initWithNibName:@"FavoritesViewController" bundle:nil];
    [self.navigationController pushViewController:favoritesController animated:YES];
}

- (void)onBtnFriends:(id)sender {
    [self clearNavigationBar];
    self.title = @"";

    FriendListViewController *friendController = [[FriendListViewController alloc] initWithNibName:@"FriendListViewController" bundle:nil];
    [self.navigationController pushViewController:friendController animated:YES];
}

- (void)onBtnMessages:(id)sender {
    [self clearNavigationBar];
    self.title = @"";

    MessageViewController *messageController = [[MessageViewController alloc] initWithNibName:@"MessageViewController" bundle:nil];
    [self.navigationController pushViewController:messageController animated:YES];
}

- (void)onBtnCart:(id)sender {
    [self clearNavigationBar];
    self.title = @"";
    
    ShoppingCartViewController *cartVC = [[ShoppingCartViewController alloc] initWithNibName:@"ShoppingCartViewController" bundle:nil];
    [self.navigationController pushViewController:cartVC animated:YES];
}

- (IBAction)openMemberInfo:(id)sender
{
    SWRevealViewController *revealController = self.revealViewController;
    
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    MemberInfoViewController *memberInfoViewController = appDelegate.memberInfoViewController;
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:memberInfoViewController];
    [revealController pushFrontViewController:frontNavigationController animated:YES];
}

- (void)updateVipPoints {
    /*
    if (g_IsLogin) {
        WebService *ws = [[WebService alloc] init];
        [ws setDelegate:self];
        [ws searchLastPointValue:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    }
    */
}

- (void)getFriendList {
    if (g_IsLogin) {
        WebService *ws = [[WebService alloc] init];
        [ws setDelegate:self];
        [ws getFriendList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    }
}

- (void)getMessageList {
    if (g_IsLogin) {
        WebService *ws = [[WebService alloc] init];
        [ws setDelegate:self];
        [ws getMessageList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    }
}

- (void)getPageList {
    WebService *ws = [[WebService alloc] init];
    [ws setDelegate:self];
    [ws getPageList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
}

- (void)onTapReload:(UITapGestureRecognizer *)tapGesture
{
    UITextView *textView = (UITextView *)tapGesture.view;
    
    // Location of the tap in text-container coordinates
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [tapGesture locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    //NSLog(@"location: %@", NSStringFromCGPoint(location));
    
    // Find the character that's been tapped on
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        NSRange range;
        NSDictionary *attributes = [textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range];
        // Based on the attributes, do something
        if ([attributes objectForKey:@"reloadPage"]) {
            WebService *ws = [WebService new];
            ws.delegate = self;
            [ws reloadLastCommand];
            [ws showWaitingView:self.view];
            [_viewNetworkError setHidden:YES];
            
            // Reload webpage
            [self reloadWebpage];
        }
    }
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            NSMutableArray *pendingFriends = [NSMutableArray new];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_AWAITING_APPROVAL) {
                        NSNumber *kind = [friend objectForKey:@"kind"];
                        if ([kind intValue] == KIND_INVITED_FRIENDS) {
                            [pendingFriends addObject:friend];
                        }
                    }
                }
                
                NSNumber *numPending = [NSNumber numberWithInteger:pendingFriends.count];
                [g_MemberInfo setObject:numPending forKey:INFO_KEY_PENDING_FRIENDS];
                [self updateNavigationBar];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_GET_MESSAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ messages", datacnt);
                
                NSArray *messages = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (messages) {
                    int unreadMessages = 0;
                    for (NSDictionary *message in messages) {
                        NSString *readTime = [message objectForKey:@"read_time"];
                        if ([readTime isEqualToString:@""]) {
                            unreadMessages++;
                        }
                    }
                    
                    [g_MemberInfo setObject:[NSNumber numberWithInt:unreadMessages] forKey:INFO_KEY_MSGS_UNREAD];
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.viewNetworkError setHidden:NO];
}

@end
