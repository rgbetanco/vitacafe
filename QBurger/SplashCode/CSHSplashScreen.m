//
//  CSHSplashScreen.m
//  QBSplash_IOS
//
//  Created by Chin-Yu Hsu on 7/25/16.
//  Copyright © 2016 Chinsoft Co., Ltd. All rights reserved.
//

#import "CSHSplashScreen.h"

@implementation CSHSplashScreen {
    NSTimeInterval mDuration;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)initSplash {
}

- (id)initWithFrame: (CGRect)frame withNibNamed : (NSString *)nibName duration : (NSTimeInterval) duration
{
    self = [super initWithFrame:frame];
    if( self ) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        self = [nib objectAtIndex:0];
        self.frame = frame;
        mDuration = duration;
    }
    
    return self;
}

+ (instancetype) splashScreenOnSuperView : (UIView *)superview withNibNamed : (NSString *)nibName duration : (NSTimeInterval) duration {
    CSHSplashScreen *loader = [[CSHSplashScreen alloc] initWithFrame:superview.bounds withNibNamed:nibName duration:duration];
    
    [superview addSubview:loader];
    
    return loader;
}

+ (instancetype) showSplashScreen : (UIViewController *)parent withNibNamed : (NSString *)nibName duration: (NSTimeInterval) duration {
    if (parent.isBeingPresented || parent.isMovingToParentViewController) {
        return [CSHSplashScreen splashScreenOnSuperView:parent.navigationController.view withNibNamed:nibName duration:duration];
    }
    
    return nil;
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    
    if( self.superview!=nil ) {
        self.frame = self.superview.bounds;
        [self showSplashScreenWithDuration:mDuration];
    }
}

- (void) showSplashScreenWithDuration : (NSTimeInterval) duration {
    // tap to close
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    [self addGestureRecognizer:tapRecognizer];

    // add a time-out if specified
    if( duration>0 ) {
        [self queueClose:duration];
    }

    //[self performSelector:@selector(initSplash) withObject:nil afterDelay:1.0];
    [self initSplash];
    
}

- (void) queueClose : (NSTimeInterval) duration {
    dispatch_after( dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC),
                    dispatch_get_main_queue(), ^{
                        [self fadeOutAndClose];
    });
}

- (IBAction) onTap: (id)sender {
    [self fadeOutAndClose];
}

- (void) fadeOutAndClose {
    // fade out the whole view and then remove it
    [CATransaction begin]; {
        [CATransaction setCompletionBlock:^{
            [self removeFromSuperview];
        }];
        
        CABasicAnimation *animFadeOut = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animFadeOut.duration = 0.2f;
        animFadeOut.fromValue = [NSNumber numberWithFloat:1.0];
        animFadeOut.toValue = [NSNumber numberWithFloat:0.0];
        animFadeOut.fillMode = kCAFillModeForwards;
        animFadeOut.removedOnCompletion = NO;
        [self.layer addAnimation:animFadeOut forKey:@"opacity"];
        
    } [CATransaction commit];
}


@end
