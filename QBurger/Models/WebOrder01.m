//
//  WebOrder01.m
//  QBurger
//
//  Created by Kevin Phua on 04/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "WebOrder01.h"

@implementation WebOrder01

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // Copy NSObject subclasses
        [copy setWorderId:[self.worderId copyWithZone:zone]];
        [copy setProdId:[self.prodId copyWithZone:zone]];
        [copy setProdName:[self.prodName copyWithZone:zone]];
        [copy setTasteMemo:[self.tasteMemo copyWithZone:zone]];
        [copy setWebOrder011s:[self.webOrder011s copyWithZone:zone]];
        
        // Set primitives
        [copy setWorderSno:self.worderSno];
        [copy setQty:self.qty];
        [copy setSalePrice:self.salePrice];
        [copy setCombSaleSno:self.combSaleSno];
        [copy setCombSno:self.combSno];
        [copy setCombType:self.combType];
        [copy setCombQty:self.combQty];
        [copy setItemDisc:self.itemDisc];
        [copy setIsDefault:self.isDefault];
        [copy setEnable:self.enable];
        [copy setStopSale:self.stopSale];
    }
    
    return copy;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]){
        self.worderSno = [aDecoder decodeIntForKey:@"worderSno"];
        self.worderId = [aDecoder decodeObjectForKey:@"worderId"];
        self.prodId = [aDecoder decodeObjectForKey:@"prodId"];
        self.prodName = [aDecoder decodeObjectForKey:@"prodName"];
        self.qty = [aDecoder decodeIntForKey:@"qty"];
        self.salePrice = [aDecoder decodeIntForKey:@"salePrice"];
        self.combSaleSno = [aDecoder decodeIntForKey:@"combSaleSno"];
        self.combSno = [aDecoder decodeIntForKey:@"combSno"];
        self.combType = [aDecoder decodeIntForKey:@"combType"];
        self.combQty = [aDecoder decodeIntForKey:@"combQty"];
        self.itemDisc = [aDecoder decodeIntForKey:@"itemDisc"];
        self.tasteMemo = [aDecoder decodeObjectForKey:@"tasteMemo"];
        self.webOrder011s = [aDecoder decodeObjectForKey:@"webOrder011s"];
        self.isDefault = [aDecoder decodeBoolForKey:@"isDefault"];
        self.enable = [aDecoder decodeBoolForKey:@"enable"];
        self.stopSale = [aDecoder decodeBoolForKey:@"stopSale"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInt:self.worderSno forKey:@"worderSno"];
    [aCoder encodeObject:self.worderId forKey:@"worderId"];
    [aCoder encodeObject:self.prodId forKey:@"prodId"];
    [aCoder encodeObject:self.prodName forKey:@"prodName"];
    [aCoder encodeInt:self.qty forKey:@"qty"];
    [aCoder encodeInt:self.salePrice forKey:@"salePrice"];
    [aCoder encodeInt:self.combSaleSno forKey:@"combSaleSno"];
    [aCoder encodeInt:self.combSno forKey:@"combSno"];
    [aCoder encodeInt:self.combType forKey:@"combType"];
    [aCoder encodeInt:self.combQty forKey:@"combQty"];
    [aCoder encodeInt:self.itemDisc forKey:@"itemDisc"];
    [aCoder encodeBool:self.isDefault forKey:@"isDefault"];
    [aCoder encodeBool:self.enable forKey:@"enable"];
    [aCoder encodeBool:self.stopSale forKey:@"stopSale"];
    [aCoder encodeObject:self.tasteMemo forKey:@"tasteMemo"];
    [aCoder encodeObject:self.webOrder011s forKey:@"webOrder011s"];
}

- (NSString *)description {
    NSMutableString *desc = [NSMutableString new];
    [desc appendFormat:@"worderSno = %d\n", self.worderSno];
    [desc appendFormat:@"worderId = %@\n", self.worderId];
    [desc appendFormat:@"prodId = %@\n", self.prodId];
    [desc appendFormat:@"prodName = %@\n", self.prodName];
    [desc appendFormat:@"qty = %d\n", self.qty];
    [desc appendFormat:@"salePrice = %d\n", self.salePrice];
    [desc appendFormat:@"combSaleSno = %d\n", self.combSaleSno];
    [desc appendFormat:@"combSno = %d\n", self.combSno];
    [desc appendFormat:@"combType = %d\n", self.combType];
    [desc appendFormat:@"combQty = %d\n", self.combQty];
    [desc appendFormat:@"itemDisc = %d\n", self.itemDisc];
    [desc appendFormat:@"isDefault = %d\n", self.isDefault];
    [desc appendFormat:@"enable = %d\n", self.enable];
    [desc appendFormat:@"stopSale = %d\n", self.stopSale];
    [desc appendFormat:@"tasteMemo = %@\n", self.tasteMemo];
    [desc appendFormat:@"webOrder011s = %@\n", self.webOrder011s];
    return desc;
}

@end
