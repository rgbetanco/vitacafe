//
//  ProductListCell.h
//  QBurger
//
//  Created by Kevin Phua on 9/9/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductListCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
