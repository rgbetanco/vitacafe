//
//  AppDelegate.h
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftSidebarController.h"
#import "SWRevealViewController.h"
#import "IntroViewController.h"
#import "AboutUsViewController.h"
#import "PromoViewController.h"
#import "ProductViewController.h"
#import "MemberInfoViewController.h"
#import "LocationsViewController.h"
#import "ContactUsViewController.h"
#import "SurveyViewController.h"
#import "SettingsViewController.h"
#import "CustomPageViewController.h"
#import "LoginViewController.h"
#import "OrderMainViewController.h"


//@interface AppDelegate : UIResponder <UIApplicationDelegate>
@interface AppDelegate : UIResponder <UIApplicationDelegate, SWRevealViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
@property (strong, nonatomic) LeftSidebarController *leftSidebarController;
@property (strong, nonatomic) IntroViewController *introViewController;
@property (strong, nonatomic) PromoViewController *promoViewController;
@property (strong, nonatomic) ProductViewController *productViewController;
@property (strong, nonatomic) SettingsViewController *settingsViewController;
@property (strong, nonatomic) MemberInfoViewController *memberInfoViewController;
@property (strong, nonatomic) LocationsViewController *locationsViewController;
@property (strong, nonatomic) CustomPageViewController *customPageViewController;
@property (strong, nonatomic) LoginViewController *loginViewController;
@property (strong, nonatomic) OrderMainViewController *orderViewController;

- (void)goToMainView;

@end

