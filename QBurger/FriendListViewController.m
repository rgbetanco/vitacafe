//
//  FriendListViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/6/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "FriendListViewController.h"
#import "FriendAddViewController.h"
#import "RaiseAlertView.h"
#import "Global.h"

@interface FriendListViewController ()

@property (nonatomic, strong) UIButton *btnAddFriend;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@property (nonatomic, strong) NSMutableArray *invitingFriends;  // 好友邀請
@property (nonatomic, strong) NSMutableArray *pendingFriends;   // 等候對方接受邀請
@property (nonatomic, strong) NSMutableArray *allFriends;       // 所有好友

@end

@implementation FriendListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.invitingFriends = [[NSMutableArray alloc] init];
    self.pendingFriends = [[NSMutableArray alloc] init];
    self.allFriends = [[NSMutableArray alloc] init];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = NSLocalizedString(@"Friends", nil);
    
    // Add friend button
    int navBarWidth = self.navigationController.navigationBar.frame.size.width;
    
    if (!_btnAddFriend) {
        _btnAddFriend = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnAddFriend setImage:[UIImage imageNamed:@"Ic_menu_addfriend"] forState:UIControlStateNormal];
        [_btnAddFriend addTarget:self action:@selector(onBtnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
        _btnAddFriend.frame = CGRectMake(navBarWidth-42, 8, 32, 32);
        [_btnAddFriend.titleLabel setHidden:YES];
    }
    [self.navigationController.navigationBar addSubview:_btnAddFriend];
    
    [self getFriendList];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (_btnAddFriend) {
        [_btnAddFriend removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getFriendList {
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws getFriendList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    [ws showWaitingView:self.view];
}

- (void)onBtnAddFriend:(id)sender {
    FriendAddViewController *addFriendController = [[FriendAddViewController alloc] initWithNibName:@"FriendAddViewController" bundle:nil];
    // Hide back button text on next page
    self.title = @"";
    [self.navigationController pushViewController:addFriendController animated:YES];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *HeaderIdentifier = @"TableHeader";
    UITableViewHeaderFooterView *header = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:HeaderIdentifier];
    
    header.textLabel.textColor = [UIColor blackColor];
    header.textLabel.backgroundColor = [UIColor clearColor];
    header.tintColor = [UIColor clearColor];
    
    switch (section) {
        case SECTION_AWAITING_JOIN:
            header.textLabel.text = NSLocalizedString(@"FriendsAwaitingJoin", nil);
            break;
        case SECTION_AWAITING_APPROVAL:
            header.textLabel.text = NSLocalizedString(@"FriendsAwaitingApproval", nil);
            break;
        case SECTION_ALL_FRIENDS:
        default:
            header.textLabel.text = NSLocalizedString(@"AllFriends", nil);
            break;
    }
    
    return header;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = [UIColor clearColor];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor blackColor]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case SECTION_AWAITING_JOIN:
            return [self.invitingFriends count];
        case SECTION_AWAITING_APPROVAL:
            return [self.pendingFriends count];
        case SECTION_ALL_FRIENDS:
        default:
            return [self.allFriends count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FriendListCell";
    FriendListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"FriendListCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    cell.delegate = self;
    
    NSDictionary *friend;
    NSInteger section = indexPath.section;
    if (section == SECTION_AWAITING_JOIN) {
        friend = [self.invitingFriends objectAtIndex:indexPath.row];
        cell.btnYes.hidden = NO;
        cell.btnNo.hidden = NO;
    } else if (section == SECTION_AWAITING_APPROVAL) {
        friend = [self.pendingFriends objectAtIndex:indexPath.row];
        cell.btnYes.hidden = YES;
        cell.btnNo.hidden = YES;
    } else {
        friend = [self.allFriends objectAtIndex:indexPath.row];
        cell.btnYes.hidden = YES;
        cell.btnNo.hidden = NO;
        cell.lblPhone.hidden = YES;
    }
    
    NSString *name = [friend objectForKey:@"name_2"];
    NSString *fId = [friend objectForKey:@"vip_2"];
    NSString *mobile = [friend objectForKey:@"mobile"];

    cell.lblName.text = name;
    if (mobile && mobile.length > 0) {
        cell.lblPhone.text = [NSString stringWithFormat:@"(%@)", mobile];
        cell.lblPhone.hidden = NO;
    } 
    
    cell.fId = fId;
    
    return cell;
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            
            [self.invitingFriends removeAllObjects];
            [self.pendingFriends removeAllObjects];
            [self.allFriends removeAllObjects];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_APPROVED) {
                        [self.allFriends addObject:friend];
                    } else if ([status intValue] == STATUS_AWAITING_APPROVAL) {
                        NSNumber *kind = [friend objectForKey:@"kind"];
                        if ([kind intValue] == KIND_INVITING_FRIENDS) {
                            [self.invitingFriends addObject:friend];
                        } else if ([kind intValue] == KIND_INVITED_FRIENDS) {
                            [self.pendingFriends addObject:friend];
                        }
                    }
                }
                
                [self.tableView reloadData];
                //[self adjustHeightOfTableview];
            } else {
                // Failure
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitle:nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_FRIEND_AGREE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                NSLog(@"Friend agree OK! %@", message);
                [self getFriendList];
            } else {
                // Failure
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_FRIEND_DENY] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                NSLog(@"Friend deny OK! %@", message);
                [self getFriendList];
            } else {
                // Failure
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_FRIEND_DELETE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                NSLog(@"Friend delete OK! %@", message);
                [self getFriendList];
            } else {
                // Failure
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                 otherButtonTitle:nil];
                [alertView show];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

//==================================================================
#pragma FriendListDelegate
//==================================================================

- (void)btnYes:(id)sender {
    FriendListCell *cell = (FriendListCell *)[[sender superview] superview];
    NSString *fId = cell.fId;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    if (indexPath.section == SECTION_AWAITING_JOIN) {
        // Agree to friend invitation
        WebService *ws = [[WebService alloc] init];
        ws.delegate = self;
        [ws friendAgree:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] fId:fId];
        [ws showWaitingView:self.view];
    }
}

- (void)btnNo:(id)sender {
    FriendListCell *cell = (FriendListCell *)[[sender superview] superview];
    NSString *fId = cell.fId;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    if (indexPath.section == SECTION_AWAITING_JOIN ||
        indexPath.section == SECTION_AWAITING_APPROVAL) {
        
        NSString *alertMsg = [NSString stringWithFormat:NSLocalizedString(@"FriendDenyMsg", nil), cell.lblName.text];
        RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"FriendDeny", nil)
                                                                  message:alertMsg
                                                        cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                         otherButtonTitle:NSLocalizedString(@"Yes", nil)];
        alertView.otherButtonAction = ^{
            // Deny friend
            WebService *ws = [[WebService alloc] init];
            ws.delegate = self;
            [ws friendDeny:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] fId:fId];
            [ws showWaitingView:self.view];
        };
        [alertView show];
        
    } else if (indexPath.section == SECTION_ALL_FRIENDS) {
        
        NSString *alertMsg = [NSString stringWithFormat:NSLocalizedString(@"FriendDeleteMsg", nil), cell.lblName.text];
        RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"FriendDelete", nil)
                                                                  message:alertMsg
                                                        cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                         otherButtonTitle:NSLocalizedString(@"Yes", nil)];
        alertView.otherButtonAction = ^{
            // Delete friend
            WebService *ws = [[WebService alloc] init];
            ws.delegate = self;
            [ws friendDelete:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] fId:fId];
            [ws showWaitingView:self.view];
        };
        [alertView show];
        
    }
}

@end
