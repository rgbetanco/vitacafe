//
//  LocationEngine.m
//  
//
//  Created by loegg little on 12/5/23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "LocationEngine.h"
#import "Global.h"

@implementation LocationEngine

@synthesize currentLocation;

LocationEngine* sharedInstance;

#pragma mark -
#pragma mark Location engine init

- (LocationEngine*)init
{
	if (locationManager == nil) {	
		locationManager = [[CLLocationManager alloc] init];
	}
	if (locationManager) {
		[locationManager setDelegate:self];
		[locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
		[locationManager setDistanceFilter:kCLDistanceFilterNone];
	}
	
	return self;
}

+ (LocationEngine*)getInstance
{
    if (!sharedInstance) {
        sharedInstance = [[LocationEngine alloc] init];
    }
	return sharedInstance;
}

- (CLLocation*) getLocation
{
	return locationManager.location;
}


#pragma mark -
#pragma mark Location start/stop update

- (void)startUpdate
{	
	[locationManager stopUpdatingLocation];
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    
	[locationManager startUpdatingLocation];
}

- (void)stopUpdate
{
	[locationManager stopUpdatingLocation];
}

/*********************************************************************
 * LocationManager related callback functions
 *********************************************************************/
#pragma mark -
#pragma mark LocationManager

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.currentLocation = newLocation;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"locationManager_didFailWithError");
}

@end
