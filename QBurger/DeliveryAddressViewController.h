//
//  DeliveryAddressViewController.h
//  QBurger
//
//  Created by Kevin Phua on 10/5/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "BaseViewController.h"

@protocol DeliveryAddressDelegate <NSObject>

- (void)deliveryAddressSelected:(NSString *)address;

@end

@interface DeliveryAddressViewController : BaseViewController

@property (nonatomic, assign) id<DeliveryAddressDelegate> delegate;

@end
