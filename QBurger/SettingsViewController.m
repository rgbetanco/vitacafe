//
//  SettingsViewController.m
//  PosApp
//
//  Created by Kevin Phua on 9/16/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "SettingsViewController.h"
#import "ChangePasswordViewController.h"
#import "ChangeInfoViewController.h"
#import "Global.h"

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblCell;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblBirthday;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.lblCell.text = [g_MemberInfo objectForKey:INFO_KEY_MOBILE];
    self.lblName.text = [g_MemberInfo objectForKey:INFO_KEY_NAME];
    self.lblBirthday.text = [g_MemberInfo objectForKey:INFO_KEY_BIRTHDAY];
    self.lblEmail.text = [g_MemberInfo objectForKey:INFO_KEY_EMAIL];

    self.title = NSLocalizedString(@"Settings", nil);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = NSLocalizedString(@"Settings", nil);
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *HeaderIdentifier = @"TableHeader";
    UITableViewHeaderFooterView *header = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:HeaderIdentifier];
    
    header.textLabel.textColor = [UIColor blackColor];
    header.textLabel.backgroundColor = [UIColor clearColor];
    header.tintColor = [UIColor clearColor];
    header.textLabel.text = NSLocalizedString(@"MemberSettings", nil);
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = [UIColor clearColor];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor blackColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"ChangePassword", nil);
    } else if (indexPath.row == 1) {
        cell.textLabel.text = NSLocalizedString(@"ChangeDetails", nil);
    }
    cell.textLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [Global colorWithType:COLOR_BTN_CAT_LIST_BG];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        ChangePasswordViewController *chgPassViewController = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
        self.title = @"";
        [self.navigationController pushViewController:chgPassViewController animated:YES];
    } else {
        ChangeInfoViewController *chgInfoVC = [[ChangeInfoViewController alloc] initWithNibName:@"ChangeInfoViewController" bundle:nil];
        self.title = @"";
        [self.navigationController pushViewController:chgInfoVC animated:YES];
    }
}

@end
