//
//  LocationsCell.m
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "LocationsCell.h"

@implementation LocationsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onBtnLocation:(id)sender {
    [self.delegate btnLocation:(LocationsCell *)sender];
}

- (IBAction)onBtnCall:(id)sender {
    [self.delegate btnCall:(LocationsCell *)sender];
}

@end
