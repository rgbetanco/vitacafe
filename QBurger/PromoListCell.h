//
//  PromoListCell.h
//  Raise
//
//  Created by Kevin Phua on 11/5/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIImageView *imgFile;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewHeightConstraint;

@end
