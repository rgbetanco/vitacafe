//
//  ProductListViewController.h
//  PosApp
//
//  Created by Kevin Phua on 10/12/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "WebService.h"

@interface ProductListViewController : BaseViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, WebServiceDelegate>

@property (nonatomic, strong) NSString *deptName;
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *deptId;
@property (nonatomic, strong) NSString *posDeptId;

@property (nonatomic) BOOL isOrder;     // 是否進入點餐流程

@end
