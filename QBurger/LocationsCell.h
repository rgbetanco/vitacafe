//
//  LocationsCell.h
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LocationsCellDelegate <NSObject>

- (void)btnLocation:(id)sender;
- (void)btnCall:(id)sender;

@end


@interface LocationsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;

@property (weak, nonatomic) id<LocationsCellDelegate> delegate;


@end
