//
//  AppDelegate.m
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsViewController.h"
#import "SCNetworkReachability.h"
#import "Global.h"
#import "WebService.h"
#import "QBurgerSplashScreen.h"
#import "LoginViewController.h"
#import "ViewController.h"
#import "LocationEngine.h"
#import "IQKeyboardManager.h"
#import "SignupViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

BOOL g_IsLogin;
BOOL g_IsOnline;
BOOL g_IsFirstUse;
NSString *g_MemberPhone;
NSString *g_MemberPassword;
NSMutableDictionary *g_MemberInfo;
NSString *g_WSLastServerUrl;
NSString *g_WSLastCommand;
NSString *g_WSLastCommandParams;
NSDictionary *g_SysConfig;
NSMutableArray *g_MyFavorites;
NSMutableArray *g_ShoppingCart;
NSMutableDictionary *g_Products;
NSString *g_SelectedShopId;
NSString *g_SelectedShopName;
int g_SelectedSourceType;
NSString *g_SelectedMealDate;
NSString *g_SelectedMealTime;
int g_SelectedSaleMethod;
int g_SelectedPaymentTerms;
NSString *g_SelectedRecAddr;
NSString *g_SelectedWebOrder01;
NSString *g_SelectedWebOrder011;
NSString *g_LastSelectedShopId;
NSString *g_LastSelectedRecAddr;
NSString *g_PrivacyUrl;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self loadSystemConfig];
    [[LocationEngine getInstance] startUpdate];
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] disableToolbarInViewControllerClass:[SignupViewController class]];
    
    // Init push notifications
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    application.applicationIconBadgeNumber = 0;
        
    [Global initUserDefaults];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window = window;
    
    self.introViewController = [[IntroViewController alloc] init];
    self.promoViewController = [[PromoViewController alloc] init];
    self.productViewController = [[ProductViewController alloc] init];
    self.leftSidebarController = [[LeftSidebarController alloc] init];
    self.settingsViewController = [[SettingsViewController alloc] init];
    self.memberInfoViewController = [[MemberInfoViewController alloc] init];
    self.locationsViewController = [[LocationsViewController alloc] init];
    self.customPageViewController = [[CustomPageViewController alloc] init];
    self.loginViewController = [LoginViewController new];
    self.orderViewController = [OrderMainViewController new];
    
    // Customize navigation bar
    [[UINavigationBar appearance] setBarTintColor:[Global colorWithType:COLOR_TYPE_NAVBAR_BG]];
    [[UINavigationBar appearance] setTintColor:[Global colorWithType:COLOR_TYPE_LINK]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[Global colorWithType:COLOR_TYPE_LINK]}];
    
    // Customize status bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // Get start page setting
    g_MemberInfo = [NSMutableDictionary new];
    g_MemberPhone = [[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_PHONE];
    g_MemberPassword = [[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_PASSWORD];
    g_IsFirstUse = [[[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_FIRST_USE] boolValue];
    
    if (!g_IsFirstUse) {
        NSNumber *firstUse = [NSNumber numberWithBool:YES];
        [[NSUserDefaults standardUserDefaults] setObject:firstUse forKey:UD_KEY_FIRST_USE];
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:self.introViewController];
            [self.leftSidebarController setCurrentRow:1];
        
            UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:self.leftSidebarController];
        
            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
            revealController.delegate = self;
        
            revealController.rearViewRevealWidth = LEFT_MENU_WIDTH - 18;
            revealController.rearViewRevealOverdraw = 0;
            revealController.bounceBackOnOverdraw = NO;
            revealController.stableDragOnOverdraw = YES;
            revealController.frontViewShadowColor = [UIColor clearColor];
        
            self.viewController = revealController;
        
            self.window.rootViewController = self.viewController;
            [self.window makeKeyAndVisible];
/*
        ViewController *viewController = [ViewController new];
        
        [QBurgerSplashScreen showSplashScreen:viewController];
        
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
    } else {
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:self.introViewController];
        [self.leftSidebarController setCurrentRow:1];
        
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:self.leftSidebarController];
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        revealController.delegate = self;
        
        revealController.rearViewRevealWidth = LEFT_MENU_WIDTH - 18;
        revealController.rearViewRevealOverdraw = 0;
        revealController.bounceBackOnOverdraw = NO;
        revealController.stableDragOnOverdraw = YES;
        revealController.frontViewShadowColor = [UIColor clearColor];
        
        //revealController.rightViewRevealWidth = LEFT_MENU_WIDTH;
        //revealController.rightViewRevealOverdraw = 0;
        
        self.viewController = revealController;
        
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];
 */
        
        [QBurgerSplashScreen showSplashScreen:revealController];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[LocationEngine getInstance] stopUpdate];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[LocationEngine getInstance] startUpdate];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString * deviceTokenString = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"Device token = %@", deviceTokenString);
    [[NSUserDefaults standardUserDefaults] setObject:deviceTokenString forKey:UD_DEVICE_TOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if (userInfo) {
        NSDictionary *aps = [userInfo objectForKey:@"aps"];
        if (aps) {
            NSNumber *badge = [aps objectForKey:@"badge"];
            NSLog(@"Push badge = %ld", [badge integerValue]);
        }
    }
}

- (void)loadSystemConfig
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"manifest" ofType:@"plist"];
    g_SysConfig = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSLog(@"System settings = %@", g_SysConfig);
}

//==================================================================
#pragma mark - SWRevealViewDelegate
//==================================================================

- (id <UIViewControllerAnimatedTransitioning>)revealController:(SWRevealViewController *)revealController animationControllerForOperation:(SWRevealControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    /*
     if ( operation != SWRevealControllerOperationReplaceRightController )
     return nil;
     
     if ( [toVC isKindOfClass:[RightViewController class]] )
     {
     if ( [(RightViewController*)toVC wantsCustomAnimation] )
     {
     id<UIViewControllerAnimatedTransitioning> animationController = [[CustomAnimationController alloc] init];
     return animationController;
     }
     }
     */
    
    return nil;
}

- (void)goToMainView {
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:self.introViewController];
    [self.leftSidebarController setCurrentRow:1];
    
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:self.leftSidebarController];
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    revealController.delegate = self;
    
    revealController.rearViewRevealWidth = LEFT_MENU_WIDTH - 18;
    revealController.rearViewRevealOverdraw = 0;
    revealController.bounceBackOnOverdraw = NO;
    revealController.stableDragOnOverdraw = YES;
    revealController.frontViewShadowColor = [UIColor clearColor];
    
    //revealController.rightViewRevealWidth = LEFT_MENU_WIDTH;
    //revealController.rightViewRevealOverdraw = 0;
    
    self.viewController = revealController;
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
}

@end
