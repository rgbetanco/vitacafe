//
//  TasteDetail.m
//  QBurger
//
//  Created by Kevin Phua on 9/8/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "TasteDetail.h"

@implementation TasteDetail

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // Copy NSObject subclasses
        [copy setTasteSno:[self.tasteSno copyWithZone:zone]];
        [copy setName:[self.name copyWithZone:zone]];
        
        // Set primitives
        [copy setPrice:self.price];
    }
    
    return copy;
}

@end
