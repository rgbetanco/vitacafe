//
//  MessageListCell.m
//  Raise
//
//  Created by Kevin Phua on 11/5/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "MessageListCell.h"
#import "Global.h"

@implementation MessageListCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.lblContent.textColor = [UIColor blackColor];
    self.lblContent.numberOfLines = 0;
    self.backgroundColor = [Global colorWithType:COLOR_BG_MAIN];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onBtnSave:(id)sender {
    _btnSave.hidden = true;
    [self.delegate btnSave:(MessageListCell *)sender];
}

@end
