//
//  CustomPageViewController.m
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "CustomPageViewController.h"
#import "SWRevealViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Global.h"

@interface CustomPageViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIntroView;

@end

@implementation CustomPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.backgroundColor = [UIColor clearColor];
    
    // Do any additional setup after loading the view from its nib.
    [self.webView setOpaque:NO];
    [self.webView setScalesPageToFit:YES];
    [self.imgIntroView setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = _subject;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_websiteUrl]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadWebpage {
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_websiteUrl]]];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_LOGIN] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *accKey = (NSString *)[jsonObject objectForKey:@"acckey"];
                if (accKey && accKey.length > 0) {
                    [g_MemberInfo setObject:accKey forKey:INFO_KEY_ACCKEY];
                }
                NSString *vipId = (NSString *)[jsonObject objectForKey:@"vip_id"];
                if (vipId && vipId.length > 0) {
                    [g_MemberInfo setObject:vipId forKey:INFO_KEY_VIPID];
                }
                NSString *vipLevel = (NSString *)[jsonObject objectForKey:@"vip_level"];
                if (vipLevel && vipLevel.length > 0) {
                    [g_MemberInfo setObject:vipLevel forKey:INFO_KEY_VIP_LEVEL];
                }
                NSString *vipLevelName = (NSString *)[jsonObject objectForKey:@"vip_level_name"];
                if (vipLevelName && vipLevelName.length > 0) {
                    [g_MemberInfo setObject:vipLevelName forKey:INFO_KEY_VIP_LEVEL_NAME];
                }
                NSString *name = (NSString *)[jsonObject objectForKey:@"name"];
                if (name && name.length > 0) {
                    [g_MemberInfo setObject:name forKey:INFO_KEY_NAME];
                }
                id zip = [jsonObject objectForKey:@"zip"];
                if ([zip isKindOfClass:[NSString class]]) {
                    NSString *zipString = (NSString *)zip;
                    [g_MemberInfo setObject:zipString forKey:INFO_KEY_ZIP];
                }
                NSString *address = (NSString *)[jsonObject objectForKey:@"address"];
                if (address && address.length > 0) {
                    [g_MemberInfo setObject:address forKey:INFO_KEY_ADDRESS];
                }
                NSString *telephone = (NSString *)[jsonObject objectForKey:@"telephone"];
                if (telephone && telephone.length > 0) {
                    [g_MemberInfo setObject:telephone forKey:INFO_KEY_TELEPHONE];
                }
                NSString *sex = (NSString *)[jsonObject objectForKey:@"sex"];
                if (sex && sex.length > 0) {
                    [g_MemberInfo setObject:sex forKey:INFO_KEY_SEX];
                }
                NSString *birthday = (NSString *)[jsonObject objectForKey:@"birthday"];
                if (birthday && birthday.length > 0) {
                    [g_MemberInfo setObject:birthday forKey:INFO_KEY_BIRTHDAY];
                }
                NSString *email = (NSString *)[jsonObject objectForKey:@"email"];
                if (email && email.length > 0) {
                    [g_MemberInfo setObject:email forKey:INFO_KEY_EMAIL];
                }
                NSString *mobile = (NSString *)[jsonObject objectForKey:@"mobile"];
                if (mobile && mobile.length > 0) {
                    [g_MemberInfo setObject:mobile forKey:INFO_KEY_MOBILE];
                }
                NSString *lastPoint = (NSString *)[jsonObject objectForKey:@"last_point"];
                if (lastPoint && lastPoint.length > 0) {
                    NSNumber *lastPointValue = [NSNumber numberWithInteger:[lastPoint intValue]];
                    [g_MemberInfo setObject:lastPointValue forKey:INFO_KEY_LAST_POINT];
                }
                NSString *lastAmt = (NSString *)[jsonObject objectForKey:@"last_amt"];
                if (lastAmt && lastAmt.length > 0) {
                    [g_MemberInfo setObject:lastAmt forKey:INFO_KEY_LAST_AMT];
                }
                NSString *lastIcPoint = (NSString *)[jsonObject objectForKey:@"last_icpoint"];
                if (lastIcPoint && lastIcPoint.length > 0) {
                    [g_MemberInfo setObject:lastIcPoint forKey:INFO_KEY_LAST_IC_POINT];
                }
                NSString *card = (NSString *)[jsonObject objectForKey:@"card"];
                if (card && card.length > 0) {
                    [g_MemberInfo setObject:card forKey:INFO_KEY_CARD];
                }
                NSString *endDate = (NSString *)[jsonObject objectForKey:@"end_date"];
                if (endDate && endDate.length > 0) {
                    [g_MemberInfo setObject:endDate forKey:INFO_KEY_END_DATE];
                }
                NSString *idCard = (NSString *)[jsonObject objectForKey:@"id_card"];
                if (idCard && idCard.length > 0) {
                    [g_MemberInfo setObject:idCard forKey:INFO_KEY_ID_CARD];
                }
                
                NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                                      dateStyle:NSDateFormatterShortStyle
                                                                      timeStyle:NSDateFormatterFullStyle];
                [[NSUserDefaults standardUserDefaults] setObject:dateString forKey:UD_KEY_LAST_LOGIN];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                g_IsLogin = YES;
                
                [self updateVipPoints];
                [self getFriendList];
                [self getMessageList];
                [self getPageList];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                if ([resultName compare:WS_LOGIN] == NSOrderedSame) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LoginFailed", nil)
                                                                        message:message
                                                                       delegate:nil
                                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                              otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }
            
            [self updateNavigationBar];
        } else if ([resultName compare:WS_REGISTER_DEVICE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSLog(@"Register device success: %@", message);
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSLog(@"Register device failed: %@", message);
            }
        } else if ([resultName compare:WS_SEARCH_LAST_POINT_VALUE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *vipPoint = (NSString *)[jsonObject objectForKey:@"vip_point"];
                if (vipPoint && vipPoint.length > 0) {
                    NSNumber *vipPointValue = [NSNumber numberWithInteger:[vipPoint intValue]];
                    [g_MemberInfo setObject:vipPointValue forKey:INFO_KEY_LAST_POINT];

                }
                [self updateVipPoints];
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            NSMutableArray *pendingFriends = [NSMutableArray new];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_AWAITING_APPROVAL) {
                        NSNumber *kind = [friend objectForKey:@"kind"];
                        if ([kind intValue] == KIND_INVITED_FRIENDS) {
                            [pendingFriends addObject:friend];
                        }
                    }
                }
                
                NSNumber *numPending = [NSNumber numberWithInteger:pendingFriends.count];
                [g_MemberInfo setObject:numPending forKey:INFO_KEY_PENDING_FRIENDS];
                [self updateFriends:PARENT_PAGE_DEFAULT];
                
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_MESSAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ messages", datacnt);
                
                NSArray *messages = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (messages) {
                    int unreadMessages = 0;
                    for (NSDictionary *message in messages) {
                        NSString *readTime = [message objectForKey:@"read_time"];
                        if ([readTime isEqualToString:@""]) {
                            unreadMessages++;
                        }
                    }
                    
                    [g_MemberInfo setObject:[NSNumber numberWithInt:unreadMessages] forKey:INFO_KEY_MSGS_UNREAD];
                    [self updateMessages:PARENT_PAGE_DEFAULT];
                }
            }
        } 
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.webView setHidden:YES];
    [self.viewNetworkError setHidden:NO];
}

//==================================================================
#pragma UIWebViewDelegate
//==================================================================
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.webView setHidden:NO];
    [self.imgIntroView setHidden:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.webView setHidden:YES];
    //[self.viewNetworkError setHidden:NO];
}

@end
