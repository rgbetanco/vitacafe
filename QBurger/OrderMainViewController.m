//
//  OrderMainViewController.m
//  QBurger
//
//  Created by Kevin Phua on 9/8/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "OrderMainViewController.h"
#import "ProductViewController.h"
#import "DeliveryAddressViewController.h"
#import "SelectShopViewController.h"
#import "WebService.h"
#import "ShopInfo.h"
#import "Global.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ActionSheetPicker.h"
#import "PostArea00.h"
#import "PostArea01.h"
#import "PostAreaRd.h"
#import "SaleMethod.h"
#import "UIView+Toast.h"
#import "RaiseAlertView.h"

@interface OrderMainViewController () <WebServiceDelegate, CLLocationManagerDelegate, SelectShopViewDelegate, DeliveryAddressDelegate>
{
    BOOL isDatePicker;
}

@property (weak, nonatomic) IBOutlet UIButton *btnOrderBranch;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderDate;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderTime;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderType;
@property (weak, nonatomic) IBOutlet UIButton *btnDeliveryAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnStartOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblWarningMsg;

@property (weak, nonatomic) IBOutlet UIImageView *imgOrderBranchOK;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrderDateOK;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrderTimeOK;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrderTypeOK;
@property (weak, nonatomic) IBOutlet UIImageView *imgDeliveryAddressOK;

@property (strong, nonatomic) NSMutableArray *saleMethods;
@property (strong, nonatomic) NSMutableArray *saleMethodNames;
@property (strong, nonatomic) NSMutableArray *mealTimes;
@property (strong, nonatomic) NSMutableArray *restTimes;

@property (strong, nonatomic) ShopInfo *selectedShop;
@property (nonatomic) NSInteger selectedTime;
@property (nonatomic) NSInteger selectedMethod;
@property (nonatomic) NSInteger selectedMealDays;

@property (strong, nonatomic) NSMutableArray *shops;
@property (strong, nonatomic) NSMutableArray *shopNames;

@end

@implementation OrderMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _saleMethods = [NSMutableArray new];
    _saleMethodNames = [NSMutableArray new];
    _mealTimes = [NSMutableArray new];
    _restTimes = [NSMutableArray new];
    _selectedMealDays = 1;
    
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"Order", nil);
    
    self.btnOrderBranch.layer.borderWidth = 1.0f;
    self.btnOrderBranch.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.btnOrderDate.layer.borderWidth = 1.0f;
    self.btnOrderDate.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.btnOrderTime.layer.borderWidth = 1.0f;
    self.btnOrderTime.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.btnOrderType.layer.borderWidth = 1.0f;
    self.btnOrderType.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.btnDeliveryAddress.layer.borderWidth = 1.0f;
    self.btnDeliveryAddress.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.btnStartOrder.layer.borderWidth = 1.0f;
    self.btnStartOrder.layer.borderColor = [UIColor blackColor].CGColor;
    
    /*
    UIImage *imgBtnBg = [UIImage imageNamed:@"btn_login"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 10, 10, 10);
 
    UIImage *stretchableImage = [imgBtnBg resizableImageWithCapInsets:insets];
    [_btnOrderBranch setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    [_btnOrderDate setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    [_btnOrderTime setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    [_btnOrderType setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    [_btnDeliveryAddress setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    [_btnStartOrder setBackgroundImage:stretchableImage forState:UIControlStateNormal];

    imgBtnBg = [UIImage imageNamed:@"btn_login_pressed"];
    UIImage *stretchableImage2 = [imgBtnBg resizableImageWithCapInsets:insets];
    [_btnOrderBranch setBackgroundImage:stretchableImage2 forState:UIControlStateSelected];
    [_btnOrderDate setBackgroundImage:stretchableImage2 forState:UIControlStateSelected];
    [_btnOrderTime setBackgroundImage:stretchableImage2 forState:UIControlStateSelected];
    [_btnOrderType setBackgroundImage:stretchableImage2 forState:UIControlStateSelected];
    [_btnDeliveryAddress setBackgroundImage:stretchableImage2 forState:UIControlStateSelected];
    [_btnStartOrder setBackgroundImage:stretchableImage2 forState:UIControlStateSelected];
*/
    [_btnOrderBranch setTitle:NSLocalizedString(@"OrderBranch", nil) forState:UIControlStateNormal];
    [_btnOrderDate setTitle:NSLocalizedString(@"OrderDate", nil) forState:UIControlStateNormal];
    [_btnOrderTime setTitle:NSLocalizedString(@"OrderTime", nil) forState:UIControlStateNormal];
    [_btnOrderType setTitle:NSLocalizedString(@"OrderType", nil) forState:UIControlStateNormal];
    [_btnDeliveryAddress setTitle:NSLocalizedString(@"OrderAddress", nil) forState:UIControlStateNormal];
    [_btnStartOrder setTitle:NSLocalizedString(@"StartOrder", nil) forState:UIControlStateNormal];
    
    // Hide delivery address button
    [_btnDeliveryAddress setHidden:YES];
    
    _shops = [NSMutableArray new];
    _shopNames = [NSMutableArray new];
    
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getShop:[Global cfgAppId] shopId:@"" areaId:@"" pstAreaId:@"" city:@"" pstAreaSNo:@""];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getPageList];
    //[self updateFavorites];
    //[self updateCart];
    
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getShop:[Global cfgAppId] shopId:@"" areaId:@"" pstAreaId:@"" city:@"" pstAreaSNo:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnOrderBranch:(id)sender {
    SelectShopViewController *selectShopVC = [[SelectShopViewController alloc] initWithNibName:@"SelectShopViewController" bundle:nil];
    
    selectShopVC.modalPresentationStyle = UIModalPresentationPopover;
    selectShopVC.delegate = self;
    [self presentViewController:selectShopVC animated:YES completion:^(void) {
        
    }];
}

- (IBAction)onBtnOrderDate:(id)sender {
    if (_imgOrderBranchOK.hidden) {
        [self.view makeToast:NSLocalizedString(@"SelectBranchFirst", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    }
    
    [ActionSheetDatePicker showPickerWithTitle:NSLocalizedString(@"OrderDate", nil)
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:[NSDate date]
                                   minimumDate:[NSDate date]
                                   maximumDate:[NSDate dateWithTimeIntervalSinceNow:_selectedMealDays*24*60*60]
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                                         // Set date or time
                                         NSDateFormatter *formatter = [NSDateFormatter new];
                                         [formatter setDateFormat:@"YYYY-MM-dd"];
                                         [_btnOrderDate setTitle:[formatter stringFromDate:selectedDate]
                                                        forState:UIControlStateNormal];
                                         
                                         g_SelectedMealDate = [formatter stringFromDate:selectedDate];
                                         
                                         // Save to user defaults
                                         [[NSUserDefaults standardUserDefaults] setObject:g_SelectedMealDate forKey:UD_KEY_LAST_SELECTED_DATE];
                                         [[NSUserDefaults standardUserDefaults] synchronize];
                                         
                                         // Get meal time
                                         if (_selectedShop) {
                                             WebService *ws = [WebService new];
                                             ws.delegate = self;
                                             [ws getMealTime:[Global cfgAppId] shopId:_selectedShop.shopId date:[formatter stringFromDate:selectedDate]];
                                         }
                                         
                                         _imgOrderDateOK.hidden = NO;
                                         
                                         // Reselect time
                                         if (!_imgOrderTimeOK.hidden) {
                                             _selectedTime = -1;
                                             [_btnOrderTime setTitle:NSLocalizedString(@"OrderTime", nil) forState:UIControlStateNormal];
                                             g_SelectedMealTime = @"";
                                             
                                             // Save to user defaults
                                             [[NSUserDefaults standardUserDefaults] setObject:g_SelectedMealTime forKey:UD_KEY_LAST_SELECTED_TIME];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             
                                             _imgOrderTimeOK.hidden = YES;
                                         }
                                         
                                         
                                     } cancelBlock:^(ActionSheetDatePicker *picker) {
                                       
                                     } origin:(UIView *)sender];
}

- (IBAction)onBtnOrderTime:(id)sender {
    if (_imgOrderBranchOK.hidden) {
        [self.view makeToast:NSLocalizedString(@"SelectBranchFirst", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    } else if (_imgOrderDateOK.hidden) {
        [self.view makeToast:NSLocalizedString(@"SelectDateFirst", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    }
    
    // Check if shop rest
    if (_mealTimes.count == 0) {
        RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"OrderRestTitle", nil)
                                                                  message:NSLocalizedString(@"OrderRestAllDayMsg", nil)
                                                        cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                         otherButtonTitle:nil];
        
        [alertView show];
        return;
    } else if (_restTimes.count > 0) {
        NSMutableString *restTimes = [NSMutableString new];
        for (NSString *time in _restTimes) {
            if (restTimes.length == 0) {
                [restTimes appendString:time];
            } else {
                [restTimes appendFormat:@", %@", time];
            }
        }
        
        NSString *alertMsg = [NSString stringWithFormat:NSLocalizedString(@"OrderRestHoursMsg", nil), restTimes];
        RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"OrderRestTitle", nil)
                                                                  message:alertMsg
                                                        cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                         otherButtonTitle:nil];
        
        alertView.cancelButtonAction = ^{
            [self showOrderTimePicker:sender];
        };
        [alertView show];
        return;
    }
    
    [self showOrderTimePicker:sender];
}

- (void)showOrderTimePicker:(id)sender {
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"OrderTime", nil)
                                            rows:_mealTimes
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id sender) {
                                           if (_mealTimes && _mealTimes.count>0) {
                                               _selectedTime = selectedIndex;
                                               
                                               NSString *time = [_mealTimes objectAtIndex:selectedIndex];
                                               [_btnOrderTime setTitle:time forState:UIControlStateNormal];
                                               
                                               g_SelectedMealTime = time;
                                               
                                               // Save to user defaults
                                               [[NSUserDefaults standardUserDefaults] setObject:g_SelectedMealTime forKey:UD_KEY_LAST_SELECTED_TIME];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                               
                                               _imgOrderTimeOK.hidden = NO;
                                           }
                                           
                                           // Get sale method
                                           if (_selectedShop) {
                                               WebService *ws = [WebService new];
                                               ws.delegate = self;
                                               [ws getSaleMethod:[Global cfgAppId] shopId:_selectedShop.shopId];
                                           }
                                           
                                       } cancelBlock:^(ActionSheetStringPicker *picker) {
                                           
                                       } origin:(UIView *)sender];
}

- (IBAction)onBtnOrderType:(id)sender {
    if (_imgOrderBranchOK.hidden) {
        [self.view makeToast:NSLocalizedString(@"SelectBranchFirst", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    } else if (_imgOrderDateOK.hidden) {
        [self.view makeToast:NSLocalizedString(@"SelectDateFirst", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    } else if (_imgOrderTimeOK.hidden) {
        [self.view makeToast:NSLocalizedString(@"SelectTimeFirst", nil)
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        return;
    }
    
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"OrderType", nil)
                                            rows:_saleMethodNames
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id sender) {
                                           if (_saleMethodNames && _saleMethodNames.count>0) {
                                               _selectedMethod = selectedIndex;
                                               
                                               [_btnOrderType setTitle:[_saleMethodNames objectAtIndex:selectedIndex] forState:UIControlStateNormal];
                                               
                                               SaleMethod *saleMethod = [_saleMethods objectAtIndex:selectedIndex];
                                               g_SelectedSaleMethod = [saleMethod.methodId intValue];
                                               NSLog(@"Selected sale method = %d", g_SelectedSaleMethod);
                                               
                                               // Save to user defaults
                                               [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)g_SelectedSaleMethod forKey:UD_KEY_LAST_SELECTED_TYPE];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                               
                                               _imgOrderTypeOK.hidden = NO;
                                               
                                               if (saleMethod.isAddr) {
                                                   [_btnDeliveryAddress setHidden:NO];
                                                   if (g_LastSelectedRecAddr && g_LastSelectedRecAddr.length>0) {
                                                       [_imgDeliveryAddressOK setHidden:NO];
                                                       [_btnDeliveryAddress setTitle:g_LastSelectedRecAddr forState:UIControlStateNormal];
                                                       g_SelectedRecAddr = g_LastSelectedRecAddr;
                                                   }
                                               } else {
                                                   [_btnDeliveryAddress setHidden:YES];
                                                   [_imgDeliveryAddressOK setHidden:YES];
                                               }
                                           }
                                       }cancelBlock:^(ActionSheetStringPicker *picker) {
                                           
                                       }  origin:(UIView *)sender];
}

- (IBAction)onBtnDeliveryAddress:(id)sender {
    DeliveryAddressViewController *deliveryAddressVC = [[DeliveryAddressViewController alloc] initWithNibName:@"DeliveryAddressViewController" bundle:nil];
    deliveryAddressVC.delegate = self;
    [self.navigationController pushViewController:deliveryAddressVC animated:YES];
}

- (IBAction)onBtnStartOrder:(id)sender {
    // Check fields
    if (_imgOrderBranchOK.hidden) {
        [self.view makeToast:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderBranch", nil)]
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        //_lblWarningMsg.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderBranch", nil)];
        return;
    } else if (_imgOrderDateOK.hidden) {
        [self.view makeToast:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderDate", nil)]
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        //_lblWarningMsg.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderDate", nil)];
        return;
    } else if (_imgOrderTimeOK.hidden) {
        [self.view makeToast:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderTime", nil)]
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        //_lblWarningMsg.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderTime", nil)];
        return;
    } else if (_imgOrderTypeOK.hidden) {
        [self.view makeToast:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderType", nil)]
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        //_lblWarningMsg.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderType", nil)];
        return;
    } else if (_imgDeliveryAddressOK.hidden && _selectedMethod == 2) {
        [self.view makeToast:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderAddress", nil)]
                    duration:3.0
                    position:CSToastPositionCenter
                       style:nil];
        //_lblWarningMsg.text = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"PleaseChoose", nil), NSLocalizedString(@"OrderAddress", nil)];
        return;
    }
    
    ProductViewController *productVC = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil];
    productVC.isOrder = YES;
    [self.navigationController pushViewController:productVC animated:YES];
}

//==================================================================
#pragma SelectShopViewDelegate
//==================================================================

- (void)onSelectShop:(ShopInfo *)shop
{
    if (!shop) {
        NSLog(@"No shop selected!");
        return;
    }
    _selectedShop = shop;
    [_btnOrderBranch setTitle:_selectedShop.shopName forState:UIControlStateNormal];
    _imgOrderBranchOK.hidden = NO;
    g_SelectedShopId = shop.shopId;
    g_LastSelectedShopId = shop.shopId;
    g_SelectedShopName = shop.shopName;
    
    // Save shop id to user defaults
    [[NSUserDefaults standardUserDefaults] setObject:shop.shopId forKey:UD_KEY_LAST_SELECTED_SHOP_ID];
    [[NSUserDefaults standardUserDefaults] setObject:shop.shopName forKey:UD_KEY_LAST_SELECTED_SHOP_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
 
    // Get shop config method
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getShop00Config:[Global cfgAppId] shopId:_selectedShop.shopId];
}

//==================================================================
#pragma DeliveryAddressDelegate
//==================================================================

- (void)deliveryAddressSelected:(NSString *)address
{
    [self checkValidDeliveryAddress:address];
}

- (void)checkValidDeliveryAddress:(NSString *)address
{
    CLGeocoder *geocoder = [CLGeocoder new];
    [geocoder geocodeAddressString:address completionHandler:^(NSArray<CLPlacemark *> *placemarks, NSError *error) {
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
            return;
        }
        if (placemarks && placemarks.count>0) {
            CLPlacemark *placeMark = [placemarks firstObject];
            CLLocation *location = placeMark.location;
            CLLocationCoordinate2D coordinate = location.coordinate;
            NSLog(@"Geocoded location Lat=%.6f, Lon=%.6f", coordinate.latitude, coordinate.longitude);
            
            // Get distance to shop location
            CLLocation *shopLocation = [[CLLocation alloc] initWithLatitude:_selectedShop.latitude longitude:_selectedShop.longitude];
            CLLocationDistance distance = [shopLocation distanceFromLocation:location];
            NSLog(@"Distance to location = %f", distance);
            
            if (distance <= 2000) {
                // OK
                [_btnDeliveryAddress setTitle:address forState:UIControlStateNormal];
                g_SelectedRecAddr = address;
                
                // Save to user defaults
                [[NSUserDefaults standardUserDefaults] setObject:address forKey:UD_KEY_LAST_SELECTED_ADDR];
                [[NSUserDefaults standardUserDefaults] synchronize];
                _imgDeliveryAddressOK.hidden = NO;

            } else if (distance > 2000 && distance <= 5000) {
                // Warning
                [_btnDeliveryAddress setTitle:address forState:UIControlStateNormal];
                g_SelectedRecAddr = address;
                
                // Save to user defaults
                [[NSUserDefaults standardUserDefaults] setObject:address forKey:UD_KEY_LAST_SELECTED_ADDR];
                [[NSUserDefaults standardUserDefaults] synchronize];
                _imgDeliveryAddressOK.hidden = NO;
                
                [self.view makeToast:NSLocalizedString(@"DistanceWarning", nil)
                            duration:3.0
                            position:CSToastPositionCenter
                               style:nil];
            } else {
                // Cannot send
                [self.view makeToast:NSLocalizedString(@"DistanceTooFar", nil)
                            duration:3.0
                            position:CSToastPositionCenter
                               style:nil];
            }
        }
        return;
    }];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_SHOP00_CONFIG] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                int mealDays = [[jsonObject objectForKey:@"meal_days"] intValue];
                /*
                NSString *mealBeginTime = [jsonObject objectForKey:@"meal_btime"];
                NSString *mealEndTime = [jsonObject objectForKey:@"meal_etime"];
                int mealGap = [[jsonObject objectForKey:@"meal_gap"] intValue];
                int gapAmtLimit = [[jsonObject objectForKey:@"gap_amt_limit"] intValue];
                int orderAmtLimit = [[jsonObject objectForKey:@"order_amt_limit"] intValue];
                int preTime = [[jsonObject objectForKey:@"pretime"] intValue];
                */
                
                _selectedMealDays = mealDays;
                
            } else {
                NSString *msg = [jsonObject objectForKey:@"message"];
                NSLog(@"GetShop00Config error: %@", msg);
            }
        } else if ([resultName compare:WS_GET_SALE_METHOD] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *methods = [jsonObject objectForKey:@"data"];
                [_saleMethods removeAllObjects];
                [_saleMethodNames removeAllObjects];
                for (NSDictionary *method in methods) {
                    SaleMethod *saleMethod = [SaleMethod new];
                    saleMethod.isAddr = [[method objectForKey:@"isaddr"] boolValue];
                    saleMethod.methodId = [method objectForKey:@"method_id"];
                    saleMethod.name = [method objectForKey:@"name"];
                    saleMethod.serviceCharge = [[method objectForKey:@"servicecharge"] boolValue];
                    saleMethod.vipNonDisc = [[method objectForKey:@"vip_nondisc"] boolValue];
                    [_saleMethods addObject:saleMethod];
                    [_saleMethodNames addObject:saleMethod.name];
                }
            }
        } else if ([resultName compare:WS_GET_MEAL_TIME] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *mealTimes = [jsonObject objectForKey:@"data"];
                [_mealTimes removeAllObjects];
                for (NSDictionary *mealTime in mealTimes) {
                    NSString *time = [mealTime objectForKey:@"time"];
                    [_mealTimes addObject:time];
                }
                NSArray *restTimes = [jsonObject objectForKey:@"rest_time"];
                [_restTimes removeAllObjects];
                for (NSDictionary *restTime in restTimes) {
                    NSString *time = [restTime objectForKey:@"time"];
                    [_restTimes addObject:time];
                }
            } else {
                
            }
        } else if ([resultName compare:WS_GET_SHOP] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *shops = [jsonObject objectForKey:@"data"];
                [_shops removeAllObjects];
                for (NSDictionary *shop in shops) {
                    ShopInfo *shopInfo = [ShopInfo new];
                    shopInfo.shopId = [shop objectForKey:@"shop_id"];
                    shopInfo.shopName = [shop objectForKey:@"shop_name"];
                    shopInfo.address = [shop objectForKey:@"address"];
                    shopInfo.fax = [shop objectForKey:@"fax"];
                    shopInfo.tel = [shop objectForKey:@"tel"];
                    shopInfo.latitude = [[shop objectForKey:@"latitude"] doubleValue];
                    shopInfo.longitude = [[shop objectForKey:@"longitude"] doubleValue];
                    shopInfo.workTime = [shop objectForKey:@"worktime"];
                    shopInfo.htmlBody = [shop objectForKey:@"htmlbody"];
                    [_shops addObject:shopInfo];
                    
                    if (g_LastSelectedShopId && [shopInfo.shopId isEqualToString:g_LastSelectedShopId]) {
                        _selectedShop = shopInfo;
                        [_btnOrderBranch setTitle:_selectedShop.shopName forState:UIControlStateNormal];
                        _imgOrderBranchOK.hidden = NO;
                        g_SelectedShopId = shopInfo.shopId;
                        g_LastSelectedShopId = shopInfo.shopId;
                        g_SelectedShopName = shopInfo.shopName;
                    }
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
    [self.viewNetworkError setHidden:NO];
}

@end
