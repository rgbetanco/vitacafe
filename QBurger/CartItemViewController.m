//
//  CartItemViewController.m
//  QBurger
//
//  Created by Kevin Phua on 9/11/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "CartItemViewController.h"
#import "WebService.h"
#import "Global.h"
#import "UIImageView+WebCache.h"
#import "DQAlertView.h"
#import "Taste.h"
#import "TasteDetail.h"
#import "Combo.h"
#import "Pack.h"

@interface CartItemViewController () <WebServiceDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *tastes;
@property (strong, nonatomic) NSMutableArray *combinations;
@property (strong, nonatomic) NSMutableArray *packs;

@end

@implementation CartItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *btnDelete = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu_delete"] style:UIBarButtonItemStylePlain target:self action:@selector(onBtnDelete:)];
    self.navigationItem.rightBarButtonItem = btnDelete;
    
    _tastes = [NSMutableArray new];
    _combinations = [NSMutableArray new];
    _packs = [NSMutableArray new];
    
    if (_product) {
        if (_product.isComb) {
            // 組合餐(combination) 流程  get_comb + get_taste
            [self getComb];
        } else if (_product.isPack) {
            // 系列組合流程 (4種計算方式)  get_pack + get_taste
            [self getPack];
        } else if (!_product.isPack && !_product.isComb) {
            // 單一商品訂購，口味加值流程 get_taste 單一商品價格計算方式
            [self getTaste];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_product.prodName1 && _product.prodName1.length>0) {
        self.title = _product.prodName1;
    } else {
        self.title = _product.prodName2;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onBtnDelete:(id)sender {
    
}

- (void)getTaste {
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getTaste:[Global cfgAppId] prodId:_product.prodId];
}

- (void)getComb {
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getComb:[Global cfgAppId] prodId:_product.prodId];
}

- (void)getPack {
    WebService *ws = [WebService new];
    ws.delegate = self;
    [ws getPack:[Global cfgAppId] prodId:_product.prodId];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tastes count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat tableWidth = self.tableView.frame.size.width;
    
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableWidth, 100)];
    
    UILabel *lblTaste = [[UILabel alloc] initWithFrame:CGRectMake(100, 40, 100, 30)];
    lblTaste.text = _product.prodName1;

    UIImageView *imgTaste = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
    NSString *imgPath = _product.imgFile1;
    [imgTaste sd_setImageWithURL:[NSURL URLWithString:imgPath]
               placeholderImage:[UIImage imageNamed:@"login_logo"]];

    [header addSubview:imgTaste];
    [header addSubview:lblTaste];
    
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    Taste *taste = [self.tastes objectAtIndex:indexPath.row];
    cell.textLabel.text = taste.tasteName;
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_btn_more"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_TASTE] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                //NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                //NSLog(@"Get taste message = %@", message);
                
                [_tastes removeAllObjects];
                NSArray *tastes = [jsonObject objectForKey:@"taste"];
                for (NSDictionary *taste in tastes) {
                    Taste *tasteItem = [Taste new];
                    tasteItem.tasteId = [taste objectForKey:@"taste_id"];
                    tasteItem.prodId = [taste objectForKey:@"prod_id"];
                    tasteItem.tasteName = [taste objectForKey:@"taste_name"];
                    tasteItem.mutex = [[taste objectForKey:@"mutex"] boolValue];
                    tasteItem.tasteKind = [[taste objectForKey:@"kind"] boolValue];
                    NSMutableArray *detailsItem = [NSMutableArray new];
                    
                    NSArray *details = [taste objectForKey:@"detail"];
                    for (NSDictionary *detail in details) {
                        TasteDetail *tasteDetail = [TasteDetail new];
                        tasteDetail.tasteSno = [detail objectForKey:@"taste_sno"];
                        tasteDetail.name = [detail objectForKey:@"name"];
                        tasteDetail.price = [[detail objectForKey:@"price"] intValue];
                        [detailsItem addObject:tasteDetail];
                    }
                    
                    tasteItem.tasteDetails = detailsItem;
                    [_tastes addObject:tasteItem];
                }
                
                [self.tableView reloadData];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_GET_COMB] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                //NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Get comb message = %@", message);
                
                [_combinations removeAllObjects];
                NSArray *combs = [jsonObject objectForKey:@"comb"];
                for (NSDictionary *comb in combs) {
                    Combo *combItem = [Combo new];
                    combItem.combSno = [comb objectForKey:@"comb_sno"];
                    combItem.prodId = [comb objectForKey:@"prod_id"];
                    combItem.prodName = [comb objectForKey:@"prod_name1"];
                    combItem.quantity = [[comb objectForKey:@"quantity"] intValue];
                    combItem.price = [[comb objectForKey:@"price"] intValue];
                    combItem.isDefault = [[comb objectForKey:@"isdefault"] boolValue];
                    NSMutableArray *detailsItem = [NSMutableArray new];
                    
                    NSArray *details = [comb objectForKey:@"detail"];
                    for (NSDictionary *detail in details) {
                        Combo *combItem = [Combo new];
                        combItem.combSno = [detail objectForKey:@"comb_sno"];
                        combItem.prodId = [detail objectForKey:@"prod_id"];
                        combItem.prodName = [detail objectForKey:@"prod_name1"];
                        combItem.quantity = [[detail objectForKey:@"quantity"] intValue];
                        combItem.price = [[detail objectForKey:@"price"] intValue];
                        [detailsItem addObject:combItem];
                    }
                    
                    combItem.combDetails = detailsItem;
                    [_combinations addObject:combItem];
                }
                
                [self getTaste];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        } else if ([resultName compare:WS_GET_PACK] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                //NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Get pack message = %@", message);
                
                [_packs removeAllObjects];
                NSArray *packs = [jsonObject objectForKey:@"pack"];
                for (NSDictionary *pack in packs) {
                    Pack *packItem = [Pack new];
                    packItem.packId = [pack objectForKey:@"pack_id"];
                    packItem.psuitId = [pack objectForKey:@"psuit_id"];
                    packItem.psuitName = [pack objectForKey:@"psuit_name"];
                    packItem.packPrcType = [[pack objectForKey:@"packprc_type"] intValue];
                    packItem.packQty = [[pack objectForKey:@"pack_qty"] intValue];
                    packItem.price = [[pack objectForKey:@"price"] intValue];
                    NSMutableArray *detailsItem = [NSMutableArray new];
                    
                    NSArray *details = [pack objectForKey:@"detail"];
                    for (NSDictionary *detail in details) {
                        Pack *packItem = [Pack new];
                        packItem.packId = [detail objectForKey:@"pack_id"];
                        packItem.psuitId = [detail objectForKey:@"psuit_id"];
                        packItem.psuitName = [detail objectForKey:@"psuit_name"];
                        packItem.packPrcType = [[detail objectForKey:@"packprc_type"] intValue];
                        packItem.packQty = [[detail objectForKey:@"pack_qty"] intValue];
                        packItem.price = [[detail objectForKey:@"price"] intValue];
                        packItem.plusPrice = [[detail objectForKey:@"plus_price"] intValue];
                        [detailsItem addObject:packItem];
                    }
                    
                    packItem.packDetails = detailsItem;
                    [_packs addObject:packItem];
                }
                
                [self getTaste];
                
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RegisterFailed", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
