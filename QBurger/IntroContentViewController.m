//
//  IntroContentViewController.m
//  VitaCafe
//
//  Created by Kevin Phua on 2016/11/28.
//  Copyright (c) 2016 hagarsoft. All rights reserved.
//

#import "IntroContentViewController.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"

@interface IntroContentViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;

@end

@implementation IntroContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set image
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    NSURL *imageURL = [NSURL URLWithString:_imagePath];
    
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                _contentImageView.image = image;
                            }
                        }];
    
    UITapGestureRecognizer *tapGestureBanner = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapBanner:)];
    [self.contentImageView setUserInteractionEnabled:YES];
    [self.contentImageView addGestureRecognizer:tapGestureBanner];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onTapBanner:(UITapGestureRecognizer *)tapGesture {
    // Open webpage url
    if (_websiteUrl && _websiteUrl.length>0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_websiteUrl]];
    }
}

@end
