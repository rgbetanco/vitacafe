//
//  Combo.m
//  QBurger
//
//  Created by Kevin Phua on 9/8/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "Combo.h"

@implementation Combo

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // Copy NSObject subclasses
        [copy setCombSno:[self.combSno copyWithZone:zone]];
        [copy setProdId:[self.prodId copyWithZone:zone]];
        [copy setProdName:[self.prodName copyWithZone:zone]];
        [copy setImgFile:[self.imgFile copyWithZone:zone]];
        [copy setCombDetails:[self.combDetails copyWithZone:zone]];
        
        // Set primitives
        [copy setQuantity:self.quantity];
        [copy setPrice:self.price];
        [copy setIsDefault:self.isDefault];
    }
    
    return copy;
}

@end
