//
//  LeftSidebarController.m
//  RunnerPlaza
//
//  Created by Kevin Phua on 10/7/14.
//  Copyright (c) 2014 Hagarsoft. All rights reserved.
//

#import "LeftSidebarController.h"
#import "SWRevealViewController.h"
#import "IntroViewController.h"
#import "PromoViewController.h"
#import "ProductViewController.h"
#import "MemberInfoViewController.h"
#import "LocationsViewController.h"
#import "SettingsViewController.h"
#import "CustomPageViewController.h"
#import "AppDelegate.h"
#import "Global.h"
#import "RaiseAlertView.h"
#import "UIView+Toast.h"

#define MENU_ITEM_INTRO             0   // 首頁
#define MENU_ITEM_ORDER             1   // 我要點餐
#define MENU_ITEM_PRODUCTS          2   // 商品介紹

@interface LeftSidebarController () <WebServiceDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSInteger presentedRow;
@property (weak,nonatomic) UILabel *lblUserName;

@property (strong, nonatomic) NSMutableArray *pageList;
@property (strong, nonatomic) NSMutableArray *pageUrls;

@end

@implementation LeftSidebarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Init reveal view controller
    //SWRevealViewController *revealController = [self revealViewController];

    /*
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu_drawer"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.rightBarButtonItem = revealButtonItem;
    
    // Main logo
    UIImageView *imgLogo = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 128, 32)];
    imgLogo.image = [UIImage imageNamed:@"main_logo"];
    [self.navigationController.navigationBar addSubview:imgLogo];
    */
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorColor = [UIColor clearColor];
    
    _menuItems = [[NSArray alloc] initWithObjects:NSLocalizedString(@"HomePage", nil),  NSLocalizedString(@"BtnMakeOrder", nil), NSLocalizedString(@"Products", nil), nil];
    
    _pageList = [NSMutableArray new];
    _pageUrls = [NSMutableArray new];
    _presentedRow = -1;
    
    // Get page list
    [self getPageList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (g_IsLogin) {
        [self.btnLogout setTitle:NSLocalizedString(@"Logout", nil) forState:UIControlStateNormal];
        [self.btnLogout setImage:[UIImage imageNamed:@"ic_menu_logout"] forState:UIControlStateNormal];
    } else {
        [self.btnLogout setTitle:NSLocalizedString(@"Login", nil) forState:UIControlStateNormal];
        [self.btnLogout setImage:[UIImage imageNamed:@"ic_menu_friends"] forState:UIControlStateNormal];
    }
    
    // Get page list
    [self getPageList];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getPageList {
    WebService *ws = [[WebService alloc] init];
    [ws setDelegate:self];
    [ws getPageList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat tableWidth = self.tableView.frame.size.width;
    
    UIView *sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableWidth, 60)];
    
    UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(20, 5, tableWidth-20, 25)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    lblName.font = [UIFont boldSystemFontOfSize:20];
    
    if (g_IsLogin) {
        NSString *name = [g_MemberInfo objectForKey:INFO_KEY_NAME];
        if (name && name.length > 0) {
            lblName.text = name;
            
            UITapGestureRecognizer *tapGestureMemberInfo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblTitleClicked:)];
            lblName.userInteractionEnabled = YES;
            [lblName addGestureRecognizer:tapGestureMemberInfo];

        } else {
            lblName.text = NSLocalizedString(@"NotLogin", nil);
        }
    } else {
        lblName.text = NSLocalizedString(@"NotLogin", nil);
    }

    [sectionView addSubview:lblName];
    
    return sectionView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _menuItems.count + _pageList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.row < _menuItems.count) {
        cell.textLabel.text = [_menuItems objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [_pageList objectAtIndex:(indexPath.row - _menuItems.count)];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.highlightedTextColor = [UIColor redColor];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
    
    UIView *selectionColor = [[UIView alloc] init];
    selectionColor.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = selectionColor;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // selecting row
    NSInteger row = indexPath.row;
    
    // if we are trying to push the same row or perform an operation that does not imply frontViewController replacement
    // we'll just set position and return
    if ( row == _presentedRow )
    {
        [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        return;
    }
    
    if (row < _menuItems.count) {
        switch (row) {
            case MENU_ITEM_INTRO:
                {
                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    IntroViewController *introViewController = appDelegate.introViewController;
                    
                    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:introViewController];
                    [revealController pushFrontViewController:frontNavigationController animated:YES];
                }
                break;

            case MENU_ITEM_ORDER:
                {
                    if (!g_IsLogin) {
                        [self.view makeToast:NSLocalizedString(@"PleaseLogin", nil)
                                    duration:3.0
                                    position:CSToastPositionCenter
                                       style:nil];
                        return;
                    }

                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    OrderMainViewController *orderViewController = appDelegate.orderViewController;
                    
                    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:orderViewController];
                    [revealController pushFrontViewController:frontNavigationController animated:YES];
                }
                break;
                
            case MENU_ITEM_PRODUCTS:
                {
                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    ProductViewController *productViewController = appDelegate.productViewController;
                    productViewController.isOrder = NO;
                    
                    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:productViewController];
                    [revealController pushFrontViewController:frontNavigationController animated:YES];
                }
                break;
            
            default:
                break;
        }
    } else {
        // Custom rows
        int customRow = (int)(row - _menuItems.count);
        
        NSString *subject = [_pageList objectAtIndex:customRow];
        NSString *url = [_pageUrls objectAtIndex:customRow];
        
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        CustomPageViewController *customPageViewController = appDelegate.customPageViewController;
        customPageViewController.subject = subject;
        customPageViewController.websiteUrl = url;
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:customPageViewController];
        [revealController pushFrontViewController:frontNavigationController animated:YES];
    }
    
    _presentedRow = row;  // <- store the presented row
}

- (void)setCurrentRow:(int)row
{
    _presentedRow = row;
}

- (void)reloadCurrentViewController
{
    SWRevealViewController *revealController = self.revealViewController;
    
    if (_presentedRow < _menuItems.count) {
        switch (_presentedRow) {
            case MENU_ITEM_ORDER:
                {
                    if (!g_IsLogin) {
                        [self.view makeToast:NSLocalizedString(@"PleaseLogin", nil)
                                    duration:3.0
                                    position:CSToastPositionCenter
                                       style:nil];
                        return;
                    }

                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    OrderMainViewController *orderViewController = appDelegate.orderViewController;
                    [orderViewController updateNavigationBar];
                    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:orderViewController];
                    [revealController pushFrontViewController:frontNavigationController animated:YES];
                }
                break;
                
            case MENU_ITEM_INTRO:
                {
                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    IntroViewController *introViewController = appDelegate.introViewController;
                    [introViewController updateNavigationBar];
                    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:introViewController];
                    [revealController pushFrontViewController:frontNavigationController animated:YES];
                }
                break;
                
            case MENU_ITEM_PRODUCTS:
                {
                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    ProductViewController *productViewController = appDelegate.productViewController;
                    productViewController.isOrder = NO;
                    
                    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:productViewController];
                    [revealController pushFrontViewController:frontNavigationController animated:YES];
                }
                break;
                
            default:
                {
                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    MemberInfoViewController *memberInfoViewController = appDelegate.memberInfoViewController;
                    [memberInfoViewController updateNavigationBar];
                    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:memberInfoViewController];
                    [revealController pushFrontViewController:frontNavigationController animated:YES];
                }
                break;
        }
    } else {
        
    }
}

#pragma mark - Drawer status delegate

- (IBAction)btnSettingsClicked:(id)sender
{
    SWRevealViewController *revealController = self.revealViewController;

    if (g_IsLogin) {
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        SettingsViewController *settingsViewController = appDelegate.settingsViewController;
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
        [revealController pushFrontViewController:frontNavigationController animated:YES];
    } else {
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        LoginViewController *loginViewController = appDelegate.loginViewController;
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
        [revealController pushFrontViewController:frontNavigationController animated:YES];
    }
}

- (IBAction)lblTitleClicked:(id)sender
{
    if (g_IsLogin) {
        SWRevealViewController *revealController = self.revealViewController;
        
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        MemberInfoViewController *memberInfoViewController = appDelegate.memberInfoViewController;
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:memberInfoViewController];
        [revealController pushFrontViewController:frontNavigationController animated:YES];

        [self setCurrentRow:1];
    }
}

- (IBAction)btnLogoutClicked:(id)sender
{
    if (g_IsLogin) {
        RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"ConfirmLogout", nil)
                                                                  message:NSLocalizedString(@"LogoutMessage", nil)
                                                        cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                         otherButtonTitle:NSLocalizedString(@"Yes", nil)];
        
        alertView.cancelButtonAction = ^{
            // Do nothing
        };
        alertView.otherButtonAction = ^{
            // Clear logout
            g_IsLogin = NO;
            g_MemberPhone = nil;
            g_MemberPassword = nil;
            [g_MemberInfo removeAllObjects];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:UD_KEY_PHONE];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:UD_KEY_PASSWORD];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.tableView reloadData];
            
            // Reload navbar of presented view controller
            _presentedRow = 0;
            [self reloadCurrentViewController];
        };
        [alertView show];
    } else {
        SWRevealViewController *revealController = self.revealViewController;
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        LoginViewController *loginViewController = appDelegate.loginViewController;
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
        [revealController pushFrontViewController:frontNavigationController animated:YES];
    }
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_PAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSArray *pages = (NSArray *)[jsonObject objectForKey:@"data"];
                if (pages) {
                    [_pageList removeAllObjects];
                    [_pageUrls removeAllObjects];
                    
                    for (NSDictionary *page in pages) {
                        NSString *subject = [page objectForKey:@"subject"];
                        NSString *url = [page objectForKey:@"url"];
                        NSString *cCode = [page objectForKey:@"c_code"];
                        if (![cCode isKindOfClass:[NSNull class]] &&
                            [cCode isEqualToString:@"PRIVACY"]) {
                            // Save url
                            g_PrivacyUrl = [NSString stringWithString:url];
                        }
                        
                        NSInteger appMenu = [[page objectForKey:@"app_menu"] integerValue];
                        if (appMenu == 1) {
                            [_pageList addObject:subject];
                            [_pageUrls addObject:url];
                        }
                    }
                    [self.tableView reloadData];
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
