//
//  MemberInfoViewController.m
//  PosApp
//
//  Created by Kevin Phua on 9/21/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import "MemberInfoViewController.h"
#import "SWRevealViewController.h"
#import "SettingsViewController.h"
#import "PromoListCell.h"
#import "BonusListViewController.h"
#import "BonusTransferViewController.h"
#import "TicketListViewController.h"
#import "TicketDetailViewController.h"
#import "Global.h"
#import "RaiseAlertView.h"
#import "OrderRecordsViewController.h"

@interface MemberInfoViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewQrcode;
@property (weak, nonatomic) IBOutlet UIImageView *imgMemberCard;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberName;
@property (weak, nonatomic) IBOutlet UIImageView *imgBarcode;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberId;
@property (weak, nonatomic) IBOutlet UIButton *btnPointHistory;
@property (weak, nonatomic) IBOutlet UIButton *btnPointTransfer;
@property (weak, nonatomic) IBOutlet UIButton *btnTicketHistory;
@property (weak, nonatomic) IBOutlet UIButton *btnPurchaseHistory;
@property (weak, nonatomic) IBOutlet UILabel *lblCollections;
@property (weak, nonatomic) IBOutlet UILabel *lblRecentPurchases;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UIButton *btnSettings;
@property (nonatomic, strong) UIButton *btnLogout;

@property (strong, nonatomic) NSMutableArray *tickets;

@end

@implementation MemberInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"MemberCenter", nil);

    // Do any additional setup after loading the view from its nib.
    [self.btnPointHistory setTitle:NSLocalizedString(@"PointHistory", nil) forState:UIControlStateNormal];
    [self.btnPointTransfer setTitle:NSLocalizedString(@"Transfer", nil) forState:UIControlStateNormal];
    [self.btnTicketHistory setTitle:NSLocalizedString(@"PointUsageHistory", nil) forState:UIControlStateNormal];
    
    [self.btnPurchaseHistory setTitle:NSLocalizedString(@"PurchaseHistory", nil) forState:UIControlStateNormal];
    
    self.lblCollections.text = NSLocalizedString(@"Collections", nil);
    self.lblRecentPurchases.text = NSLocalizedString(@"RecentPurchases", nil);
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = TABLEVIEW_CORNER_RADIUS;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _viewQrcode.layer.borderWidth = 2.0f;
    _viewQrcode.layer.borderColor = [UIColor blackColor].CGColor;
    _viewQrcode.layer.cornerRadius = 5.0f;
    
    self.tickets = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self clearNavigationBar];
    
    [self addNavBarButtons];
    
    // Try to login
    g_MemberPhone = [[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_PHONE];
    g_MemberPassword = [[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_PASSWORD];
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:UD_DEVICE_TOKEN];
    if (g_MemberPhone && g_MemberPassword && deviceToken) {
        WebService *ws = [[WebService alloc] init];
        ws.delegate = self;
        [ws login:[Global cfgAppId] sId:g_MemberPhone password:g_MemberPassword deviceId:deviceToken];
        [ws showWaitingView:self.view];
    }

    int points = [[g_MemberInfo objectForKey:INFO_KEY_LAST_POINT] intValue];

    _lblMemberName.text = [g_MemberInfo objectForKey:INFO_KEY_NAME];
    _lblMemberId.text = [g_MemberInfo objectForKey:INFO_KEY_VIPID];
    _lblMemberPoints.text = [NSString stringWithFormat:@"%@: %d%@",
                             NSLocalizedString(@"MemberPoints", nil),
                             points,
                             NSLocalizedString(@"Points", nil)];
    
    [self getTicketList];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (_btnLogout) {
        [_btnLogout removeFromSuperview];
    }
    if (_btnSettings) {
        [_btnSettings removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addNavBarButtons {
    int navBarWidth = self.navigationController.navigationBar.frame.size.width;
    
    // Settings button
    if (!_btnSettings) {
        _btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnSettings setImage:[UIImage imageNamed:@"ic_menu_settings"] forState:UIControlStateNormal];
        [_btnSettings addTarget:self action:@selector(onBtnSettings:) forControlEvents:UIControlEventTouchUpInside];
        _btnSettings.frame = CGRectMake(navBarWidth-90, 8, 32, 32);
        [_btnSettings.titleLabel setHidden:YES];
    }
    [self.navigationController.navigationBar addSubview:_btnSettings];
    
    // Logout button
    if (!_btnLogout) {
        _btnLogout = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnLogout setImage:[UIImage imageNamed:@"ic_menu_logout"] forState:UIControlStateNormal];
        [_btnLogout addTarget:self action:@selector(onBtnLogout:) forControlEvents:UIControlEventTouchUpInside];
        _btnLogout.frame = CGRectMake(navBarWidth-42, 8, 32, 32);
        [_btnLogout.titleLabel setHidden:YES];
    }
    [self.navigationController.navigationBar addSubview:_btnLogout];
    
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)getTicketList {
    WebService *ws = [[WebService alloc] init];
    [ws getTicketList:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    [ws setDelegate:self];
    [ws showWaitingView:self.view];
}

- (void)getCardNumber {
    WebService *ws = [[WebService alloc] init];
    [ws getVirtualCard:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY]];
    [ws setDelegate:self];
}

- (void)updateVipCard {
    // Set member card background
    NSString *vipLevel = [g_MemberInfo objectForKey:INFO_KEY_VIP_LEVEL];
    if ([vipLevel compare:VIP_LEVEL_BLACK] == NSOrderedSame) {
        self.imgMemberCard.image = [UIImage imageNamed:@"id_bkgnd_a"];
    } else if ([vipLevel compare:VIP_LEVEL_RED] == NSOrderedSame) {
        self.imgMemberCard.image = [UIImage imageNamed:@"id_bkgnd_b"];
    } else if ([vipLevel compare:VIP_LEVEL_SILVER] == NSOrderedSame) {
        self.imgMemberCard.image = [UIImage imageNamed:@"id_bkgnd_c"];
    } else if ([vipLevel compare:VIP_LEVEL_GOLD] == NSOrderedSame) {
        self.imgMemberCard.image = [UIImage imageNamed:@"id_bkgnd_d"];
    }
}

- (IBAction)onBtnSettings:(id)sender {
    SettingsViewController *settingsController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    [self.navigationController pushViewController:settingsController animated:YES];
}

- (IBAction)onBtnLogout:(id)sender {
    RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"ConfirmLogout", nil)
                                                              message:NSLocalizedString(@"LogoutMessage", nil)
                                                    cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                     otherButtonTitle:NSLocalizedString(@"Yes", nil)];
    
    alertView.cancelButtonAction = ^{
        // Do nothing
    };
    alertView.otherButtonAction = ^{
        // Clear logout
        g_IsLogin = NO;
        g_MemberPhone = nil;
        g_MemberPassword = nil;
        [g_MemberInfo removeAllObjects];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:UD_KEY_PHONE];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:UD_KEY_PASSWORD];
        [[NSUserDefaults standardUserDefaults] synchronize];
    };
    [alertView show];
}

- (IBAction)onBtnPointHistory:(id)sender {
    BonusListViewController *bonusListController = [[BonusListViewController alloc] initWithNibName:@"BonusListViewController" bundle:nil];
    
    // Hide back button text on next page
    self.title = @"";
    [self clearNavigationBar];

    [self.navigationController pushViewController:bonusListController animated:YES];
}

- (IBAction)onBtnPointTransfer:(id)sender {
    BonusTransferViewController *bonusTransferController = [[BonusTransferViewController alloc] initWithNibName:@"BonusTransferViewController" bundle:nil];
    
    // Hide back button text on next page
    self.title = @"";
    [self clearNavigationBar];
    
    [self.navigationController pushViewController:bonusTransferController animated:YES];
}

- (IBAction)onBtnTicketHistory:(id)sender {
    TicketListViewController *ticketListController = [[TicketListViewController alloc] initWithNibName:@"TicketListViewController" bundle:nil];
    
    // Hide back button text on next page
    self.title = @"";
    [self clearNavigationBar];
    
    [self.navigationController pushViewController:ticketListController animated:YES];
}

- (IBAction)onBtnPurchaseHistory:(id)sender {
    // Hide back button text on next page
    self.title = @"";
    [self clearNavigationBar];

    OrderRecordsViewController *orderRecords = [[OrderRecordsViewController alloc] initWithNibName:@"OrderRecordsViewController" bundle:nil];
    [self.navigationController pushViewController:orderRecords animated:YES];
}

//==================================================================
#pragma mark - Table view delegate
//==================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tickets count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TicketCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }

    NSDictionary *ticket = [self.tickets objectAtIndex:[indexPath row]];
    NSString *ticketName = [ticket objectForKey:@"subject"];
    
    NSString *validDate = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Valid", nil), [ticket objectForKey:@"edate"]];
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [Global colorWithType:COLOR_BTN_CAT_LIST_BG];
    
    cell.textLabel.text = ticketName;
    cell.detailTextLabel.text = validDate;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *ticket = [self.tickets objectAtIndex:[indexPath row]];
    
    // Open ProductDetailViewController
    TicketDetailViewController *ticketDetailController = [[TicketDetailViewController alloc] initWithNibName:@"TicketDetailViewController" bundle:nil];
    [ticketDetailController setTicket:ticket];
    
    // Hide back button text on next page
    self.title = @"";
    [self clearNavigationBar];
    
    [self.navigationController pushViewController:ticketDetailController animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_GET_TICKET_LIST] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ tickets", datacnt);
                
                NSArray *tickets = (NSArray *)[jsonObject objectForKey:@"ticket"];
                
                if (tickets) {
                    [self.tickets setArray:tickets];
                }
                [self.tableView reloadData];                
                [self getCardNumber];
            } else {
                // Failure
                /*
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_SEARCH_LAST_POINT_VALUE] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *vipPoint = (NSString *)[jsonObject objectForKey:@"vip_point"];
                if (vipPoint && vipPoint.length > 0) {
                    NSNumber *vipPointValue = [NSNumber numberWithInteger:[vipPoint intValue]];
                    [g_MemberInfo setObject:vipPointValue forKey:INFO_KEY_LAST_POINT];
                }
                [self updateVipPoints];
                
                int points = [[g_MemberInfo objectForKey:INFO_KEY_LAST_POINT] intValue];
                _lblMemberPoints.text = [NSString stringWithFormat:@"%@: %d%@",
                                         NSLocalizedString(@"MemberPoints", nil),
                                         points,
                                         NSLocalizedString(@"Points", nil)];
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_FRIEND_LIST] == NSOrderedSame) {
            NSMutableArray *pendingFriends = [NSMutableArray new];
            
            long code = [[jsonObject objectForKey:@"code"] longValue];
            //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
            if (code == 0) {
                // Success
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ friends", datacnt);
                
                NSArray *friends = (NSArray *)[jsonObject objectForKey:@"friends"];
                for (NSDictionary *friend in friends) {
                    NSNumber *status = [friend objectForKey:@"status"];
                    if ([status intValue] == STATUS_AWAITING_APPROVAL) {
                        NSNumber *kind = [friend objectForKey:@"kind"];
                        if ([kind intValue] == KIND_INVITED_FRIENDS) {
                            [pendingFriends addObject:friend];
                        }
                    }
                }
                
                NSNumber *numPending = [NSNumber numberWithInteger:pendingFriends.count];
                [g_MemberInfo setObject:numPending forKey:INFO_KEY_PENDING_FRIENDS];
                [self updateFriends:PARENT_PAGE_DEFAULT];
                
            } else {
                /*
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil, nil];
                [alertView show];
                 */
            }
        } else if ([resultName compare:WS_GET_MESSAGE_LIST] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *datacnt = (NSString *)[jsonObject objectForKey:@"datacnt"];
                
                NSLog(@"Total %@ messages", datacnt);
                
                NSArray *messages = (NSArray *)[jsonObject objectForKey:@"data"];
                
                if (messages) {
                    int unreadMessages = 0;
                    for (NSDictionary *message in messages) {
                        NSString *readTime = [message objectForKey:@"read_time"];
                        if ([readTime isEqualToString:@""]) {
                            unreadMessages++;
                        }
                    }
                    
                    [g_MemberInfo setObject:[NSNumber numberWithInt:unreadMessages] forKey:INFO_KEY_MSGS_UNREAD];
                    [self updateMessages:PARENT_PAGE_DEFAULT];
                }
            }
        } else if ([resultName compare:WS_GET_VIRTUAL_CARD] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                //NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                NSString *cardNo = (NSString *)[jsonObject objectForKey:@"card_no"];

                NSLog(@"cardNo = %@", cardNo);
                if (cardNo) {
                    // Generate QRCode
                    CGImageRef qrCodeImage = [Global createQRImageForString:cardNo size:CGSizeMake(120,120)];
                    _imgBarcode.image = [UIImage imageWithCGImage:qrCodeImage];
                }
            }
        } else if ([resultName compare:WS_LOGIN] == NSOrderedSame) {
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *accKey = (NSString *)[jsonObject objectForKey:@"acckey"];
                if (accKey && accKey.length > 0) {
                    [g_MemberInfo setObject:accKey forKey:INFO_KEY_ACCKEY];
                }
                NSString *vipId = (NSString *)[jsonObject objectForKey:@"vip_id"];
                if (vipId && vipId.length > 0) {
                    [g_MemberInfo setObject:vipId forKey:INFO_KEY_VIPID];
                }
                NSString *vipLevel = (NSString *)[jsonObject objectForKey:@"vip_level"];
                if (vipLevel && vipLevel.length > 0) {
                    [g_MemberInfo setObject:vipLevel forKey:INFO_KEY_VIP_LEVEL];
                }
                NSString *vipLevelName = (NSString *)[jsonObject objectForKey:@"vip_level_name"];
                if (vipLevelName && vipLevelName.length > 0) {
                    [g_MemberInfo setObject:vipLevelName forKey:INFO_KEY_VIP_LEVEL_NAME];
                }
                NSString *name = (NSString *)[jsonObject objectForKey:@"name"];
                if (name && name.length > 0) {
                    [g_MemberInfo setObject:name forKey:INFO_KEY_NAME];
                }
                id zip = [jsonObject objectForKey:@"zip"];
                if ([zip isKindOfClass:[NSString class]]) {
                    NSString *zipString = (NSString *)zip;
                    [g_MemberInfo setObject:zipString forKey:INFO_KEY_ZIP];
                }
                NSString *address = (NSString *)[jsonObject objectForKey:@"address"];
                if (address && address.length > 0) {
                    [g_MemberInfo setObject:address forKey:INFO_KEY_ADDRESS];
                }
                NSString *telephone = (NSString *)[jsonObject objectForKey:@"telephone"];
                if (telephone && telephone.length > 0) {
                    [g_MemberInfo setObject:telephone forKey:INFO_KEY_TELEPHONE];
                }
                NSString *sex = (NSString *)[jsonObject objectForKey:@"sex"];
                if (sex && sex.length > 0) {
                    [g_MemberInfo setObject:sex forKey:INFO_KEY_SEX];
                }
                NSString *birthday = (NSString *)[jsonObject objectForKey:@"birthday"];
                if (birthday && birthday.length > 0) {
                    NSRange range = [birthday rangeOfString:@"T"];
                    if (range.location != NSNotFound) {
                        birthday = [birthday substringToIndex:range.location];
                    }
                    [g_MemberInfo setObject:birthday forKey:INFO_KEY_BIRTHDAY];
                }
                NSString *email = (NSString *)[jsonObject objectForKey:@"email"];
                if (email && email.length > 0) {
                    [g_MemberInfo setObject:email forKey:INFO_KEY_EMAIL];
                }
                NSString *mobile = (NSString *)[jsonObject objectForKey:@"mobile"];
                if (mobile && mobile.length > 0) {
                    [g_MemberInfo setObject:mobile forKey:INFO_KEY_MOBILE];
                }
                NSString *lastPoint = (NSString *)[jsonObject objectForKey:@"last_point"];
                if (lastPoint && lastPoint.length > 0) {
                    NSNumber *lastPointValue = [NSNumber numberWithInteger:[lastPoint intValue]];
                    [g_MemberInfo setObject:lastPointValue forKey:INFO_KEY_LAST_POINT];
                }
                NSString *lastAmt = (NSString *)[jsonObject objectForKey:@"last_amt"];
                if (lastAmt && lastAmt.length > 0) {
                    [g_MemberInfo setObject:lastAmt forKey:INFO_KEY_LAST_AMT];
                }
                NSString *lastIcPoint = (NSString *)[jsonObject objectForKey:@"last_icpoint"];
                if (lastIcPoint && lastIcPoint.length > 0) {
                    [g_MemberInfo setObject:lastIcPoint forKey:INFO_KEY_LAST_IC_POINT];
                }
                NSString *card = (NSString *)[jsonObject objectForKey:@"card"];
                if (card && card.length > 0) {
                    [g_MemberInfo setObject:card forKey:INFO_KEY_CARD];
                }
                NSString *endDate = (NSString *)[jsonObject objectForKey:@"end_date"];
                if (endDate && endDate.length > 0) {
                    [g_MemberInfo setObject:endDate forKey:INFO_KEY_END_DATE];
                }
                NSString *idCard = (NSString *)[jsonObject objectForKey:@"id_card"];
                if (idCard && idCard.length > 0) {
                    [g_MemberInfo setObject:idCard forKey:INFO_KEY_ID_CARD];
                }
                
                NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                                      dateStyle:NSDateFormatterShortStyle
                                                                      timeStyle:NSDateFormatterFullStyle];
                [[NSUserDefaults standardUserDefaults] setObject:dateString forKey:UD_KEY_LAST_LOGIN];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                g_IsLogin = YES;
                
                [self getTicketList];
                [self updateVipCard];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                if ([resultName compare:WS_LOGIN] == NSOrderedSame) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"LoginFailed", nil)
                                                                        message:message
                                                                       delegate:nil
                                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                              otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
