//
//  ProductDetailViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/1/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Product.h"
#import "CartItem.h"

@interface ProductDetailViewController : BaseViewController

@property (nonatomic, strong) Product *product;
@property (nonatomic, strong) CartItem *cartItem;
@property (nonatomic) NSInteger cartItemIndex;
@property (nonatomic) BOOL isOrder;     // 是否進入點餐流程

@end
