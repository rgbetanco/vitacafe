//
//  TicketDetailViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/1/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TicketDetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary *ticket;

@end
