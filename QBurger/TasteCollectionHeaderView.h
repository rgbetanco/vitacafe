//
//  TasteCollectionHeaderView.h
//  QBurger
//
//  Created by Kevin Phua on 10/30/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TasteCollectionHeaderView : UICollectionReusableView

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@end
