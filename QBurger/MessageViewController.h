//
//  MessageViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/6/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface MessageViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, WebServiceDelegate>


@end
