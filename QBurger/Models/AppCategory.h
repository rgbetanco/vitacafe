//
//  AppCategory.h
//  QBurger
//
//  Created by Kevin Phua on 28/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppCategory : NSObject <NSCoding>

@property (nonatomic, strong) NSString *imgFile1;
@property (nonatomic, strong) NSString *imgFile2;
@property (nonatomic) int serNo;
@property (nonatomic, strong) NSString *subject;

@end
