//
//  BonusTransferViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/26/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface BonusTransferViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, WebServiceDelegate>

@end
