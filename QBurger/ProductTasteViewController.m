//
//  ProductTasteViewController.m
//  QBurger
//
//  Created by Kevin Phua on 10/28/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "ProductTasteViewController.h"
#import "TasteViewCell.h"
#import "TasteDetail.h"
#import "TasteCollectionHeaderView.h"

@interface ProductTasteViewController ()<TasteViewCellDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *selectedTastes;
@property (nonatomic, strong) NSMutableArray *selectedTasteIndexes; // Stores bitmap of selected taste indexes

@end

@implementation ProductTasteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UINib *cellNib = [UINib nibWithNibName:@"TasteViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"TasteViewCell"];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.collectionView registerClass:[TasteCollectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    // Build selected tastes and indexes
    _selectedTastes = [NSMutableArray new];
    _selectedTasteIndexes = [NSMutableArray new];
    
    for (int i=0; i<_tastes.count; i++) {
        [_selectedTasteIndexes addObject:[NSNumber numberWithInt:0]];
      
        Taste *selectedTaste = [Taste new];
        Taste *taste = [_tastes objectAtIndex:i];
        selectedTaste.tasteId = taste.tasteId;
        selectedTaste.tasteName = taste.tasteName;
        selectedTaste.prodId = taste.prodId;
        selectedTaste.tasteKind = taste.tasteKind;
        selectedTaste.mutex = taste.mutex;
        [_selectedTastes addObject:selectedTaste];
    }
    
    // Merge input selected tastes into main selected tastes
    for (Taste *inputSelectedTaste in _inputSelectedTastes) {
        for (Taste *selectedTaste in _selectedTastes) {
            if ([inputSelectedTaste.prodId isEqualToString:selectedTaste.prodId] &&
                [inputSelectedTaste.tasteId isEqualToString:selectedTaste.tasteId]) {
                selectedTaste.tasteDetails = [NSArray arrayWithArray:inputSelectedTaste.tasteDetails];
                
                // Sync selected taste indexes
                if (selectedTaste.tasteDetails && selectedTaste.tasteDetails.count>0) {
                    for (TasteDetail *tasteDetail in selectedTaste.tasteDetails) {
                        NSLog(@"Selected taste detail = %@", tasteDetail.name);
                    }
                }
            }
        }
    }
    
    // Sync selected taste indexes
    for (int i=0; i<_tastes.count; i++) {
        Taste *taste = [_tastes objectAtIndex:i];
        int currentSelectTaste = 0;
        for (Taste *selectedTaste in _selectedTastes) {
            if (![selectedTaste.tasteId isEqualToString:taste.tasteId]) {
                continue;
            }
            
            if (selectedTaste.mutex) {
                TasteDetail *selectedTasteDetail = selectedTaste.tasteDetails.firstObject;
                int tasteIndex = 1 << ([selectedTasteDetail.tasteSno intValue] - 1);
                NSNumber *selectedTasteIndex = [NSNumber numberWithInt:tasteIndex];
                [_selectedTasteIndexes setObject:selectedTasteIndex atIndexedSubscript:i];
            } else {
                if ([taste.prodId isEqualToString:selectedTaste.prodId]) {
                    int tasteIndex = 0;
                    for (int j=0; j<taste.tasteDetails.count; j++) {
                        TasteDetail *tasteDetail = [taste.tasteDetails objectAtIndex:j];
                        for (TasteDetail *selectedTasteDetail in selectedTaste.tasteDetails) {
                            if ([selectedTasteDetail.tasteSno isEqualToString:tasteDetail.tasteSno]) {
                                tasteIndex |= (1 << j);
                                break;
                            }
                        }
                    }
                    NSNumber *selectedTasteIndex = [NSNumber numberWithInt:tasteIndex];
                    [_selectedTasteIndexes setObject:selectedTasteIndex atIndexedSubscript:i];
                }
            }
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnConfirm:(id)sender {
    if (self.delegate) {
        [self.delegate onSelectedTaste:_selectedTastes sender:self];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onBtnCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//==================================================================
#pragma mark - UICollectionViewDelegate
//==================================================================

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return _tastes.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    Taste *taste = [_tastes objectAtIndex:section];
    return taste.tasteDetails.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    Taste *taste = [_tastes objectAtIndex:indexPath.section];
    TasteDetail *tasteDetail = [taste.tasteDetails objectAtIndex:indexPath.row];

    TasteViewCell *cell = (TasteViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TasteViewCell" forIndexPath:indexPath];
    
    if (taste.mutex) {
        cell.checkBox.boxType = BEMBoxTypeCircle;
    } else {
        cell.checkBox.boxType = BEMBoxTypeSquare;
    }
    cell.checkBox.animationDuration = 0.2;
    
    NSNumber *selectedTasteIndex = [_selectedTasteIndexes objectAtIndex:indexPath.section];
    int selectedIndex = [selectedTasteIndex intValue];
    if ((selectedIndex & (1 << indexPath.row)) != 0) {
        cell.checkBox.on = YES;
    } else {
        cell.checkBox.on = NO;
    }
    
    if (tasteDetail.price > 0) {
        cell.lblTitle.text = [NSString stringWithFormat:@"%@ +%d", tasteDetail.name, tasteDetail.price];
    } else {
        cell.lblTitle.text = tasteDetail.name;
    }
    cell.delegate = self;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(collectionView.frame.size.width, 50);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        TasteCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        Taste *taste = [_tastes objectAtIndex:indexPath.section];
        
        BOOL labelExists = NO;
        for (UIView *subview in headerView.subviews) {
            if ([subview isKindOfClass:[UILabel class]]) {
                UILabel *label = (UILabel *)subview;
                label.text = taste.tasteName;
                labelExists = YES;
            }
        }
        
        if (!labelExists) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 320, 50)];
            label.text = taste.tasteName;
            label.textColor = [UIColor darkGrayColor];
            label.font = [UIFont systemFontOfSize:20];
            [headerView addSubview:label];
        }
        
        reusableview = headerView;
    }
    
    return reusableview;
}

//==================================================================
#pragma mark - TasteViewCellDelegate
//==================================================================

- (void)onTasteSelected:(BEMCheckBox *)sender
{
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
    
    Taste *taste = [_tastes objectAtIndex:indexPath.section];
    NSNumber *selectedTasteIndex = [_selectedTasteIndexes objectAtIndex:indexPath.section];
    if (taste.mutex) {
        // Only one can be set
        int newIndex = (1 << (int)(indexPath.row));
        selectedTasteIndex = [NSNumber numberWithInt:newIndex];
        NSLog(@"Radiobox ON: selectedTasteIndex = 0x%.2x", [selectedTasteIndex intValue]);
    } else {
        // Multiple selections possible
        if (sender.on) {
            int newIndex = [selectedTasteIndex intValue] | (1 << (int)(indexPath.row));
            selectedTasteIndex = [NSNumber numberWithInteger:newIndex];
            NSLog(@"Checkbox ON: selectedTasteIndex = 0x%.2x", [selectedTasteIndex intValue]);
        } else {
            int newIndex = [selectedTasteIndex intValue] & ~(1 << (int)(indexPath.row));
            selectedTasteIndex = [NSNumber numberWithInteger:newIndex];
            NSLog(@"Checkbox OFF: selectedTasteIndex = 0x%.2x", [selectedTasteIndex intValue]);
        }
    }
    
    Taste *selectedTaste = [_selectedTastes objectAtIndex:indexPath.section];
    NSMutableArray *selectedTasteDetails = [NSMutableArray new];
    for (int i=0; i<taste.tasteDetails.count; i++) {
        if ([selectedTasteIndex intValue] & (1 << i)) {
            TasteDetail *tasteDetail = [taste.tasteDetails objectAtIndex:i];
            [selectedTasteDetails addObject:tasteDetail];
        }
    }
    selectedTaste.tasteDetails = selectedTasteDetails;
    [_selectedTasteIndexes setObject:selectedTasteIndex atIndexedSubscript:indexPath.section];
    [self.collectionView reloadData];
}

@end
