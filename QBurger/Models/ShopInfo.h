//
//  ShopInfo.h
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopInfo : NSObject

@property (nonatomic, strong) NSString *companyId;
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *shopName;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *fax;
@property (nonatomic, strong) NSString *zip;
@property (nonatomic, strong) NSString *workTime;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *htmlBody;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@end
