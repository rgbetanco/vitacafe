//
//  TasteViewCell.h
//  QBurger
//
//  Created by Kevin Phua on 7/18/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckbox.h"

@protocol TasteViewCellDelegate <NSObject>

- (void)onTasteSelected:(BEMCheckBox *)sender;

@end

@interface TasteViewCell : UICollectionViewCell <BEMCheckBoxDelegate>

@property (weak, nonatomic) IBOutlet BEMCheckBox *checkBox;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (assign, nonatomic) id<TasteViewCellDelegate> delegate;

@end
