//
//  SelectShopViewController.h
//  QBurger
//
//  Created by Kevin Phua on 15/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopInfo.h"

@protocol SelectShopViewDelegate <NSObject>

- (void)onSelectShop:(ShopInfo *)shop;

@end

@interface SelectShopViewController : UIViewController

@property (nonatomic, assign) id<SelectShopViewDelegate> delegate;

@end
