//
//  LocationsViewController.h
//  Raise
//
//  Created by Kevin Phua on 11/6/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "BaseViewController.h"
#import "WebService.h"

@interface LocationsViewController : BaseViewController <WebServiceDelegate>

@end
