//
//  CAKeyframeAnimation+FunctionBased.m
//  tthsu
//
//  Created by Chin-Yu Hsu on 2/7/14.
//  Copyright (c) 2014 Chinsoft Co., Ltd. All rights reserved.
//

#import "CAKeyframeAnimation+FunctionBased.h"

@implementation CAKeyframeAnimation (FunctionBased)

+ (id) animationWithKeyPath:(NSString *)path interpolator:(TimeInterpolator)fncBlock fromValue:(CGFloat)fromValue toValue:(CGFloat)toValue {
	return [CAKeyframeAnimation animationWithKeyPath:path interpolator:fncBlock fromValue:fromValue toValue:toValue steps:100];
}

+ (id) animationWithKeyPath:(NSString *)path interpolator:(TimeInterpolator)fncBlock fromValue:(CGFloat)fromValue toValue:(CGFloat)toValue duration:(CFTimeInterval)seconds fps:(CGFloat)fps {
	
	NSUInteger steps = ceilf(seconds * fps);
	
	CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:path interpolator:fncBlock fromValue:fromValue toValue:toValue steps:steps];
	
	animation.duration = seconds;
	
	return animation;
}

//  the more steps, the smoother the animation
+ (id) animationWithKeyPath:(NSString *)path interpolator:(TimeInterpolator)fncBlock fromValue:(CGFloat)fromValue toValue:(CGFloat)toValue steps:(NSUInteger)steps {
	
	if( steps<=1 )
		return nil;	// error to have less than 2 steps!
	
	CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:path];
	
	NSMutableArray *values = [NSMutableArray arrayWithCapacity:steps];
	
	float time = 0.0;
	float timeStep = 1.0 / (double)(steps - 1);
	
	for(NSUInteger i = 0; i < steps; i++) {
		float value = fromValue + (fncBlock(time) * (toValue - fromValue));
		[values addObject:[NSNumber numberWithFloat:value]];
		time += timeStep;
	}
	
	// we want linear animation between keyframes, with equal time steps
	animation.calculationMode = kCAAnimationCubic;

	// set keyframes
	animation.values = values;

	return animation;
}


@end
