//
//  OrderConfirmViewController.m
//  QBurger
//
//  Created by Kevin Phua on 16/11/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "OrderConfirmViewController.h"
#import "Global.h"

@interface OrderConfirmViewController ()

@end

@implementation OrderConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (g_SelectedShopName) {
        _lblOrderShop.text = g_SelectedShopName;
        _lblOrderShop.numberOfLines = 0;
        [_lblOrderShop sizeToFit];
    } else {
        _lblOrderShop.text = NSLocalizedString(@"None", nil);
    }
    if (g_SelectedMealDate && g_SelectedMealTime) {
        _lblOrderTime.text = [NSString stringWithFormat:@"%@ %@", g_SelectedMealDate, g_SelectedMealTime];
        _lblOrderTime.numberOfLines = 0;
        [_lblOrderTime sizeToFit];
    } else {
        _lblOrderTime.text = NSLocalizedString(@"None", nil);
    }
    switch (g_SelectedSaleMethod) {
        case 0:
            _lblOrderType.text = NSLocalizedString(@"OrderEatIn", nil);
            break;
        case 1:
            _lblOrderType.text = NSLocalizedString(@"OrderTakeOut", nil);
            break;
        case 2:
            _lblOrderType.text = NSLocalizedString(@"OrderDelivery", nil);
            break;
        case 3:
            _lblOrderType.text = NSLocalizedString(@"OrderOwnself", nil);
            break;
        default:
            _lblOrderType.text = NSLocalizedString(@"None", nil);
            break;
    }
    if (g_SelectedRecAddr && g_SelectedSaleMethod == 2) {
        _lblOrderAddress.text = g_SelectedRecAddr;
        _lblOrderAddress.numberOfLines = 0;
        [_lblOrderAddress sizeToFit];
    } else {
        _lblOrderAddress.text = NSLocalizedString(@"None", nil);
    }
    if (g_IsLogin) {
        _lblOrderContact.text = [g_MemberInfo objectForKey:INFO_KEY_NAME];
        _lblOrderPhone.text = [g_MemberInfo objectForKey:INFO_KEY_MOBILE];
    } else {
        _lblOrderContact.text = NSLocalizedString(@"None", nil);
        _lblOrderPhone.text = NSLocalizedString(@"None", nil);
    }
    
    _lblOrderTotal.text = [NSString stringWithFormat:@"%d", _orderTotal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnConfirm:(id)sender {
    if (self.delegate) {
        [self.delegate onConfirmOrder:self];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onBtnModify:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
