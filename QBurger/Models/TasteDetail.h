//
//  TasteDetail.h
//  QBurger
//
//  Created by Kevin Phua on 9/8/16.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TasteDetail : NSObject <NSCopying>

@property (nonatomic, strong) NSString *tasteSno;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) int price;

@end
