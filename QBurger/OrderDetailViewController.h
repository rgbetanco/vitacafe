//
//  OrderDetailViewController.h
//  QBurger
//
//  Created by Kevin Phua on 30/10/2016.
//  Copyright © 2016 Lafresh. All rights reserved.
//

#import "BaseViewController.h"

@interface OrderDetailViewController : BaseViewController

@property (nonatomic, strong) NSString *worderId;
@property (nonatomic, strong) NSString *mealDate;
@property (nonatomic, strong) NSString *methodName;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *totalSales;

@end
