//
//  FriendListCell.h
//  Raise
//
//  Created by Kevin Phua on 11/7/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FriendListDelegate <NSObject>

- (void)btnYes:(id)sender;
- (void)btnNo:(id)sender;

@end

@interface FriendListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;

@property (assign, nonatomic) NSString *fId;

@property (assign, nonatomic) id<FriendListDelegate> delegate;

@end

