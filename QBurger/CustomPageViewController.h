//
//  CustomPageViewController.h
//  PosApp
//
//  Created by Kevin Phua on 9/8/15.
//  Copyright (c) 2015 hagarsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "WebService.h"
#import "Global.h"

@interface CustomPageViewController : BaseViewController <WebServiceDelegate>

@property (strong, nonatomic) NSString *subject;
@property (strong, nonatomic) NSString *websiteUrl;

@end
