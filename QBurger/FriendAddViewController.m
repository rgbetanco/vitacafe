//
//  FriendAddViewController.m
//  Raise
//
//  Created by Kevin Phua on 11/6/15.
//  Copyright © 2015 hagarsoft. All rights reserved.
//

#import "FriendAddViewController.h"
#import "RaiseAlertView.h"
#import "Global.h"

@interface FriendAddViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *textPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFriend;

@end

@implementation FriendAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.lblTitle.text = NSLocalizedString(@"PleaseEnterFriendMsg", nil);
    self.textPhone.placeholder = NSLocalizedString(@"PhoneNumber", nil);
    self.textPhone.textColor = [UIColor blackColor];
    self.textPhone.backgroundColor = [Global colorWithType:COLOR_TYPE_TEXTBOX_BG];
    [self.btnAddFriend setTitle:NSLocalizedString(@"Confirm", nil) forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapView:)];
    [self.view addGestureRecognizer:tapView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = NSLocalizedString(@"AddFriend", nil);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnAddFriend:(id)sender {
    WebService *ws = [[WebService alloc] init];
    ws.delegate = self;
    [ws inviteFriend:[Global cfgAppId] accKey:[g_MemberInfo objectForKey:INFO_KEY_ACCKEY] fId:self.textPhone.text];
    [ws showWaitingView:self.view];
}

- (void)onTapView:(UITapGestureRecognizer *)tapGesture
{
    // Dismiss keyboard
    [self.view endEditing:YES];
}

//==================================================================
#pragma WebServiceDelegate
//==================================================================
- (void)didReceiveData:(NSData *)data resultName:(NSString *)resultName userObject:(id)userObject {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received data for %@: %@", resultName, dataString);
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        NSLog(@"Error received: %@", [error localizedDescription]);
    }
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSArray *jsonArray = (NSArray *)jsonObject;
        NSLog(@"jsonArray - %@", jsonArray);
    } else {
        NSDictionary *jsonDict = (NSDictionary *)jsonObject;
        NSLog(@"jsonDict - %@", jsonDict);
        
        if ([resultName compare:WS_INVITE_FRIEND] == NSOrderedSame) {
            // Register
            long code = [[jsonObject objectForKey:@"code"] longValue];
            if (code == 0) {
                // Success
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"AddFriendSuccess", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"Close", nil)
                                                                 otherButtonTitle:nil];
                
                alertView.cancelButtonAction = ^{
                    // Go back to previous page
                    [self.navigationController popViewControllerAnimated:YES];
                };
                [alertView show];
            } else {
                // Failure
                NSString *message = (NSString *)[jsonObject objectForKey:@"message"];
                if ([message containsString:@"[invite]："]) {
                    message = NSLocalizedString(@"FriendAddErrorMsg", nil);
                } else if ([message containsString:@"[invite]-WEB:"]) {
                    NSRange range = [message rangeOfString:@"[invite]-WEB:"];
                    message = [message substringFromIndex:range.location+range.length];
                }
                
                RaiseAlertView *alertView = [[RaiseAlertView alloc] initWithTitle:NSLocalizedString(@"AddFriendFail", nil)
                                                                          message:message
                                                                cancelButtonTitle:NSLocalizedString(@"Close", nil)
                                                                 otherButtonTitle:nil];
                
                alertView.cancelButtonAction = ^{
                    // Do nothing
                };
                [alertView show];
            }
        }
    }
}

- (void)connectFail:(NSString*)resultName {
    NSLog(@"Connect fail for %@", resultName);
}

@end
